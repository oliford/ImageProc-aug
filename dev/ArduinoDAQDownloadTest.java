import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

/** Should use com.fazecast.jSerialComm.SerialPort instead */
public class ArduinoDAQDownloadTest {
	public static void main(String[] args) throws Exception {
		String portName = "/dev/ttyACM0";
		int baudRate = 115200;
				
		
		CommPortIdentifier portIdentifier;
		portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
	
		if ( portIdentifier.isCurrentlyOwned() )
			throw new RuntimeException("Error: Port is currently in use");
	
		CommPort commPort = portIdentifier.open("wtf", 2000);
	
		if(!(commPort instanceof SerialPort)) 
			throw new RuntimeException("Error: '"+portName+"' is not a serial port.");
	
		
		SerialPort port = (SerialPort) commPort;
		port.setSerialPortParams(baudRate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
		//port.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN | SerialPort.FLOWCONTROL_RTSCTS_OUT);
		//port.setFlowControlMode(SerialPort.FLOWCONTROL_XONXOFF_IN | SerialPort.FLOWCONTROL_XONXOFF_OUT);
		port.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);
		System.out.println("RXTX: bufSize = " + port.getInputBufferSize());
		port.setInputBufferSize(65536);
		System.out.println("RXTX: bufSize = " + port.getInputBufferSize());
		
		port.enableReceiveTimeout(1000);
		//port.
		
		InputStream inStream = port.getInputStream();
		OutputStream outStream = port.getOutputStream();
		PrintWriter pw = new PrintWriter(outStream);
		
		System.out.println(readStuff(inStream));
		
		pw.println("DAQ-TDLY500");
		pw.flush();
		System.out.println(readStuff(inStream));
		
		pw.println("PING\nDAQ-INIT\nDAQ-COUNT0");
		pw.flush();
		System.out.println(readStuff(inStream));
		
		pw.println("DAQ-START0");
		pw.flush();
		readStuff(inStream);
		
		
		port.close();
	}
	
	private static String readStuff(InputStream inStream) throws IOException{
		byte data[] = new byte[4096];
		
		int n=0;
		while(n < data.length){
			int nRead = inStream.read(data, n, data.length-n);
			n+=nRead;
			if(nRead == 0){
				break;
			}
			System.out.print(n + " ");	
		}
		System.out.println("\n" + "Read " + n);// new String(data));
		return new String(data);
	}
}
