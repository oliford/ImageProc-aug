	import java.io.FileInputStream;
import java.io.IOException;

import binaryMatrixFile.BinaryMatrixWriter;


public class MouseDisp {

	public static void main(String[] args) throws IOException {
		FileInputStream fin = new FileInputStream("/dev/input/mouse1");
		BinaryMatrixWriter bmw = new BinaryMatrixWriter("/tmp/mouseOut", 5); 

		byte b[] = new byte[3];
		
		int posX=0, posY=0;
		long t0 = System.nanoTime();
		
		while(true){
			fin.read(b); 
			posX += b[1];
			posY += b[2];
			System.out.println(posX + "\t" + posY + "\t" + b[1] + "\t" + b[2]);
			bmw.writeRow(new double[]{ System.nanoTime() - t0, posX, posY, b[1], b[2] });
		}
		
	}
}
