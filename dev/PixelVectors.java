import java.io.IOException;

import descriptors.gmds.GMDSSignalDesc;
import fusionDefs.transform.FeatureTransformCubic;
import fusionDefs.transform.PhysicalROIGeometry;
import imageProc.database.gmds.GMDSUtil;
import ipp.imse.fusionOptics.mse.AugGenericMSESystem;
import net.jafama.FastMath;
import otherSupport.SettingsManager;
import signals.gmds.GMDSSignal;

/** Output netCDF filse containing virtual camera view position
 * and unit vectors in AUG coordinates for the transform of a given GMDS shot.
 * 
 * @author oliford
 */
public class PixelVectors {
	
	private static int gmdsPulseWithTransform = 1329;

	public static void main(String[] args) throws IOException {
		new SettingsManager("minerva", true);
		
		double rot = -AugGenericMSESystem.vrmlToAUGDDDRotation;
		
		FeatureTransformCubic xform = new FeatureTransformCubic(GMDSUtil.globalGMDS(), "AUG", gmdsPulseWithTransform );
		
		PhysicalROIGeometry ccdGeom = new PhysicalROIGeometry(GMDSUtil.globalGMDS(), "AUG", gmdsPulseWithTransform);
		
		double[][][] pixelUnitVectors = xform.calcPixelLOSVectors(ccdGeom);
		
		for(int iY=0; iY < pixelUnitVectors.length; iY++){
			for(int iX=0; iX < pixelUnitVectors[0].length; iX++){
				pixelUnitVectors[iY][iX] = new double[]{
						pixelUnitVectors[iY][iX][0] * FastMath.cos(rot) + pixelUnitVectors[iY][iX][1] * FastMath.sin(rot),
						-pixelUnitVectors[iY][iX][0] * FastMath.sin(rot) + pixelUnitVectors[iY][iX][1] * FastMath.cos(rot),
						pixelUnitVectors[iY][iX][2]
				};
			}
		}
			
		
		GMDSSignalDesc desc = new GMDSSignalDesc(gmdsPulseWithTransform, "AUG", "/Transform/pixelVectorsAUG");
		GMDSSignal sig = new GMDSSignal(desc, pixelUnitVectors);
		sig.writeToFile("/tmp/pixelVectorsAUG-" + gmdsPulseWithTransform);
		
		double v[] = xform.getViewPosition();
		v = new double[]{
				v[0] * FastMath.cos(rot) + v[1] * FastMath.sin(rot),
				-v[0] * FastMath.sin(rot) + v[1] * FastMath.cos(rot),
				v[2]
		};
		
		desc = new GMDSSignalDesc(gmdsPulseWithTransform, "AUG", "/Transform/viewPositionAUG");
		sig = new GMDSSignal(desc, v);
		sig.writeToFile("/tmp/viewPositionAUG-" + gmdsPulseWithTransform);
		
	}

}
