# ImageProc settings profile for acquisition system.
#
# Diagnostic: QRI - Coherence Imaging (AEF31) used at AUG in 2019
# Detector: PCO Edge 5.5
# Purpose: Scrape off layer flows with Coherence Imaging Spectroscopy
#

# Acqusition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=QRI_PCOEdge_AEF30
imageProc.w7xPilot.statusComm.defaultPort=9752
imageProc.profile.assertHostname=pc-e4-qri-3

#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.sources.capture.pco.PCOCamSource
imageProc.profile.source.config=jsonfile:/data/minerva/jsonSettings/PCOCamSource/AEF30_OPb_laser_refdischarge2

imageProc.sources.capture.andorCam.settingsType=json

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=true
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.aug.AugPilot
imageProc.profile.sink2.init=true
imageProc.profile.sink3.class=imageProc.control.arduinoComm.ArduinoCommHanger
imageProc.profile.sink3.init=false
imageProc.profile.sink4.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink4.init=false
imageProc.profile.sink5.class=imageProc.database.GMDSSinkIMSE
imageProc.profile.sink5.findFirstEmpty=ASDEX2019/0
imageProc.profile.sink5.init=true
#imageProc.profile.sink6.class=imageProc.proc.spec.SpecCalProcessor
#imageProc.profile.sink6.init=false
imageProc.profile.sink7.class=imageProc.proc.transform.TransformProcessor
imageProc.profile.sink7.init=false
imageProc.profile.sink8.class=imageProc.proc.fft.FFTProcessor
imageProc.profile.sink8.init=false
imageProc.profile.sink9.class=imageProc.proc.demod.DSHDemod
imageProc.profile.sink9.init=false

# W7X Archive sink database and path name
imageProc.w7x.archiveDB.path=ArchiveDB/raw/W7X/QRI_CoherenceImaging/PCOEdgeAEF30_DATASTREAM/0/Images
imageProc.w7x.archiveDB.writeToCache=true
imageProc.w7x.archiveDB.writeToDB=false
	
# GMDS Sink (local files) 'experiment', path and starting shot	
# Valeria used 'DEFAULT', not much point in changing it now
imageProc.gmds.experiment=DEFAULT
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW
	
# W7X Pilot configuration
imageProc.w7xPilot.inShot.shotPollIntervalMS=1000
imageProc.w7xPilot.inShot.dayPollIntervalMS=60000
imageProc.w7xPilot.inShot.startAtInit=true
imageProc.w7xPilot.inShot.acquireStartTimeMS=20000
imageProc.w7xPilot.inShot.fault.reopenAcquisitionOnFault=false
imageProc.w7xPilot.inShot.fault.resetPower=false
imageProc.w7xPilot.inShot.fault.cameraOffTime=10000
imageProc.w7xPilot.inShot.acquireTimeout=400000
imageProc.w7xPilot.inShot.saveTimeout=400000
imageProc.w7xPilot.inShot.processChainDelay=1000
imageProc.w7xPilot.inShot.acquireStartTimeout=120000
imageProc.w7xPilot.powerCtrlClass=None
imageProc.w7xPilot.cameraWarmUpTime=10000
imageProc.w7xPilot.clockSyncCommand=/usr/sbin/ntpdate -u ntp.ipp-hgw.mpg.de
imageProc.w7xPilot.clockSyncAfterShot=false
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.2
imageProc.gui.scaleY=0.2