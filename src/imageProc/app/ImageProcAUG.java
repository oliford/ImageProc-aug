package imageProc.app;

import org.eclipse.swt.widgets.Display;

import descriptors.gmds.GMDSSignalDesc;
import imageProc.auxiliary.faraday.FaradayProcessor;
import imageProc.auxiliary.radialField.RadialFieldProcessor;
import imageProc.control.arduinoComm.ArduinoCommHanger;
import imageProc.control.arduinoComm.ArduinoCommHangerAUG;
import imageProc.core.ImageProcUtil;
import imageProc.core.swt.WindowShell;
import imageProc.database.GMDSSinkIMSE;
import imageProc.database.GMDSSourceIMSE;
import imageProc.database.gmds.GMDSSink;
import imageProc.database.gmds.GMDSSource;
import imageProc.graph.GraphUtilProcessor;
import imageProc.graph.GraphUtilProcessorIMSE;
import imageProc.metaview.MetaDataManager;
import imageProc.pilot.aug.AugPilot;
import imageProc.proc.demod.DSHDemod;
import imageProc.proc.fastPolFull.FastPolProcessor;
import imageProc.proc.fft.FFTProcessor;
import imageProc.proc.imgFit.ImageFitProcessor;
import imageProc.proc.pitch.PitchProcessor;
import imageProc.proc.reduce.ReduceProcessor;
import imageProc.proc.seriesAvg.SeriesProcessorAUG;
import imageProc.proc.transform.TransformProcessor;
import imageProc.sources.capture.andorCam.AndorCamSource;
import imageProc.sources.capture.avaspec.AvaspecSource;
import imageProc.sources.capture.pco.PCOCamSource;
import imageProc.sources.capture.sensicam.SensicamSource;
import imageProc.sources.sim.dshGen.DSHGenSource;
import imageProc.sources.sim.noiseGen.NoiseGenSource;
import imageProc.sources.sim.platesGen.PlatesGenSource;
import mds.AugShotfileFetcher;
import otherSupport.SettingsManager;

/** The IMSE GUI App to tie all the SWT modules together
 * 
 * This version starts with no source selected and no sinks attached. * 
 * 
 * All the big image and DAC/ADC allocations are done via direct NIO
 * buffers, so it shouldn't need a huge amount of java heap space, but
 * does need the "-XX:MaxDirectMemorySize" setting to be turned up a bit.
 * 
 * Meanwhile, the heap size needs to be kept as small as possible.
 * 
 * Run with something like:
 * -Xms100M -Xmx512M -XX:+UseParNewGC -XX:MinHeapFreeRatio=10
 * -XX:MaxHeapFreeRatio=40 -XX:MaxDirectMemorySize=4096M
 * 
 * Environment variables need to be set for the following modules:
 * 
 * Bitflow CameraLink framegrabber for Andor Zyla camera: 
 *    BITFLOW_INSTALL_DIRS /opt/andor/bitflow
 *    LD_LIBRARY_PATH /opt/andor/bitflow/64b/lib
 * 
 * RXTX native library for all serial/serial over USB: 
 *    LD_LIBRARY_PATH /usr/lib64/rxtx
 *    
 * DDWW native libraries for ASDEX Upgrade shotfile access:
 *    LD_LIBRARY_PATH /shares/software/aug-dv/moduledata/ads/Linux-generic-x86_64/lib64
 * 
 *  LD_LIBRARY_PATH /opt/andor/bitflow/64b/lib:/usr/lib64/rxtx:/afs/ipp/aug/ads/@sys/lib64/
 */
public class ImageProcAUG {
	public static void main(String[] args) {
		new SettingsManager("minerva", true);
		
		AugShotfileFetcher.defaultInstance().setIsolateFetch(true); //don't crash the program please
				
		Display swtDisplay = new Display();
		
		
		
		///ConcurrencyUtils.setThreadPool(ImageProcUtil.getThreadPool()); //bad idea :(
		   
		//Read data from PCCIS MDS+ DB (HGW Lab)
		
		//Sources need to be instantiated
		GMDSSourceIMSE mdsSrcIMSE = new GMDSSourceIMSE();		
		NoiseGenSource noiseSrc = new NoiseGenSource(344,260);
		//JotterSource jotter = new JotterSource();
		SensicamSource camSrc = new SensicamSource();
		AndorCamSource andorSrc = new AndorCamSource();
		PCOCamSource pcoSrc = new PCOCamSource();
		DSHGenSource dshSrc = new DSHGenSource();
		PlatesGenSource platesSrc = new PlatesGenSource();
		AvaspecSource avaspecSrc = new AvaspecSource();
		
		//Sinks get instantiated as required, but we need to register the classes
		ImageProcUtil.addImgPipeClass(GraphUtilProcessorIMSE.class);
		//ImageProcUtil.addImgPipeClass(JotterSink.class);		
		ImageProcUtil.addImgPipeClass(GMDSSinkIMSE.class);
		ImageProcUtil.addImgPipeClass(FFTProcessor.class);
		ImageProcUtil.addImgPipeClass(DSHDemod.class);
		//ImageProcUtil.addImgPipeClass(StepperHanger.class); // The old stepper driver, now done via DACTiming, or ArduinoCommHanger
		ImageProcUtil.addImgPipeClass(ArduinoCommHangerAUG.class);
		ImageProcUtil.addImgPipeClass(TransformProcessor.class);
		ImageProcUtil.addImgPipeClass(SeriesProcessorAUG.class);
		ImageProcUtil.addImgPipeClass(ImageFitProcessor.class);
		ImageProcUtil.addImgPipeClass(PitchProcessor.class);
		ImageProcUtil.addImgPipeClass(FastPolProcessor.class);		
		ImageProcUtil.addImgPipeClass(AugPilot.class);
		ImageProcUtil.addImgPipeClass(ReduceProcessor.class);
		ImageProcUtil.addImgPipeClass(FaradayProcessor.class);
		ImageProcUtil.addImgPipeClass(RadialFieldProcessor.class);
		ImageProcUtil.addImgPipeClass(MetaDataManager.class);

		//GraphUtilProcessor gu = new GraphUtilProcessor();
		
		//GMDSSignalDesc sigDesc = new GMDSSignalDesc(0, GMDSUtil.getMetaExp(noiseSrc), "/signalLists/blah");
		//GMDSUtil.globalGMDS().getSig(sigDesc);
		
		WindowShell imgWin1 = new WindowShell(swtDisplay, null, 0);
				
		ImageProcUtil.msgLoop(swtDisplay, true);
	}
}