package imageProc.app;

import org.eclipse.swt.widgets.Display;

import imageProc.auxiliary.faraday.FaradayProcessor;
import imageProc.auxiliary.radialField.RadialFieldProcessor;
import imageProc.control.arduinoComm.ArduinoCommHanger;
import imageProc.control.arduinoComm.ArduinoCommHangerAUG;
import imageProc.core.ImageProcUtil;
import imageProc.core.swt.SplitImgInfoView;
import imageProc.core.swt.WindowShell;
import imageProc.database.GMDSSinkIMSE;
import imageProc.database.GMDSSourceIMSE;
import imageProc.database.gmds.GMDSSink;
import imageProc.database.gmds.GMDSSource;
import imageProc.graph.GraphUtilProcessor;
import imageProc.graph.GraphUtilProcessorIMSE;
import imageProc.metaview.MetaDataManager;
import imageProc.pilot.aug.AugPilot;
import imageProc.proc.demod.DSHDemod;
import imageProc.proc.fastPolFull.FastPolProcessor;
import imageProc.proc.fft.FFTProcessor;
import imageProc.proc.imgFit.ImageFitProcessor;
import imageProc.proc.pitch.PitchProcessor;
import imageProc.proc.reduce.ReduceProcessor;
import imageProc.proc.seriesAvg.SeriesProcessorAUG;
import imageProc.proc.transform.TransformProcessor;
import imageProc.proc.transform.TransformProcessorAUG;
import imageProc.sources.sim.platesGen.PlatesGenSource;
import mds.AugShotfileFetcher;
import otherSupport.SettingsManager;

/** The IMSE GUI App to tie all the SWT modules together
 * 
 * This version starts with the standard analysis chain setup.
 * 
 * All the big image and DAC/ADC allocations are done via direct NIO
 * buffers, so it shouldn't need a huge amount of java heap space, but
 * does need the "-XX:MaxDirectMemorySize" setting to be turned up a bit.
 * 
 * Meanwhile, the heap size needs to be kept as small as possible.
 * 
 * Run with something like:
 * -Xms100M -Xmx512M -XX:+UseParNewGC -XX:MinHeapFreeRatio=10
 * -XX:MaxHeapFreeRatio=40 -XX:MaxDirectMemorySize=4096M
 *  
 */
public class ImageProcAUG_Analysis {
	public static void main(String[] args) {
		new SettingsManager("minerva", true);
		
		AugShotfileFetcher.defaultInstance().setIsolateFetch(true); //don't crash the program please
		
		Display swtDisplay = new Display();
		
		///ConcurrencyUtils.setThreadPool(ImageProcUtil.getThreadPool()); //bad idea :(
		   
		String info = SettingsManager.defaultGlobal().getProperty("ImageProcUtil.augControl.cameraSetup.normal", "AUG/1000");
		String parts[] = info.split("/");
		String gmdsExp = parts[0];
		int setupPulse = Integer.parseInt(parts[1]);
		
		//Sources need to be instantiated
		GMDSSourceIMSE mdsSrc = new GMDSSourceIMSE();		
		//SensicamSource camSrc = new SensicamSource();
		PlatesGenSource platesSrc = new PlatesGenSource();
		
		//Sinks get instantiated as required, but we need to register the classes
		ImageProcUtil.addImgPipeClass(GraphUtilProcessorIMSE.class);
		ImageProcUtil.addImgPipeClass(GMDSSinkIMSE.class);
		ImageProcUtil.addImgPipeClass(FFTProcessor.class);
		ImageProcUtil.addImgPipeClass(DSHDemod.class);
		//ImageProcUtil.addImgPipeClass(AquisitionHanger.class);
		//ImageProcUtil.addImgPipeClass(DACTimingHanger.class);
		ImageProcUtil.addImgPipeClass(ArduinoCommHangerAUG.class);
		ImageProcUtil.addImgPipeClass(TransformProcessorAUG.class);
		ImageProcUtil.addImgPipeClass(SeriesProcessorAUG.class);
		ImageProcUtil.addImgPipeClass(ImageFitProcessor.class);
		ImageProcUtil.addImgPipeClass(PitchProcessor.class);		
		ImageProcUtil.addImgPipeClass(FastPolProcessor.class);		
		ImageProcUtil.addImgPipeClass(ReduceProcessor.class);
		ImageProcUtil.addImgPipeClass(AugPilot.class); //to use the processing system
		ImageProcUtil.addImgPipeClass(FaradayProcessor.class);
		ImageProcUtil.addImgPipeClass(RadialFieldProcessor.class);
		ImageProcUtil.addImgPipeClass(MetaDataManager.class);
				
		SeriesProcessorAUG seriesAvg = new SeriesProcessorAUG(mdsSrc, 0);		
		FFTProcessor fft = new FFTProcessor(seriesAvg, 0);
		fft.setAutoUpdate(false);
		
		DSHDemod dsh = new DSHDemod(fft, 0); //dsh.loadSelectionsFromGMDS("AUG", 0);
		dsh.setAutoUpdate(false);
		try{ dsh.loadConfigGMDS(gmdsExp, setupPulse); }catch(RuntimeException err){ err.printStackTrace(); }
		
		//FaradayProcessor faraday = new FaradayProcessor(dsh, 0);
		//faraday.setAutoUpdate(false);;
		//try{ faraday.loadConfig(gmdsExp, setupPulse); }catch(RuntimeException err){ err.printStackTrace(); }
		
		//RadialFieldProcessor radialField = new RadialFieldProcessor(dsh, 0);
		//radialField.setAutoUpdate(false);
		
		TransformProcessor trans = new TransformProcessor(dsh, 0);
		trans.setAutoUpdate(false);
		try{ trans.loadConfigGMDS(gmdsExp, setupPulse); }catch(RuntimeException err){ err.printStackTrace(); }
		
		ReduceProcessor reduce = new ReduceProcessor(dsh, 0); // needs to be after TransformProcessor in dsh's sinks
		reduce.setAutoUpdate(false);
		try{ reduce.loadConfig(gmdsExp, setupPulse);  }catch(RuntimeException err){ err.printStackTrace(); }
		
		//and saving for trans
		GMDSSinkIMSE gmdsSinkTrans = new GMDSSinkIMSE();
		gmdsSinkTrans.setSource(trans);
		gmdsSinkTrans.setData(gmdsExp, setupPulse, "/ANLYS/NOCAL/TRANS");
		
		//and saving for trans
		GMDSSinkIMSE gmdsSinkReduce = new GMDSSinkIMSE();
		gmdsSinkReduce.setSource(reduce);
		gmdsSinkReduce.setData(gmdsExp, setupPulse, "/ANLYS/NOCAL/REDUCED");

		
		WindowShell imgWin = new WindowShell(swtDisplay, mdsSrc, 0);

		ImageProcUtil.msgLoop(swtDisplay, true);

	}
}