package imageProc.app;

import org.eclipse.swt.widgets.Display;

import imageProc.auxiliary.faraday.FaradayProcessor;
import imageProc.auxiliary.radialField.RadialFieldProcessor;
import imageProc.control.arduinoComm.ArduinoCommHanger;
import imageProc.control.arduinoComm.ArduinoCommHangerAUG;
import imageProc.core.ImageProcUtil;
import imageProc.core.swt.WindowShell;
import imageProc.database.GMDSSinkIMSE;
import imageProc.database.GMDSSourceIMSE;
import imageProc.database.gmds.GMDSSettingsControl;
import imageProc.database.gmds.GMDSSink;
import imageProc.database.gmds.GMDSSource;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.graph.GraphUtilProcessor;
import imageProc.graph.GraphUtilProcessorIMSE;
import imageProc.metaview.MetaDataManager;
import imageProc.pilot.aug.AugPilot;
import imageProc.proc.demod.DSHDemod;
import imageProc.proc.fastPolFull.FastPolProcessor;
import imageProc.proc.fft.FFTProcessor;
import imageProc.proc.pitch.PitchProcessor;
import imageProc.proc.reduce.ReduceProcessor;
import imageProc.proc.seriesAvg.SeriesProcessorAUG;
import imageProc.proc.transform.TransformProcessor;
import imageProc.sources.capture.andorCam.AndorCamSource;
import imageProc.sources.capture.sensicam.SensicamSource;
import mds.AugShotfileFetcher;
import otherSupport.SettingsManager;

/** The IMSE GUI App to tie all the SWT modules together
 * 
 * Set up for aquisition using AndorCam;
 *  
 */
public class ImageProcAUG_Aqui {
	public static void main(String[] args) {
		new SettingsManager("minerva", true);
		
		AugShotfileFetcher.defaultInstance().setIsolateFetch(true); //don't crash the program please
		
		Display swtDisplay = new Display();
		
		///ConcurrencyUtils.setThreadPool(ImageProcUtil.getThreadPool()); //bad idea :(
		   
		//Read data from PCCIS MDS+ DB (HGW Lab)
		
		//Sources need to be instantiated
		GMDSSourceIMSE mdsSrc = new GMDSSourceIMSE();		
		SensicamSource camSrc = new SensicamSource();
		AndorCamSource andorSrc = new AndorCamSource();
		
		//Sinks get instantiated as required, but we need to register the classes
		ImageProcUtil.addImgPipeClass(GraphUtilProcessorIMSE.class);
		//ImageProcUtil.addImgPipeClass(JotterSink.class);
		ImageProcUtil.addImgPipeClass(GMDSSinkIMSE.class);
		ImageProcUtil.addImgPipeClass(FFTProcessor.class);
		ImageProcUtil.addImgPipeClass(DSHDemod.class);
		ImageProcUtil.addImgPipeClass(ArduinoCommHangerAUG.class);
		ImageProcUtil.addImgPipeClass(TransformProcessor.class);
		ImageProcUtil.addImgPipeClass(SeriesProcessorAUG.class);
		ImageProcUtil.addImgPipeClass(PitchProcessor.class);
		ImageProcUtil.addImgPipeClass(FastPolProcessor.class);		
		ImageProcUtil.addImgPipeClass(AugPilot.class);
		ImageProcUtil.addImgPipeClass(ReduceProcessor.class);
		ImageProcUtil.addImgPipeClass(FaradayProcessor.class);
		ImageProcUtil.addImgPipeClass(RadialFieldProcessor.class);
		ImageProcUtil.addImgPipeClass(MetaDataManager.class);
		
		ImageProcUtil.setPreferredSettings(GMDSSettingsControl.class);
		
		AugPilot augCtrl = new AugPilot();
		augCtrl.setSource(andorSrc);
		
		int latestPulseSearchStart = 3650;
		String info = "AUG/2999"; //erm, dont know what to do for this with the new AugControl config structure
		String parts[] = info.split("/");
		String gmdsExp = parts[0];
		int setupPulse = Integer.parseInt(parts[1]);
		
		GMDSSinkIMSE gmdsSink = new GMDSSinkIMSE();
		gmdsSink.setSource(andorSrc);
		//find next empty pulse record, starting at start of permIMSE data
		int nextGMDSPulse = gmdsSink.findNextEmpty(gmdsExp, latestPulseSearchStart, "/RAW");
		augCtrl.setNextDatabaseID(nextGMDSPulse);
		gmdsSink.setData(gmdsExp, nextGMDSPulse, "/RAW");
		
		//ArduinoCommHanger ardComm1 = new ArduinoCommHanger(false); //in-hall
		//ardComm1.setSource(andorSrc);	
		//ardComm1.serialComm().connectSerial("/dev/ttyS0", 115200); //connect port already. If it's off it'll say hello when switched on 		
			
		ArduinoCommHangerAUG ardComm = new ArduinoCommHangerAUG(false); //outside hall aburckha 2025: changed to AUG
		ardComm.setSource(andorSrc);
		ardComm.serialComm().connectSerial("/dev/ttyACM0", 115200); //connect already		
		
		(new GraphUtilProcessorIMSE()).setSource(andorSrc);
		
		//init analysis change but turn all auto-calc's off because augCtrl will do it sequentially
		SeriesProcessorAUG seriesAvg = new SeriesProcessorAUG(andorSrc, 0);
		seriesAvg.setAutoUpdate(false);
		
		FFTProcessor fft = new FFTProcessor(seriesAvg, 0);
		fft.setAutoUpdate(false);
		
		DSHDemod dsh = new DSHDemod(fft, 0); //dsh.loadSelectionsFromGMDS("AUG", 0);
		dsh.setAutoUpdate(false);
		//try{ dsh.loadConfig(gmdsExp, setupPulse); }catch(RuntimeException err){ err.printStackTrace(); }
		
		FaradayProcessor faraday = new FaradayProcessor(dsh, 0);
		faraday.setAutoUpdate(false);;
		try{ faraday.loadConfig(gmdsExp, setupPulse); }catch(RuntimeException err){ err.printStackTrace(); }
		
		TransformProcessor trans = new TransformProcessor(dsh, 0);
		trans.setAutoUpdate(false);
		//try{ trans.loadMapPoints(gmdsExp, setupPulse); }catch(RuntimeException err){ err.printStackTrace(); }
		
		ReduceProcessor reduce = new ReduceProcessor(dsh, 0); // needs to be after TransformProcessor in dsh's sinks
		reduce.setAutoUpdate(false);
		//try{ reduce.loadConfig(gmdsExp, setupPulse);  }catch(RuntimeException err){ err.printStackTrace(); }
						
		//and saving for trans
		//GMDSSinkIMSE gmdsSinkTrans = new GMDSSinkIMSE();
		//gmdsSinkTrans.setSource(trans);
		//gmdsSinkTrans.setData(gmdsExp, nextGMDSPulse, "/ANLYS/NOCAL/TRANS");
		
		//and saving for trans
		GMDSSinkIMSE gmdsSinkReduce = new GMDSSinkIMSE();
		gmdsSinkReduce.setSource(reduce);
		gmdsSinkReduce.setData(gmdsExp, nextGMDSPulse, "/ANLYS/NOCAL/REDUCED");
					
				
		WindowShell imgWin1 = new WindowShell(swtDisplay, andorSrc, 0);

		//wake up the main controller to do the rest
		augCtrl.runController();
		
		ImageProcUtil.msgLoop(swtDisplay, true);
	}
}