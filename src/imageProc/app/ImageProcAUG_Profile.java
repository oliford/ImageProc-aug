package imageProc.app;


import java.lang.reflect.Constructor;
import java.util.Set;
import org.eclipse.swt.widgets.Display;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.database.GMDSPipeIMSE;
import imageProc.database.GMDSSinkIMSE;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.pilot.aug.AugPilot;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

/** The ImageProc GUI App to tie all the SWT modules together
 *  Based on a settings-file style profile definition that has to be passed as a parameter
 *
 * All class names must be fully qualified
 * 
 * imageProc.profile.assertHostname=hostname	# Only run this profile if on this host
 *
 * imageProc.profile.source.class={Class}
 * imageProc.profile.source.config={ID} - call loadConfig() for source with this parameter}
 * 
 * imageProc.profile.sinkXX.class={Class} - sink class name
 * imageProc.profile.sinkXX.init=true/false - Instantiate this class? If not just available
 * imageProc.profile.sinkXX.config={config} - Configure the sink, if inited
 * 
 * W7X specifics:
 * 
 *  w7x.archive.dastbase={'W7X_TEST','W7X_ARCHIVE'}
	w7x.archive.path={path} where to save

	imageProc.w7xPilot.systemID
	imageProc.w7xPilot.logPath
	imageProc.w7xPilot.statusComm.defaultPort
	imageProc.w7xPilot.inShot.acquireStartTimeMS
	
	imageProc.w7xPilot.inShot.fault.reopenAcquisitionOnFault
	imageProc.w7xPilot.inShot.fault.resetPower
	imageProc.w7xPilot.inShot.fault.cameraOffTime
	imageProc.w7xPilot.inShot.acquireTimeout
	imageProc.w7xPilot.inShot.saveTimeout
	imageProc.w7xPilot.inShot.processChainDelay
	imageProc.w7xPilot.inShot.acquireStartTimeout
	imageProc.w7xPilot.ips.host
	imageProc.w7xPilot.ips.username
	imageProc.w7xPilot.ips.password
	imageProc.w7xPilot.ips.faultResetOutlets
	imageProc.w7xPilot.powerCtrlClass
	imageProc.w7xPilot.cameraWarmUpTime
	
	 
 * 
 * 
 * 
 * ----- Memory notes -------
 * All the big image and DAC/ADC allocations are done via direct NIO
 * buffers, so it shouldn't need a huge amount of java heap space, but
 * does need the "-XX:MaxDirectMemorySize" setting to be turned up a bit.
 * 
 * Meanwhile, the heap size needs to be kept as small as possible.
 * 
 * Run with something like:
 * -Xms100M -Xmx512M -XX:+UseParNewGC -XX:MinHeapFreeRatio=10
 * -XX:MaxHeapFreeRatio=40 -XX:MaxDirectMemorySize=4096M
 * 
 * Environment variables need to be set for the following modules:
 * 
 * Bitflow CameraLink framegrabber for Andor Zyla camera: 
 *    BITFLOW_INSTALL_DIRS /opt/andor/bitflow
 *    LD_LIBRARY_PATH /opt/andor/bitflow/64b/lib
 * 
 * RXTX native library for all serial/serial over USB: 
 *    LD_LIBRARY_PATH /usr/lib64/rxtx
 *    
 * DDWW native libraries for ASDEX Upgrade shotfile access:
 *    LD_LIBRARY_PATH /afs/ipp/aug/ads/@sys/lib64
 * 
 * Ocean Optics OmniDriver:
 * 		LD_LIBRARY_PATH /opt/omniDriver/OOI_HOME
 *  
 */
public class ImageProcAUG_Profile extends ImageProc_Profile {

	public static void main(String[] args) {
		(new ImageProcAUG_Profile()).run(args);
	}
	
	
	@Override
	protected void sourceSpecial(ImgSource source, SettingsManager profile) {
		
	}
	
	@Override
	protected void sinkSpecial(ImgSink sink, SettingsManager profile, String basePath, int sinkID) {
		if(sink instanceof GMDSSinkIMSE && profile.propertyDefined(basePath + ".sink"+sinkID+".findFirstEmpty")){
			//((GMDSPipeIMSE)sink).set(profile.getProperty(basePath + ".sink"+sinkID+".findFirstEmpty"));
			String info = profile.getProperty(basePath + ".sink"+sinkID+".findFirstEmpty");
			
			String parts[] = info.split("/");
			String gmdsExp = parts[0];
			int setupPulse = Integer.parseInt(parts[1]);
			
			//find next empty pulse record, starting at start of permIMSE data
			int nextGMDSPulse = ((GMDSSinkIMSE)sink).findNextEmpty(gmdsExp, setupPulse, "/RAW");
			((GMDSSinkIMSE)sink).setData(gmdsExp, nextGMDSPulse, "/RAW");
			
			//look for an AUGPilot and set the databaseID there too
			for(ImgSink sink2 : sink.getConnectedSource().getConnectedSinks()){
				if(sink2 instanceof AugPilot){
					((AugPilot)sink2).setNextDatabaseID(nextGMDSPulse);
				}
			}
		}
	}
	
}