package imageProc.app;

import imageProc.core.ImageProcUtil;
import imageProc.database.gmds.GMDSSource;
import imageProc.proc.transform.TransformProcessor;
import mds.AugShotfileFetcher;
import otherSupport.SettingsManager;

/** The IMSE App to copy transform data from a single pulse to multiple.
 * The imageXY data (image to physical CCD conversion) need to be set in each case
 * because the ROI can change for each. 
 *  
 */
public class ImageProcCopyTransforms {
	public static void main(String[] args) {
		new SettingsManager("minerva", true);
		
		AugShotfileFetcher.defaultInstance().setIsolateFetch(true); //don't crash the program please
		
		String gmdsExp = "AUG";		
		String setName = "RAW";
		int inPulse = 705;
		
		GMDSSource mdsSrc = new GMDSSource();
		TransformProcessor xform = new TransformProcessor(null, 0);
		xform.setAutoUpdate(false);
		xform.setSource(mdsSrc);
		
		for(int outPulse=700; outPulse < 930; outPulse++){
			
			//Sources need to be instantiated
			mdsSrc.setMaxImages(1); //don't actually need to read these, just need metadata
			mdsSrc.loadTarget(gmdsExp, outPulse, setName);
			
			while(!mdsSrc.isIdle()){
				try { Thread.sleep(500); } catch (InterruptedException e) { }		
			}
			if(mdsSrc.getNumImages() < 1){
				System.err.println("No images for shot " + outPulse);
				continue;
			}
			try { Thread.sleep(500); } catch (InterruptedException e) { } //wait for the initial calc to fail before retrigger		
			xform.loadConfigGMDS(gmdsExp, inPulse);
			xform.calc(); //which should also do the calcCCDCorners()
			
			while(!xform.isIdle()){
				System.out.println(xform.getStatus());
				try { Thread.sleep(500); } catch (InterruptedException e) { }		
			}
			
			xform.saveTransformSettings(gmdsExp, outPulse);
			
			
			//GMDSSink mdsSink = new GMDSSink();
			//mdsSink.setSource(xform);		
			//mdsSink.setData("AUG", pulse, transName);
			//mdsSink.save();
					
			//while(!mdsSink.isIdle()){
			//	try { Thread.sleep(500); } catch (InterruptedException e) { }		
			//}
			
			//ImageProcUtil.addImgPipeClass(GMDSSink.class);		
			//ImageProcUtil.addImgPipeClass(TransformProcessor.class);
				
			//ImageWindow imgWin1 = new ImageWindow(swtDisplay, null, 0);
			
			//ImageProcUtil.msgLoop(swtDisplay);
		}
		
		ImageProcUtil.shutdown();
		
		//swtDisplay.dispose();
	}
}