package imageProc.app;

import imageProc.core.ImageProcUtil;
import imageProc.database.gmds.GMDSSink;
import imageProc.database.gmds.GMDSSource;
import imageProc.proc.transform.TransformProcessor;
import mds.AugShotfileFetcher;
import otherSupport.SettingsManager;

/** The IMSE App to perform transform on sim results 
 * 
 * No GUI, just uses the sources/sinks.
 *  
 */
public class ImageProcSimTrans {
	public static void main(String[] args) {
		new SettingsManager("minerva", true);
		
		AugShotfileFetcher.defaultInstance().setIsolateFetch(true); //don't crash the program please
		
		//int pulse = 178;
		//int pulse = 351; String setName = "SIM/EQH/faroNov2013/DSH/DSH"; String transName = "SIM/EQH/faroNov2013/TRANS/TRANS";
		//int pulse = 351; String setName = "ANLYS/CAL354/DSH/DSH"; String transName = "ANLYS/CAL354/DSH_TRANS/DSH_TRANS";		
		//int pulse = 65; String setName = "ANLYS/CAL93/DSH/DSH"; String transName = "ANLYS/CAL93/DSH_TRANS/DSH_TRANS";
		//int pulse = 351; String setName = "SIM/EQH/faroNov2013/DSH/DSH"; String transName = "SIM/EQH/faroNov2013/TRANS/TRANS";
		int pulse = 351; String setName = "SIM/EQH/minerva3/DSH/"; String transName = "SIM/EQH/minerva3/TRANS/";
		
		//Display swtDisplay = new Display();
		
		///ConcurrencyUtils.setThreadPool(ImageProcUtil.getThreadPool()); //bad idea :(
		   
		//Read data from PCCIS MDS+ DB (HGW Lab)
		
		//Sources need to be instantiated
		GMDSSource mdsSrc = new GMDSSource();
		mdsSrc.loadTarget("AUG", pulse, setName);
		
		while(!mdsSrc.isIdle()){
			try { Thread.sleep(500); } catch (InterruptedException e) { }		
		}
		
		TransformProcessor xform = new TransformProcessor(mdsSrc, 0);
		try { Thread.sleep(500); } catch (InterruptedException e) { } //wait for the initial calc to fail before retrigger		
		xform.loadConfigGMDS("AUG", pulse);
		xform.calc();
		
		while(!xform.isIdle()){
			try { Thread.sleep(500); } catch (InterruptedException e) { }		
		}
		
		GMDSSink mdsSink = new GMDSSink();
		mdsSink.setSource(xform);
		
		mdsSink.setData("AUG", pulse, transName);
		mdsSink.save();
				
		while(!mdsSink.isIdle()){
			try { Thread.sleep(500); } catch (InterruptedException e) { }		
		}
		
		//ImageProcUtil.addImgPipeClass(GMDSSink.class);		
		//ImageProcUtil.addImgPipeClass(TransformProcessor.class);
			
		//ImageWindow imgWin1 = new ImageWindow(swtDisplay, null, 0);
		
		//ImageProcUtil.msgLoop(swtDisplay);
		
		ImageProcUtil.shutdown();
		
		//swtDisplay.dispose();
	}
}