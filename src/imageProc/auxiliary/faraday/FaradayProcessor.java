package imageProc.auxiliary.faraday;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.Composite;

import algorithmrepository.ExtrapolationMode;
import algorithmrepository.Interpolation1D;
import algorithmrepository.InterpolationMode;
import algorithmrepository.LinearInterpolation1D;
import descriptors.aug.AUGSignalDesc;
import descriptors.gmds.GMDSSignalDesc;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.database.gmds.GMDSUtil;
import imageProc.graph.Series;
import mds.AugShotfileFetcher;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import signals.aug.AUGSignal;
import signals.gmds.GMDSSignal;

/** Calculate Faraday rotation for a given field based on coil currents and the given
 * coefficients. Plot, and add to metadata.
 */
public class FaradayProcessor extends ImgSourceOrSinkImpl implements ImgSink, ConfigurableByID {
	
	/** DONT USE THIS! Might generalise this to an interface later */
	private FaradaySWTControl swtFaradayControl;
	
	private boolean idle = true;
	private boolean autoCalc;
	private boolean calcDone = true;
	private boolean signalAbort;
	
	public static class CoilResponse {
		public String name;
		public double response;
		public String signal;
		public boolean plot;
		public double faraday[];
		
		//coil current loaded from signal, interpolated onto frame timebase
		public double current[] = null;
		public int loadedShot = -1;
	}
	
	private ArrayList<CoilResponse> coilResponses = new ArrayList<CoilResponse>();
	
	
	private String signalsDiag = "MAD";
	
	/** Total faraday rotation for whole time trace */
	private double[] faradayTotal;
		
	/** List of all series from the image and other modules */
	private List<Series> seriesList;

	private String calibSourceInfo;

	private String lastLoadedConfig;
			
	public FaradayProcessor() { this(null, -1);	}
	
	public FaradayProcessor(ImgSource source, int idx) {
		setSource(source);
		setSelectedSourceIndex(idx);
	}

	@Override
	protected void selectedImageChanged() {
		calc();		
	}
		
	@Override
	public void imageChangedRangeComplete(int idx) {
		if(autoCalc)
			calc();
	}
		
	@Override
	public void calc(){
		calcDone = false;
		signalAbort = false;
		idle = false;
		ImageProcUtil.ensureFinalUpdate(this, new Runnable() { @Override public void run() { doCalc();  } });
	}
	
	/** external direct use of doScan() */
	public void doCalcNow(){
		doCalc();
	}
	
	
	private void doCalc(){
		if(connectedSource == null)
			return;
		
		try{
			loadSignals();
			calcFaraday();
			calcDone = true;
		}finally{
			idle = true;
			updateAllControllers();
		}
	}
	
	private void loadSignals(){
		
		int shot = GMDSUtil.getMetaAUGShot(connectedSource);
//		shot=34668;
		AugShotfileFetcher amds = AugShotfileFetcher.defaultInstance();
		
		double t[] = (double[])connectedSource.getSeriesMetaData("time");
		
		for(CoilResponse response : coilResponses){
			if(signalAbort)
				return;
			if(response.current == null || response.loadedShot != shot || response.current.length != t.length){
				if(response.response == 0){ //ignore
					response.current = null;
					response.loadedShot = shot;
					continue;
				}
				
				try{
					AUGSignalDesc desc = new AUGSignalDesc("/aug/"+shot+"/sig/"+response.signal);
					AUGSignal sig = (AUGSignal)amds.getSig(desc);
					
					double tSig[] = (double[]) sig.getCoordsAsType(0, double.class);
					double dSig[] = (double[]) sig.getDataAsType(double.class);
					
					Interpolation1D interp = new Interpolation1D(tSig, dSig, InterpolationMode.LINEAR, ExtrapolationMode.CONSTANT, Double.NaN);
					
					response.current = interp.eval(t);
					response.loadedShot = shot;
					
				}catch(RuntimeException err){
					System.err.println("FaradayProcessor: Unable to load signal " + response.signal + ": " + err);
					response.current = null;
					response.loadedShot = shot;
				}
			}
		}
	}
	
	private void calcFaraday(){
		ArrayList<Series> newSeriesList = new ArrayList<Series>();
		
		int nImages = connectedSource.getNumImages();
		double faradayTotal[] = new double[nImages];
		
		double t[] = (double[])connectedSource.getSeriesMetaData("time");
		
		for(CoilResponse response : coilResponses){
			if(signalAbort)
				return;
			if(response.response == 0){
				continue;
				
			}if(response.current == null || response.current.length != nImages) {
				response.faraday = Mat.fillArray(Double.NaN, nImages);
				faradayTotal = response.faraday.clone();
				
			}else{
				response.faraday = new double[nImages];
				for(int i=0; i < response.current.length; i++){
					response.faraday[i] = response.response * response.current[i]/1000;
					faradayTotal[i] += response.faraday[i];
					
				}
			}
			
		}
		
		connectedSource.setSeriesMetaData("Faraday/faradayAngle", faradayTotal, true);
		connectedSource.setSeriesMetaData("Faraday/calibSourceInfo", calibSourceInfo, false);

		newSeriesList.add(new Series("Total", t, faradayTotal));
		
		for(CoilResponse response : coilResponses){

			if(response.plot)
				newSeriesList.add(new Series(response.name, t, response.faraday));
		}
		seriesList = newSeriesList;
	
	}
	
	
	

	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			FaradaySWTControl control = new FaradaySWTControl(this, (Composite)args[0], (Integer)args[1]);
			controllers.add(control);
			return control;
		}else
			return null;
		
		
	}
	public List<Series> getSeriesList(){ return seriesList; }
	
	@Override
	public void destroy() {
		super.destroy();
	}
	
	@Override
	public void updateAllControllers() { //make public for subProcs
		super.updateAllControllers();
	}
	
	@Override
	public boolean isIdle() { return idle;}

	public List<CoilResponse> getCoilResponses(){ return this.coilResponses; }

	@Override
	public void saveConfig(String id){
		String parts[] = id.split("/");
		if(parts[0].equals("gmds"))
			saveConfig(parts[1].length() > 0 ? parts[1] : null, Algorithms.mustParseInt(parts[2]));
	}
	
	public void saveConfig(String gmdsExp, int pulse) {
		if(gmdsExp == null)
			gmdsExp = GMDSUtil.getMetaExp(connectedSource);
		if(pulse <= -1)
			pulse = GMDSUtil.getMetaDatabaseID(connectedSource);
		
		String names[], signals[];
		double responseVals[];
		
		synchronized (coilResponses) {
			int n = coilResponses.size();
			names = new String[n];
			signals = new String[n];
			responseVals = new double[n];
			for(int i=0; i < n; i++){
				CoilResponse response = coilResponses.get(i);
				names[i] = response.name;
				signals[i] = response.signal;
				responseVals[i] = response.response;
			}
		}
		
		GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, gmdsExp, "Faraday/names");		
		GMDSSignal sig = new GMDSSignal(sigDesc, names);
		GMDSUtil.globalGMDS().writeToCache(sig);

		sigDesc = new GMDSSignalDesc(pulse, gmdsExp, "Faraday/signals");		
		sig = new GMDSSignal(sigDesc, signals);
		GMDSUtil.globalGMDS().writeToCache(sig);

		sigDesc = new GMDSSignalDesc(pulse, gmdsExp, "Faraday/responses");		
		sig = new GMDSSignal(sigDesc, responseVals);
		GMDSUtil.globalGMDS().writeToCache(sig);
		
		sigDesc = new GMDSSignalDesc(pulse, gmdsExp, "Faraday/sourceInfo");		
		sig = new GMDSSignal(sigDesc, new String[]{ calibSourceInfo });
		GMDSUtil.globalGMDS().writeToCache(sig);		
	}
	
	@Override
	public void loadConfig(String id){
		String parts[] = id.split("/");
		if(parts[0].equals("gmds")) {
			loadConfig(parts[1].length() > 0 ? parts[1] : null, Algorithms.mustParseInt(parts[2]));
			lastLoadedConfig = new String(id);
		}
	}
	
	public void loadConfig(String gmdsExp, int pulse) {
		if(gmdsExp == null)
			gmdsExp = GMDSUtil.getMetaExp(connectedSource);
		if(pulse <= -1)
			pulse = GMDSUtil.getMetaDatabaseID(connectedSource);
		
		
		GMDSSignal sig = (GMDSSignal)GMDSUtil.globalGMDS().getSig(new GMDSSignalDesc(pulse, gmdsExp, "Faraday/names"));
		String names[] = (String[])sig.getData();		
		sig = (GMDSSignal)GMDSUtil.globalGMDS().getSig(new GMDSSignalDesc(pulse, gmdsExp, "Faraday/signals"));
		String signals[] = (String[])sig.getData();
		sig = (GMDSSignal)GMDSUtil.globalGMDS().getSig(new GMDSSignalDesc(pulse, gmdsExp, "Faraday/responses"));
		double responseVals[] = (double[])sig.getData();

		try{
			sig = (GMDSSignal)GMDSUtil.globalGMDS().getSig(new GMDSSignalDesc(pulse, gmdsExp, "Faraday/calibSourceInfo"));
			String strs[] = (String[])sig.getData();
			calibSourceInfo = strs[0];
			
		}catch(RuntimeException err){
			calibSourceInfo = "Unknown";
		}
		
		int n = names.length;
		synchronized (coilResponses) {
			coilResponses.clear();
			for(int i=0; i < names.length; i++){
				CoilResponse response = new CoilResponse();
				response.name = names[i];
				response.signal = signals[i];
				response.response = responseVals[i];						
				coilResponses.add(response);
			}
		}
		updateAllControllers();
	}

	public void clearSignals() {
		synchronized (coilResponses) {
			for(CoilResponse response : coilResponses){
				response.faraday = null;
				response.current = null;
				response.loadedShot = -1;
			}
		}
	}

	@Override
	public void setAutoUpdate(boolean enable) { this.autoCalc = enable; }	
	@Override
	public boolean getAutoUpdate() { 	return autoCalc; }
	@Override
	public boolean wasLastCalcComplete() { return calcDone;	}
	@Override
	public void abortCalc() { signalAbort = true;	}
	
	@Override
	public String toShortString() {
		return "Faraday";
	}

	public void setCalibSourceText(String calibSourceInfo) { this.calibSourceInfo = calibSourceInfo; }

	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig; }
	
}
