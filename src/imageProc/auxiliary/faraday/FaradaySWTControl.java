package imageProc.auxiliary.faraday;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import imageProc.auxiliary.faraday.FaradayProcessor.CoilResponse;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.database.gmds.GMDSPipe;
import imageProc.graph.JFreeChartGraph;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** The Graph Util GUI component */
public class FaradaySWTControl implements ImagePipeController {
	
	public static final DecimalFormat tableValueFormat = new DecimalFormat("#.000000");
	
	private static final String[] colNames = { "Name", "Signal", "Response", "Current Now", "Faraday Now", "Plot" };
	private static final int COL_NAME = 0;
	private static final int COL_SIGNAL = 1;
	private static final int COL_RESPONSE = 2;
	private static final int COL_CURRENTNOW = 3;
	private static final int COL_FARADAYNOW = 4;
	private static final int COL_PLOT = 5;
	
	private Group swtGroup;
	
	private SashForm swtSashForm;
	private ScrolledComposite swtTopScrollComp;
	private Composite swtTopComp;
	
	private FaradayProcessor proc;
	private Label basicInfo;
	private Text calibSourceInfoTextbox;

	private Table pointsTable;
	private TableEditor pointsTableEditor;
	private TableItem tableItemEditing = null;
	
	private Combo loadExpCombo; 
	private Spinner loadPulseSpiner;
	private Button loadPointsButton;
	private Button savePointsButton;
	
	private Button autocalcCheckbox;
	private Button loadButton;
	private Button calcButton;
		
	private JFreeChartGraph graph;
	
	public FaradaySWTControl(FaradayProcessor proc, Composite parent, int style) {
		this.proc = proc;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("Faraday calculation");
		swtGroup.setLayout(new FillLayout());	
		swtGroup.addListener(SWT.Dispose, new Listener() { @Override public void handleEvent(Event event) { destroy(); }});
		
		swtSashForm =  new SashForm(swtGroup, SWT.VERTICAL | SWT.BORDER);
        swtSashForm.setLayout(new FillLayout());
        
        //swtTopScrollComp = new ScrolledComposite(swtSashForm, SWT.V_SCROLL);
        //swtTopScrollComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
                
        swtTopComp = new Composite(swtSashForm, SWT.NONE);
        swtTopComp.setLayout(new GridLayout(6, false));
        
		basicInfo = new Label(swtTopComp, SWT.NONE);
		basicInfo.setText("Init\n.\n.\n.\n.\n.");
		basicInfo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		
		calibSourceInfoTextbox = new Text(swtTopComp, SWT.MULTI);
		calibSourceInfoTextbox.setText("Init\n.\n.\n.\n");
		calibSourceInfoTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		calibSourceInfoTextbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { calibSourceTextboxEvent(event); } });
		
						
		pointsTable = new Table(swtTopComp, SWT.BORDER | SWT.MULTI);				
		pointsTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 0));
		pointsTable.setHeaderVisible(true);
		pointsTable.setLinesVisible(true);
		
		calcButton = new Button(swtTopComp, SWT.PUSH);
		calcButton.setText("Calc");
		calcButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { calcButtonEvent(event); } });
		calcButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		
		autocalcCheckbox = new Button(swtTopComp, SWT.CHECK);
		autocalcCheckbox.setText("auto");
		autocalcCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { autocalcCheckboxEvent(event); } });
		autocalcCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		
		loadButton = new Button(swtTopComp, SWT.PUSH);
		loadButton.setText("Reload signals");
		loadButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { forceLoadButtonEvent(event); } });
		loadButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
		
		
		Label lOP = new Label(swtTopComp, SWT.NONE); lOP.setText("Load:");		
		loadExpCombo = new Combo(swtTopComp, SWT.NONE);
		loadExpCombo.setItems(GMDSPipe.getAvailableExperiments());
		loadExpCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		loadPulseSpiner = new Spinner(swtTopComp, SWT.NONE);
		loadPulseSpiner.setValues(-1, -1, Integer.MAX_VALUE, 0, 1, 10);
		loadPulseSpiner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		
		loadPointsButton = new Button(swtTopComp, SWT.PUSH);
		loadPointsButton.setText("Load");
		loadPointsButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { loadAltPulseButtonEvent(event); } });
		
		savePointsButton = new Button(swtTopComp, SWT.PUSH);
		savePointsButton.setText("Save (current pulse)");
		savePointsButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { saveButtonEvent(event); } });
		savePointsButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
		
		TableColumn cols[] = new TableColumn[colNames.length];
		for(int i=0; i < colNames.length; i++){
			cols[i] = new TableColumn(pointsTable, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
			cols[i].setText(colNames[i]); 
		}

		dataToGUI();
		
		pointsTableEditor = new TableEditor(pointsTable);
		pointsTableEditor.horizontalAlignment = SWT.LEFT;
		pointsTableEditor.grabHorizontal = true;
		pointsTable.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { tableMouseDownEvent(event); } });
		
		graph = new JFreeChartGraph(swtSashForm, SWT.NONE);
		//graph.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		swtTopComp.pack();
		swtGroup.pack();
		//swtTopScrollComp.setContent(swtTopComp);	 
		//swtTopScrollComp.setExpandHorizontal(true);
		
		generalControllerUpdate();
		
	}

	protected void calibSourceTextboxEvent(Event event) {
		proc.setCalibSourceText(calibSourceInfoTextbox.getText());
	}

	protected void forceLoadButtonEvent(Event event) {
		proc.clearSignals();
		proc.calc();
	}


	protected void autocalcCheckboxEvent(Event event) {
		proc.setAutoUpdate(autocalcCheckbox.getSelection());
	}

	protected void calcButtonEvent(Event event) {
		proc.calc();
	}


	private void loadAltPulseButtonEvent(Event e) {
		proc.loadConfig(loadExpCombo.getText(), loadPulseSpiner.getSelection());
		dataToGUI();
	}
	
	private void saveButtonEvent(Event e) {
		proc.saveConfig(null, -1);
		dataToGUI();
	}
	
	private void dataToGUI() {
		
		List<CoilResponse> responses = proc.getCoilResponses();
		synchronized (responses) {
			responses = new ArrayList<CoilResponse>(responses); //clone to release lock but avoid concurrent modification
		}
			
		//convert map to list and sort
		Collections.sort(responses, new Comparator<CoilResponse>() {
			@Override
			public int compare(CoilResponse r1, CoilResponse r2) {				
				return r1.name.compareTo(r2.name);
			}
		});
		
		//clear and repopulate table
		synchronized (pointsTable) {
				
			TableItem selected[] = pointsTable.getSelection();
			String selectedPoint = (selected.length >= 1) ? selected[0].getText(1) : null; 
			pointsTable.removeAll();
			
			int frameIdx = proc.getSelectedSourceIndex();
			
			for(CoilResponse r : responses){
				
				if(r.name.length() <= 0)
					continue;
				TableItem item = new TableItem(pointsTable, SWT.NONE);
				item.setText(COL_NAME, r.name);
				item.setText(COL_SIGNAL, r.signal == null ? "" : r.signal);
				item.setText(COL_RESPONSE, tableValueFormat.format(r.response));
				item.setText(COL_CURRENTNOW, (frameIdx > 0 && r.current != null && r.current.length >= frameIdx) ? tableValueFormat.format(r.current[frameIdx]) : "");
				item.setText(COL_FARADAYNOW, (frameIdx > 0 && r.faraday != null && r.faraday.length >= frameIdx) ? tableValueFormat.format(r.faraday[frameIdx]) : "");
				item.setText(COL_PLOT, r.plot ?  "Y" : "N");
			}
			
			//and finally a blank item for creating new point
			TableItem blankItem = new TableItem(pointsTable, SWT.NONE);
			blankItem.setText(COL_NAME, "<new>");
			
			if(selectedPoint != null){
				for(int i=0; i < pointsTable.getItemCount(); i++){
					if(selectedPoint.equals(pointsTable.getItem(i).getText(1))){
						pointsTable.select(i);
						break;
					}
				}
			}
		}
		
		for(int i=0; i < colNames.length; i++){
			pointsTable.getColumn(i).pack();			
		}
	}
	
	private Text editBox = null;
	/** Copied from 'Snippet123'  Copyright (c) 2000, 2004 IBM Corporation and others. 
	 * [ org/eclipse/swt/snippets/Snippet124.java ] */
	private void tableMouseDownEvent(Event event){
		Rectangle clientArea = pointsTable.getClientArea ();
		Point pt = new Point (clientArea.x + event.x, event.y);
		int index = pointsTable.getTopIndex ();
		while (index < pointsTable.getItemCount ()) {
			boolean visible = false;
			final TableItem item = pointsTable.getItem (index);
			for (int i=0; i<pointsTable.getColumnCount (); i++) {
				Rectangle rect = item.getBounds (i);
				if (rect.contains (pt)) {
					final int column = i;
					if(editBox != null){
						editBox.dispose(); //oops, one left over
					}
					editBox = new Text(pointsTable, SWT.NONE);
					tableItemEditing = item;
					Listener textListener = new Listener () {
						public void handleEvent (final Event e) {
							switch (e.type) {
							case SWT.FocusOut:
								tableColumnModified(item, column, editBox.getText());								
								editBox.dispose();
								editBox = null;
								tableItemEditing = null;
								break;
							case SWT.Traverse:
								switch (e.detail) {
								case SWT.TRAVERSE_RETURN:
									tableColumnModified(item, column, editBox.getText());
									//FALL THROUGH
								case SWT.TRAVERSE_ESCAPE:
									editBox.dispose ();
									editBox = null;
									tableItemEditing = null;
									e.doit = false;
								}
								break;
							}
						}
					};
					editBox.addListener (SWT.FocusOut, textListener);
					editBox.addListener (SWT.Traverse, textListener);
					pointsTableEditor.setEditor (editBox, item, i);
					editBox.setText (item.getText (i));
					editBox.selectAll ();
					editBox.setFocus ();
					return;
				}
				if (!visible && rect.intersects (clientArea)) {
					visible = true;
				}
			}
			if (!visible) return;
			index++;
		}
	}
	
	private void tableColumnModified(TableItem item, int column, String text){
		
		List<CoilResponse> responses = proc.getCoilResponses();
		synchronized (responses) {
			if(item.isDisposed())
				return; // ... what?
			
			String coilName = item.getText(COL_NAME);
			
			clearJunk: do{//clear detritus
				for(CoilResponse rsp : responses){				
					if(rsp.name == null || rsp.name.length() == 0 || rsp.name.equals("<new>")){
						responses.remove(rsp);
						continue clearJunk;
					}
				}
				break;
			}while(true);
			
			CoilResponse response = null;
			for(CoilResponse rsp : responses){				
				if(coilName.equals(rsp.name)){
					response = rsp;
					break;
				}
			}
			
			switch(column){
				case COL_NAME:
				
				if(coilName.length() <= 0 || coilName.equals("<new>")){ //creating new point
					if(text.equals("<new>"))
						return;
					response = new CoilResponse();
					response.name = text;
					responses.add(response);
					
				}else if(text.length() <= 0 || text.equals("<new>")){ //deleting point
					if(response != null)
						responses.remove(response);
					
				}else{ //just changing name
					response.name = text;
				}
				
				item.setText(column, text);
				
				break;
			case COL_SIGNAL:
				response.signal = text;
				item.setText(column, text);
				
				break;
			case COL_RESPONSE:
				response.response = Algorithms.mustParseDouble(text);
				item.setText(column, tableValueFormat.format(response.response));
				break;
			case COL_PLOT:
				response.plot = text.toLowerCase().startsWith("y");
				item.setText(column, response.plot ? "Y" : "N");
				
				break;
			}
			
			TableItem lastItem = pointsTable.getItem(pointsTable.getItemCount()-1);
			if(!lastItem.getText(COL_NAME).equals("<new>")){
				TableItem blankItem = new TableItem(pointsTable, SWT.NONE);
				blankItem.setText(COL_NAME, "<new>");
			}
			
			//proc.updateAllControllers();
			proc.calc();
		}
	}

	
	@Override
	public Composite getInterfacingObject() {
		return swtGroup;
	}	
	
	public void generalControllerUpdate() {	
		if(swtGroup.isDisposed()) return;
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable(){	
			@Override
			public void run() { doGeneralUpdate();  }
		});
	}
	
	
	private void doGeneralUpdate(){
		if(swtGroup.isDisposed()) return;
		
		String infoText = proc.isIdle() ? "Idle" : "Busy";
		
		dataToGUI();
		
		basicInfo.setText(infoText);
		
		graph.setData(proc.getSeriesList());
		
	}

	@Override
	public void destroy() {
		swtGroup.dispose();
		proc.controllerDestroyed(this);
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { return proc;	}

	

}
