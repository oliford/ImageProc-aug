package imageProc.auxiliary.fluxSurfs;

import org.eclipse.swt.widgets.Composite;

import base.SignalFetcher;
import descriptors.aug.AUGSignalDesc;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import mds.AugMDSFetcher;
import signals.aug.AUGSignal;

/** Doesn't really do anything, just here to attach the FluxSurfacesSWT */
public class FluxSurfacesHanger extends ImgSourceOrSinkImpl implements ImgSink {
	private FluxSurfacesSWTControl swtController;
	
	private String equiSignalPFMNode = "PFM#augdiag";			

	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			if(swtController == null){
				swtController = new FluxSurfacesSWTControl(this, (Composite)args[0], (Integer)args[1]);
				controllers.add(swtController);
			}
			return swtController;
		}else
			return null;
	}

	public void load(int pulse, String signalPath) {

		SignalFetcher amds = AugMDSFetcher.defaultInstance();
		//AugShotfileFetcher amds = AugShotfileFetcher.defaultInstance();
		
		AUGSignalDesc desc = new AUGSignalDesc("/aug/" + pulse + "/" + signalPath + "/" + equiSignalPFMNode);
		System.out.print("Loading AUG Psi Map from signal '" + desc + "'... ");
		AUGSignal sig = (AUGSignal)amds.getSig(desc); //This can take a while, even from disk
		System.out.println("Done");
		
		
		
	}
	
	public boolean isIdle(){ throw new RuntimeException("NotImplemented"); }

	@Override
	public boolean getAutoUpdate() { return false; }
	@Override
	public void setAutoUpdate(boolean enable) { }
	@Override
	public void calc() {  }
	@Override
	public boolean wasLastCalcComplete(){ return true; }
	@Override
	public void abortCalc(){ }
}
