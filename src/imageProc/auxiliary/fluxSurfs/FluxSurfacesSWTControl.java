package imageProc.auxiliary.fluxSurfs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.swt.SWTControllerInfoDraw;

public class FluxSurfacesSWTControl implements ImagePipeController, SWTControllerInfoDraw {
	private FluxSurfacesHanger proc;
	
	private Group swtGroup;
	
	private Label status;
	private Spinner pulseSpinner;
	private Text sigPathTextbox;
	private Button loadButton;
		
	public FluxSurfacesSWTControl(FluxSurfacesHanger proc, Composite parent, int style) {
		this.proc = proc;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("Equilibrium");
		swtGroup.setLayout(new GridLayout(4, false));
		
		status = new Label(swtGroup, SWT.NONE);
		status.setText("init");
		status.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));

		Label lP = new Label(swtGroup, SWT.NONE); lP.setText("Pulse:");
		pulseSpinner = new Spinner(swtGroup, SWT.NONE);
		pulseSpinner.setValues(30000, 0, Integer.MAX_VALUE, 0, 1, 10);
		pulseSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));

		Label lS = new Label(swtGroup, SWT.NONE); lP.setText("Signal:");
		sigPathTextbox = new Text(swtGroup, SWT.NONE);
		sigPathTextbox.setText("/sig/EQI");
		sigPathTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));

		loadButton = new Button(swtGroup, SWT.NONE);
		loadButton.setText("Load");
		loadButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { loadButtonEvent(event); }});
	}
	
	private void loadButtonEvent(Event event) {
		int pulse = pulseSpinner.getSelection();
		String signalPath = sigPathTextbox.getText();
		proc.load(pulse, signalPath);
	}
	
	@Override
	public void generalControllerUpdate() {
		
	}	

	@Override
	public void destroy() {
		proc.destroy();
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { return proc;	}

	@Override
	public Object getInterfacingObject() { return swtGroup; }

	@Override
	public void drawOnImage(GC gc, double scale[], int imageWidth,
			int imageHeight, boolean asSource) {
		
		
	}


	
}
