package imageProc.auxiliary.radialField;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.Composite;

import algorithmrepository.ExtrapolationMode;
import algorithmrepository.Interpolation1D;
import algorithmrepository.InterpolationMode;
import algorithmrepository.interfaces.Interpolation2D;
import aug.equi.AUGEquilibriumData;
import descriptors.aug.AUGSignalDesc;
import imageProc.core.ImageProcUtil;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.database.gmds.GMDSUtil;
import imageProc.graph.Series;
import net.jafama.FastMath;
import mds.AugShotfileFetcher;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import seed.digeom.FunctionND;
import seed.digeom.RectangularDomain;
import seed.optimization.HookeAndJeeves;
import signals.aug.AUGSignal;

/**  Load charge exchange data for toroidal rotation, fit and show electric field 
 * 
 */
public class RadialFieldProcessor extends ImgSourceOrSinkImpl implements ImgSink {
	
	/** Might generalise this to an interface later */
	private RadialFieldSWTControl swtErControl;
	
	private boolean idle = true;
	private boolean calcDone = true;
	private boolean signalAbort;
	
	public static String[] diags = new String[]{ "CEZ", "COZ", "CUZ", "CCZ", "CAZ" }; 
	private boolean diagEnable[] = new boolean[]{ true, true, true, false, false };
	
	/** Positions of CX data [diag][pos] */ 
	private double wTorR[][], wTorZ[][];	
	/** Values of CX wTor data [diag][time][pos] */
	private double wTorData[][][], wTorSigma[][][];
	/** Equilibrium normed flux at CX data positions [diag][time][pos] */
	private double wTorRho[][][];
	
	private double fitKnotsRho[];
	private double fitKnotsWTor[][];
	
	private String equiExp = "AUGD";
	private String equiDiag = "EQI";
		
	/** List of all series from the image and other modules */
	private List<Series> spatialSeriesList, timeSeriesList;

	private AUGEquilibriumData equi;
	
	private String status;
	private boolean calcSingleFrame;

	private int nKnots = 5;

			
	public RadialFieldProcessor() {
		this(null, -1);
		status = "Init";
	}
	
	public RadialFieldProcessor(ImgSource source, int idx) {
		setSource(source);
		setSelectedSourceIndex(idx);
	}
		
	@Override
	public void calc(){
		calcDone = false;
		signalAbort = false;
		idle = false;
		ImageProcUtil.ensureFinalUpdate(this, new Runnable() { @Override public void run() { doCalc();  } });
	}
	
	/** external direct use of doScan() */
	public void doCalcNow(){
		doCalc();
	}
	
	
	private void doCalc(){
		if(connectedSource == null)
			return;
		
		try{
			if(fitKnotsRho == null || fitKnotsRho.length != nKnots){
				fitKnotsRho = Mat.linspace(0.0, 1.2, nKnots);
			}
			
			loadSignals();
			
			int nDiagsLoaded =0;
			for(int i=0; i < wTorData.length; i++)
				if(wTorData[i] != null && wTorData[i].length > 0)
					nDiagsLoaded++;
			if(nDiagsLoaded == 0)
				throw new RuntimeException("No CXRS diagnostics data available.");
			
			loadEquiMapping();
			
			if(fitKnotsWTor == null){
				if(calcSingleFrame){
					doFit(getSelectedSourceIndex());
				}else{
					for(int iF=0; iF < connectedSource.getNumImages(); iF++)
						doFit(iF);
				}
			}
				
			connectedSource.setSeriesMetaData("RadialField/fitRho", fitKnotsRho, false);
			connectedSource.setSeriesMetaData("RadialField/fitWTor", fitKnotsWTor, true);
			
			makeSeries();
			calcDone = true;
			status = "Done";
			updateAllControllers();
			
		}catch(RuntimeException err){
			err.printStackTrace();
			status = "ERROR: " + err;
			
			connectedSource.setSeriesMetaData("RadialField/fitRho", null, false);
			connectedSource.setSeriesMetaData("RadialField/fitVTor", null, true);
			
		}finally{
			idle = true;
			updateAllControllers();
		}
	}
	
	private void loadSignals(){
		status = "Loading CXRS signals";
		updateAllControllers();
		
		int shot = GMDSUtil.getMetaAUGShot(connectedSource);
		
		AugShotfileFetcher amds = AugShotfileFetcher.defaultInstance();
		
		double tFrame[] = (double[])connectedSource.getSeriesMetaData("time");
		
		int nDiag = diags.length;
				
		if(wTorData == null){
			wTorR = new double[nDiag][];				
			wTorZ = new double[nDiag][];
			wTorData = new double[nDiag][][];
			wTorSigma = new double[nDiag][][];
		}
		
		for(int iD=0; iD < nDiag; iD++){
			if(signalAbort)
				return;
			
			if(wTorData[iD] != null)
				continue;
			
			if(wTorRho != null)
				wTorRho[iD] = null;
			
			if(!diagEnable[iD]){
				wTorData[iD] = new double[0][];
				continue; //skip diagnostic
			}
			
			AUGSignal vPhiSig, VPhiErrSig, RSig, ZSig; 
			try{
				vPhiSig = (AUGSignal)amds.getSig(new AUGSignalDesc(shot, diags[iD], "vrot"));
				VPhiErrSig = (AUGSignal)amds.getSig(new AUGSignalDesc(shot, diags[iD], "err_vrot"));
				RSig = (AUGSignal)amds.getSig(new AUGSignalDesc(shot, diags[iD], "R"));
				ZSig = (AUGSignal)amds.getSig(new AUGSignalDesc(shot, diags[iD], "z"));
				
			}catch(RuntimeException err){
				System.err.println("RadialFieldProcessor: No data for diagnostic '"+diags[iD]+"'");
				wTorData[iD] = new double[0][];
				continue; //skip diagnostic
			}
				
			wTorR[iD] = (double[])RSig.getDataAsType(double.class);
			wTorZ[iD] = (double[])ZSig.getDataAsType(double.class);
			double[][] vTorDataAll = (double[][])vPhiSig.getDataAsType(double.class);
			double[][] vTorSigmaAll = (double[][])VPhiErrSig.getDataAsType(double.class);
			
			double tCX[] = (double[])vPhiSig.getCoordsAsType(0, double.class);
			int nCh = wTorR[iD].length;
			
			//interpolate each channel of Vtor data onto frame timebase
			wTorData[iD] = new double[nCh][];
			wTorSigma[iD] = new double[nCh][];			
			for(int iCh=0; iCh < nCh; iCh++){
				Interpolation1D interpData = new Interpolation1D(tCX, vTorDataAll[iCh], InterpolationMode.LINEAR, ExtrapolationMode.CONSTANT, 0);
				Interpolation1D interpSigma = new Interpolation1D(tCX, vTorSigmaAll[iCh], InterpolationMode.LINEAR, ExtrapolationMode.CONSTANT, 0);
				
				wTorData[iD][iCh] = Mat.mulElem(interpData.eval(tFrame), 1 / wTorR[iD][iCh]);
				wTorSigma[iD][iCh] = Mat.mulElem(interpSigma.eval(tFrame), 1 / wTorR[iD][iCh]);
			}

			wTorData[iD] = Mat.transpose(wTorData[iD]);
			wTorSigma[iD] = Mat.transpose(wTorSigma[iD]);
		}
	}
	
	private void loadEquiMapping(){
		status = "Loading equilibrium ";
		updateAllControllers();
		
		int shot = GMDSUtil.getMetaAUGShot(connectedSource);
		double tFrame[] = (double[])connectedSource.getSeriesMetaData("time");
		
		if(equi == null)
			equi = new AUGEquilibriumData();
		
		if(!equiExp.equals(equi.getExp()) || !equiDiag.equals(equi.getDiag()) || shot != equi.getPulse())
			equi.loadData(equiExp, equiDiag, shot);
		
		
		int nDiag = diags.length;
		if(wTorRho == null)
			wTorRho = new double[nDiag][][];
		
		boolean needsUpdate = false;
		for(int iD=0; iD < nDiag; iD++){
			if(wTorData[iD] == null || wTorData[iD].length == 0){
				wTorRho[iD] = null;			
			}else if(wTorRho[iD] == null){
				wTorRho[iD] = new double[tFrame.length][];
				needsUpdate = true;
			}
		}
		if(!needsUpdate)
			return;
		
		double equiTime[] = equi.getTimeOrdered();
		if(equiTime == null)
			throw new RuntimeException("No Equi time vector !??");
		
		for(int iF=0; iF < tFrame.length; iF++){
			int iEqTime = Mat.getNearestIndex(equiTime, tFrame[iF]);
			Interpolation2D interp = equi.createPsiNInterpolator(iEqTime);
			
			for(int iD=0; iD < nDiag; iD++){
				if(wTorRho[iD] == null)
					continue;
				
				wTorRho[iD][iF] = interp.eval(wTorR[iD], wTorZ[iD]);
				for(int iCh=0; iCh < wTorRho[iD][iF].length; iCh++){
					wTorRho[iD][iF][iCh] = FastMath.sqrt(wTorRho[iD][iF][iCh]);
				}
			}
		}
		
	}
	
	private class CostFunc extends FunctionND {
		public final Interpolation1D interp;
		public int iF;
		public double sigmaEdge = 100; 
		
		public CostFunc(int iF) {
			this.interp = new Interpolation1D(fitKnotsRho, fitKnotsWTor[iF], InterpolationMode.CUBIC, ExtrapolationMode.EXCEPTION);
			this.iF = iF;			
		}
		
		@Override
		public double eval(double[] x) {
			interp.setF(x);
			
			double logP = 0;
			for(int iD=0; iD < diags.length; iD++){
				if(wTorData[iD] == null || wTorData[iD].length == 0)
					continue;
				
				double pred[] = interp.eval(wTorRho[iD][iF]);
				for(int iCh=0; iCh < wTorData[iD][iF].length; iCh++){
					if(wTorSigma[iD][iF][iCh] > 0)
						logP -= FastMath.pow2(wTorData[iD][iF][iCh] - pred[iCh]) / 2 /FastMath.pow2(wTorSigma[iD][iF][iCh]);
				}
				
				//don't do silly things at the edge (one for each diagnostic, for weighting)
				logP -= FastMath.pow2(0 - interp.eval(1.2)) / 2 /FastMath.pow2(sigmaEdge);
			}
			
			return -logP;
		}
	};
	
	private void doFit(int iF){
			
		status = "Fitting frame " + iF + " / " + connectedSource.getNumImages();
		updateAllControllers();
		
		double tFrame[] = (double[])connectedSource.getSeriesMetaData("time");
		
		if(fitKnotsWTor == null || fitKnotsWTor.length != tFrame.length){
			fitKnotsWTor = new double[tFrame.length][];
		}
		
		if(fitKnotsWTor[iF] == null){
			fitKnotsWTor[iF] = Mat.fillArray(10000.0, fitKnotsRho.length);
		}
		fitKnotsWTor[iF] = Mat.fillArray(1000.0, fitKnotsRho.length);
		fitKnotsWTor[iF][fitKnotsWTor[iF].length-1] = 0;
		
		HookeAndJeeves opt = new HookeAndJeeves(new CostFunc(iF));
		opt.getObjectiveFunction().setDomain(new RectangularDomain(Mat.fillArray(0.0, fitKnotsWTor[iF].length), 
																	Mat.fillArray(500000.0, fitKnotsWTor[iF].length)));
		opt.init(fitKnotsWTor[iF]);
		System.out.print("RadialField.doFit(): " + iF + ", init:  cost=" + opt.getCurrentValue() + ", p =");
		for(int i=0; i < 200; i++){
			opt.refine();
			Mat.dumpArray(opt.getCurrentPos());
		}
		System.out.print("RadialField.doFit(): " + iF + ", final:  cost=" + opt.getCurrentValue() + ", p =");
		
		fitKnotsWTor[iF] = opt.getCurrentPos();
	}
	
	private void makeSeries(){
		ArrayList<Series> newSeriesList = new ArrayList<Series>();
		
		int iF = getSelectedSourceIndex();
		if(iF < 0)
			return;
		
		int nDiag = diags.length;
		for(int iD=0; iD < nDiag; iD++){
			if(signalAbort)
				return;
			if(wTorData[iD] != null && wTorData[iD].length > 0 && wTorData[iD][iF] != null){
				double m[] = new double[wTorData[iD][iF].length];
				double p[] = new double[wTorData[iD][iF].length];
				for(int i=0; i < wTorData[iD][iF].length; i++){
					double fwhm = 2.35 * wTorSigma[iD][iF][i];
					m[i] = wTorData[iD][iF][i] - fwhm/2;
					p[i] = wTorData[iD][iF][i] + fwhm/2;
				}
				newSeriesList.add(new Series(diags[iD], wTorRho[iD][iF], wTorData[iD][iF]));			
				newSeriesList.add(new Series(diags[iD], wTorRho[iD][iF], p));			
				newSeriesList.add(new Series(diags[iD], wTorRho[iD][iF], m));			
							
			}
		}
		
		if(fitKnotsRho != null && fitKnotsWTor != null && fitKnotsWTor[iF] != null){
			double psiN[] = Mat.linspace(fitKnotsRho[0], fitKnotsRho[fitKnotsRho.length-1], 100);
			double fit[] = new Interpolation1D(fitKnotsRho, fitKnotsWTor[iF], InterpolationMode.CUBIC, ExtrapolationMode.EXCEPTION).eval(psiN);
			newSeriesList.add(new Series("Fit", psiN, fit));
			
		}
		
		spatialSeriesList = newSeriesList;
		
		double tFrame[] = (double[])connectedSource.getSeriesMetaData("time");
		newSeriesList = new ArrayList<Series>();
		for(int i=0; i < fitKnotsRho.length; i++){
			double vTor[] = new double[tFrame.length];
			for(iF=0; iF < tFrame.length; iF++){
				vTor[iF] = (fitKnotsWTor[iF] == null) ? Double.NaN : fitKnotsWTor[iF][i];
			}
			newSeriesList.add(new Series("wTor_" +i, tFrame, vTor));
		}
		timeSeriesList = newSeriesList;
	}
	
	
	

	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			if(swtErControl == null){
				swtErControl = new RadialFieldSWTControl(this, (Composite)args[0], (Integer)args[1]);
				controllers.add(swtErControl);
			}
			return swtErControl;
		}else
			return null;
		
		
	}
	
	public List<Series> getSpatialSeriesList(){ return spatialSeriesList; }
	public List<Series> getTimeSeriesList(){ return timeSeriesList; }
	
	@Override
	public void destroy() {
		super.destroy();
	}
	
	@Override
	public void updateAllControllers() { //make public for subProcs
		super.updateAllControllers();
	}
	
	@Override
	public boolean isIdle() { return idle;}

	@Override
	public void setAutoUpdate(boolean enable) { } //doesn't change based on images	
	@Override
	public boolean getAutoUpdate() { return false; }
	@Override
	public boolean wasLastCalcComplete() { return calcDone;	}
	@Override
	public void abortCalc() { signalAbort = true;	}
	
	public String getStatus() { return status;	}
	public void setCalcSingleFrame(boolean calcSingleFrame){ this.calcSingleFrame = calcSingleFrame; }
	public boolean getCalcSingleFrame(){ return this.calcSingleFrame; }

	public void clearLoadedData() {
		wTorData = null;
		wTorSigma = null;
		wTorRho = null;
		equi.loadData(null,  null, -1);
	}

	public void clearFit() {
		fitKnotsWTor = null;
	}

	public void setNumKnots(int nKnots) { this.nKnots = nKnots;	}

	public void setDiagEnable(int i, boolean enable) {
		diagEnable[i] = enable;
	}

	public boolean getDiagEnable(int i) { return diagEnable[i];  }

	@Override
	protected void selectedImageChanged() {
		super.selectedImageChanged();
		
		calc();
	}
	
	@Override
	public String toShortString() {
		return "RadialField";
	}
}
