package imageProc.auxiliary.radialField;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.database.gmds.GMDSPipe;
import imageProc.graph.JFreeChartGraph;

/** The Graph Util GUI component */
public class RadialFieldSWTControl implements ImagePipeController {
	
	public static final DecimalFormat tableValueFormat = new DecimalFormat("#.###");
	
	private static final String[] colNames = { "Name", "Signal", "Response", "Faraday Now", "Plot" };
	private static final int COL_NAME = 0;
	private static final int COL_SIGNAL = 1;
	private static final int COL_RESPONSE = 2;
	private static final int COL_FARADAYNOW = 3;
	private static final int COL_PLOT = 4;
	
	private Group swtGroup;
	
	private SashForm swtSashForm;
	private ScrolledComposite swtTopScrollComp;
	private Composite swtTopComp;
	
	private RadialFieldProcessor proc;
	private Label basicInfo;
	
	private Combo loadExpCombo; 
	private Spinner loadPulseSpiner;
	private Button loadPointsButton;
	private Button savePointsButton;
	
	private Button calcSingleFrameCheckbox;
	private Button doFitButton;
	private Button autoUpdateCheckbox;
	private Button loadButton;
	private Button updateButton;
	
	private Spinner numKnotsSpinner;
	
	private JFreeChartGraph graph1, graph2;
	
	private Button[] diagButtons;
	
	public RadialFieldSWTControl(RadialFieldProcessor proc, Composite parent, int style) {
		this.proc = proc;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("Faraday calculation");
		swtGroup.setLayout(new FillLayout());	
		swtGroup.addListener(SWT.Dispose, new Listener() { @Override public void handleEvent(Event event) { destroy(); }});
		
		swtSashForm =  new SashForm(swtGroup, SWT.VERTICAL | SWT.BORDER);
        swtSashForm.setLayout(new FillLayout());
        
        swtTopScrollComp = new ScrolledComposite(swtSashForm, SWT.V_SCROLL);
        //swtTopScrollComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        
        swtTopComp = new Composite(swtTopScrollComp, SWT.NONE);
        swtTopComp.setLayout(new GridLayout(6, false));
        
		basicInfo = new Label(swtTopComp, SWT.NONE);
		basicInfo.setText("Init\n.\n.\n.\n.\n.");
		basicInfo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		
		updateButton = new Button(swtTopComp, SWT.PUSH);
		updateButton.setText("Update");
		updateButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { calcButtonEvent(event); } });
		updateButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		
		autoUpdateCheckbox = new Button(swtTopComp, SWT.CHECK);
		autoUpdateCheckbox.setText("Auto Update");
		autoUpdateCheckbox.setSelection(proc.getAutoUpdate());
		autoUpdateCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		autoUpdateCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		
		doFitButton = new Button(swtTopComp, SWT.PUSH);
		doFitButton.setText("Force Fit");
		doFitButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { forceFitButtonEvent(event); } });
		doFitButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		
		calcSingleFrameCheckbox = new Button(swtTopComp, SWT.CHECK);
		calcSingleFrameCheckbox.setText("Single Frame");
		calcSingleFrameCheckbox.setSelection(proc.getCalcSingleFrame());
		calcSingleFrameCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		calcSingleFrameCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		
		
		loadButton = new Button(swtTopComp, SWT.PUSH);
		loadButton.setText("Reload signals");
		loadButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { forceLoadButtonEvent(event); } });
		loadButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
		
		
		Label lOP = new Label(swtTopComp, SWT.NONE); lOP.setText("Load:");		
		loadExpCombo = new Combo(swtTopComp, SWT.NONE);
		loadExpCombo.setItems(GMDSPipe.getAvailableExperiments());
		loadExpCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		loadPulseSpiner = new Spinner(swtTopComp, SWT.NONE);
		loadPulseSpiner.setValues(-1, -1, Integer.MAX_VALUE, 0, 1, 10);
		loadPulseSpiner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
		
		
	/*	loadPointsButton = new Button(swtTopComp, SWT.PUSH);
		loadPointsButton.setText("Load");
		loadPointsButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { loadAltPulseButtonEvent(event); } });
		
		savePointsButton = new Button(swtTopComp, SWT.PUSH);
		savePointsButton.setText("Save (current pulse)");
		savePointsButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { saveButtonEvent(event); } });
		savePointsButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
*/
		
		Label lNK = new Label(swtTopComp, SWT.NONE); lNK.setText("Knots:");
		numKnotsSpinner = new Spinner(swtTopComp, SWT.NONE);
		numKnotsSpinner.setValues(5, 1, Integer.MAX_VALUE, 0, 1, 10);
		numKnotsSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		numKnotsSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 5, 1));
		
		diagButtons = new Button[RadialFieldProcessor.diags.length];
		for(int i=0; i < diagButtons.length; i++){
			diagButtons[i] = new Button(swtTopComp, SWT.CHECK);
			diagButtons[i].setText(RadialFieldProcessor.diags[i]);
			diagButtons[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
			int w = (i < diagButtons.length-1) ? 1 : (6 - (i % 6));
			diagButtons[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, w, 1));			
		}
		
		graph1 = new JFreeChartGraph(swtSashForm, SWT.NONE);
		graph2 = new JFreeChartGraph(swtSashForm, SWT.NONE);
		//graph.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		generalControllerUpdate();
		
		swtTopComp.pack();
		swtTopScrollComp.setContent(swtTopComp);	 
		swtTopScrollComp.setExpandHorizontal(true);
		
	}

	protected void forceLoadButtonEvent(Event event) {
		proc.clearLoadedData();
		proc.calc();
	}

	protected void forceFitButtonEvent(Event event) {
		proc.clearFit();
		proc.calc();
	}


	protected void settingsChangedEvent(Event event) {
		proc.setAutoUpdate(autoUpdateCheckbox.getSelection());
		proc.setCalcSingleFrame(calcSingleFrameCheckbox.getSelection());
		proc.setNumKnots(numKnotsSpinner.getSelection());
		for(int i=0; i < diagButtons.length; i++)
			proc.setDiagEnable(i, diagButtons[i].getSelection());		
	}

	protected void calcButtonEvent(Event event) {
		proc.calc();
	}

	@Override
	public Composite getInterfacingObject() {
		return swtGroup;
	}	
	
	public void generalControllerUpdate() {	
		if(swtGroup.isDisposed()) return;
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable(){	
			@Override
			public void run() { doGeneralUpdate();  }
		});
	}
	
	
	private void doGeneralUpdate(){
		if(swtGroup.isDisposed()) return;
		
		basicInfo.setText(proc.getStatus());
		
		for(int i=0; i < diagButtons.length; i++){
			diagButtons[i].setSelection(proc.getDiagEnable(i));		
		}
		
		graph1.setData(proc.getSpatialSeriesList());
		graph2.setData(proc.getTimeSeriesList());
		
		
	}

	@Override
	public void destroy() {
		swtGroup.dispose();
		proc.controllerDestroyed(this);
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { return proc;	}

	

}
