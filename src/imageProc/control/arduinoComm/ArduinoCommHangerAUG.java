package imageProc.control.arduinoComm;

import org.eclipse.swt.widgets.Composite;

import imageProc.control.arduinoComm.swt.ArduinoCommSWTControl;
import imageProc.control.arduinoComm.swt.ArduinoCommSWTControlAUG;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.EventReciever.Event;

/** Communications with the Arduino in-hall controller code IMSEArduinoCtrl */

public class ArduinoCommHangerAUG extends ArduinoCommHanger {

	private LaserControl laserCtrl;
	private InHallControl inHallCtrl;
	
	/** This controller is outside the hall */
	private boolean isOutsideHall;
	
	
	private int watchdogTimeoutMS = 3600000;	
	
	
	
	public ArduinoCommHangerAUG() {
		this(false); //preset
		ArduinoCommConfig config = config();
		
		//default presets for motor
		config.motor = new MotorConfig[MotorControl.nMotors];
		
		// filter slider on OWIS stage (new backend)
		config.motor[0] = new MotorConfig();		
		config.motor[0].name = "Filter Slider";
		config.motor[0].presets.put("0", 5250.0);
		config.motor[0].presets.put("1", 21250.0);
		config.motor[0].presets.put("2", 37250.0);
		config.motor[0].presets.put("3", 53250.0);
		//{ 7, 293, 580 }, Oli's filter slider
		
		// Megatron lens focus (new backend)
		config.motor[1] = new MotorConfig();		
		config.motor[1].name = "Lens Focus";
		config.motor[1].presets.put("0", 8000.0);
		config.motor[1].presets.put("1", 11000.0);
		config.motor[1].presets.put("2", 22000.0);
		config.motor[1].presets.put("3", 32000.0);
		
		// big internal calibration wheel (new backend internal)	
		config.motor[2] = new MotorConfig();		
		config.motor[2].name =  "Calibration Polariser";
		config.motor[2].presets.put("0", 0.0);
		config.motor[2].presets.put("1", 2200.0);
		config.motor[2].presets.put("2", 4500.0);
		config.motor[2].presets.put("3", 9000.0);
		
	}
	
	public ArduinoCommHangerAUG(boolean presetIsOutsideHall) {
		super();
		this.isOutsideHall = presetIsOutsideHall;
		laserCtrl = new LaserControl(this);
		inHallCtrl = new InHallControl(this);
	}
		
	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			if(swtController == null){
				swtController = new ArduinoCommSWTControlAUG(this, (Composite)args[0], (Integer)args[1]);
				controllers.add(swtController);
			}
			return swtController;
		}else
			return null;
	}
	
	public void receivedLine(String line){
		
		//incoming
		if(line.startsWith("OVN")){
			inHallCtrl.receivedLine(line);			
		}else if(line.startsWith("FLC")){
			inHallCtrl.receivedLine(line);			
		}else if(line.startsWith("LAS")){
			laserCtrl.recievedCommand(line);
		}else if(line.startsWith("SQO")){ //outside-hall sequence conroller (also laser control)
			laserCtrl.recievedCommand(line);
		}else{
			super.receivedLine(line);
		}			
			
		updateAllControllers();
	}

	@Override
	protected void setupController(String type){
		
		boolean isOutside = type.startsWith("OUT");
		
		this.isOutsideHall = isOutside;
		
		//get everything in sync
		
		if(isOutside){
			laserCtrl.setLaserPower(laserCtrl.getLaserPowerState());
			laserCtrl.setDepolPower(laserCtrl.getDepolPowerState());
			laserCtrl.requestTemp();
			laserCtrl.setTargetTemp(laserCtrl.getTargetTemp());
		}else{
			inHallCtrl.setupController();			
		}
		setWatchdogTimeout(watchdogTimeoutMS);
		
	}
	
	@Override
	public void connected(){
		super.connected();		
	}
	
	@Override
	public void logChanged() { //call through from LoggedSerialComm
		updateAllControllers();
	}
	
	public LoggedComm serialComm(){ return serialComm; }
	
	@Override
	public void destroy() {
		serialComm.destroy();
		super.destroy();
	}
	
	public ArduinoCommSWTControl getSWTController(){ return swtController; }
		
	public long getKeepAliveTimeMS(){ return keepAliveRunner.getPeriodMS(); }
	public void setKeepAliveTimeMS(long keepAliveTimeMS){ keepAliveRunner.setPeriodMS(keepAliveTimeMS);	};

	public DataAqCtrl dataAqCtrl(){ return dataAqCtrl; }
	
	private class KeepAliveRunner implements Runnable {
		private Thread thread;
		private boolean death = false;
		private long periodMS;
		
		public KeepAliveRunner() {
			setPeriodMS(60000); //60s default period
		}
		
		@Override
		public void run() {
			while(!death){
				if(serialComm != null && serialComm.isConnected()){
					if(inhibitKeepAlive()){
						System.out.println("Keep-alive inhibited.");
					}else{
						serialComm.send("PING");
					}
				}
				try {
					Thread.sleep(periodMS);
				} catch (InterruptedException e) { }
			}
		}
		public void setPeriodMS(long periodMS) {
			this.periodMS = periodMS;
			if(periodMS <= 0){
				death = true;
				if(thread != null && thread.isAlive()){ 
					thread.interrupt();
				}
			}else{
				death = false;
				if(thread == null || !thread.isAlive()){
					thread = new Thread(this);
					thread.start();
				}else{
					thread.interrupt();
				}
			}		
			
		}
		public long getPeriodMS() { return periodMS; }
	}
	
	private KeepAliveRunner keepAliveRunner = new KeepAliveRunner();
	
	@Override
	public void event(Event event) {
		switch(event) {
		case GlobalStart:
			if(isOutsideHall){
				laserCtrl.syncroniseState();
			}else{
				//in-hall state sync
				inHallCtrl.requestOvenTemp(); //req oven temp now (at soft acq start), response will be hung in metadata when/if if returns
			}
			break;
			
		case GlobalAbort:
			if(serialComm.isConnected())
				dataAqCtrl.abort();
			break;
		default:
			break;
		}
		super.event(event);
	}
	
 	public int getWatchdogTimeout() { return watchdogTimeoutMS; }
 	public void setWatchdogTimeout(int watchdogTimeoutMS) { 
 		this.watchdogTimeoutMS = Math.max(5000, watchdogTimeoutMS);
 		serialComm.send("CTL-WDOG" + this.watchdogTimeoutMS);
	}

 	public boolean isIdle(){ return false; }

 	public void lensInit() { 
		serialComm.send("LNS-INIT");
	}

 	public void lensClose() { 
		serialComm.send("LNS-CLOSE");
	}

	public void moveFocus(int focusValue) { 
		serialComm.send("LNS-MFOCUS"+focusValue);
	}

	public void setAperture(int value) { 
		serialComm.send("LNS-APER" + value);
	}
	
	private String lensMoveSyncObject = "lensMoveSyncObject";
	// Move focus all the way to one end, by initing the lens and then sending the command multiple times
	public void lensFullMove(boolean focusFar, int nTries){
		final int n = nTries;
		final boolean far = focusFar;
		ImageProcUtil.ensureFinalUpdate(lensMoveSyncObject, new Runnable() {
			@Override
			public void run() {
				lensInit();
				
				for(int i=0; i < n; i++){
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) { }
					
					moveFocus(far ? -10000 : 10000);
				}
			}
		});
	}

	public LaserControl getLaserCtrl() { return laserCtrl; }

	public boolean isOutsideHall() { return isOutsideHall; }

	public void setupForShot() {
		// MT1 --> home
		// wait for home
		// MT1 --> 22000
		if(isOutsideHall){
			laserCtrl.setLaserPower(false);
			laserCtrl.setDepolPower(false);
			
		}else{
			lensFullMove(true, 6); //full far for beam focus
			inHallCtrl.ovenSetPower(true);
		}
		
		updateAllControllers();
	}

	public void setupForLampMeasurement() {
		// MT1 --> home
		if(isOutsideHall){
			laserCtrl.setLaserPower(true);
			laserCtrl.setDepolPower(true);
		}else{
			lensFullMove(false, 6); //go fulll near to blur lamps
		}		
		
		updateAllControllers();
	}

	@Override
	public boolean getAutoUpdate() { return false; }
	@Override
	public void setAutoUpdate(boolean enable) { }
	@Override
	public void calc() { 	}
	@Override
	public boolean wasLastCalcComplete(){ return true; }
	@Override
	public void abortCalc(){ }

	public InHallControl getInHallCtrl() { return inHallCtrl;	}	

	@Override
	public String getTypeName() {
		return isOutsideHall ? "cabinet" : "hall";
	}
	
	@Override
	public boolean hasMotors() { return isOutsideHall ? false : true; }
	
	@Override
	public boolean isRemoteActive() {
		return seqCtrl().isRemoteActive();
	}
}
