package imageProc.control.arduinoComm;

import algorithmrepository.ExtrapolationMode;
import algorithmrepository.Interpolation1D;
import algorithmrepository.InterpolationMode;
import imageProc.core.ImgSource;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** Misc bits of in-hall arduino controller:
 * motors
 * lens
 * oven
 * flc
 *  
 *  
 *  
 *  9/1/17: Definitive motor config for 2017 back-end:
 *    Motor 0: Filter slider: RevSwith, NOT Rev motor
 *     
 */
public class InHallControl {
	//Last reported state of hardware
	private double ovenTemp = Double.NaN;
	
	private boolean flcGateState = true;
	private boolean flcDriveState = false;
	private boolean ovenPowerState = false;
	
	
	private ArduinoCommHanger proc;
	
	private Interpolation1D tempCalibInterp = new Interpolation1D( 
			new double[]{ 500, 564, 629, 657, 720 }, //Analogue signal (0=0v, 1023~5v)
			new double[]{ 24.0, 30, 36, 39, 45.5 }, // temp according to display
			InterpolationMode.LINEAR,
			ExtrapolationMode.LINEAR
			);
	
	public InHallControl(ArduinoCommHanger proc) {
		this.proc = proc;
	}

	public void receivedLine(String line){
		ImgSource src = proc.getConnectedSource();
		
		if(line.startsWith("OVN:RAWTEMP")){
			int rawTemp = Algorithms.mustParseInt(line.substring(11).trim());
			ovenTemp = tempCalibInterp.eval(rawTemp);
			
			if(src != null){
				src.setSeriesMetaData("Oven/RawTemp", rawTemp, false);
				src.setSeriesMetaData("Oven/Temp", ovenTemp, false);
			}
			
		}else if(line.startsWith("FLC:STAT")){
			//FLC:STAT G=#, D=#
			
			flcGateState = (line.charAt(11) == '1');
			flcDriveState = (line.charAt(16) == '1');
			
		}else if(line.startsWith("OVN:POWER")){
			//OVN:POWER #
			ovenPowerState = (Algorithms.mustParseInt(line.substring(9).trim()) != 0);			
		}
	}

	public void setupController() {
		setFLCState(flcGateState, flcDriveState);
		ovenSetPower(ovenPowerState);		
		getOvenTemp();
	}
	
	
	public void setFLCState(boolean gate, boolean drive) {
		proc.serialComm().send("FLC-SET" + (gate ? "1" : "0") + (drive ? "1" : "0"));
		
	}

	public void ovenSetPower(boolean enable) { 
		proc.serialComm().send("OVN-POWR" + (enable ? "1" : "0"));
	}

	public void requestOvenTemp() {
		proc.serialComm().send("OVN-TEMP");
	}
	
	public double getOvenTemp(){ return ovenTemp; }

	public boolean getOvenPowerState() { return ovenPowerState; }
	public boolean getFLCGateState() { return flcGateState; }
	public boolean getFLCDriveState() { return flcDriveState;	}
}
