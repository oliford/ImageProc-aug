package imageProc.control.arduinoComm;

import imageProc.core.ImgSource;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

public class LaserControl {

	private ArduinoCommHanger proc;
	
	private boolean laserPowerState = false;
	private boolean depolPowerState = false;
	private double currentTemp = Double.NaN;;
	private double currentWavelen = Double.NaN;;
	
	private double targetTemp = 20.00;
	private double targetWavelen = 653.5;
	
	private double tempToWavelengthOffset = Algorithms.mustParseDouble(SettingsManager.defaultGlobal().getProperty("imageProc.arduinoComm.laserControl.tempToWavelengthOffset", "648.775"));
	private double tempToWavelengthGradient = Algorithms.mustParseDouble(SettingsManager.defaultGlobal().getProperty("imageProc.arduinoComm.laserControl.tempToWavelengthGradient", "0.1868"));

	private double rawTargetToTempOffset = Algorithms.mustParseDouble(SettingsManager.defaultGlobal().getProperty("imageProc.arduinoComm.laserControl.rawSetToTempOffset", "5.00"));
	private double rawTargetToTempGradient = Algorithms.mustParseDouble(SettingsManager.defaultGlobal().getProperty("imageProc.arduinoComm.laserControl.rawSetToTempGradient", "0.37239"));
	
	private double rawSenseToTempOffset = Algorithms.mustParseDouble(SettingsManager.defaultGlobal().getProperty("imageProc.arduinoComm.laserControl.rawGetToTempOffset", "0.21352"));
	private double rawSenseToTempGradient = Algorithms.mustParseDouble(SettingsManager.defaultGlobal().getProperty("imageProc.arduinoComm.laserControl.rawGetToTempGradient", "0.09837"));
	
	
	public LaserControl(ArduinoCommHanger proc) {
		this.proc = proc;
	}
	
	public LaserControl() {
	}

	public void requestTemp() {
		proc.serialComm().send("LAS-GETT");
	}

	public void setTargetWavelength(double wavelength) {
		targetTemp = (wavelength - tempToWavelengthOffset) / tempToWavelengthGradient;
		setTargetTemp(targetTemp);
	}

	public void setTargetTemp(double temp) {
		int rawTarget = (int)((temp - rawTargetToTempOffset) / rawTargetToTempGradient);
		if(rawTarget < 0) rawTarget = 0;
		if(rawTarget > 255) rawTarget = 255;
		targetTemp = rawTarget * rawTargetToTempGradient + rawTargetToTempOffset;
		targetWavelen = targetTemp * tempToWavelengthGradient + tempToWavelengthOffset; 
		proc.serialComm().send("LAS-SETT" + rawTarget);
		proc.updateAllControllers();
	}

	/** Tries to set the current state according to this module to the Arduino. The return from
	 * the Arduino will cause everything to be written to the metadata*/
	public void syncroniseState(){
		setLaserPower(laserPowerState);
		setDepolPower(depolPowerState);
		setTargetTemp(targetTemp);
		getCurrentTemp();
	}

	public void setLaserPower(boolean state) {
		proc.serialComm().send("LAS-LPWR"+(state?"1":"0"));
	}

	public void setDepolPower(boolean state) {
		proc.serialComm().send("LAS-DPWR"+(state?"1":"0"));
	}

	public double getCurrentTemp() { return currentTemp; }
	public double getCurrentWavelen() { return currentWavelen; }
	public double getTargetTemp() { return targetTemp; }
	public double getTargetWavelen() { return targetWavelen; }

	public boolean getLaserPowerState() { return laserPowerState;	}
	public boolean getDepolPowerState() { return depolPowerState;	}

	public void recievedCommand(String line) {
		ImgSource src = proc.getConnectedSource();
		if(line.startsWith("LAS:LPWR")){
			laserPowerState = (line.charAt(9) == '1');

			if(src != null)
				src.setSeriesMetaData("Laser/LaserPower", laserPowerState, false);
			
		}else if(line.startsWith("LAS:DPWR")){
			depolPowerState = (line.charAt(9) == '1');	

			if(src != null)
				src.setSeriesMetaData("Laser/DepolariserPower", depolPowerState, false);
			
		}else if(line.startsWith("LAS:RAWTEMP")){
			double rawIn = Algorithms.mustParseInt(line.substring(12));
			currentTemp = rawIn * rawSenseToTempGradient + rawSenseToTempOffset;
			currentWavelen = currentTemp * tempToWavelengthGradient + tempToWavelengthOffset;
			
			if(src != null){
				src.setSeriesMetaData("Laser/TempRaw", rawIn, false);
				src.setSeriesMetaData("Laser/Temp", currentTemp, false);
				src.setSeriesMetaData("Laser/Wavelength", currentWavelen, false);
			}
						
		}else if(line.startsWith("LAS:RAWTARG")){
			double rawSet = Algorithms.mustParseInt(line.substring(12));
			targetTemp = rawSet * rawTargetToTempGradient + rawTargetToTempOffset;
			targetWavelen = targetTemp * tempToWavelengthGradient + tempToWavelengthOffset; 
			
			if(src != null){
				src.setSeriesMetaData("Laser/TempTargetRaw", rawSet, false);
				src.setSeriesMetaData("Laser/TempTarget", targetTemp, false);
				src.setSeriesMetaData("Laser/WavelengthTarget", targetWavelen, false);
			}
		}
		
		if(src != null){ //store the cal too
			src.setSeriesMetaData("Laser/Calibration/tempToWavelengthOffset", tempToWavelengthOffset, false);
			src.setSeriesMetaData("Laser/Calibration/tempToWavelengthGradient", tempToWavelengthGradient, false);
			src.setSeriesMetaData("Laser/Calibration/rawTargetToTempOffset", rawTargetToTempOffset, false);
			src.setSeriesMetaData("Laser/Calibration/rawTargetToTempGradient", rawTargetToTempGradient, false);
			src.setSeriesMetaData("Laser/Calibration/rawSenseToTempOffset", rawSenseToTempOffset, false);
			src.setSeriesMetaData("Laser/Calibration/rawSenseToTempGradient", rawSenseToTempGradient, false);		
		}
		proc.updateAllControllers();
	}
	

	public double getTempToWavelenOffset() { return tempToWavelengthOffset; }
	public double getTempToWavelenGradient() { return tempToWavelengthGradient; }

	public void setTempToWavelenRelation(double offset, double gradient) {
		this.tempToWavelengthOffset = offset;
		this.tempToWavelengthGradient = gradient;
		proc.updateAllControllers();
	}
	
	public double getRawTargetToTempOffset() { return rawTargetToTempOffset; }
	public double getRawTargetToTempGradient() { return rawTargetToTempGradient; }

	public void setRawTargetToTempRelation(double offset, double gradient) {
		this.rawTargetToTempOffset = offset;
		this.rawTargetToTempGradient = gradient;
		proc.updateAllControllers();
	}

	
	public double getRawSenseToTempOffset() { return rawSenseToTempOffset; }
	public double getRawSenseToTempGradient() { return rawSenseToTempGradient; }

	public void setRawSenseToTempRelation(double offset, double gradient) {
		this.rawSenseToTempOffset = offset;
		this.rawSenseToTempGradient = gradient;
		proc.updateAllControllers();
	}

	
	public void setTargetTempOffset(double offset){
		this.rawTargetToTempOffset = offset;
		setTargetWavelength(targetWavelen);
		proc.updateAllControllers();
	}

	
	// SequenceOutside Stuff:
	public void sqoStart(SeqOutConfig sqoConfig){
		
		double initTemp = (sqoConfig.initWavelength - tempToWavelengthOffset) / tempToWavelengthGradient;
		sqoConfig.targetRawInit = (int)((initTemp - rawTargetToTempOffset) / rawTargetToTempGradient);
		
		double tempDelta = (sqoConfig.wavelengthDelta) / tempToWavelengthGradient;
		sqoConfig.targetRawDelta = (int)(tempDelta / rawTargetToTempGradient);
		
		
		proc.serialComm().send("SQO-INIT" +
							sqoConfig.nFrames + "," +
							sqoConfig.framesAtwavelength + "," +
							sqoConfig.laserOffFrameDelta + "," +
							sqoConfig.targetRawInit + "," +
							sqoConfig.targetRawDelta + "," +
							sqoConfig.lineTrigger + "," +
							sqoConfig.selfAdvanceUS + "," + 
							(sqoConfig.daqOnAdvance ? 1 : 0));
	}
	
	public void sqoAbort() {
		proc.serialComm().send("SQO-STOP");
	}
}
