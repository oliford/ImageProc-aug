package imageProc.control.arduinoComm;

import java.util.HashMap;
import java.util.Map;

/** Terminology is a bit confusing here.
 * 
 * Under the sequencer, a 'frame' is one set of exposures at single motor position.
 * It's usually 1 exposure per 'frame', but 2 if the FLC interlace is on.
 * 
 * @author oliford
 */
public class SeqOutConfig {
	
	public static final int EXPOSURE_PULSE_LENGTH_NS = 100000; //100µs	
	public static final int FAST_ADVANCE_PULSE_LENGTH_NS = 100000; //100µs
	
	public static final int ADVANCE_NONE = 0;
	public static final int ADVANCE_LINE = 1;
	public static final int ADVANCE_EXPOSE_RISING = 2;
	public static final int ADVANCE_EXPOSE_FALLING = 3;
	public static final int ADVANCE_TIMED = 4;

	public int nFrames;	
	
	/** Initial wavelength for frame 0 */
	public double initWavelength;	
	public int targetRawInit; //raw version
	
	/** Hold temp for n frames */
	public int framesAtwavelength;
	
	/** Increase temp this much after every tempFramesAtTemp frames*/
	public double wavelengthDelta;
	public int targetRawDelta; //raw version
	
	/** Turn laser off every n frames, (and don't advance temperature) */
	public int laserOffFrameDelta;
	
	public int lineTrigger;
	public long selfAdvanceUS; //for ADVANCE_TIMED

	public boolean daqOnAdvance;
	
	
	public Map<String, Object> toMap(String prefix) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put(prefix + "/nFrames", nFrames);
		map.put(prefix + "/initWavelength", initWavelength);
		map.put(prefix + "/targetRawInit", targetRawInit);
		map.put(prefix + "/framesAtwavelength", framesAtwavelength);
		map.put(prefix + "/wavelengthDelta", wavelengthDelta);
		map.put(prefix + "/targetRawDelta", targetRawDelta);
		map.put(prefix + "/laserOffFrameDelta", laserOffFrameDelta);
		map.put(prefix + "/lineTrigger", lineTrigger);
		map.put(prefix + "/selfAdvanceUS", selfAdvanceUS);
		map.put(prefix + "/daqOnAdvance", daqOnAdvance);
		
		return map;
	}

}
