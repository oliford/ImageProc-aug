package imageProc.control.arduinoComm.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import imageProc.control.arduinoComm.ArduinoCommHanger;
import imageProc.control.arduinoComm.ArduinoCommHangerAUG;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.ImagePipeSWTController;
import imageProc.core.ImgSourceOrSinkImpl;

public class ArduinoCommSWTControlAUG extends ArduinoCommSWTControl {
	
	private ArduinoCommHangerAUG procAUG;
	
	private CTabItem swtStaticsTab;
	private CTabItem swtCanonEFTab;
	private CTabItem swtDataAqTab;
	private CTabItem swtLaserCtrlTab;
		
	private StaticsSWTGroup staticsGroup;
	private CanonEFSWTGroup canonEFGroup;
	private LaserSWTGroup laserCtrlGroup;
	
	public ArduinoCommSWTControlAUG(ArduinoCommHangerAUG procAUG, Composite parent, int style) {
			super(procAUG, parent, style);
			this.procAUG = procAUG;
			
			staticsGroup = new StaticsSWTGroup(swtTabFoler, procAUG);

			canonEFGroup = new CanonEFSWTGroup(swtTabFoler, procAUG);
	        
			laserCtrlGroup = new LaserSWTGroup(swtTabFoler, procAUG);
			
			setupTabs();
			
			generalControllerUpdate();			
	}
	
	protected void doUpdate(){
		super.doUpdate();
				
		staticsGroup.doUpdate();
		laserCtrlGroup.doUpdate();
	}
	
	@Override
	protected void setupTabs() {
		super.setupTabs();
		
		if(procAUG == null) //during construction
			return;
		
		if(procAUG.isOutsideHall()){			
			setupAUGTabs(false, true);	
			motorGroups=null;
		}else{
			setupAUGTabs(true, false);
		}
	}
	
	private  void setupAUGTabs(boolean hallTabs, boolean cabinetTabs){
				
		if(swtStaticsTab == null && hallTabs){
			swtStaticsTab = new CTabItem(swtTabFoler, SWT.NONE);
			swtStaticsTab.setControl(staticsGroup.getSWTGroup());
			swtStaticsTab.setText("Statics");
		}else if(swtStaticsTab != null && !hallTabs){
			swtStaticsTab.dispose();
			swtStaticsTab = null;
		}
		
		if(swtCanonEFTab == null && hallTabs){
			swtCanonEFTab = new CTabItem(swtTabFoler, SWT.NONE);
			swtCanonEFTab.setControl(canonEFGroup.getSWTGroup());
			swtCanonEFTab.setText("Lens");
		}else if(swtCanonEFTab != null && !hallTabs){
			swtCanonEFTab.dispose();
			swtCanonEFTab = null;
		}
		
		if(swtLaserCtrlTab == null && cabinetTabs){
			swtLaserCtrlTab = new CTabItem(swtTabFoler, SWT.NONE);
			swtLaserCtrlTab.setControl(laserCtrlGroup.getSWTGroup());
			swtLaserCtrlTab.setText("Laser");
		}else if(swtLaserCtrlTab != null && !cabinetTabs){
			swtLaserCtrlTab.dispose();
			swtLaserCtrlTab = null;
		}
		
	}
	
}
