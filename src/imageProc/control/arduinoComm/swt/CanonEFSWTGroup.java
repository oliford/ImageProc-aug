package imageProc.control.arduinoComm.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import imageProc.control.arduinoComm.ArduinoCommHanger;
import imageProc.control.arduinoComm.ArduinoCommHangerAUG;

public class CanonEFSWTGroup {
	private ArduinoCommHangerAUG proc;
	
	private Group swtGroup;
	private Button initButton;
	private Button closeButton;
	private Spinner focusSpinner;
	private Button focusSetButton;
	private Button focusFarButton;
	private Button focusStepButton;
	private Button focusNearButton;
	private Spinner apertureSpinner;
	private Button apertureSetButton;
	private Button aperture0Button;
	private Button aperture40Button;
	private Button aperture80Button;
	
	private boolean inhibitListeners = false;
	
	public CanonEFSWTGroup(Composite parent, ArduinoCommHangerAUG proc) {
		this.proc = proc;
		
		swtGroup = new Group(parent, SWT.BORDER);
		swtGroup.setText("Canon EF");
		swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		swtGroup.setLayout(new GridLayout(6, false));

		initButton = new Button(swtGroup, SWT.PUSH);
		initButton.setText("Init");
		initButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		initButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { initButtonEvent(event); } });

		closeButton = new Button(swtGroup, SWT.PUSH);
		closeButton.setText("Close");
		closeButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 5, 1));
		closeButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { closeButtonEvent(event); } });

		Label lF = new Label(swtGroup, SWT.NONE); lF.setText("Focus:");
		
		focusSpinner = new Spinner(swtGroup, SWT.NONE);
		focusSpinner.setValues(0, Integer.MIN_VALUE, Integer.MAX_VALUE, 0, 10, 100);
		focusSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		
		focusSetButton = new Button(swtGroup, SWT.PUSH);
		focusSetButton.setText("Set");
		focusSetButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		focusSetButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { focusMoveButton(event); } });
	
		focusFarButton = new Button(swtGroup, SWT.PUSH);
		focusFarButton.setText("Far");
		focusFarButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		focusFarButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setFocus(-10000); } });
	
		focusNearButton = new Button(swtGroup, SWT.PUSH);
		focusNearButton.setText("Near");
		focusNearButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		focusNearButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setFocus(10000); } });
	
		focusStepButton = new Button(swtGroup, SWT.PUSH);
		focusStepButton.setText("Step 100");
		focusStepButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		focusStepButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setFocus(100); } });
	
		
		Label lA = new Label(swtGroup, SWT.NONE); lA.setText("Aperture:");
		apertureSpinner = new Spinner(swtGroup, SWT.NONE);
		apertureSpinner.setValues(0, 0, Integer.MAX_VALUE, 0, 10, 100);
		apertureSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		
		apertureSetButton = new Button(swtGroup, SWT.PUSH);
		apertureSetButton.setText("Set");
		apertureSetButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		apertureSetButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { apertureSetButton(event); } });
		
		aperture0Button = new Button(swtGroup, SWT.PUSH);
		aperture0Button.setText("Open");
		aperture0Button.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		aperture0Button.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setAperture(0); } });
		
		aperture40Button = new Button(swtGroup, SWT.PUSH);
		aperture40Button.setText("Mid");
		aperture40Button.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		aperture40Button.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setAperture(40); } });
		
		aperture80Button = new Button(swtGroup, SWT.PUSH);
		aperture80Button.setText("Closed");
		aperture80Button.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		aperture80Button.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setAperture(80); } });
		
	}

	private void initButtonEvent(Event event) {
		proc.lensInit();
	}

	private void closeButtonEvent(Event event) {
		proc.lensClose();
	}
	
	private void focusMoveButton(Event event) {
		proc.moveFocus(focusSpinner.getSelection());
	}
	
	private void apertureSetButton(Event event) {
		proc.setAperture(apertureSpinner.getSelection());
	}
	
	private void setFocus(int value) {
		proc.moveFocus(value);
	}

	private void setAperture(int value){
		proc.setAperture(value);
	}
/*
 * 
	if(strncmp(cmd, , 8) == 0){
		initSequence();

	}else if(strncmp(cmd, "LNS-CMD", 7) == 0){
		directCommand(cmd+7);

	}else if(strncmp(cmd, "LNS-APER", 8) == 0){
		setAperture(atoi(cmd + 8));

	}else if(strncmp(cmd, , 10) == 0){
		setFocus(atoi(cmd + 10));

	}else if(strncmp(cmd, "LNS-MFOCUS", 10) == 0){
		moveFocus(atoi(cmd + 10));

	}else if(strncmp(cmd, "LNS-GFOCUS", 10) == 0){
		long focus = getFocus();
		Serial.print("LNS:FOCUS ");
		Serial.println(focus, DEC);
	}
 * 
 */
	public void doUpdate() {
		inhibitListeners = true;
		
		// ??
		
		inhibitListeners = false;		
	}
	
	public Group getSWTGroup(){ return swtGroup; }
}
