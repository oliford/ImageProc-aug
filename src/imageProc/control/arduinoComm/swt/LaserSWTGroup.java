package imageProc.control.arduinoComm.swt;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import imageProc.core.ImageProcUtil;

import imageProc.control.arduinoComm.ArduinoCommHanger;
import imageProc.control.arduinoComm.ArduinoCommHangerAUG;
import imageProc.control.arduinoComm.SeqOutConfig;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class LaserSWTGroup {
	
	private ArduinoCommHangerAUG proc;
	
	private Group swtGroup;
	private Button laserPowerCheckbox;
	private Button despecklerPowerCheckbox;
	private Spinner targetTempSpinner;
	private Spinner targetWavelenSpinner;
	private Button reqTempButton;
	private Label tempLabel;
	
	private Text tempWavelenOffsetTextbox;
	private Text tempWavelenGradientTextbox;
	private Text rawTargetToTempOffsetTextbox;
	private Text rawTargetToTempGradientTextbox;
	private Text rawSenseToTempOffsetTextbox;
	private Text rawSenseToTempGradientTextbox;

	private Spinner targetTempOffsetSpinner;
	
	private boolean inhibitListeners = false;
	
	private Group swtSeqOutGroup;
	private Label sqoStatusLabel;
	private Spinner sqoNFramesSpinner;
	private Spinner sqoNFramesAtWavelengthSpinner;
	private Spinner sqoWavelengthDeltaSpinner;
	private Spinner sqoLaserOffFrameDeltaSpinner;
	private Button sqoStartButton;
	private Button sqoAbortButton;

	private Combo sqoAdvanceTriggerCombo;
	private Button sqoDAQOnAdvanceCheckbox;
	
	public static final DecimalFormat tempFormat = new DecimalFormat("##.#");
	static{ //somewhat tedious...
		DecimalFormatSymbols dfs = tempFormat.getDecimalFormatSymbols();
		dfs.setNaN("?");
		tempFormat.setDecimalFormatSymbols(dfs);
	}
	
	
	public LaserSWTGroup(Composite parent, ArduinoCommHangerAUG proc) {
		this.proc = proc;
		
		
		swtGroup = new Group(parent, SWT.BORDER);
		swtGroup.setText("Laser Control");
		swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		swtGroup.setLayout(new GridLayout(5, false));

		laserPowerCheckbox = new Button(swtGroup, SWT.CHECK);
		laserPowerCheckbox.setText("Laser Power");
		laserPowerCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		laserPowerCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { powerButtonsEvent(event); } });
		
		despecklerPowerCheckbox = new Button(swtGroup, SWT.CHECK);
		despecklerPowerCheckbox.setText("Despeckler Power");
		despecklerPowerCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		despecklerPowerCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { powerButtonsEvent(event); } });
		
		tempLabel = new Label(swtGroup, SWT.NONE);
		tempLabel.setText("Laser Temp = ??.?°C");
		tempLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		reqTempButton = new Button(swtGroup, SWT.PUSH);
		reqTempButton.setText("Update");
		reqTempButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
		reqTempButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { getTempButtonEvent(event); } });
		
		Label lTT = new Label(swtGroup, SWT.NONE); lTT.setText("Target: Temp:");
		targetTempSpinner = new Spinner(swtGroup, SWT.NONE);
		targetTempSpinner.setValues(0, -1000, 10000, 2, 10, 100);
		targetTempSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		targetTempSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setTempSpinnerEvent(event); } });
		
		Label lTW = new Label(swtGroup, SWT.NONE); lTW.setText("Wavelength:");
		targetWavelenSpinner = new Spinner(swtGroup, SWT.NONE);
		targetWavelenSpinner.setValues(0, 64000, 65800, 2, 10, 1000);
		targetWavelenSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		targetWavelenSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setWavelenSpinnerEvent(event); } });
		
	
		
		Label lCA = new Label(swtGroup, SWT.NONE); lCA.setText("Wavelenth/Temp: Offset (nm):");
		tempWavelenOffsetTextbox = new Text(swtGroup, SWT.NONE);
		tempWavelenOffsetTextbox.setText(Double.toString(proc.getLaserCtrl().getTempToWavelenOffset()));
		tempWavelenOffsetTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		tempWavelenOffsetTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { setRelationsEvent(event); } });
		
		Label lCB = new Label(swtGroup, SWT.NONE); lCB.setText("Gradient (nm/'C):");
		tempWavelenGradientTextbox = new Text(swtGroup, SWT.NONE);
		tempWavelenGradientTextbox.setText(Double.toString(proc.getLaserCtrl().getTempToWavelenGradient()));
		tempWavelenGradientTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		tempWavelenGradientTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { setRelationsEvent(event); } });
		
		Label lCC = new Label(swtGroup, SWT.NONE); lCC.setText("Target Temp/Raw: Offset ('C):");
		rawTargetToTempOffsetTextbox = new Text(swtGroup, SWT.NONE);
		rawTargetToTempOffsetTextbox.setText(Double.toString(proc.getLaserCtrl().getRawTargetToTempOffset()));
		rawTargetToTempOffsetTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		rawTargetToTempOffsetTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { setRelationsEvent(event); } });
		
		Label lCD = new Label(swtGroup, SWT.NONE); lCD.setText("Gradient ('C/unit):");
		rawTargetToTempGradientTextbox = new Text(swtGroup, SWT.NONE);
		rawTargetToTempGradientTextbox.setText(Double.toString(proc.getLaserCtrl().getRawTargetToTempGradient()));
		rawTargetToTempGradientTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		rawTargetToTempGradientTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { setRelationsEvent(event); } });
	
		Label lCE = new Label(swtGroup, SWT.NONE); lCE.setText("Sense Temp/Raw: Offset ('C):");
		rawSenseToTempOffsetTextbox = new Text(swtGroup, SWT.NONE);
		rawSenseToTempOffsetTextbox.setText(Double.toString(proc.getLaserCtrl().getRawSenseToTempOffset()));
		rawSenseToTempOffsetTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		rawSenseToTempOffsetTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { setRelationsEvent(event); } });
		
		Label lCF = new Label(swtGroup, SWT.NONE); lCF.setText("Gradient ('C/unit):");
		rawSenseToTempGradientTextbox = new Text(swtGroup, SWT.NONE);
		rawSenseToTempGradientTextbox.setText(Double.toString(proc.getLaserCtrl().getRawSenseToTempGradient()));
		rawSenseToTempGradientTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		rawSenseToTempGradientTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { setRelationsEvent(event); } });
	
			
		
		swtSeqOutGroup = new Group(swtGroup, SWT.BORDER);
		swtSeqOutGroup.setText("Sequence");
		swtSeqOutGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		swtSeqOutGroup.setLayout(new GridLayout(5, false));

		sqoStatusLabel = new Label(swtSeqOutGroup, SWT.NONE);
		sqoStatusLabel.setText("init");
		sqoStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));

		Label lNF = new Label(swtSeqOutGroup, SWT.NONE); lNF.setText("Num. Frames:");
		sqoNFramesSpinner = new Spinner(swtSeqOutGroup, SWT.NONE);
		sqoNFramesSpinner.setValues(0, 1, Integer.MAX_VALUE, 0, 10, 100);
		sqoNFramesSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));

		Label lHW = new Label(swtSeqOutGroup, SWT.NONE); lHW.setText("Hold wavelength for n frames:");
		sqoNFramesAtWavelengthSpinner = new Spinner(swtSeqOutGroup, SWT.NONE);
		sqoNFramesAtWavelengthSpinner.setValues(0, 1, Integer.MAX_VALUE, 0, 10, 100);
		sqoNFramesAtWavelengthSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));

		Label lWD = new Label(swtSeqOutGroup, SWT.NONE); lWD.setText("Wavelength Delta:");
		sqoWavelengthDeltaSpinner = new Spinner(swtSeqOutGroup, SWT.NONE);
		sqoWavelengthDeltaSpinner.setValues(0, -10000, 10000, 2, 1, 10);
		sqoWavelengthDeltaSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));

		Label lLO = new Label(swtSeqOutGroup, SWT.NONE); lLO.setText("Blank (Laser off) frame every:");
		sqoLaserOffFrameDeltaSpinner = new Spinner(swtSeqOutGroup, SWT.NONE);
		sqoLaserOffFrameDeltaSpinner.setValues(0, -1, Integer.MAX_VALUE, 0, 1, 10);
		sqoLaserOffFrameDeltaSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		

		Label lLT = new Label(swtSeqOutGroup, SWT.NONE); lLT.setText("Advance:");
		sqoAdvanceTriggerCombo = new Combo(swtSeqOutGroup, SWT.MULTI | SWT.DROP_DOWN | SWT.READ_ONLY);
		sqoAdvanceTriggerCombo.setItems(new String[]{ "None", "Fast Advance (obsolete)", "Exposure Start", "Exposure End", "Self Timed" });
		sqoAdvanceTriggerCombo.select(3);
		sqoAdvanceTriggerCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));

		sqoDAQOnAdvanceCheckbox = new Button(swtSeqOutGroup, SWT.CHECK);
		sqoDAQOnAdvanceCheckbox.setText("Arduino DAQ on advance");
		sqoDAQOnAdvanceCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
				
		sqoStartButton = new Button(swtSeqOutGroup, SWT.PUSH);
		sqoStartButton.setText("Start");
		sqoStartButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		sqoStartButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sqoStartButtonEvent(event); } });
		
		sqoAbortButton = new Button(swtSeqOutGroup, SWT.PUSH);
		sqoAbortButton.setText("Abort");
		sqoAbortButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		sqoAbortButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sqoAbortButtonEvent(event);	} });
		
		ImageProcUtil.addRevealWriggler(swtGroup);
		
	}
	
	private void sqoAbortButtonEvent(Event event) {
		proc.getLaserCtrl().sqoAbort();
	}

	private void sqoStartButtonEvent(Event event){
		SeqOutConfig sqoConfig = new SeqOutConfig();
		sqoConfig.nFrames = sqoNFramesSpinner.getSelection();
		sqoConfig.initWavelength = (double)targetWavelenSpinner.getSelection() / 100;
		sqoConfig.laserOffFrameDelta = sqoLaserOffFrameDeltaSpinner.getSelection();
		sqoConfig.framesAtwavelength = sqoNFramesAtWavelengthSpinner.getSelection();
		sqoConfig.wavelengthDelta = (double)sqoWavelengthDeltaSpinner.getSelection() / 100;
		sqoConfig.lineTrigger = sqoAdvanceTriggerCombo.getSelectionIndex();
		sqoConfig.daqOnAdvance = sqoDAQOnAdvanceCheckbox.getSelection();
		proc.getLaserCtrl().sqoStart(sqoConfig);
	}

	
	private void setRelationsEvent(Event event){
		proc.getLaserCtrl().setTempToWavelenRelation(
				Algorithms.mustParseDouble(tempWavelenOffsetTextbox.getText()),
				Algorithms.mustParseDouble(tempWavelenGradientTextbox.getText()));

		proc.getLaserCtrl().setRawTargetToTempRelation(
				Algorithms.mustParseDouble(rawTargetToTempOffsetTextbox.getText()),
				Algorithms.mustParseDouble(rawTargetToTempGradientTextbox.getText()));

		proc.getLaserCtrl().setRawSenseToTempRelation(
				Algorithms.mustParseDouble(rawSenseToTempOffsetTextbox.getText()),
				Algorithms.mustParseDouble(rawSenseToTempGradientTextbox.getText()));
		
	}

	private void powerButtonsEvent(Event event){
		if(!inhibitListeners){
			if(!proc.getLaserCtrl().getLaserPowerState() && laserPowerCheckbox.getSelection()){
				//laser on - both go on
				proc.getLaserCtrl().setLaserPower(true);
				proc.getLaserCtrl().setDepolPower(true);
			}else if(proc.getLaserCtrl().getLaserPowerState() && !laserPowerCheckbox.getSelection()){
				//laser of - both go off
				proc.getLaserCtrl().setLaserPower(false);
				proc.getLaserCtrl().setDepolPower(false);
				
			}else{ //otherwise setting depolariser by itself
				proc.getLaserCtrl().setDepolPower(despecklerPowerCheckbox.getSelection());
			}
		}
	}
	
	

	private void getTempButtonEvent(Event event) {
		if(!inhibitListeners)
			proc.getLaserCtrl().requestTemp();
	}
	
	private void setTempSpinnerEvent(Event event) {
		if(!inhibitListeners)
			proc.getLaserCtrl().setTargetTemp((double)targetTempSpinner.getSelection() / 100);
	}
	
	private void setWavelenSpinnerEvent(Event event) {
		if(!inhibitListeners)
			proc.getLaserCtrl().setTargetWavelength((double)targetWavelenSpinner.getSelection() / 100);
	}
	
	public void doUpdate() {
				
		inhibitListeners = true;
		tempLabel.setText("Laser Temp = "+tempFormat.format(proc.getLaserCtrl().getCurrentTemp())
							+"°C, Wavelenght = " + tempFormat.format(proc.getLaserCtrl().getCurrentWavelen()));
		laserPowerCheckbox.setSelection(proc.getLaserCtrl().getLaserPowerState());
		despecklerPowerCheckbox.setSelection(proc.getLaserCtrl().getDepolPowerState());
		
		if(!targetTempSpinner.isFocusControl())laserPowerCheckbox.setSelection(proc.getLaserCtrl().getLaserPowerState());
			targetTempSpinner.setSelection((int)(proc.getLaserCtrl().getTargetTemp() * 100));
		
		if(!targetWavelenSpinner.isFocusControl())
			targetWavelenSpinner.setSelection((int)(proc.getLaserCtrl().getTargetWavelen() * 100));
		
		inhibitListeners = false;		
	}
	
	public Group getSWTGroup(){ return swtGroup; }
}
