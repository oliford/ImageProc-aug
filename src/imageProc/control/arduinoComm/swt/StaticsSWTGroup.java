package imageProc.control.arduinoComm.swt;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

import imageProc.control.arduinoComm.ArduinoCommHanger;
import imageProc.control.arduinoComm.ArduinoCommHangerAUG;
import imageProc.control.arduinoComm.InHallControl;

public class StaticsSWTGroup {
	private ArduinoCommHangerAUG proc;
	
	private Group swtGroup;
	private Button flcGateCheckbox;
	private Button flcDriveCheckbox;
	private Button ovenPowerCheckbox;
	private Button reqOvenTempButton;
	private Label ovenTempLabel;
	
	private boolean inhibitListeners = false;

	private InHallControl ctrl;
	
	public StaticsSWTGroup(Composite parent, ArduinoCommHangerAUG proc) {
		this.proc = proc;
		this.ctrl = proc.getInHallCtrl();
		
		swtGroup = new Group(parent, SWT.BORDER);
		swtGroup.setText("FLC/Oven");
		swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		swtGroup.setLayout(new GridLayout(5, false));

		ovenTempLabel = new Label(swtGroup, SWT.NONE);
		ovenTempLabel.setText("Oven Temp = ??.?°C");
		ovenTempLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		reqOvenTempButton = new Button(swtGroup, SWT.PUSH);
		reqOvenTempButton.setText("Update");
		reqOvenTempButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		reqOvenTempButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { ovenReqTempButtonEvent(event); } });
		
		ovenPowerCheckbox = new Button(swtGroup, SWT.CHECK);
		ovenPowerCheckbox.setText("Oven Power");
		ovenPowerCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		ovenPowerCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { ovenPowerButtonEvent(event); } });
		
		flcGateCheckbox = new Button(swtGroup, SWT.CHECK);
		flcGateCheckbox.setText("FLC Gate");
		flcGateCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		flcGateCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { flcButtonEvent(event); } });
		
		flcDriveCheckbox = new Button(swtGroup, SWT.CHECK);
		flcDriveCheckbox.setText("FLC Drive");
		flcDriveCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		flcDriveCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { flcButtonEvent(event); } });
		
	}
	

	private void flcButtonEvent(Event event) {
		if(!inhibitListeners)
			ctrl.setFLCState(flcGateCheckbox.getSelection(), flcDriveCheckbox.getSelection());
	}
	
	private void ovenPowerButtonEvent(Event event) {
		if(!inhibitListeners)
			ctrl.ovenSetPower(ovenPowerCheckbox.getSelection());
	}
	
	private void ovenReqTempButtonEvent(Event event) {
		ctrl.requestOvenTemp();
	}
	
	
	public static final DecimalFormat tempFormat = new DecimalFormat("##.#");
	static{ //somewhat tedious...
		DecimalFormatSymbols dfs = tempFormat.getDecimalFormatSymbols();
		dfs.setNaN("?");
		tempFormat.setDecimalFormatSymbols(dfs);
	}
	
	public void doUpdate() {
		inhibitListeners = true;
		ovenTempLabel.setText("Oven Temp = "+tempFormat.format(ctrl.getOvenTemp())+"°C");
		ovenPowerCheckbox.setSelection(ctrl.getOvenPowerState());
		flcGateCheckbox.setSelection(ctrl.getFLCGateState());
		flcDriveCheckbox.setSelection(ctrl.getFLCDriveState());		
		inhibitListeners = false;		
	}
	
	public Group getSWTGroup(){ return swtGroup; }
}
