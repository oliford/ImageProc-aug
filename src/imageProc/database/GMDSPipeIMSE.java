package imageProc.database;

import mds.MdsPlusException;
import signals.Signal;
import signals.gmds.GMDSSignal;
import descriptors.gmds.GMDSSignalDesc;
import imageProc.core.ImgSource;
import imageProc.database.gmds.GMDSPipe;
import imageProc.database.gmds.GMDSSource;
import imageProc.pilot.aug.AugPilot;

/** Extentions to GMDSPipe. It would be nice to extend it, but GMDSSource and GMDSSink are extended from it already */
public class GMDSPipeIMSE  {
	private GMDSPipe pipe;
	
	public final static String augShotPath = "RAW/seriesData/aug/pulse";
	public final static String augIsCalibPath = "RAW/seriesData/aug/isCalib";
	public final static String flagsPath = "RAW/seriesData/flags";
	
	public GMDSPipeIMSE(GMDSPipe pipe) { 
		this.pipe = pipe;
	}
	
	public int[] getFlags(String exp, int gmdsPulse) {
		GMDSSignalDesc flagNamesDesc = new GMDSSignalDesc(gmdsPulse, exp, flagsPath + "/names");
		GMDSSignalDesc flagValsDesc = new GMDSSignalDesc(gmdsPulse, exp, flagsPath + "/vals");
		int flagVals[] = new int[AugPilot.flagNames.length];
		
		try{
			Signal flagsNamesSig = pipe.getFetcher().getSig(flagNamesDesc);
			Signal flagsValsSig = pipe.getFetcher().getSig(flagValsDesc);
			String flagNamesIn[] = ((String[])flagsNamesSig.getData());
			int flagValsIn[] = ((int[])flagsValsSig.getData());
			
			for(int i=0; i < AugPilot.flagNames.length; i++){
				for(int j=0; j < flagNamesIn.length; j++){
					if(AugPilot.flagNames[i].equals(flagNamesIn[j])){
						flagVals[i] = flagValsIn[j];
					}
				}
			}
			
		}catch(RuntimeException err){
			System.err.println("Error reading shot flags from '"+flagNamesDesc+"','"+flagValsDesc+"': " + err.toString());
		}
		
		return flagVals;
	}
	
	public void setFlags(String exp, int gmdsPulse, int flags[]){
		for(int i=0; i < AugPilot.flagNames.length; i++){
			
			//write to the gmds entry first
			GMDSSignalDesc flagNamesDesc = new GMDSSignalDesc(gmdsPulse, exp, flagsPath + "/names");
			GMDSSignalDesc flagValsDesc = new GMDSSignalDesc(gmdsPulse, exp, flagsPath + "/vals");
			try{
				pipe.getFetcher().writeToCache(new GMDSSignal(flagNamesDesc, AugPilot.flagNames));			
				pipe.getFetcher().writeToCache(new GMDSSignal(flagValsDesc, flags));
				
			}catch(MdsPlusException err){
				System.err.println("Error writing shot flags to '"+flagNamesDesc+"','"+flagValsDesc+"': " + err.toString());
			}
			
			//if that's this entry, we'll assume the user is trying to change the currently loaded metadata
			//from whatever source that comes
			if(gmdsPulse == pipe.getPulse() && exp.equals(pipe.getExperiment())){
				ImgSource src = (ImgSource)((pipe instanceof GMDSSource) ? this : pipe.getConnectedSource());
				src.setSeriesMetaData("flags/names", AugPilot.flagNames.clone(), false);
				src.setSeriesMetaData("flags/vals", flags, false);
				
			}
		}
	}
	
	public int getAUGShot(String exp, int gmdsPulse) {
		GMDSSignalDesc augPulseDesc = new GMDSSignalDesc(gmdsPulse, exp, augShotPath);
		try{
			Signal augPulseSig = pipe.getFetcher().getSig(augPulseDesc);
			Integer augPulse = ((int[])augPulseSig.getData())[0];
			return augPulse;
			
		}catch(RuntimeException err){
			System.err.println("Error reading aug pulse from '"+augPulseDesc+"': " + err.toString());
		}
		
		return -1;
	}
	
	public boolean isAugShotCalib(String exp, int gmdsPulse) {
		GMDSSignalDesc desc = new GMDSSignalDesc(gmdsPulse, exp, augIsCalibPath);
		try{
			Signal sig = pipe.getFetcher().getSig(desc);
			Integer isCalib = ((int[])sig.getData())[0];
			return (isCalib != 0);
			
		}catch(RuntimeException err){
			System.err.println("Error reading aug pulse from '"+desc+"': " + err.toString());
		}
		
		return false;
	}
	
	public void setAUGShot(String exp, int gmdsPulse, int augShot, boolean calibration){
		//write to the gmds entry first
		GMDSSignalDesc desc = new GMDSSignalDesc(gmdsPulse, exp, augShotPath);
		try{
			GMDSSignal sig = new GMDSSignal(desc, new int[]{ augShot });			
			pipe.getFetcher().writeToCache(sig);

		}catch(MdsPlusException err){
			System.err.println("Error writing aug pulse to '"+desc+"': " + err.toString());			
		}
		
		desc = new GMDSSignalDesc(gmdsPulse, exp, augIsCalibPath);
		try{
			GMDSSignal sig = new GMDSSignal(desc, new int[]{ calibration ? 1 : 0 });			
			pipe.getFetcher().writeToCache(sig);
	
		}catch(MdsPlusException err){
			System.err.println("Error writing isCalib state to '"+desc+"': " + err.toString());			
		}
		
		//if that's this entry, we'll assume the user is trying to change the currently loaded metadata
		//from whatever source that comes
		if(gmdsPulse == pipe.getPulse() && exp.equals(pipe.getExperiment())){
			ImgSource src = (ImgSource)((pipe instanceof GMDSSource) ? this : pipe.getConnectedSource());
			src.setSeriesMetaData("aug/pulse", augShot, false);			
		}
	}
	
	/** Find the next calibration series shot ID */
	public int getNextCalibSeriesShot(String exp, int startID){
		//arg, this is now very hacky
		//String cacheRoot = gmds.getCacheRoot() + "/gmds";
				
		for(int gmdsID=startID; gmdsID >= 0; gmdsID--){
			
			if(isAugShotCalib(exp, gmdsID)){
				return getAUGShot(exp, gmdsID) + 1;
			}
				
		}
		return -1;			
	}
		
}
