package imageProc.database;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImageProcUtil;
import imageProc.database.gmds.GMDSPipe;
import imageProc.database.gmds.GMDSPipeSWTControl;
import imageProc.pilot.aug.AugPilot;

public class GMDSPipeSWTControlIMSE extends GMDSPipeSWTControl {
	private Button swtSetAugShotButton;
	private Button swtIsCalShotCheckbox;
	private Button swtNextCalShotButton;
	private Spinner swtAUGShotSpinner;
	private Text journalTextBox;
	protected Button swtJournalCheckbox;	
	protected Button swtFlagsCheckboxes[];
	
	private GMDSPipeIMSE pipeExt;
	
	public GMDSPipeSWTControlIMSE(Composite parent, Integer style, GMDSPipe pipe, GMDSPipeIMSE pipeExt) {
		super();
		this.pipeExt = pipeExt;
		init(parent, style, pipe);		
	}
	
	@Override
	protected void addExtensionsSWT1() {
		swtJournalCheckbox = new Button(swtGroup, SWT.CHECK);
		swtJournalCheckbox.setText("Show Journal");
		swtJournalCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { journalCheckboxEvent(); } });
		swtJournalCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		Group swtAugGroup = new Group(swtGroup, SWT.BORDER);
		swtAugGroup.setText("AUG Shot numbering");
		swtAugGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		swtAugGroup.setLayout(new GridLayout(5, false));		
		
		Label ASN = new Label(swtAugGroup, SWT.NONE); ASN.setText("AUG Shot Number:"); 
		ASN.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		swtAUGShotSpinner = new Spinner(swtAugGroup, SWT.NONE);
		swtAUGShotSpinner.setValues(-1, -1, Integer.MAX_VALUE, 0, 1, 10);
		swtAUGShotSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		swtIsCalShotCheckbox = new Button(swtAugGroup, SWT.CHECK);
		swtIsCalShotCheckbox.setText("Cal Series");
		swtIsCalShotCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setAugShotButtonEvent(event); } });
		swtIsCalShotCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		swtSetAugShotButton = new Button(swtAugGroup, SWT.PUSH);
		swtSetAugShotButton.setText("Set");
		swtSetAugShotButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setAugShotButtonEvent(event); } });
		
		swtNextCalShotButton = new Button(swtAugGroup, SWT.PUSH);
		swtNextCalShotButton.setText("Next Cal");
		swtNextCalShotButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { getNextCalibShotButtonEvent(event); } });				
	
		//leaholl 02/2025: add wriggle
		ImageProcUtil.addRevealWriggler(swtAugGroup);
	}
	
	@Override
	protected void addExtensionsSWT2() {
		journalTextBox = new Text(swtGroup, SWT.MULTI | SWT.V_SCROLL | SWT.BORDER | SWT.READ_ONLY );
		GridData journalGD = new GridData(SWT.FILL, SWT.END, true, false, 5, 1);
		journalGD.heightHint = 70;
		journalTextBox.setLayoutData(journalGD);
		journalTextBox.setText("\n\n\n\n\n\n");
	}
	
	@Override
	protected void addExtensionsSWT3() {
		
		swtAUGShotSpinner.setSelection(pipeExt.getAUGShot(swtExperimentCombo.getText(), swtPulseSpinner.getSelection()));
		swtIsCalShotCheckbox.setSelection(pipeExt.isAugShotCalib(swtExperimentCombo.getText(), swtPulseSpinner.getSelection()));
		
		Group swtFlagGroup = new Group(swtGroup, SWT.BORDER);
		swtFlagGroup.setText("Flags");
		swtFlagGroup.setLayout(new GridLayout(3, false));
		swtFlagGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		
		swtFlagsCheckboxes = new Button[AugPilot.flagNames.length];
		int flags[] = pipeExt.getFlags(swtExperimentCombo.getText(), swtPulseSpinner.getSelection());
		for(int i=0; i < AugPilot.flagNames.length; i++){
			swtFlagsCheckboxes[i] = new Button(swtFlagGroup, SWT.CHECK);
			swtFlagsCheckboxes[i].setText(AugPilot.flagNames[i]);
			swtFlagsCheckboxes[i].setSelection(flags[i] != 0);
			swtFlagsCheckboxes[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setFlagsCheckboxEvent(event); } });
		}
	}
	
	protected void journalCheckboxEvent() {
		if(swtJournalCheckbox.getSelection()){ //if turned on, force the load of current pulse
			if(pipeExt.isAugShotCalib(swtExperimentCombo.getText(), swtPulseSpinner.getSelection())){
				journalTextBox.setText("[ Calib Series Shot ]");
				return;
			}
			int augShot = pipeExt.getAUGShot(swtExperimentCombo.getText(), swtPulseSpinner.getSelection());
			String journalRemarks = pipe.getJournalRemarks(augShot, true);
			journalTextBox.setText(journalRemarks); //will probably just be 'loading...'
		}else{
			dataChangingEvent();
		}
	}

	@Override
	public void dataChangingEvent() {
		if(dataChangeInhibit)
			return;
		
		super.dataChangingEvent();
				
		int augShot = pipeExt.getAUGShot(swtExperimentCombo.getText(), swtPulseSpinner.getSelection());
		swtAUGShotSpinner.setSelection(augShot);
		boolean isCalib = pipeExt.isAugShotCalib(swtExperimentCombo.getText(), swtPulseSpinner.getSelection());
		swtIsCalShotCheckbox.setSelection(isCalib);
		swtAUGShotSpinner.setForeground(swtGroup.getDisplay().getSystemColor(isCalib ? SWT.COLOR_DARK_GREEN : SWT.COLOR_BLACK));
		
		int flags[] = pipeExt.getFlags(swtExperimentCombo.getText(), swtPulseSpinner.getSelection());
		if(flags != null){
			for(int i=0; i < AugPilot.flagNames.length; i++){
				swtFlagsCheckboxes[i].setSelection(flags[i] != 0);
			}
		}
		
		if(swtJournalCheckbox.getSelection()){
			if(pipeExt.isAugShotCalib(swtExperimentCombo.getText(), swtPulseSpinner.getSelection())){
				journalTextBox.setText("[ Calib Series Shot ]");
				
			}else{
				String journalRemarks = pipe.getJournalRemarks(augShot, false);
				journalTextBox.setText(journalRemarks);
			}
		}else{
			journalTextBox.setText("[Disabled]");
		}
		
	}
	

	private void setAugShotButtonEvent(Event event){
		pipeExt.setAUGShot(swtExperimentCombo.getText(), swtPulseSpinner.getSelection(), 
				swtAUGShotSpinner.getSelection(), swtIsCalShotCheckbox.getSelection());
	}
	
	private void getNextCalibShotButtonEvent(Event event){
		//find the last filled gmdsID
		int lastGmdsID = pipe.findNextEmpty(swtExperimentCombo.getText(), swtPulseSpinner.getSelection(), "/RAW") - 1;
		//and go backwards from there for the last marked as calib series
		int nextCalibShot = 1*pipeExt.getNextCalibSeriesShot(swtExperimentCombo.getText(), lastGmdsID);
		
		pipeExt.setAUGShot(swtExperimentCombo.getText(), swtPulseSpinner.getSelection(), 
				nextCalibShot, true);
		
		swtAUGShotSpinner.setSelection(nextCalibShot);
		swtIsCalShotCheckbox.setSelection(true);
	}
	
	private void setFlagsCheckboxEvent(Event event){
		int flags[] = new int[swtFlagsCheckboxes.length];
		for(int i=0; i < swtFlagsCheckboxes.length; i++){
			flags[i] = swtFlagsCheckboxes[i].getSelection() ? 1 : 0;
		}
		pipeExt.setFlags(swtExperimentCombo.getText(), swtPulseSpinner.getSelection(), flags);
	}
	
	@Override
	protected void doUpdate() {
		super.doUpdate();
		
		int flags[] = pipeExt.getFlags(swtExperimentCombo.getText(), swtPulseSpinner.getSelection());
		for(int i=0; i < AugPilot.flagNames.length; i++){
			swtFlagsCheckboxes[i].setSelection(flags[i] != 0);
		}
		
		setTextColour();
		
		if(swtJournalCheckbox.getSelection()){
			int augShot = pipeExt.getAUGShot(swtExperimentCombo.getText(), swtPulseSpinner.getSelection());
			String journalRemarks = pipe.getJournalRemarks(augShot, false);
			journalTextBox.setText(journalRemarks);
		}else{
			journalTextBox.setText("[Disabled]");
		}
	}
}
