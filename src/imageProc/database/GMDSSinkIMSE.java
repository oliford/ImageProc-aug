package imageProc.database;

import org.eclipse.swt.widgets.Composite;

import imageProc.core.ImagePipeController;
import imageProc.database.gmds.GMDSSink;

public class GMDSSinkIMSE extends GMDSSink {
	GMDSPipeIMSE pipeExt;
	
	public GMDSSinkIMSE() {
		this.pipeExt = new GMDSPipeIMSE(this);
	}


	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		ImagePipeController controller = null;
		if(interfacingClass == Composite.class){
			controller = new GMDSPipeSWTControlIMSE((Composite)args[0], (Integer)args[1], this, pipeExt);
			controllers.add(controller);			
		}
		return controller;
	}
	
	@Override
	protected void extensionsPrepMetadataForSave() {

		Object o = connectedSource.getSeriesMetaData("aug/pulse");
		int augShot;
		if(o == null){
			augShot = -1;
		}else if(o instanceof Integer[]){
			augShot = ((Integer[])o)[0];
		}else if(o instanceof int[]){
			augShot = ((int[])o)[0];
		}else if(o instanceof Integer){
				augShot = (Integer)o;
		}else{
			augShot = -1;
		}
		
		o = connectedSource.getSeriesMetaData("aug/isCalib");
		boolean isCalib;
		if(o == null){
			isCalib = false;
		}else if(o instanceof Integer[]){
			isCalib = ((Integer[])o)[0] != 0;
		}else if(o instanceof int[]){
			isCalib = ((int[])o)[0] != 0;
		}else if(o instanceof Integer){
			isCalib = (Integer)o  != 0;
		}else if(o instanceof Boolean){
			isCalib = ((Boolean)o);
		}else{
			isCalib = false;
		}
		
		//set AUGShotNr according to the metadata
		pipeExt.setAUGShot(experiment, pulse, augShot, isCalib);
			
	}
}
