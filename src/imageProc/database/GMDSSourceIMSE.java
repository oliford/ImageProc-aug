package imageProc.database;

import org.eclipse.swt.widgets.Composite;

import imageProc.core.ImagePipeController;
import imageProc.database.gmds.GMDSSource;

public class GMDSSourceIMSE extends GMDSSource {
	GMDSPipeIMSE pipeExt;
	
	public GMDSSourceIMSE() {
		this.pipeExt = new GMDSPipeIMSE(this);
	}

	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		ImagePipeController controller = null;
		if(interfacingClass == Composite.class){
			controller = new GMDSPipeSWTControlIMSE((Composite)args[0], (Integer)args[1], this, pipeExt);
			controllers.add(controller);			
		}
		return controller;
	}
}
