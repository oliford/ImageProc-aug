package imageProc.graph;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.swt.widgets.Composite;

import imageProc.core.ImagePipeController;
import imageProc.graph.fastPolCalc.FastPolGraphProc;
import imageProc.graph.signals.ExtSignalsPlotter;

public class GraphUtilProcessorIMSE  extends GraphUtilProcessor {

	private ExtSignalsPlotter signalPlotter = new ExtSignalsPlotter(this);
	private FastPolGraphProc polCalc = new FastPolGraphProc(this);
	
	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			GraphUtilSWTControlIMSE swtGraphControl = new GraphUtilSWTControlIMSE(this, (Composite)args[0], (Integer)args[1]);
			controllers.add(swtGraphControl);
			return swtGraphControl;
		}else
			return null;
	}

	public ExtSignalsPlotter getSignalPlotter(){ return signalPlotter; }
	public FastPolGraphProc getFastPolProc(){ return polCalc; }
	

	protected void addExtensionSeries(LinkedList<Series> newSeriesList) {
		if(scanType == SCAN_DIR_T){
			List<Series> extSeriesList = signalPlotter.getSeriesList();
			if(extSeriesList != null)
				newSeriesList.addAll(extSeriesList);
		}
		
		if(polCalc != null){
			polCalc.calcNow(); //fire the calc, 
			List<Series> polSeriesList = polCalc.getSeriesList();
			if(polSeriesList != null)
				newSeriesList.addAll(polSeriesList);
		}
		
	}
	
	@Override
	protected boolean extensionsNeedUpdate() { return signalPlotter.needsUpdate(); }
	
}
