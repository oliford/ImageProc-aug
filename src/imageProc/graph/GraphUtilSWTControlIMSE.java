package imageProc.graph;

import imageProc.graph.fastPolCalc.FastPolCalcSWTControl;
import imageProc.graph.signals.ExtSignalsSWTControl;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.widgets.Composite;

public class GraphUtilSWTControlIMSE  extends GraphUtilSWTControl {

	private ExtSignalsSWTControl signalsControl;
	private FastPolCalcSWTControl polCalcControl;
	
	private CTabItem swtSignalsTab;
	private CTabItem swtPolCalcTab;
		
	public GraphUtilSWTControlIMSE(GraphUtilProcessorIMSE proc, Composite parent, int style) {
		super(proc, parent, style);
	}

	@Override
	protected void addSWTExtensions(){
		signalsControl = new ExtSignalsSWTControl(swtTabFoler, SWT.BORDER, (GraphUtilProcessorIMSE)proc);
		swtSignalsTab = new CTabItem(swtTabFoler, SWT.NONE);
		swtSignalsTab.setControl(signalsControl.getSWTGroup());
		swtSignalsTab.setText("Signals");
		swtSignalsTab.setToolTipText("Add external signals to the graph (From DataSignal(JET,AUG,MAST,TJII,W7X...))");
		
		polCalcControl = new FastPolCalcSWTControl(swtTabFoler, SWT.BORDER, (GraphUtilProcessorIMSE)proc);
		swtPolCalcTab = new CTabItem(swtTabFoler, SWT.NONE);
		swtPolCalcTab.setControl(polCalcControl.getSWTGroup());
		swtPolCalcTab.setText("Fast Pitch/Pol");
		swtPolCalcTab.setToolTipText("Graph based version of the fast pitch/polarisation angle calculator for IMSE");
		
	}
	
	@Override
	protected void updateExtensions() {
		signalsControl.update();
		polCalcControl.update();
	}
}
