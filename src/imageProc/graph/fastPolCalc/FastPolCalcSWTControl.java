package imageProc.graph.fastPolCalc;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import imageProc.core.ImageProcUtil;

import imageProc.graph.GraphUtilProcessor;
import imageProc.graph.GraphUtilProcessorIMSE;
import imageProc.proc.fastPolFull.FastPolSettingsSWTGroup;

public class FastPolCalcSWTControl {
	private Group swtGroup;
	private GraphUtilProcessorIMSE proc;
	
	private Button enableCheckbox;
	private Label statusLabel;
	private Button calcButton;
	private Button reloadButton;
	
	private Spinner timeDeltaSpinner;	
	private Spinner angleAdjustCoreSpinner;
	private Spinner angleAdjustEdgeSpinner;
	private Spinner angleAdjustTopSpinner;
	private FastPolSettingsSWTGroup fastPolSettingsGroup;
		
	public static final DecimalFormat tableValueFormat = new DecimalFormat("#.###");
	
	public FastPolCalcSWTControl(Composite parent, int style, GraphUtilProcessorIMSE proc) {
			this.proc = proc;
			
			swtGroup = new Group(parent, style);
			swtGroup.setText("Fast Pitch/Pol");
			swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
			swtGroup.setLayout(new GridLayout(4, false));
			
			Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Status:");
			statusLabel = new Label(swtGroup, SWT.NONE);
			statusLabel.setText("");
			statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
						
			enableCheckbox = new Button(swtGroup, SWT.CHECK);
			enableCheckbox.setText("Enable");
			enableCheckbox.setSelection(false);
			enableCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
			enableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});
			
			Label lTD = new Label(swtGroup, SWT.NONE); lTD.setText("Min ��t");
			timeDeltaSpinner = new Spinner(swtGroup, SWT.NONE);
			timeDeltaSpinner.setValues(10, 0, 10000, 3, 10, 100);
			timeDeltaSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
			timeDeltaSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});
			
			Label lOC = new Label(swtGroup, SWT.NONE); lOC.setText("Adjust Core (1.6m, Z=0):");
			angleAdjustCoreSpinner = new Spinner(swtGroup, SWT.NONE);
			angleAdjustCoreSpinner.setValues(0, -1000000, 1000000, 3, 100, 1000);
			angleAdjustCoreSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
			angleAdjustCoreSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});
			
			Label lOE = new Label(swtGroup, SWT.NONE); lOE.setText("Adjust Edge (2.0m, Z=0):");
			angleAdjustEdgeSpinner = new Spinner(swtGroup, SWT.NONE);
			angleAdjustEdgeSpinner.setValues(0, -1000000, 1000000, 3, 100, 1000);
			angleAdjustEdgeSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
			angleAdjustEdgeSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});
			
			Label lOT = new Label(swtGroup, SWT.NONE); lOT.setText("Adjust Top (1.8m, Z=+0.2m):");
			angleAdjustTopSpinner = new Spinner(swtGroup, SWT.NONE);
			angleAdjustTopSpinner.setValues(0, -1000000, 1000000, 3, 100, 1000);
			angleAdjustTopSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
			angleAdjustTopSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});
						
			calcButton = new Button(swtGroup, SWT.PUSH);
			calcButton.setText("Calc");
			calcButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
			calcButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { calcButtonEvent(event); }});
			
			reloadButton = new Button(swtGroup, SWT.PUSH);
			reloadButton.setText("Force reload");
			reloadButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
			reloadButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { reloadButtonEvent(event); }});
			
			fastPolSettingsGroup = new FastPolSettingsSWTGroup(swtGroup, SWT.BORDER, proc.getFastPolProc().getFastPolCalc());
	        ImageProcUtil.addRevealWriggler(swtGroup);
			
	}

	private void settingsChangedEvent(Event event) {
		FastPolGraphProc polCalc = proc.getFastPolProc();
		polCalc.setEnable(enableCheckbox.getSelection());
		polCalc.getFastPolCalc().setAnglesAdjustments(
				angleAdjustCoreSpinner.getSelection() / 1000.0,
				angleAdjustEdgeSpinner.getSelection() / 1000.0,
				angleAdjustTopSpinner.getSelection() / 1000.0);

		polCalc.setMinDeltaTime(timeDeltaSpinner.getSelection() / 1000.0);
	}

	private void calcButtonEvent(Event event) {
		proc.calc();
	}
	
	private void reloadButtonEvent(Event event) {
		proc.getFastPolProc().getFastPolCalc().forceReloadEqui();
		proc.calc();
	}
	
	public void update(){
		statusLabel.setText(proc.getFastPolProc().getStatus());
	}
		
	public Group getSWTGroup(){ return swtGroup; }


}
