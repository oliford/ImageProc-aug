package imageProc.graph.fastPolCalc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import algorithmrepository.CubicInterpolation2D;
import algorithmrepository.Interpolation2D;
import algorithmrepository.InterpolationMode;
import aug.equi.AUGEquilibriumData;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSource;
import imageProc.database.gmds.GMDSUtil;
import imageProc.graph.GraphUtilProcessor;
import imageProc.graph.Series;
import imageProc.proc.fastPolFull.FastPolCalc;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** Highly simplified pitch and polarisation angles calculation */
public class FastPolGraphProc {
	private GraphUtilProcessor proc;
	
	private boolean enable = false;

	private String status = "init";
	
	private FastPolCalc fastPol = new FastPolCalc();
	
	private double minDeltaT = 0;
	
	private double x[], ang[];	
	
	public FastPolGraphProc(GraphUtilProcessor proc) {
		this.proc = proc;		
	}
	
	public void setEnable(boolean enable) {
		this.enable = enable;
		proc.updateAllControllers();
	}
	
	//public void calc(){
	//	ImageProcUtil.ensureFinalUpdate(this, new Runnable() { @Override public void run() { doCalc();  } });
	//}
	
	public void calcNow() {
		try{
			doCalc();
		}catch(RuntimeException err){
			status = "ERROR: " + err;
			proc.updateAllControllers();
			throw err;
		}
	}
	
	private String equiLoadSyncObject = new String("equiLoadSyncObject");

	

	protected void doCalc() {
		if(!enable){
			status = "Disabled";
			proc.updateAllControllers();
			return;
		}
				
		int pulse = GMDSUtil.getMetaAUGShot(proc.getConnectedSource());
				
		ImgSource source = proc.getConnectedSource();
		
		//check the transform info before loading the equi
		if(source == null)			
			return;		
		double imgR[] = (double[])source.getSeriesMetaData("Transform/imageOutR");
		double imgZ[] = (double[])source.getSeriesMetaData("Transform/imageOutZ");
		Interpolation2D imgPhi[] = new Interpolation2D[8];
		
		double viewPos[] = (double[])source.getSeriesMetaData("Transform/viewPosition");
		if(imgR == null || imgZ == null || viewPos == null){
			//System.err.println("FastPolCalc.doCalc(): Incomplete transform data");
			status = "Incomplete transform data";
			proc.updateAllControllers();	
			
			return;
		}
		
		boolean isBox2 = viewPos[1] > 0; //+ve y means we're probably the permIMSE system
		for(int iB=0; iB < 8; iB++){
			if((isBox2 && iB < 4) || (!isBox2 && iB >= 4))
				continue;
			if(fastPol.getBeamSelection() != FastPolCalc.BEAMSEL_AUTO && fastPol.getBeamSelection() != iB)
				continue;
					
			double imgPhiGrid[][] = (double[][])source.getSeriesMetaData("Transform/outputPhiClosest-Q"+(iB+1));
				 	
			if(imgPhiGrid == null){
				System.out.println("No phi data for beam "+(iB+1)+", check transform is doing this beam!");
				status = "No transform phi for beam " + (iB+1);
				proc.updateAllControllers();
				return;
			}
			
			double pX[]  = Mat.linspace(0.0, 1.0, imgPhiGrid[0].length);
			double pY[]  = Mat.linspace(0.0, 1.0, imgPhiGrid.length);
			imgPhi[iB] = new Interpolation2D(pX, pY, Mat.transpose(imgPhiGrid),InterpolationMode.CUBIC);
		}
		
		AUGEquilibriumData loadedEqui = fastPol.getLoadedEqui();
		if(!fastPol.isEquiLoaded(pulse)){
			
			//we don't want the equi loading which can takes ages (several minutes)
			//to hang the graph update, so fire another update here and get it to fire us back
			
			status = "Awaiting equi signals load";
			final int pulseToLoad = pulse;
			
			ImageProcUtil.ensureFinalUpdate(equiLoadSyncObject, new Runnable() { @Override public void run() { doEquiLoad(pulseToLoad); } });
			
			proc.updateAllControllers();
			return;
		}
		
		int gmdsPulse = GMDSUtil.getMetaDatabaseID(source);
		fastPol.prepareGeometry( gmdsPulse, viewPos);
		
		doScan(imgR, imgZ, imgPhi, viewPos);
	}
	
	private void doScan(double imgR[], double imgZ[], Interpolation2D imgPhi[], double viewPos[]){
		status = "Calculating angles";
		proc.updateAllControllers();		
		
		x = null;
		ang = null;
		
		ImgSource source = proc.getConnectedSource();
		Img img = proc.getConnectedSource().getImage(0);
		
		int w = img.getWidth();
		int h = img.getHeight();
		int xSel = proc.getPosX();
		int ySel = proc.getPosY();
		if(xSel < 0 || ySel < 0 || xSel >= w || ySel >= h)
			throw new RuntimeException("Selected point outside image");
		
		double frameTimes[] = (double[])proc.getConnectedSource().getSeriesMetaData("time");
		
		fastPol.prepareMetadata(proc.getConnectedSource(), "Transform");
		
		if(proc.getScanDir() == GraphUtilProcessor.SCAN_DIR_T){
			
			double equiTime[] = fastPol.getLoadedEqui().getTimeOrdered();
			int nAll = equiTime.length;
			
			double xAll[] = new double[nAll];
			double angAll[] = new double[nAll];
			int n = 0;
			for(int i=0; i < nAll; i++){
				//skip until a time point is at least minDeltaT further on
				if(n==0 || (equiTime[i] - xAll[n-1]) >= minDeltaT){
					status = "Calculating vs time " + i + " / " + nAll;
					proc.updateAllControllers();	
					
					int frameIndex = Mat.getNearestIndex(frameTimes, equiTime[i]);
					
					fastPol.prepareFrame(frameIndex);
					double phi = imgPhi[fastPol.getFrameBeamIndex()].eval((double)xSel/w, (double)ySel/h);
					
					
					double posRPZ[][] = new double[][]{{ imgR[xSel], phi, imgZ[ySel] }};
					
					
					xAll[n] = equiTime[i];
					angAll[n] = fastPol.calcAng(xAll[n], posRPZ, viewPos)[0];
					
					
					n++;
				}
			}
			if(n == nAll){ //truncate arrays if nec.
				x = xAll;
				ang = angAll;
			}else{
				x = Arrays.copyOf(xAll, n);
				ang = Arrays.copyOf(angAll, n);
			}
			
		}else{
			int frameIndex = proc.getSelectedSourceIndex();
			fastPol.prepareFrame(frameIndex);
			
	    	Object o = source.getSeriesMetaData("/time");
	    	if(o == null || !(o instanceof double[]))
	    		return;
	    	
	    	double t[] = (double[])o;
	    	if(frameIndex < 0 || frameIndex > t.length)
	    		return;
	    	        
	    	double posRPZ[][];
	    	if(proc.getScanDir() == GraphUtilProcessor.SCAN_DIR_X){
	    		x = imgR;
	    		posRPZ = new double[imgR.length][3];
	    		for(int i=0; i < imgR.length; i++){
	    			posRPZ[i][0] = imgR[i]; 
	    			posRPZ[i][1] = imgPhi[fastPol.getFrameBeamIndex()].eval((double)i/w, (double)ySel/h); 
	    			posRPZ[i][2] = imgZ[ySel];
	    		}
	    		ang = fastPol.calcAng(t[frameIndex], posRPZ, viewPos);
	    		
	    	}else{
	    		x = imgZ;
	    		posRPZ = new double[imgZ.length][3];
	    		for(int i=0; i < imgZ.length; i++){
	    			posRPZ[i][0] = imgR[xSel]; 
	    			posRPZ[i][1] = imgPhi[fastPol.getFrameBeamIndex()].eval((double)xSel/w, (double)i/h); 
	    			posRPZ[i][2] = imgZ[i];
	    		}
	    		ang = fastPol.calcAng(t[frameIndex], posRPZ, viewPos);        		
	    	}
	    	
        }     
		
		status = "Done.";
		proc.updateAllControllers();
	}
	
		
	private void doEquiLoad(int pulse){
		try{
			fastPol.loadEqui(pulse);
			
			status = "Equilibrium loaded";
			proc.updateAllControllers();
			
			//and re-fire the calc now that it's loaded
			proc.calc();
			
		}catch(RuntimeException err){
			err.printStackTrace();
			status = "Equilibrium load failed: " + err.getMessage();
			proc.updateAllControllers();
			return;
		}		
		fastPol.prepareGeometry(-1, null);
	}
	
	public List<Series> getSeriesList() {		
				
		ArrayList<Series> signalList = new ArrayList<Series>();
		
		if(!enable)
			return signalList;
		if(ang	 != null){
			Series s = new Series();
			s.name("FastPol");
			s.x = x;
			s.data = ang;
			signalList.add(s);
		}
		
		proc.updateAllControllers();
		
		return signalList;
	}

	public String getStatus() { return status; }

	public void setMinDeltaTime(double minDt) {
		this.minDeltaT = minDt;	
	}

	public FastPolCalc getFastPolCalc() { return fastPol; }

}
