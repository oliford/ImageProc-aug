package imageProc.graph.signals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import algorithmrepository.ExtrapolationMode;
import algorithmrepository.Interpolation1D;
import algorithmrepository.InterpolationMode;
import algorithmrepository.LinearInterpolation1D;
import base.SignalFetcher;
import descriptors.gmds.GMDSSignalDesc;
import imageProc.core.ImageProcUtil;
import imageProc.database.gmds.GMDSUtil;
import imageProc.graph.GraphUtilProcessor;
import imageProc.graph.Series;
import mds.AugShotfileFetcher;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import signals.aug.AUGSignal;
import signals.gmds.GMDSSignal;

public class ExtSignalsPlotter {
	
	public static class ExtSignalPlotInfo extends Series implements Comparable<ExtSignalPlotInfo>, Serializable {		
		private static final long serialVersionUID = 7499234410392846985L;
		
		public String sigName; //has to be defined here, because parent class members are no serialised 
		public String path = "";
		/** Rescale to this max. 0 means leave as it was, NaN means autoscale to first series */
		public double scale = 1.0;
		public double offset = 0;
		/** axis slicing of multidimension arrays, index of all but last dimension */
		public int slice[];
		public boolean enable;
		
		public String name(){ return sigName; }
		public void name(String name){ this.sigName = name; }
				
		@Override
		public int compareTo(ExtSignalPlotInfo o) {			
			return name().compareTo(o.name());
		}
	}
	
	private GraphUtilProcessor proc;
	private ArrayList<ExtSignalPlotInfo> sigList = new ArrayList<ExtSignalsPlotter.ExtSignalPlotInfo>();
	private boolean changed = false;
	private boolean enable = false;
	private double minDeltaTime;
		
	public ExtSignalsPlotter(GraphUtilProcessor proc) {
		this.proc = proc;
		load("/default");
	}

	public boolean needsUpdate() {		
		return changed;
	}

	public List<Series> getSeriesList() {		
		Object o = proc.getConnectedSource().getSeriesMetaData("/aug/pulse");
		if(o != null && o.getClass().isArray())
			o = Array.get(o, 0); //grrrr
		int pulse = (o != null && o instanceof Integer) ? (Integer)o : 0;
		
		ArrayList<Series> signalList = new ArrayList<Series>();
		
		if(!enable)
			return signalList;
		
		for(ExtSignalPlotInfo sigInfo : sigList){
			if(!sigInfo.enable)
				continue;
			
			if(sigInfo.data == null || sigInfo.x == null){
					
				//SignalFetcher amds = AugMDSFetcher.defaultInstance();
				SignalFetcher amds = AugShotfileFetcher.defaultInstance();
				try{	
					String fullPath = (sigInfo.path.startsWith("/aug") || sigInfo.path.startsWith("aug")) 
											? sigInfo.path 
											: "/aug/" + pulse + "/" + sigInfo.path;
					AUGSignal sig = (AUGSignal) amds.getSig(fullPath);
					
						
					//check entry array matches rank
					if(sigInfo.slice == null)
						sigInfo.slice = new int[0];
					if(sigInfo.slice.length != (sig.getRank()-1)){						
						sigInfo.slice = Arrays.copyOf(sigInfo.slice, sig.getRank()-1);
						System.err.println("Entry list "+sigInfo.slice+" (x,y,z...) doesn't match rank "+sig.getRank()+" of signal " + sig.getDescriptor() + ", resizing.");
					}
							
					//slice down to last dimension by given entry array
					Object o2 = sig.getDataAsType(double.class);
					for(int i=0; i < sigInfo.slice.length; i++){
						o2 = Array.get(o2, sigInfo.slice[i]);
					}
					double d[] = (double[])o2;
					
					//try to find a t-like coordinate with the same dimensionality as the data 
					double x[] = null;
					for(int j=0; j < sig.getRank(); j++){
						double tryX[] = (double[])sig.getCoordsAsType(j, double.class);
						if(tryX != null && tryX.length == d.length){
							x = tryX;
							break;
						}
					}
					
					if(x == null) //otherwise just use sample number
						x = Mat.linspace(0, d.length, 1.0);
						
					
					
					if(minDeltaTime > 0 && minDeltaTime > (x[1] - x[0])){
						sigInfo.x = Mat.linspace(x[0], x[x.length-1], minDeltaTime);
						sigInfo.data = (new Interpolation1D(x, d, InterpolationMode.LINEAR, ExtrapolationMode.CONSTANT_END)).eval(sigInfo.x);
					}else{
						sigInfo.x = x;
						sigInfo.data = d;
					}
					
					if(sigInfo.scale != 0 || sigInfo.offset != 0.0){
						double scale = sigInfo.scale;
						double offset = sigInfo.offset;
						
						if(Double.isNaN(sigInfo.scale)){ //NaN means autoscale
							Series s = proc.getSeriesList().get(0);							
							scale = s.max - s.min;
							offset = s.min;
						}
						
						double range[] = Mat.getRange(sigInfo.data);
						for(int i=0; i < sigInfo.data.length; i++)
							sigInfo.data[i] = (sigInfo.data[i] - range[0]) * scale / (range[1] - range[0]) + offset; 
					}
					
				}catch(RuntimeException err){
					err.printStackTrace();
					sigInfo.enable = false;
				}
			}
			
			if(sigInfo.data != null){
				signalList.add(sigInfo);
			}
			
		}
		
		proc.updateAllControllers();
		
		return signalList;
	}
	
	public void save(String name){
		try {
 			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos;
			oos = new ObjectOutputStream(bos);
			oos.writeObject(sigList);			
			byte data[] = bos.toByteArray();
			
			GMDSSignalDesc sigDesc = new GMDSSignalDesc(0, GMDSUtil.getMetaExp(proc.getConnectedSource()), "/signalLists/" + name);
			GMDSSignal sig = new GMDSSignal(sigDesc, data);
			sig.writeToCache(GMDSUtil.globalGMDS().getCacheRoot());
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void load(String name){
		try{
			GMDSSignalDesc sigDesc = new GMDSSignalDesc(0, GMDSUtil.getMetaExp(proc.getConnectedSource()), "/signalLists/" + name);
			GMDSSignal sig = (GMDSSignal) GMDSUtil.globalGMDS().getSig(sigDesc);
			if(sig != null){
				byte data[] = (byte[])sig.getData();
				
				ByteArrayInputStream bis = new ByteArrayInputStream(data);
				ObjectInputStream ois = new ObjectInputStream(bis);
				sigList = (ArrayList<ExtSignalPlotInfo>)ois.readObject();
				
				//temperory workaround due to dropped 'name' field in higher ExtSignalPlotInfo class 04/11/2015  
				int k=0;
				for(ExtSignalPlotInfo s : sigList){
					if(s.name() == null)
						s.name("UNK" + k++);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		proc.updateAllControllers();
	}
	
	public String[] loadList(){
		GMDSUtil.globalGMDS().dumpTree("", 0, "", true);
		
		String sigsPaths[] = GMDSUtil.globalGMDS().dumpTree(GMDSUtil.getMetaExp(proc.getConnectedSource()), 0, "signalLists/", true);
		
		return sigsPaths;		
	}

	public void signalsListModified() {
		changed = true;			
	}

	public List<ExtSignalPlotInfo> getSignalsList() { return sigList; }

	public void setEnable(boolean enable){
		this.enable = enable;
		proc.updateAllControllers();
	}

	public void setMinDeltaT(double minDeltaTime) {
		this.minDeltaTime = minDeltaTime;
		proc.updateAllControllers();
	}
}
