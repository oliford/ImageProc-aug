package imageProc.pilot.aug;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.swt.widgets.Composite;

import aug.ddww.AugShotFile;
import imageProc.control.arduinoComm.ArduinoCommHanger;
import imageProc.control.arduinoComm.ArduinoCommHangerAUG;
import imageProc.core.AcquisitionDevice;
import imageProc.core.ConfigurableByID;
import imageProc.core.EventReciever;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgProcPipe;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.PowerControl;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.database.gmds.GMDSSource;
import imageProc.sources.capture.andorCam.AndorCamSource;
import imageProc.sources.capture.base.CaptureSource;
import imageProc.sources.capture.sensicam.SensicamSource;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;
import otherSupport.SingleProcessLocking;
/** General integration into AUG diagnostic control
 *  
 *  This is split into various bits:
 *    Main ProgramController (Day Controller) 
 *                    		- checks camera is on and ready at start, waits for shot, called the InShot controller
 *                    		- will also later do time based things like start/end day, non-shot days, rads tests etc
 *    InShotControler  - Gets run for each shot when the shot number is distributed
 *    PowerControl (IPS) - Deal with the remote control power socket in the hall
 *    
 *  
 * Stuff to do:
 * D		Wait for shot number and trigger global start when we get it
 * D		Set the shot number meta-data
 * D		Trigger abort and save some time after setup
 * D			Somehow notice if we got a TS06 or not (i.e did the camera run?)
 * D		Notice if the camera started but didn't complete and reset it's power
 * X		Maybe reset the camera anyway, even if it was just because there was no TS06
 * 
 * 
 * X		Might need a more general inter-pipe triggering system
 * X		Send triggers to specific pipes in the chain, for processing etc?
 * 				(nah, just run down the tree)
 * 		
 * 		Don't handle shotfiles in here - write an equivalent of GMDSPipe for it (reading AND writing)
 * 			(It just needs to implement DatabaseSink)
 * 
 * 		Detection of weird camera problems and reset (lines, bit errors etc)
 * D	Outside hall arduino controlker - turning the TEC controller and laser on/off, checking if TS06 ever came etc
 * D  	Setup, check and manage the in-hall arduino
 * D  	Turn on lamps and change focus for the probe-tests
 * D  	Manage processing chain inter-shot (probably in or after InShotControl)
 *   			
 *  
 * */
public class AugPilot extends ImgSourceOrSinkImpl implements ImgSink {
	
	public final static String flagNames[] = {
			"plasma", "beams", "lamps", "radsCheck",
			"cameraStopped", "cameraBitFailure", "cameraOtherFailure",
			"interestingCalibration", "interestingPhysics"
		};
	
	public static final int SHOT_TYPE_UNKNOWN = 0;
	public static final int SHOT_TYPE_TEST = 1; //webservice test
	public static final int SHOT_TYPE_PLASMA = 2;
	public static final int SHOT_TYPE_PROBE_TEST = 3;
	public static final int SHOT_TYPE_FORCED = 4; //force locally
	public static final int SHOT_TYPE_AUTOCONFIG = 5; //forced by configuration
	public static final String shotTypeNames[] = {"Unknown","Test","Plasma","Probe Test","Forced","AutoByConfig"};
			
	private static String shotServiceNumberURL = "http://ssr-mir.aug.ipp.mpg.de:9099/mcs:Adm/Discharge/Number";
	private static int shotServiceConnectTimeout = 2000;
	private static int shotServiceReadTimeout = 2000;
	private static String shotServiceModeURL = "http://ssr-mir.aug.ipp.mpg.de:9090/par:Adm/Discharge/Mode";
	private static String shotServiceTypeURL = "http://ssr-mir.aug.ipp.mpg.de:9090/par:Adm/Discharge/Type";
		
	private int shotPollIntervalMS = 20000;
	private int dayPollIntervalMS = 60000; //every minute check what day and time it is... yeah...

	private boolean configBeforeShot = true;
	
	private String status = "Init";
	
	private OperationConfig currentConfig = null; //find the config we should be on now
	/** Make/Keep this config active reardless of time of day */ 
	private OperationConfig forceCurrentConfig = null;
		
	private StringBuffer log = new StringBuffer(1048576);
	
	public StringBuffer getLog() { return log; }
	
	private AugPilotSWTControl swtController;
	
	private int currentShotType;

	public boolean manualForceShot = false;
	
	private final static SimpleDateFormat timestampFormat = new SimpleDateFormat("dd/MM HH:mm:ss");
	
	private ProgramRunner controller;
	private Thread controllerThread;
	
	/** Controller for the remote power switch */
	private PowerControl powerCtrl;
	
	/** inShotProc - the thing that drives the camera and saving etc for each shot */
	private InShotControl inShotCtrl;
	
	/** Which days are shot days. Sun,Mon,.... Fri,Sat*/
	private boolean shotDays[];
	
	private SingleProcessLocking pilotLock;
	
	public static class OperationConfig {
		public OperationConfig(String name, boolean enable, int time, boolean power, int autoStartSecs, boolean lamps, 
								String setupID, int onlyForShotType) {
			this.name = name; this.enable = enable; this.time = time; this.power = power; this.autoStartSecs = autoStartSecs;
			this.lampsMeasurement = lamps; this.setupID = setupID; this.onlyForShotType = onlyForShotType;
		}
		
		/** Human readbale name */
		String name;
		/** Enabled */
		boolean enable;
		/** Minutes since midnight to start this config, regardless of shot type*/
		int time;
		/** Only use this config (still after the given time) when a shot starts with the given type */
		int onlyForShotType;
		/** Power on and wait for shot */ 
		boolean power;
		/** Force a shot to start after this many seconds*/
		int autoStartSecs;
		/** Tell Arduinos to do lamps? */
		boolean lampsMeasurement;
		/** Setup anything ID-configurable to this database entry */
		String setupID;
		/** Timeout for camera driver open */ 
		public long openCameraTimeout = 30000;
	}
	
	private LinkedList<OperationConfig> configs;
	
	
	/** Set to day that the current day isn't a shot day, even if it fits the normal criteria */
	private Calendar inhibitDay = null; 

	private int cameraWarmUpTime;
	
	private String logFilePath;
	private FileOutputStream logFileStream;
	private PrintWriter logFileWriter;
	private Calendar logDate = null;
	
	private boolean isInShotControl = false;
	
	private int nextDatabaseID = -1000;
	
	public AugPilot() {	
		inShotCtrl = new InShotControl(this);
		powerCtrl = new InfratecPowerControl(this);
		
		configs = new LinkedList<OperationConfig>();
		configs.add(new OperationConfig("OffMorning", true, 0*60 +  0, false, -1, false, "", -1));
		configs.add(new OperationConfig("Rads", true, 7*60 + 55, true, 120, false, "AUG/1395", -1));
		configs.add(new OperationConfig("DriftTracking1", true, 8*60 + 1, true, 60, true, "AUG/3735", -1));
		configs.add(new OperationConfig("Normal", true, 8*60 + 10, true, -1, false, "AUG/2999", -1));
		configs.add(new OperationConfig("ProbeTest", true, 8*60 + 11, true, 20, true, "AUG/3775", SHOT_TYPE_PROBE_TEST));
		configs.add(new OperationConfig("DriftTracking2", false, 18*60 + 20, true, 60, true, "AUG/3735", -1));
		configs.add(new OperationConfig("OffNight", true, 18*60 + 40, false, -1, false, "", -1));
		
		cameraWarmUpTime = Algorithms.mustParseInt(SettingsManager.defaultGlobal().getProperty("imageProc.augControl.cameraWarmUpTime", "20000"));
		logFilePath = SettingsManager.defaultGlobal().getPathProperty("imageProc.augControl.logPath", 
				System.getProperty("java.io.tmpdir") + "/imseProc/augControlLogFiles");
		shotDays = new boolean[]{ false, false, true, false, true, true, false }; //tue, thurs, fri - normal shot days
		
		pilotLock = new SingleProcessLocking(
						Algorithms.mustParseInt(SettingsManager.defaultGlobal().getProperty("imageProc.augPilot.lockPort","17324")));
	}
		
	public void logWrite(String str){
		Calendar now = Calendar.getInstance();
		String timeStamp = timestampFormat.format(now.getTime());
		String lineStr = timeStamp + ": " + str;
		
		try{
			if(logDate == null || now.get(Calendar.DAY_OF_YEAR) != logDate.get(Calendar.DAY_OF_YEAR)){
				//want a new log file
				if(logFileStream != null){
					logFileStream.close();
					logFileWriter.close();
				}
				String logFileName = "augControl-" + 
							(new SimpleDateFormat("yyyy-MM-dd")).format(now.getTime()) + ".log";
				logFileStream = new FileOutputStream(logFilePath + "/" + logFileName, true);
				logFileWriter = new PrintWriter(logFileStream, true);
				
				logDate = now;
			}
			
			logFileWriter.println(lineStr);
		} catch (Exception err) {
			//can't do much else without risking messing things up
			err.printStackTrace();
		}
		
		log.append(lineStr + "\n");
		updateAllControllers();
	}
	

	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			if(swtController == null){
				swtController = new AugPilotSWTControl(this, (Composite)args[0], (Integer)args[1]);
				controllers.add(swtController);
			}
			return swtController;
		}else
			return null;
	}

	@Override
	public boolean isIdle() { 	return true; 	} //we're always idle
	
	/** The main runner. This is running whenever the auto system is active 
	 * (even on non-operational days)
	 * 
	 * @author oliford
	 */
	private static class ProgramRunner implements Runnable {
		public boolean death = false;		
		private AugPilot proc;
		private int id;
		private static int nextID = 0;
		
		public ProgramRunner(AugPilot proc) {
			this.proc = proc;
		}
		
		@Override
		public void run() {
			proc.startAuto();
		}
	}
	
	private void pilotLockAbort(){
		logWrite("PilotLock: Aborting because another AUGPilot asked us to.");
		
		killController(true);
		AugShotFile.abortShotNumberWaiting();
		
		//since we know another process is about to start using things
		//ask the source to release it's hold of the AcquisitionDevice
		ImgSource connectedSource = getConnectedSource();
		if(connectedSource instanceof AcquisitionDevice){
			((AcquisitionDevice)connectedSource).close();
		}
		
		logWrite("PilotLock: Aborted, letting the other pilot start.");
	}
	
	/** The main auto-operation loop */
	private void startAuto(){	
		logWrite("Controller thread running.");
		
		logWrite("PilotLock: Obtaining lock on port " + pilotLock.getPortNumber());
		try{
			boolean gotLock = pilotLock.startLock(true, new Runnable() { @Override public void run() { pilotLockAbort(); } });
			if(!gotLock){
				logWrite("PilotLock: Lock refused. Cannot start.");
				return;
			}
		}catch(RuntimeException err){
			logWrite("PilotLock: Failed to obtain lock: " + err);
			return;
		}
		
		
		logWrite("PilotLock: Lock obtained OK.");
		
		
		
		boolean surppressNotDayMsg = false;
		while(!controller.death){
			
			if(isInShotDay() || manualForceShot){
				try{
					startDay();
				}catch(Throwable err){
					// Nothing should get down this far, but we'll catch in case it does so we don't just exit the thread.
					// This should loop back and restart the day controller (if we're in day)
					logWrite("ERROR: Caught unhandled exception in control system somewhere. Will try to re-start the day control. Error was: " + err);
					err.printStackTrace();
				}
				surppressNotDayMsg = false;
			}else{	
				if(!surppressNotDayMsg){
					//message first time we decide not to run
					logWrite("Not a shot day, or config is powerOff");
					surppressNotDayMsg = true;
				}
				
				if(controller.death)
					break;
				
				try {
					Thread.sleep(dayPollIntervalMS);
				} catch (InterruptedException e) {
					//user is getting bored, tell them we're waiting
					surppressNotDayMsg = false;
				}
			}
		}

		logWrite("PilotLock: Dropping lock");
		pilotLock.endLock();
		
		logWrite("Controller thread ended by death signal");
		
	}
	
	public boolean isInShotDay(){
		Calendar now = Calendar.getInstance();
		
		if(inhibitDay != null){
			if(now.get(Calendar.YEAR) == inhibitDay.get(Calendar.YEAR) &&
				now.get(Calendar.DAY_OF_YEAR) == inhibitDay.get(Calendar.DAY_OF_YEAR)){
					return false; // not today!
			}		
		}
		
		
		boolean isShotDay;
		switch(now.get(Calendar.DAY_OF_WEEK)){ //not sure the number is reliable
			case Calendar.SUNDAY: isShotDay = shotDays[0]; break;
			case Calendar.MONDAY: isShotDay = shotDays[1]; break;
			case Calendar.TUESDAY: isShotDay = shotDays[2]; break;
			case Calendar.WEDNESDAY: isShotDay = shotDays[3]; break;
			case Calendar.THURSDAY: isShotDay = shotDays[4]; break;
			case Calendar.FRIDAY: isShotDay = shotDays[5]; break;
			case Calendar.SATURDAY: isShotDay = shotDays[6]; break;
			default:
				throw new RuntimeException("ERROR: Someone invented a new day of the week.");
		}
		return isShotDay;
	}
	
	
	public OperationConfig getConfigNow(){
		int minsNow = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)*60
				+ Calendar.getInstance().get(Calendar.MINUTE);
		return getConfigByTime(minsNow);
	}
	
	/** Find the latest enabled config entry before the given time */
	public OperationConfig getConfigByTime(int time){
		if(forceCurrentConfig != null)
			return forceCurrentConfig;
			
		
		//find the config with the latest time before the given time
		// and that shouldn't be ignored
		OperationConfig bestMatch = null;
		for(OperationConfig config : configs ){
			if(config.enable && config.time <= time 
					&& (config.onlyForShotType < 0 || config.onlyForShotType == currentShotType)
					&& (bestMatch == null || config.time > bestMatch.time)){
				bestMatch = config;
			}
		}
		return bestMatch;
	}
	
	/** Run by ProgrammRunner at the start of a shot day for automatic operation */
	private void startDay(){
		setStatus("Day control init");
		
		currentConfig =null; //don't know yet
			
		int shot = -1;
		int lastShot = -1;
		boolean suppressShotWaitMsg = false;  
		while(!controller.death){

			boolean newConfig = checkConfigChanged(); //some configs force a shot on activation
			boolean configForceShot = newConfig && currentConfig.autoStartSecs > 0;
						
			if(!manualForceShot && !configForceShot){
				//only wait for shot number if we are 'on'
				if(currentConfig.power){
					setStatus("Waiting for shot number");
					if(!suppressShotWaitMsg)
						logWrite("Waiting for shot number");
				
	
					// WebService based shot waiting
					//If the service calls block, the thread interrupt is ignored, 
					// so we need to clear it here after each timeout
					shot = shotNumberFromWS();
					if(lastShot < 0)
						lastShot = shot;
					if(Thread.interrupted()){ //clear interrupted flag
						continue;
					}
					
				}else{
					//power is off, so don't wait for a shot number
					//because the service is often broken
					shot = -1;
					currentShotType = SHOT_TYPE_UNKNOWN;
					//just wait
					try {
						Thread.sleep(shotPollIntervalMS);
					} catch (InterruptedException e) {
						//the user appears bored, so tell them we're just waiting
						suppressShotWaitMsg = false;
					}	
				}
			}

			if(manualForceShot || configForceShot){ //overrride to force a shot if there is one
				shot = -1;
				currentShotType = manualForceShot ? SHOT_TYPE_FORCED : SHOT_TYPE_AUTOCONFIG;
				//make damn sure the interrupt is cleared so that this actually happens
				Thread.interrupted();
			}
			
			if((shot > 0 && currentShotType != SHOT_TYPE_TEST) || manualForceShot || configForceShot){
				if(controller.death){
					logWrite("Controller ignoring shot because death already signalled.");
					break;
				}
				if(shot != lastShot || manualForceShot || configForceShot){
					manualForceShot = false;
					
					if(!manualForceShot && !configForceShot){
						currentShotType = shotTypeFromWS();
						if(Thread.interrupted()) //clear interrupted flag again
							continue;
						logWrite("Shot type is " + shotTypeNames[currentShotType]);
						
						//that may have changed our config again due to the shot type...
						if(checkConfigChanged()){
							logWrite("Config changed because shot type is " + shotTypeNames[currentShotType]);
							
						}
						//we hope that change ges through before the shot is ready
						//which will be pretty close for a probe test
						//we ignore the autostart of the config since we're giong into in-shot anyway
					}
					
					if(configForceShot){
						try{
							logWrite("Waiting "+currentConfig.autoStartSecs+" seconds before autostart.");
							Thread.sleep(currentConfig.autoStartSecs * 1000);
							
						}catch(InterruptedException err){
							logWrite("Autostart delay interrupted, not doing a forced shot");
							continue; //nevermind
						}
					}
					
					logWrite("Controller got new shot number: " + shot);
					try{
						isInShotControl = true;
						inShotCtrl.start(shot, currentShotType, nextDatabaseID);			
					}finally{
						isInShotControl = false;
						nextDatabaseID++;
						lastShot = shot;
						currentShotType = SHOT_TYPE_UNKNOWN; //don't confuse the config finder, so back to unknown/normal
					}
					
					suppressShotWaitMsg = false;
				}else{
					if(!suppressShotWaitMsg)
						logWrite("Controller: Shot number still " + shot);
					
					suppressShotWaitMsg = true; //don't keep repeating this
					
					//wait for a bit before polling for next shot
					if(controller.death)
						break;
					try {
						Thread.sleep(shotPollIntervalMS);
					} catch (InterruptedException e) {
						//the user appears bored, so tell them we're just waiting
						suppressShotWaitMsg = false;
					}	
				}
			}
			
			if(!isInShotDay() && !controller.death){
				checkCameraIsOff();
				break;
			}
		}
		setStatus("Day control ending.");
		logWrite("Day control ending.");
	}
	
	private boolean checkConfigChanged(){
		OperationConfig cfg = getConfigNow();
		if(cfg != currentConfig){
			logWrite("Config changed from " + ((currentConfig == null) ? "???" : currentConfig.name)
								+ " to " + cfg.name);
			
			//FIXME: should be catching errors here, otherwise we fall out of day control
			//not quite sure what to do about it though since we're not in a 'reset the bloody thing' loop here
			
			if(cfg.power){
				checkCameraIsSetup();
				
			}else{
				checkCameraIsOff();
			}
			setupToConfig(cfg); //setup all hardware and processing modules to current mode
			//also for off, since we might need to turn the lamps off etc
			
			currentConfig = cfg;
			updateAllControllers();
			return true;
		}
		
		return false;
	}
	
	public void checkCameraIsSetup(){
		ImgSource src = getConnectedSource();
		if(!(src instanceof AcquisitionDevice))
			throw new RuntimeException("Image source is not an acquisition device");
		
		AcquisitionDevice cam = (AcquisitionDevice)src;
		
		if(cam.getAcquisitionStatus() != Status.notInitied && cam.getAcquisitionStatus() != Status.errored){ //and it's now connected ('open')
			logWrite("Acqusition device appears to be ready. ("+cam.getAcquisitionStatus() + ", "+ cam.getAcquisitionStatusString() + ")");
			return;
		}
	
		try{
			if(getPowerControl().switchOn(null)){; //check the power is on
				
				logWrite("Camera was (probably) off, waiting "+cameraWarmUpTime+" ms for it to warm up.");
				
				Thread.interrupted();//don't skip this delay
				try{ Thread.sleep(cameraWarmUpTime); }catch(InterruptedException err){} //wait for it to wake up
			}else{
				logWrite("Necessary power already all on.");
				
			}
		}catch(RuntimeException err){
			logWrite("ERROR: Couldn't check/switch on camera power, will try to set it up anyway: " + err);
			err.printStackTrace();
		}
		
		OperationConfig cfg = getConfigNow();
		logWrite("Setting up camera to '"+cfg.setupID+"'");
		Thread.interrupted();//don't skip delays here
		try{
			if(cam instanceof ConfigurableByID)
				((ConfigurableByID)cam).loadConfig(cfg.setupID);
			cam.open(cfg.openCameraTimeout ); //and set it up
		}catch(Exception err){
			if(controller.death){
				logWrite("ERRROR: Exception setting up camera. Controller is aborting though, so this might be intentional. " + err);
				return;
			}
				
			err.printStackTrace();
			logWrite("ERRROR: Exception setting up camera in config prep, will make one attempt to reset it: " + err);
			try{
				inShotCtrl.resetCamera();
			}catch(InterruptedException err2){ }
		}				
		
		//make sure it's ready to be triggered by InShot
		if(src instanceof CaptureSource)
			((CaptureSource)src).enableEventResponse(true, false);
	
	}
	
	private void checkCameraIsOff(){
		
		logWrite("Setting camera off");
		//might want to do rads check or something here
		
		
		//The andor camera driver will crash the entire JVM if
		//the camera is switched off while the driver is connected
		if(connectedSource instanceof AcquisitionDevice){
			((AcquisitionDevice)connectedSource).close();
			Thread.interrupted();
			try { Thread.sleep(1000); } catch (InterruptedException e) { }
		}
		
		try{
			getPowerControl().switchOff(null);
		}catch(RuntimeException err){
			logWrite("ERROR: Camera power-off failed");
		}
	}
	
	/** Attempts to get the shot number from shotServiceNumberURL
	 * @return The shot number, or -1 if one couldn't be found
	 * @throws */
	private int shotNumberFromWS() {
		List<String> lines = getDataFromURL(shotServiceNumberURL);
		
		for(String line : lines){
			  try{
	   		   int shot = Integer.parseInt(line);
	   		   if(shot > 20000){
	   			   return shot;
	   		   }
	   	   }catch(NumberFormatException err){}
	   	   
		}
	   	logWrite("No number in shot service response.");
	   	return -1;
	}
	
	private int shotTypeFromWS(){
		List<String> lines = getDataFromURL(shotServiceModeURL);
		for(String line : lines){
			logWrite("DBG: shotTypeFromWS(): Mode Line '"+line+"'");
			if(line.toLowerCase().contains("test")){
				return SHOT_TYPE_TEST;
			}
		}
			
		lines = getDataFromURL(shotServiceTypeURL);
		for(String line : lines){
			logWrite("DBG: shotTypeFromWS(): Type Line '"+line+"'");
			if(line.toLowerCase().contains("probe")){
				return SHOT_TYPE_PROBE_TEST;
			}
			if(line.toLowerCase().contains("plasma")){
				return SHOT_TYPE_PLASMA;
			}
		}
		
		return SHOT_TYPE_UNKNOWN;
		
/*  mcs:Adm/Discharge/Number = shot number that changes at start of shot
 *  par:Adm/Discharge/Number = shot number that changes after end of shot
 *  par:Adm/Discharge/Mode       : "Shot" or "Test" mode of the SLS
	par:Adm/Discharge/Type       : the "type" extracted from the DP
                               either "Plasma" or "ProbeTest"
                               
   Hmm, not sure of Mode/Type can be used with the mcs: shot number
    erm...
        */
	}
	
	/** Gets a list of lines from a url */
	private List<String> getDataFromURL(String urlString){
		ArrayList<String> lines = new ArrayList<String>();
		
		URL shotReqURL;
		try {
			shotReqURL = new URL(urlString);
			 URLConnection urlConn = shotReqURL.openConnection();
			 urlConn.setConnectTimeout(shotServiceConnectTimeout);
			 urlConn.setReadTimeout(shotServiceReadTimeout);
		     BufferedReader in = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
		       
			 while(true){
				 String s = in.readLine();
				 if(s == null)
					 break;
				 lines.add(s);
			 }
		} catch (IOException err) {
			logWrite("Error in shot service request: " + err);
			err.printStackTrace();
		} catch (RuntimeException err) {
			logWrite("Error in shot service request: " + err);
			err.printStackTrace();
		}
        return lines;
	}

	public void runController() {
		inhibitDay = null;
		
		if(controller != null)
			killController(false); //don't wait for it, it can take an hour
	
		logWrite("Starting controller thread");
		controller = new ProgramRunner(this);
		controllerThread = new Thread(controller);
		controllerThread.start();
	}
	
	private void killController(boolean waitForDeath) {
		logWrite("Killing controller thread");
		if(controller == null)
			return;
			
		controller.death = true;
		controllerThread.interrupt(); 
		
		try {
			if(waitForDeath)
				while(controllerThread.isAlive())
						Thread.sleep(10);
		} catch (InterruptedException e) { }
	}


	public void forceShot() {
		manualForceShot  = true; //tell the day controller to start
		if(controller != null && controllerThread.isAlive()){ //if we have a controller already
			controllerThread.interrupt(); //kick it
		}else{
			runController(); //otherwise start a one
		}
	}
	
	@Override
	/** Make visible for AugControlIPS */
	public void updateAllControllers() {
		super.updateAllControllers();
	}
	
	/** Aborts/kills the day controller, without turning anything off */
	public void abortController() {
		//signal the controller to die, and kill the AUGshotnr process
		killController(false);
		AugShotFile.abortShotNumberWaiting();
	}

	@Override
	public void destroy() {
		super.destroy();
		killController(false);
		logFileWriter.close();
		try { logFileStream.close(); } catch (IOException e) { }
		inShotCtrl = null;
		powerCtrl = null;		
	}
	
	public String getStatus() { return status; }
	private void setStatus(String status){ 
		this.status = status; 
		updateAllControllers();
	}
	
	/** Stop whatever delay the controller is currently in */
	public void kickController(){ if(controllerThread != null) controllerThread.interrupt(); }
	

	public int getShotPollIntervalMS() { return shotPollIntervalMS; }
	public void setShotPollIntervalMS(int shotPollIntervalMS) { this.shotPollIntervalMS = shotPollIntervalMS;	}
	public PowerControl getPowerControl() { return powerCtrl; }
	public InShotControl getInShotCtrl() { return this.inShotCtrl; }
	public LinkedList<OperationConfig> getConfigs(){ return this.configs; }
	
	public void setShotDays(boolean shotDays[]){ this.shotDays = shotDays;	 }
	public void inhibitToday(){ 	
		inhibitDay = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("MM/dd/yy");
		
		logWrite("Inhibiting current day " + formatter.format(inhibitDay.getTime()) + " (Assuming not shot day)");		
	}
	public boolean isInShotControl(){ return isInShotControl; }

	public void setCameraWarmUpTime(int cameraWarmUpTime) { this.cameraWarmUpTime = cameraWarmUpTime; }
	public int getCameraWarmUpTime(){ return cameraWarmUpTime; }

	public boolean[] getShotDays() { return shotDays; }

	public void setNextDatabaseID(int nextDatabaseID) { this.nextDatabaseID = nextDatabaseID; }
	public int getNextDatabaseID(){ return this.nextDatabaseID; }

	String bulkProcessSyncObj = "bulkProcessSyncObj";
	
	public void bulkProcess(int databaseID0, int databaseID1) {
		if(!(connectedSource instanceof GMDSSource)){
			logWrite("BulkProcess: ERROR: Source isn't GMDSSource");
			return;
		}
		if(controllerThread != null && controllerThread.isAlive()){
			logWrite("BulkProcess: ERROR: Controller is running, stop it first!");
			return;
		}		
		
		final int id0 = databaseID0, id1 = databaseID1;
		
		ImageProcUtil.ensureFinalUpdate(bulkProcessSyncObj, new Runnable() {
			@Override
			public void run() {
				try {
					inShotCtrl.doBulkProcess(id0, id1);
					
				} catch (InterruptedException err) {
					err.printStackTrace();
				}
			}
		});
	}

	public void setupToConfig(OperationConfig cfg) {
		//go through the whole source/sink tree, setting up anything that can be setup to
		//the given ID
		String parts[] = cfg.setupID.split("/");
		String exp = parts[0];
		int id= parts.length < 2 ? -1 : Integer.parseInt(parts[1]);
		
		logWrite("Cfg("+cfg.name+"): Setting up all modules to "+exp+"/"+id+".");
		try{
			setupToConfig(cfg.name, connectedSource, cfg, exp, id);
		}catch(InterruptedException err){
			logWrite("Config interrupted");
		}
	}
	
	public void setupToConfig(String exp, int databaseID) {
		String setupName = exp + "/" + databaseID;
		logWrite("Cfg("+setupName+"): Setting up all processing modules to "+exp+"/"+databaseID+".");
		try{
			setupToConfig(setupName, connectedSource, null, exp, databaseID);
		}catch(InterruptedException err){
			logWrite("Config interrupted");
		}
	}
	
	private void setupToConfig(String setupName, ImgSource base, OperationConfig cfg, String exp, int databaseID) throws InterruptedException {
		if(databaseID > 0 && base instanceof ConfigurableByID){
			try{
				((ConfigurableByID)base).loadConfig("gmds/" + exp + "/" + databaseID);
					
				logWrite("Cfg("+setupName+"): Set " + base + " setup to " + exp + "/" + databaseID);
				
				
			}catch(RuntimeException err){
				logWrite("Cfg("+setupName+"): ERROR: " + base + " setup to " + exp + "/" + databaseID + " failed:\n    " + err);
				err.printStackTrace();
			}
		}
		
		for(ImgSink sink : base.getConnectedSinks()){
			
			if(sink instanceof ImgProcPipe){
				ImgProcPipe pipe = ((ImgProcPipe)sink);
				
				//now do it's children
				setupToConfig(setupName, pipe, cfg, exp, databaseID);
				
			}else if(sink instanceof ConfigurableByID){
				try{
					((ConfigurableByID)sink).loadConfig("gmds/" + exp + "/" + databaseID);
					
					logWrite("Cfg("+setupName+"): Set " + sink + " setup to " + exp + "/" + databaseID);
					
				}catch(RuntimeException err){
					logWrite("Cfg("+setupName+"): ERROR: " + sink + " setup to " + exp + "/" + databaseID + " failed:\n    " + err);
					err.printStackTrace();
				}
			}
			
			//and setup the aux hardware accordingly
			if(sink instanceof ArduinoCommHangerAUG && cfg != null){ // (don't do hardware config if only given a shot number)
				
				if(cfg.lampsMeasurement){
					logWrite("Cfg("+setupName+"): Arduino setting up hardware for lamp measurement ("+sink+")");
					((ArduinoCommHangerAUG)sink).setupForLampMeasurement();					
				}else{
					logWrite("Cfg("+setupName+"): Arduino setting up hardware for normal operation ("+sink+")");
					((ArduinoCommHangerAUG)sink).setupForShot();
				}
			}
		}
	}

	public void setForceConfig(OperationConfig cfg) {
		forceCurrentConfig = cfg;
		updateAllControllers();
	}
	
	public OperationConfig getForceConfig(){
		return forceCurrentConfig;
	}

	/** Gets the currently set config, which is the last activated/forced/whatever by the day
	 * controller. This isn't necessarily the same as what you get from getConfigNow() 
	 * and might be null */
	public OperationConfig getLastSetConfig() { return currentConfig; }

	@Override
	public boolean getAutoUpdate() { return false; }
	@Override
	public void setAutoUpdate(boolean enable) { }
	@Override
	public void calc() { 	}
	@Override
	public boolean wasLastCalcComplete(){ return true; }
	@Override
	public void abortCalc(){ }
	

	public static void setFlag(ImgSource src, String name, int val) {
		String names[] = (String[])src.getSeriesMetaData("flags/names");
		int flags[] = (int[])src.getSeriesMetaData("flags/vals");
		if(names == null || flags == null){
			names = flagNames.clone();
			flags = new int[flagNames.length];
		}
		
		for(int i=0; i < names.length; i++){
			if(names[i].equalsIgnoreCase(name)){
				flags[i] = val;
				src.setSeriesMetaData("flags/vals", flags, false);
				return;
			}
		}
		
		String newNames[] = Arrays.copyOf(names, names.length+1);
		int newVals[] = Arrays.copyOf(flags, names.length+1);
		newNames[names.length] = name;
		flags[names.length] = val;
		src.setSeriesMetaData("flags/names", newNames, false);
		src.setSeriesMetaData("flags/vals", newVals, false);
		
	}

	@Override
	public String toShortString() {
		return "AUGPilot"; 
	}

	public void setReconfigBeforeShot(boolean enable) { this.configBeforeShot = enable; }
	public boolean getReconfigBeforeShot(){ return this.configBeforeShot; }
}
