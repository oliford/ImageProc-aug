package imageProc.pilot.aug;

import java.text.DecimalFormat;
import java.util.LinkedList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImageProcUtil;
import imageProc.pilot.aug.AugPilot.OperationConfig;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class ConfigSWTControl {
	private Group swtGroup;
	
	public static final String colNames[] = new String[] {
			"Name          ", "Enable", "Time", "Power?", "Start capture(secs)", 
			"Lamps?", "SetupID", "Only Type", "Active (F to force)" 
			};	
	
	public static final int COL_NAME = 0;
	public static final int COL_ENABLE = 1;
	public static final int COL_TIME= 2;
	public static final int COL_POWER = 3;
	public static final int COL_AUTOSTART = 4;
	public static final int COL_LAMPS = 5;
	public static final int COL_SETUPID = 6;
	public static final int COL_SHOTTYPE = 7;
	public static final int COL_ACTIVENOW = 8;
	
	private Table cfgTable;
	private TableEditor cfgTableEditor;
	private TableItem tableItemEditing = null;
	
	private Spinner nextDatabaseIDSpinner;
	private Button reconfigEveryShotCheckbox;
	private Spinner cameraWarmUpTimeSpinner;
	private Button resetCameraButton;
	
	private AugPilot proc;
	
	public ConfigSWTControl(AugPilot augCtrl, Composite parent, int style) {
		this.proc = augCtrl;
		
        swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("Config+Processing");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        swtGroup.setLayout(new GridLayout(6, false));

        Label lID = new Label(swtGroup, SWT.NONE); lID.setText("Next database ID:");
        nextDatabaseIDSpinner = new Spinner(swtGroup, SWT.NONE);
        nextDatabaseIDSpinner.setValues(proc.getNextDatabaseID(), 1, Integer.MAX_VALUE, 0, 10, 100);
        nextDatabaseIDSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        nextDatabaseIDSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        reconfigEveryShotCheckbox = new Button(swtGroup, SWT.CHECK);
        reconfigEveryShotCheckbox.setText("Reconfigure sources and sinks before every shot");
        reconfigEveryShotCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
        reconfigEveryShotCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        cfgTable = new Table(swtGroup, SWT.BORDER);				
		cfgTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 0));
		cfgTable.setHeaderVisible(true);
		cfgTable.setLinesVisible(true);
		
		TableColumn cols[] = new TableColumn[colNames.length];
		for(int i=0; i < colNames.length; i++){
			cols[i] = new TableColumn(cfgTable, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
			cols[i].setText(colNames[i]); 
			//cols[i].pack();
		}

		//proc.loadConfig(null, 0);
		configToTable();
		for(int i=0; i < colNames.length; i++){
			cols[i].pack();
		}
		cfgTableEditor = new TableEditor(cfgTable);
		cfgTableEditor.horizontalAlignment = SWT.LEFT;
		cfgTableEditor.grabHorizontal = true;
		cfgTable.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { tableMouseDownEvent(event); } });
		
        
        Label lWT = new Label(swtGroup, SWT.NONE); lWT.setText("Camera warm-up time (secs):");
        cameraWarmUpTimeSpinner = new Spinner(swtGroup, SWT.NONE);
        cameraWarmUpTimeSpinner.setValues(proc.getCameraWarmUpTime()/1000, 1, Integer.MAX_VALUE, 0, 10, 60);
        cameraWarmUpTimeSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 5, 1));
        cameraWarmUpTimeSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        Label lRC = new Label(swtGroup, SWT.NONE); lRC.setText("Reset camera:");
        resetCameraButton = new Button(swtGroup, SWT.NONE);
        resetCameraButton.setText("Reset Camera Now");
        resetCameraButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 5, 1));
        resetCameraButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { resetCameraButtonEvent(event); }});
        
        ImageProcUtil.addRevealWriggler(swtGroup);
        
	}

	private String resetCameraSync = new String("resetCameraSync"); 
	protected void resetCameraButtonEvent(Event event) {
		ImageProcUtil.ensureFinalUpdate(resetCameraSync, new Runnable() {
			@Override
			public void run() {
				try {
					proc.getInShotCtrl().resetCamera();
				} catch (InterruptedException e) { }
			}
		});
	}


	private void configToTable() {
		OperationConfig cfgNow = !proc.isInShotDay() ? null : proc.getConfigNow();
		OperationConfig cfgForce = proc.getForceConfig();
		
		//clear and repopulate table
		cfgTable.removeAll();
		for(OperationConfig cfg : proc.getConfigs()){
			if(cfg.name.length() <= 0)
				continue;
			TableItem item = new TableItem(cfgTable, SWT.NONE);
			item.setText(COL_NAME, cfg.name);
			item.setText(COL_ENABLE, cfg.enable ? "True" : "False");
			item.setText(COL_TIME, toTimeStr(cfg.time));
			item.setText(COL_POWER, Boolean.toString(cfg.power));
			item.setText(COL_AUTOSTART, Integer.toString(cfg.autoStartSecs));
			item.setText(COL_LAMPS, Boolean.toString(cfg.lampsMeasurement));
			item.setText(COL_SETUPID, cfg.setupID);
			item.setText(COL_SHOTTYPE, (cfg.onlyForShotType < 0) ? "" : AugPilot.shotTypeNames[cfg.onlyForShotType]);
			item.setText(COL_ACTIVENOW, (cfg == cfgNow) ? ((cfg == cfgForce) ? "Force" : "Active") : "");
						
		}
		
		//and finally a blank item for creating new point
		TableItem blankItem = new TableItem(cfgTable, SWT.NONE);
		blankItem.setText(0, "");
	}
	
	/** Copied from 'Snippet123'  Copyright (c) 2000, 2004 IBM Corporation and others. 
	 * [ org/eclipse/swt/snippets/Snippet124.java ] */
	private void tableMouseDownEvent(Event event){
		
		Rectangle clientArea = cfgTable.getClientArea ();
		Point pt = new Point (clientArea.x + event.x, event.y);
		int index = cfgTable.getTopIndex ();
		while (index < cfgTable.getItemCount ()) {
			boolean visible = false;
			final TableItem item = cfgTable.getItem (index);
			for (int i=0; i<cfgTable.getColumnCount (); i++) {
				Rectangle rect = item.getBounds (i);
				if (rect.contains (pt)) {
					final int column = i;
					final Text text = new Text(cfgTable, SWT.NONE);
					tableItemEditing = item;
					Listener textListener = new Listener () {
						public void handleEvent (final Event e) {
							switch (e.type) {
							case SWT.FocusOut:
								tableColumnModified(item, column, text.getText());								
								text.dispose ();
								tableItemEditing = null;
								break;
							case SWT.Traverse:
								switch (e.detail) {
								case SWT.TRAVERSE_RETURN:
									tableColumnModified(item, column, text.getText());
									//FALL THROUGH
								case SWT.TRAVERSE_ESCAPE:
									text.dispose ();
									tableItemEditing = null;
									e.doit = false;
								}
								break;
							}
						}
					};
					text.addListener (SWT.FocusOut, textListener);
					text.addListener (SWT.Traverse, textListener);
					cfgTableEditor.setEditor (text, item, i);
					text.setText (item.getText (i));
					text.selectAll ();
					text.setFocus ();
					return;
				}
				if (!visible && rect.intersects (clientArea)) {
					visible = true;
				}
			}
			if (!visible) return;
			index++;
		}
	}
	
	private void tableColumnModified(TableItem item, int column, String text){
		LinkedList<OperationConfig> configs = proc.getConfigs();
		
		String configName = item.getText(COL_NAME);
		
		OperationConfig cfg = null;
		for(OperationConfig config : configs)
			if(configName.equals(config.name)){
				cfg = config;
			}
		
		if(cfg == null){
			return;
			//cfg = new OperationConfig(configName, -1, false, -1, false, "?/-1");
			//configs.add(cfg);
		}
						
		switch(column){
		case COL_NAME:
			//changed point name (or new point)
			/*if(configName.length() > 0)
				configs.remove(cfg);
			
			configName = text;
			item.setText(column, text);*/
			break;
		case COL_ENABLE:
			cfg.enable = (text.startsWith("T") || text.startsWith("t") || text.startsWith("Y") || text.startsWith("y"));
			break;
			
		case COL_TIME:
			String parts[] = text.split(":");
			int hours = Algorithms.mustParseInt(parts[0]);
			int mins = (parts.length < 2) ? 0 : Algorithms.mustParseInt(parts[1]);
			
			cfg.time = hours * 60 + mins;
			item.setText(column, toTimeStr(cfg.time)); 
			break;

		case COL_POWER:
			cfg.power = Algorithms.mustParseBoolean(text);
			item.setText(column, Boolean.toString(cfg.power)); 
			break;
			
		case COL_AUTOSTART:
			cfg.autoStartSecs = Algorithms.mustParseInt(text);
			item.setText(column, Integer.toString(cfg.autoStartSecs)); 
			break;
			
		case COL_LAMPS:
			cfg.lampsMeasurement = Algorithms.mustParseBoolean(text);
			item.setText(column, Boolean.toString(cfg.lampsMeasurement));
			break;
			
		case COL_SETUPID:
			cfg.setupID = text;
			item.setText(column, cfg.setupID); 
			break;
			
		case COL_SHOTTYPE:
			if(text.trim().length() == 0){
				cfg.onlyForShotType = -1;
				item.setText(column, "");
				break;
			}
			int idx = OneLiners.findBestMatchingString(AugPilot.shotTypeNames, text);
			cfg.onlyForShotType = idx;
			if(idx >= 0){
				item.setText(column, AugPilot.shotTypeNames[idx]);
			}
			break;
			
		case COL_ACTIVENOW:
			if(text.startsWith("F") || text.startsWith("f")){
				proc.setForceConfig(cfg);
				item.setText(column, "Force");
			}else if(cfg == proc.getForceConfig()){
				proc.setForceConfig(null);
				item.setText(column, "");
			}
			break;
			
		}
		
		TableItem lastItem = cfgTable.getItem(cfgTable.getItemCount()-1);
		if(lastItem.getText(0).length() > 0){
			TableItem blankItem = new TableItem(cfgTable, SWT.NONE);
			blankItem.setText(0, "");
		}
		
		//proc.kickController();
	}
	
	private String toTimeStr(int mins){
		int hours = mins / 60;
		DecimalFormat fmt = new DecimalFormat("##");
		fmt.setMinimumIntegerDigits(2);
		return fmt.format(hours) + ":" + fmt.format(mins - hours * 60); 
	}
	
	protected void settingsChangingEvent(Event event) {
		proc.setCameraWarmUpTime(cameraWarmUpTimeSpinner.getSelection()*1000);
		proc.setNextDatabaseID(nextDatabaseIDSpinner.getSelection());
		proc.setReconfigBeforeShot(reconfigEveryShotCheckbox.getSelection());
	}
	
	public void doUpdate() {
		nextDatabaseIDSpinner.setSelection(proc.getNextDatabaseID());
		
		if(tableItemEditing == null)
			configToTable();	
	}

	public Control getSWTGroup() { return swtGroup; }
}
