package imageProc.pilot.aug;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import imageProc.core.ImageProcUtil;

public class DaySWTControl {
	private Group swtGroup;
	
	private Spinner shotPollSpinner;
	
	private Button shotDayCheckboxes[];
	private Button dayInhibitButton;
		
	private AugPilot proc;
	
	public DaySWTControl(AugPilot augCtrl, Composite parent, int style) {
		this.proc = augCtrl;
		
        swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("Day");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        swtGroup.setLayout(new GridLayout(3, false));
       
		String days[] = new String[]{ "Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat" };
		shotDayCheckboxes = new Button[7];
		boolean shotDays[] = proc.getShotDays(); 
		for(int i=0; i < 7; i++){
			shotDayCheckboxes[i] = new Button(swtGroup, SWT.CHECK);
			shotDayCheckboxes[i].setText(days[i]);;
			shotDayCheckboxes[i].setSelection(shotDays[i]);
			shotDayCheckboxes[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
		}
		
		dayInhibitButton = new Button(swtGroup, SWT.PUSH);
		dayInhibitButton.setText("Inhibit Day (No shots today)");
		dayInhibitButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
		dayInhibitButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.inhibitToday(); } });
		
        Label lSP = new Label(swtGroup, SWT.NONE); lSP.setText("Shot poll period (secs):");
        shotPollSpinner = new Spinner(swtGroup, SWT.NONE);
        shotPollSpinner.setValues(proc.getShotPollIntervalMS()/1000, 1, Integer.MAX_VALUE, 0, 10, 60);
        shotPollSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
        shotPollSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
 
        ImageProcUtil.addRevealWriggler(swtGroup);
        
	}



	protected void settingsChangingEvent(Event event) {
		proc.setShotPollIntervalMS(shotPollSpinner.getSelection()*1000);
		
		boolean shotDays[] = new boolean[7];
		for(int i=0; i < 7; i++){
			shotDays[i] = shotDayCheckboxes[i].getSelection();
		}
		proc.setShotDays(shotDays);
	}

	public void doUpdate() {
	
	}

	public Control getSWTGroup() { return swtGroup; }
}
