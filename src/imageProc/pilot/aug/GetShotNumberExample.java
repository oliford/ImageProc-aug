package imageProc.pilot.aug;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class GetShotNumberExample {
	private static String shotServiceNumberURL = "http://ssr-mir.aug.ipp.mpg.de:9099/mcs:Adm/Discharge/Number";
	private static int shotServiceConnectTimeout = 5000; //miliSeconds
	private static int shotServiceReadTimeout = 5000; //miliSeconds

	/** Gets a list of lines from a url */
	private static List<String> getDataFromURL(String urlString){
		ArrayList<String> lines = new ArrayList<String>();
		
		URL shotReqURL;
		try {
			shotReqURL = new URL(urlString);
			 URLConnection urlConn = shotReqURL.openConnection();
			 urlConn.setConnectTimeout(shotServiceConnectTimeout);
			 urlConn.setReadTimeout(shotServiceReadTimeout);
		     BufferedReader in = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
		       
			 while(true){
				 String s = in.readLine();
				 if(s == null)
					 break;
				 lines.add(s);
			 }
			 
		} catch (IOException err) { //Must catch various errors from the URL calls
			System.err.println("Error in shot service request: " + err);
			err.printStackTrace();
			
		} catch (RuntimeException err) { //probably socket timeout errors etc
			System.err.println("Error in shot service request: " + err);
			err.printStackTrace();
		}
        return lines;
	}

	/** Attempts to get the shot number from shotServiceNumberURL
	 * @return The shot number, or -1 if one couldn't be found*/
	private static int shotNumberFromWS() {
		List<String> lines = getDataFromURL(shotServiceNumberURL);
		
		for(String line : lines){
			  try{
	   		   int shot = Integer.parseInt(line);
	   		   if(shot > 20000){
	   			   return shot;
	   		   }
	   	   }catch(NumberFormatException err){}
	   	   
		}
		System.err.println("No number in shot service response.");
	   	return -1;
	}

	public static void main(String[] args) {
		System.out.println("Current/Last AUG shot number = " + shotNumberFromWS());
	}
}
