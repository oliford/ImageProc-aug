package imageProc.pilot.aug;

import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;

import imageProc.core.AcquisitionDevice;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.core.ConfigurableByID;
import imageProc.core.DatabaseSink;
import imageProc.core.EventReciever;
import imageProc.core.EventReciever.Event;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgProcPipe;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.database.gmds.GMDSSource;
import imageProc.pilot.aug.AugPilot.OperationConfig;
import imageProc.proc.seriesAvg.SeriesProcessorAUG;
import imageProc.sources.capture.andorCam.AndorCamSource;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

public class InShotControl {
	private String status = "Init";
	
	private AugPilot proc;
	
	/** Time to switch camera off for when it stops mid-range */
	private int cameraOfflineTime;
	private int cameraCaptureTimeoutMS;
	private int cameraAcquireStartTimeoutMS;
	
	private int databaseSaveTimeoutMS;
	
	private int processChainDelayMS;
	
	private boolean processChainInhibit = false;
	
	/** The Controller fires start or aborts, then waits this long before continuing */
	private int postTriggerDelay = 3000;
	
	/** Time the controller waits between polling sources and sinks for their busy status */
	private int busyPollDelayMS = 500; // 500ms
	
	private boolean shotInhibit = false;

	private boolean resetPowerOnFault;
	
	private int databaseID;

	private MulticastTriggerWaiter mcastTriggers;
	
	/** Signal worker processing to stop. Since this is an ensureFinalUpdate() call, the next process will then start.
	 * The user just has to hammer away at Abort */
	private boolean abortProcessing = false;
	
	public InShotControl(AugPilot augControl) {
		this.proc = augControl;

		resetPowerOnFault = Algorithms.mustParseBoolean(SettingsManager.defaultGlobal().getProperty("imageProc.augControl.inShot.fault.resetPower", "True"));
		cameraOfflineTime = Algorithms.mustParseInt(SettingsManager.defaultGlobal().getProperty("imageProc.augControl.inShot.fault.cameraOffTime", "10000"));
		cameraCaptureTimeoutMS = Algorithms.mustParseInt(SettingsManager.defaultGlobal().getProperty("imageProc.augControl.inShot.acquireTimeout", "300000")); // 5mins
		databaseSaveTimeoutMS = Algorithms.mustParseInt(SettingsManager.defaultGlobal().getProperty("imageProc.augControl.inShot.saveTimeout", "300000"));
		processChainDelayMS = Algorithms.mustParseInt(SettingsManager.defaultGlobal().getProperty("imageProc.augControl.inShot.processChainDelay", "30000"));
		cameraAcquireStartTimeoutMS = Algorithms.mustParseInt(SettingsManager.defaultGlobal().getProperty("imageProc.augControl.inShot.acquireStartTimeout", "5000")); //3secs
		
		mcastTriggers =  new MulticastTriggerWaiter(augControl);
	}
	
	public String getStatus() {
		return null;
	}
	
	
	/** This is the guts of the controller, it is fired whenever a new shot number is distributed */ 
	public void start(int shotNumber, int shotType, int databaseID) {
		this.databaseID = databaseID;
		long t0, t1;
		//make sure it's not SIMCAM
		
		if(!(proc.getConnectedSource() instanceof AcquisitionDevice))
			throw new RuntimeException("Connected source is not an AcquisitionDevice. In-shot control not possible!");
		
		ImgSource connectedSource = proc.getConnectedSource();
		AcquisitionDevice acq = (AcquisitionDevice)connectedSource;
		
		try{
			if(connectedSource == null){
				proc.logWrite("InShot: Controller can't start - no source");
				return;
			}
			if(shotInhibit){
				proc.logWrite("InShot: Controller start inhibited");
				return;
			}
			status = "Controller started";
			
			proc.logWrite("InShot: Started in-shot for databaseID = " + databaseID);
						
			if(proc.getReconfigBeforeShot()){
				proc.setupToConfig(proc.getConfigNow()); 
			}
			
			for(int nPrepAttempts=0; nPrepAttempts < 6; nPrepAttempts++){ //start preparation loop
				
				if(attemptAcquisitionPrep(shotNumber, shotType)){
					break; //prep OK, ready to acquire
				}else{
					if(nPrepAttempts >= 5){ //too many attempts, give up
						for(int j=0; j < 5; j++)
							proc.logWrite("InShot["+databaseID+"]: ERROR: ****!!!!!***** Tried to reset camera 5 times, giving up! ****!!!!!***** ");
						
						return;
					}
					resetCamera();
				}	
			}
			
			//mcastTriggers.clear();
			//mcastTriggers.start();
			
			// At this point the camera is either running or armed and waiting
			//for the hardware trigger
			proc.logWrite("InShot["+databaseID+"]: Waiting for capture to (start?) and finish.");
			
			//Wait for it to stop acquiring, or for the capture timeout
			t0 = System.currentTimeMillis();
			t1 = t0;
			while(!connectedSource.isIdle() && (t1 - t0) < cameraCaptureTimeoutMS){
				t1=System.currentTimeMillis();
				Thread.sleep(busyPollDelayMS);
			}
			
			connectedSource.setSeriesMetaData("inshot/acqStatus", acq.getAcquisitionStatus().toString(), false);
			
			if((t1 - t0) >= cameraCaptureTimeoutMS){ //timed out waiting for camera to actually take images
				proc.logWrite("InShot["+databaseID+"]: Timed out waiting for aquire on " + connectedSource + ". Aborting it.");

				//shot may or may not be over, but we've no way of telling yet, so assume it is.
				//in any case: report the problem, clean things up and try to reset if necessary
				postMortem();

				return; //no point in doing anything else, since resetting the camera probably lost the images
				
			}else{ //timed out waiting for camera to actually take images
				
				proc.logWrite("InShot["+databaseID+"]: Acquire done on " + connectedSource + ". Captured "+
						acq.getNumAcquiredImages() +" / "+connectedSource.getNumImages()+"images.");
				//proc.logWrite("InShot["+databaseID+"]: Acquire done on " + connectedSource + ". Captured  / "+connectedSource.getNumImages()+"images.");
						
				connectedSource.setSeriesMetaData("inshot/status", "AcquiredOK", false);
				attemptSave();	
				
				resetCamera();// aburckha: reset every shot because of power supply errors (remove when power fine)
								
			
				//mcastTriggers.kill();
			
				
				int numCaptured = -1;
				if(connectedSource instanceof AndorCamSource){
					numCaptured = ((AndorCamSource)connectedSource).getNumCaptured();
				}
				
				if(numCaptured == 0 || isCameraBroken()){
					proc.logWrite("InShot["+databaseID+"]: Camera is broken, so resetting it and not processing.");
					resetCamera();
					return;
				}
				
				proc.logWrite("InShot["+databaseID+"]: Waiting " + processChainDelayMS + "ms before processing.");
				Thread.sleep(processChainDelayMS);
				if(!processChainInhibit)
					processChain();
				
				//abort everything to make sure
				connectedSource.broadcastEvent(Event.GlobalAbort);
				Thread.sleep(postTriggerDelay);
			}
		}catch(InterruptedException err){
			status = "Controller error";
			proc.logWrite("InShot: Controller interrupted, triggering general abort.");
			connectedSource.broadcastEvent(Event.GlobalAbort);
		}catch(Exception err){
			status = "Controller error";
			proc.logWrite("InShot: Controller exception: " + err);
			err.printStackTrace();
			connectedSource.broadcastEvent(Event.GlobalAbort);
		}
				
	}
	
	/** Called when acquisition fails. 
	 * Report problem, clean up and make a decision if camera should be reset.
	 * @throws InterruptedException */
	private void postMortem() throws InterruptedException {
		boolean resetCamera;
		ImgSource connectedSource = proc.getConnectedSource();

		AcquisitionDevice acq = (AcquisitionDevice)connectedSource;

		Status aqStatus = acq.getAcquisitionStatus();
		proc.logWrite("postMortem(): AcquisitionDevice.getAcquisitionStatus() = " + aqStatus);
		proc.logWrite("postMortem(): AcquisitionDevice.getAcquisitionStatusString() = " + acq.getAcquisitionStatusString());

		if(aqStatus != Status.awaitingHardwareTrigger && aqStatus != Status.capturing){

			proc.logWrite("InShot["+databaseID+"]: ERROR: ** AcquisitionDevice was NOT ready to acquire! **");
			//This can't happen!, so only the user can fix this
			connectedSource.setSeriesMetaData("inshot/status", "AcquireNotReady", false);
			AugPilot.setFlag(proc.getConnectedSource(), "cameraOtherFailure", 1);
			resetCamera = true;
		}else{
			if(aqStatus != Status.awaitingHardwareTrigger && aqStatus != Status.capturing){

				//if the camera simply never triggered, that's ok, probably TS06 never came
				//but if the camera died in the middle, we probably need to reset it

				proc.logWrite("InShot["+databaseID+"]: AcquisitionDevice was ready to acquire but didn't start, probably no H/W trigger.");
				connectedSource.setSeriesMetaData("inshot/status", "AcquireNotStarted", false);
				AugPilot.setFlag(proc.getConnectedSource(), "cameraOtherFailure", 1);
				resetCamera = false;
			}else{
				proc.logWrite("InShot["+databaseID+"]: AcquisitionDevice started acquiring but never finished, will reset it.");
				connectedSource.setSeriesMetaData("inshot/status", "AcquireTimedOut", false);
				AugPilot.setFlag(proc.getConnectedSource(), "cameraOtherFailure", 1);
				resetCamera = true;
			}
		}


		connectedSource.broadcastEvent(Event.GlobalAbort);
		Thread.sleep(postTriggerDelay);

		attemptSave(); //attempt to save whatever we did get first, don't process
		resetCamera = true;// aburckha: reset every shot because of power supply errors (remove when power fine)
		if(resetCamera)
			resetCamera();
		
		connectedSource.broadcastEvent(Event.GlobalAbort);
		
	}

	public void resetCamera() throws InterruptedException {		
		AcquisitionDevice acq = (AcquisitionDevice)proc.getConnectedSource();
					
		proc.logWrite("InShot["+databaseID+"]: Closing camera.");
		acq.close();
		status = "Resetting camera";
		if(resetPowerOnFault){ //... && connectedSource.getImage(0) != null){ ... Why????
			try{
				proc.logWrite("InShot: Power cycling camera for " + cameraOfflineTime + " ms");
				proc.getPowerControl().switchOff(null);
				try{ 
					Thread.sleep(cameraOfflineTime); 
				}catch(InterruptedException err){ //we must leave when poked! 
					throw new RuntimeException("Interrupted while power cycling camera"); 
				}
				proc.getPowerControl().switchOn(null);
				proc.logWrite("InShot: Camera power cycle done.");
			}catch(RuntimeException err){
				proc.logWrite("InShot: ERROR: Couldn't cycle camera power, will try to set it up again anyway: " + err);
				err.printStackTrace();
			}
		}
		
		proc.logWrite("InShot["+databaseID+"]: Waiting " + proc.getCameraWarmUpTime() + " ms for camera to init/settle.");
		try{
			Thread.sleep(proc.getCameraWarmUpTime());
		}catch(InterruptedException err){ 
			throw new RuntimeException("Interrupted while waiting for camera to warm up"); //we must leave when poked!
		} 
		
		if(acq instanceof ConfigurableByID)
			((ConfigurableByID)acq).loadConfig(proc.getConfigNow().setupID);
		acq.open(proc.getConfigNow().openCameraTimeout);
		proc.logWrite("InShot["+databaseID+"]: Opened camera and configured to " + proc.getConfigNow().setupID);
		
	}
	
	/** Prepare the source for acquisition
	 * @return true if all is OK, false if it failed and should be restarted
	 * @throws InterruptedException 
	 */
	private boolean attemptAcquisitionPrep(int shotNumber, int shotType) throws InterruptedException{
		long t0,t1;
		
		ImgSource connectedSource = proc.getConnectedSource();
		AcquisitionDevice acq = (AcquisitionDevice)connectedSource;
		
		//trigger abort to stop and clear everything
		connectedSource.broadcastEvent(Event.GlobalAbort);
		proc.logWrite("InShot: Triggered global abort (as preparation)");			
		Thread.sleep(postTriggerDelay);
			
		//set new shot number
		connectedSource.getCompleteSeriesMetaDataMap().clear();
		connectedSource.setSeriesMetaData("gmds/experiment", "AUG", false);
		connectedSource.setSeriesMetaData("gmds/pulse", (Integer)databaseID, false);
		connectedSource.setSeriesMetaData("aug/pulse", (Integer)shotNumber, false);
		connectedSource.setSeriesMetaData("aug/shotType", AugPilot.shotTypeNames[shotType], false);
		OperationConfig cfg = proc.getLastSetConfig();
		connectedSource.setSeriesMetaData("aug/config", cfg == null ? "NULL" : cfg.name, false);				
		proc.logWrite("InShot["+databaseID+"]: Cleared metadata and set aug/pulse = " + shotNumber + 
							", type = " + AugPilot.shotTypeNames[shotType] +
							", config = " + cfg);
		
		//clear the 'flags'
		connectedSource.setSeriesMetaData("flags/names", AugPilot.flagNames, false);
		connectedSource.setSeriesMetaData("flags/vals", new int[AugPilot.flagNames.length], false);
		if(shotType == AugPilot.SHOT_TYPE_PLASMA)
			AugPilot.setFlag(connectedSource, "plasma", 1);
			
		try{ //Preparation loop
			proc.logWrite("InShot["+databaseID+"]: Triggering start");
			connectedSource.broadcastEvent(Event.GlobalStart);
			t0 = System.currentTimeMillis();
			t1 = t0;
			
			proc.logWrite("InShot["+databaseID+"]: Waiting for camera to start");
			//wait for the source to do something
			while(connectedSource.isIdle() && (t1 - t0) < cameraAcquireStartTimeoutMS){
				t1=System.currentTimeMillis();
				Thread.sleep(busyPollDelayMS);
			}
			status = "Camera acquire ready";
						
			//The camera module is now busy, but it'll take it a little while to configure the camera
			//until which it won't say it's ready. So wait for it to be ready (or time out)
			t0 = System.currentTimeMillis();
			t1 = t0;				
			while((t1 - t0) < cameraAcquireStartTimeoutMS){
				
				if(acq.getAcquisitionStatus() == Status.awaitingHardwareTrigger || acq.getAcquisitionStatus() == Status.capturing ){ 
					//ok, it's ready, drop out of the retry loop
					status = "Camera acquire ready";
					
					proc.logWrite("InShot["+databaseID+"]: Acquire ready (running, or armed waiting for H/W trigger)");					
					return true;  //We're done, all is well
				}
				
				t1=System.currentTimeMillis();
				Thread.sleep(busyPollDelayMS);
			}
			
			
		}catch(RuntimeException err){
			proc.logWrite("InShot["+databaseID+"]: Caught Exception during camera set-up, dropping through to reset camera: " + err);					
			err.printStackTrace();
		}
		
		//more hackery for Andor camera at AUG. This should be a general thing ImgSource
		
		//config didn't engage or camera is otherwise not armed, reset it				
		proc.logWrite("InShot["+databaseID+"]: ERROR: ** AcquisitionDevice was NOT ready to acquire! **" + acq.getAcquisitionStatusString());
		proc.logWrite("InShot["+databaseID+"]: acq.getAcquisitionStatus() = " + acq.getAcquisitionStatus());
		proc.logWrite("InShot["+databaseID+"]: acq.getAcquisitionStatusString() = " + acq.getAcquisitionStatusString());
		connectedSource.setSeriesMetaData("inshot/status", "AcquireNotReady", false);
		AugPilot.setFlag(proc.getConnectedSource(), "cameraOtherFailure", 1);
		
		return false;
	}
	
	private void attemptSave() throws InterruptedException {
		ImgSource connectedSource = proc.getConnectedSource();
		
		status = "Saving";
		proc.updateAllControllers();
			
		//find the database things directly connected to the source, and trigger a save
		int nSaved = 0;
		for(ImgSink sink : connectedSource.getConnectedSinks()){
			if(sink instanceof DatabaseSink){
				doSinkSave((DatabaseSink)sink, databaseID);
				nSaved++;
			}
		}
		if(nSaved == 0){
			proc.logWrite("InShot["+databaseID+"]: WARNING: No DatabaseSink found, *** IMAGES NOT SAVED ***");
		}
	}
	
	private void doSinkSave(DatabaseSink sink, int id) throws InterruptedException {

		proc.logWrite("InShot["+databaseID+"]: Triggering save on " + sink.getClass().getSimpleName());		
		sink.triggerSave(id);
		
		long t0 = System.currentTimeMillis();
		long t1 = t0;
		
		//wait for the sink to do something
		while(sink.isIdle() && (t1 - t0) < databaseSaveTimeoutMS){
			t1=System.currentTimeMillis();
			Thread.sleep(busyPollDelayMS);
			if(abortProcessing)
				throw new RuntimeException("Processing aborted in doSinkSave()");
		
		}
		proc.logWrite("InShot: Save started");
		
		//and for it to stop doing it
		while(!sink.isIdle() && (t1 - t0) < databaseSaveTimeoutMS){
			t1=System.currentTimeMillis();
			Thread.sleep(busyPollDelayMS);
			if(abortProcessing)
				throw new RuntimeException("Processing aborted in doSinkSave()");
		}
		
		
		if((t1 - t0) >= databaseSaveTimeoutMS){
			proc.logWrite("InShot["+databaseID+"]: Timed out waiting for save on " + sink + ". Aborting it.");
			if(sink instanceof EventReciever)
				((EventReciever)sink).event(Event.GlobalAbort);
			Thread.sleep(postTriggerDelay);
			if(abortProcessing)
				throw new RuntimeException("Processing aborted in doSinkSave()");
		}else{
			proc.logWrite("InShot["+databaseID+"]: Save done on " + sink);
		}		
	}
	
	
	
	/** Run through the sinks doing to run the processing chain */
	private void processChain() throws InterruptedException {
		
		ImgSource source = proc.getConnectedSource();
		if(source instanceof GMDSSource){
			//if the source is the database, rather than a camera, we probably want
			// to write processed data into that databaseID
			int dbSourceID = ((GMDSSource)source).getPulse();
			proc.logWrite("InShot["+databaseID+"].ProcessChain: Source is GMDSSource with ID="+dbSourceID+", so will write outputs there.");
			processChain(proc.getConnectedSource(), dbSourceID); 
		}else{
			processChain(proc.getConnectedSource(), databaseID);
		}
	}
	
	private void processChain(ImgSource base, int databaseID) throws InterruptedException {
		abortProcessing = false;
		
		for(ImgSink sink : base.getConnectedSinks()){
			if(sink == proc) //don't process this AugControl sink
				continue;
			
			if(sink instanceof SeriesProcessorAUG){
				boolean beamsOn = ((SeriesProcessorAUG)sink).configFromNBI(
						new int[]{	
								SeriesProcessorAUG.BEAMSEL_CANTSEE,
								SeriesProcessorAUG.BEAMSEL_CANTSEE,
								SeriesProcessorAUG.BEAMSEL_CANTSEE,
								SeriesProcessorAUG.BEAMSEL_CANTSEE,
								SeriesProcessorAUG.BEAMSEL_AT_LEAST_ONE, 
								SeriesProcessorAUG.BEAMSEL_AT_LEAST_ONE,
								SeriesProcessorAUG.BEAMSEL_AT_LEAST_ONE, 
								SeriesProcessorAUG.BEAMSEL_AT_LEAST_ONE });
				
				//set the 'beams' flag, although this wont actually change the GMDS thing unless its told to write, err...
				AugPilot.setFlag(proc.getConnectedSource(), "beams", beamsOn ? 1 : 0);
				
				if(!beamsOn){
					proc.logWrite("InShot.ProcessChain: SeriesProcessor says there are no beams on. Aborting chain.");
					Thread.sleep(postTriggerDelay);
					continue; //give up on that chain
				}
			}
			
			if(sink instanceof DatabaseSink){
				if(base == proc.getConnectedSource())
					continue; // writers on the lowest level have already been done 
				
				doSinkSave((DatabaseSink)sink, databaseID);
				
			}
			
			if(!sink.isIdle()){
				proc.logWrite("InShot["+databaseID+"].ProcessChain: ImgProcPipe " + sink + " is already processing, skipping!");
				continue;
			}
			
			sink.calc(); //start processing
			

			
			long t0 = System.currentTimeMillis();
			long t1 = t0;
			//wait for the sink to do something
			while(sink.isIdle() && (t1 - t0) < databaseSaveTimeoutMS){
				t1=System.currentTimeMillis();
				Thread.sleep(busyPollDelayMS);
				if(abortProcessing)
					throw new RuntimeException("Processing aborted in processChain()");
			}
			proc.logWrite("InShot["+databaseID+"].ProcessChain: Started processing of ImgProcPipe " + sink);
			
			//and for it to stop doing it
			while(!sink.isIdle() && (t1 - t0) < databaseSaveTimeoutMS){
				t1=System.currentTimeMillis();
				Thread.sleep(busyPollDelayMS);
				if(abortProcessing)
					throw new RuntimeException("Processing aborted in processChain()");
			}
			boolean procPipeComplete = sink.wasLastCalcComplete();
			
			if(!procPipeComplete){
				proc.logWrite("InShot["+databaseID+"].ProcessChain: Sink " + sink + " didn't seem to complete properly. Aborting this chain.");
				sink.abortCalc();
				if(sink instanceof EventReciever) //also trigger a full/chain abort if it supports it
					((EventReciever)sink).event(Event.GlobalAbort);
				Thread.sleep(postTriggerDelay);
				if(abortProcessing)
					throw new RuntimeException("Processing aborted in processChain()");
				continue; //give up on that chain
				
			}else if((t1 - t0) >= databaseSaveTimeoutMS){
				proc.logWrite("InShot.ProcessChain: Timed out waiting for process on " + sink + ". Aborting this chain.");
				sink.abortCalc();
				if(sink instanceof EventReciever) //also trigger a full/chain abort if it supports it
					((EventReciever)sink).event(Event.GlobalAbort);
				Thread.sleep(postTriggerDelay);
				if(abortProcessing)
					throw new RuntimeException("Processing aborted in processChain()");
				continue; //give up on that chain
			}else{
				proc.logWrite("InShot["+databaseID+"].ProcessChain: finished processing sink" + sink);
			}
			
			if(sink instanceof ImgProcPipe){
				
				ImgProcPipe pipe = ((ImgProcPipe)sink);
								
				//now do it's children
				processChain(pipe, databaseID);
				
			}
		}
	}
	
	private String forceProcessSyncObj = "forceProcessSyncObj";
	public void forceProcess(){
		//we might not have our own thread, so run this in it's own processing unit

		abortProcessing = true; //abort any that are running
		//processChain() will not get run until it notices and dies
		
		ImageProcUtil.ensureFinalUpdate(forceProcessSyncObj, new Runnable() {

			@Override
			public void run() {
				try{
					processChain();
				}catch(RuntimeException err){
					proc.logWrite("Error during processing chain: " + err);
					err.printStackTrace();
				}catch(InterruptedException err){ 
					proc.logWrite("Interrupted during processing chain. Aborting it.");
				}
			}
		});
	}

	public void setShotInhibit(boolean inhibit) { this.shotInhibit = inhibit;	}
	public boolean getShotInhibit() { return shotInhibit;	}
	public int getCameraCaptureTimeoutMS() { return cameraCaptureTimeoutMS; }
	public void setCameraCaptureTimeoutMS(int cameraCaptureTimeoutMS) { this.cameraCaptureTimeoutMS = cameraCaptureTimeoutMS;	}
	public int getDatabaseSaveTimeoutMS() { return databaseSaveTimeoutMS; }
	public void setDatabaseSaveTimeoutMS(int databaseSaveTimeoutMS) { this.databaseSaveTimeoutMS = databaseSaveTimeoutMS;	}
	public int getProcessChainDelayMS() { return processChainDelayMS; }
	public void setProcessChainDelayMS(int processChainDelayMS) { this.processChainDelayMS = processChainDelayMS;	}
	public boolean getResetCameraPower() { return resetPowerOnFault;	}
	public void setResetCameraPower(boolean resetCameraPower) { 	this.resetPowerOnFault = resetCameraPower;	}
	public int getCameraOfflineTime() { 	return cameraOfflineTime;	}
	public void setCameraOfflineTime(int cameraOfflineTime) { this.cameraOfflineTime = cameraOfflineTime;	}

	public void setProcessChainInhibit(boolean processChainInhibit) { this.processChainInhibit = processChainInhibit; }
	public boolean getProcessChainInhibit() { return this.processChainInhibit; }

	public void doBulkProcess(int id0, int id1) throws InterruptedException{
		for(int id=id0; id <= id1; id++){
			try{
				GMDSSource gmdsSrc = (GMDSSource)proc.getConnectedSource();
				proc.logWrite("Bulk processing from " + id0 + " to "  +id1 + ", now at " + id);
				String exp = gmdsSrc.getExperiment();
				String path = gmdsSrc.getPath();			
				gmdsSrc.loadTarget(exp, id, path);
	
				long t0 = System.currentTimeMillis();
				long t1 = t0;
				//wait for the sink to do something
				while(gmdsSrc.isIdle() && (t1 - t0) < databaseSaveTimeoutMS){
					t1=System.currentTimeMillis();
					Thread.sleep(busyPollDelayMS);
				}
				proc.logWrite("InShot.BulkProcess: Started loading from source " + gmdsSrc);
	
				//and for it to stop doing it
				while(!gmdsSrc.isIdle() && (t1 - t0) < databaseSaveTimeoutMS){
					t1=System.currentTimeMillis();
					Thread.sleep(busyPollDelayMS);
				}
				proc.logWrite("InShot.BulkProcess: Loading complete from source " + gmdsSrc);
				
				//check there are actually some images 
				int n = gmdsSrc.getNumImages();
				boolean hasImages = false;
				for(int i=0; i < n; i++){
					if(gmdsSrc.getImage(i) != null){
						hasImages = true;
						break;
					}
				}
				if(!hasImages){
					proc.logWrite("InShot.BulkProcess: No images for " + id);
					continue;
				}
				
				processChain(gmdsSrc, id);
				
			}catch(RuntimeException err){
				proc.logWrite("BulkProcess: ERROR: Exception while processing "+id+": " + err);
				err.printStackTrace();
			}
		}

	}
	
	/** Checks the images to see if the camera has failed*/
	public boolean isCameraBroken(){
		final int maxZeroPixels = 50; // [A. Burckhard], no idea how he worked this out
		
		ImgSource src = proc.getConnectedSource();
		Img image = null;
		for(int i=src.getNumImages()-1; i >= 0; i--){
			image = src.getImage(i);
			if(image != null && image.isRangeValid())
				break;
		}
		if(image == null){
			proc.logWrite("InShot: isCameraBroken(): No valid images, assuming camera is ok.");
			return false;
		}
		
		int nZeroPixels = 0;
		try{ 
			ReadLock readLock = image.readLock();
			readLock.lockInterruptibly(); 
			try{
				for(int y=0; y < image.getHeight(); y++)
					for(int x=0; x < image.getWidth(); x++)
						if(image.getPixelValue(readLock, x, y) == 0)
							nZeroPixels++;
			
			}finally{
				readLock.unlock();
			}			
		}catch(InterruptedException e){
			proc.logWrite("InShot: isCameraBroken(): Interrupted, not sure the decision is right.");			
		}	
		
		String str = "InShot: isCameraBroken(): Found " + nZeroPixels + " pixels with value == 0. ";
		if(nZeroPixels >= maxZeroPixels){
			AugPilot.setFlag(proc.getConnectedSource(), "cameraOtherFailure", 1);
			proc.logWrite(str + "** CAMERA IS BROKEN!**");
			return true;
		}else{
			proc.logWrite(str + "Camera seems to be fine.");
			return false;
			
		}
		
	}

}
