package imageProc.pilot.aug;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import imageProc.core.ImageProcUtil;

public class InShotSWTControl {
	private Group swtGroup;
	
	private Label statusLabel;
	private Button forceStartButton; 
	private Button inhibitControlCheckbox;
	private Spinner acquireTimeoutSpinner;
	private Spinner saveTimeoutSpinner;
	private Button resetCameraPowerCheckbox;
	private Spinner powerOffTimeSpinner;
	private Label isCameraBrokenLabel;
	private Button isCameraBrokenButton;
	
	private Spinner processChainDelaySpinner;	
	private Button processChainInhibitCheckbox;
	private Button processChainButton;
	
	private AugPilot proc;
	private InShotControl inShotProc;
	
	public InShotSWTControl(AugPilot augCtrl, Composite parent, int style) {
		this.proc = augCtrl;
		this.inShotProc = proc.getInShotCtrl();
		
        swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("Shot Control");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        swtGroup.setLayout(new GridLayout(5, false));
        
        Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Status:");
        statusLabel = new Label(swtGroup, SWT.NONE);
        statusLabel.setText("Init");
        statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
        
        Label lSC = new Label(swtGroup, SWT.NONE); lSC.setText("Shot controller:");
        inhibitControlCheckbox = new Button(swtGroup, SWT.CHECK);
        inhibitControlCheckbox.setText("Inhibit - (Don't respond to shots)");
        inhibitControlCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
        inhibitControlCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); } });
       
        forceStartButton = new Button(swtGroup, SWT.PUSH);
        forceStartButton.setText("Force shot now");
        forceStartButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
        forceStartButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.forceShot(); }});
        
        
        Label lAT = new Label(swtGroup, SWT.NONE); lAT.setText("Acquire timeout (secs):");
        acquireTimeoutSpinner = new Spinner(swtGroup, SWT.NONE);
        acquireTimeoutSpinner.setValues(inShotProc.getCameraCaptureTimeoutMS()/1000, 1, Integer.MAX_VALUE, 0, 10, 60);
    	acquireTimeoutSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
        acquireTimeoutSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        Label lST = new Label(swtGroup, SWT.NONE); lST.setText("Save timeout (secs):");
        saveTimeoutSpinner = new Spinner(swtGroup, SWT.NONE);
        saveTimeoutSpinner.setValues(inShotProc.getDatabaseSaveTimeoutMS()/1000, 1, Integer.MAX_VALUE, 0, 10, 60);
        saveTimeoutSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
        saveTimeoutSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        Label lRC = new Label(swtGroup, SWT.NONE); lRC.setText("When stuck acquiring:");
        resetCameraPowerCheckbox = new Button(swtGroup, SWT.CHECK);
        resetCameraPowerCheckbox.setSelection(inShotProc.getResetCameraPower());
        resetCameraPowerCheckbox.setText("Cycle camera power");
        resetCameraPowerCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
        resetCameraPowerCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); } });
        
        Label lRC2 = new Label(swtGroup, SWT.NONE); lRC2.setText(" ");
        Label lCT = new Label(swtGroup, SWT.NONE); lCT.setText("Off time (secs):");
        powerOffTimeSpinner = new Spinner(swtGroup, SWT.NONE);
        powerOffTimeSpinner.setValues(inShotProc.getCameraOfflineTime()/1000, 1, Integer.MAX_VALUE, 0, 10, 60);
        powerOffTimeSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 3, 1));
        powerOffTimeSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        Label lPCD = new Label(swtGroup, SWT.NONE); lPCD.setText("Process after (secs):");
        processChainDelaySpinner = new Spinner(swtGroup, SWT.NONE);
        processChainDelaySpinner.setValues(inShotProc.getProcessChainDelayMS()/1000, 1, Integer.MAX_VALUE, 0, 10, 60);
        processChainDelaySpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        processChainDelaySpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        processChainInhibitCheckbox = new Button(swtGroup, SWT.CHECK);
        processChainInhibitCheckbox.setSelection(inShotProc.getProcessChainInhibit());
        processChainInhibitCheckbox.setText("Inhibit");
        processChainInhibitCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        processChainInhibitCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); } });
         
        processChainButton = new Button(swtGroup, SWT.PUSH);
        processChainButton.setText("Force process now");
        processChainButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
        processChainButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.getInShotCtrl().forceProcess(); }});
        
        Label lCB = new Label(swtGroup, SWT.NONE); lCB.setText("Camera looks broken: ");
        isCameraBrokenLabel = new Label(swtGroup, SWT.NONE); isCameraBrokenLabel.setText("Not checked yet.");
        
        isCameraBrokenButton = new Button(swtGroup, SWT.PUSH);
        isCameraBrokenButton.setText("Check now");
        isCameraBrokenButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 3, 1));
        isCameraBrokenButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { isCameraBrokenButtonEvent(event); }});
        
        ImageProcUtil.addRevealWriggler(swtGroup);
       
	}

	protected void isCameraBrokenButtonEvent(Event event) {
		isCameraBrokenLabel.setText(proc.getInShotCtrl().isCameraBroken() ? "Yes" : "No");
	}

	protected void settingsChangingEvent(Event event) {
		inShotProc.setShotInhibit(inhibitControlCheckbox.getSelection());
		inShotProc.setCameraCaptureTimeoutMS(acquireTimeoutSpinner.getSelection()*1000);
		inShotProc.setDatabaseSaveTimeoutMS(saveTimeoutSpinner.getSelection()*1000);
		inShotProc.setProcessChainDelayMS(processChainDelaySpinner.getSelection()*1000);
		inShotProc.setProcessChainInhibit(processChainInhibitCheckbox.getSelection());
		inShotProc.setResetCameraPower(resetCameraPowerCheckbox.getSelection());	
		inShotProc.setCameraOfflineTime(powerOffTimeSpinner.getSelection()*1000);
		
	}
	
	public void doUpdate() {
		statusLabel.setText(proc.getInShotCtrl().getStatus());	
	}

	public Control getSWTGroup() { return swtGroup; }
}
