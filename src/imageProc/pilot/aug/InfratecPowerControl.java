package imageProc.pilot.aug;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import imageProc.core.GeneralController;
import imageProc.core.PowerControl;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.RandomManager;
import otherSupport.SettingsManager;

/** Control for the power switch */
public class InfratecPowerControl implements PowerControl {

	private AugPilot proc;
	
	private int maxOutlets = 8;
	
	private String status = "Init";
	
	private long sessionID = -1;
	private HttpURLConnection connection;
	
	private String host;
	private String username;
	private String password;
		
	private int powerState[] = new int[0];
	private String names[] = new String[0];
	
	/** Power sockets (bitfield) to reset when the camera times out */
	private int faultResetPowerOutlets = 0;

	private boolean inhibit = false;
	
	public InfratecPowerControl(AugPilot augControl) {
		this.proc = augControl;
		
		host = SettingsManager.defaultGlobal().getProperty("imageProc.augControl.ips.host", "powth-vsw17.aug.ipp.mpg.de");
		username = SettingsManager.defaultGlobal().getProperty("imageProc.augControl.ips.username", "imse");
		password = SettingsManager.defaultGlobal().getProperty("imageProc.augControl.ips.password", "");
		faultResetPowerOutlets = Algorithms.mustParseInt(SettingsManager.defaultGlobal().getProperty("imageProc.augControl.ips.faultResetOutlets", "15"));
	}

	private HashMap<String, Object> get(String query){
		try{
			//if(connection == null){
				URL url = new URL("http://" + host + query);
				
				HttpURLConnection con = (HttpURLConnection) url.openConnection();
	
				con.setRequestMethod("GET");
				
				int responseCode = con.getResponseCode();
				
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
		 
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				
				HashMap<String, Object> responseFields = new HashMap<String, Object>();
				String parts[] = response.toString().split("&");
				for(String part : parts){
					String bits[] = part.split("=");
					if(bits.length > 1){
						Object o;
						try{
							o = Long.parseLong(bits[1]);
						}catch(NumberFormatException err){
							o = bits[1];
						}
						responseFields.put(bits[0], o);
					}
				}
				
				if(responseFields.containsKey("LogOut") && (Long)responseFields.get("LogOut") == 1){
					sessionID = -1;
					names = new String[0];
					powerState = new int[0];
					status = "Logged out";
					proc.logWrite("Power: Request '"+query+"' returned 'Logged out'");
					proc.updateAllControllers();
					return null;
				}
				
				return responseFields;
			//}
		} catch (Exception err) {
			status = "Error: " + err;
			throw new RuntimeException(err); 
		}
	}
	
	public void login() {
		if(inhibit)
			return;
		
		status = "Logging in";
		proc.logWrite("Power: Logging in");
		String truncPass = password.length() <= 8 ? password : password.substring(0, 8);
		
		int z = (int)RandomManager.instance().nextUniform(0, 10000); //erm... what?
		
		HashMap<String, Object> ret = get("/li?u="+username+"&p="+truncPass+"&Z="+z); 
		if(ret == null)
			return;
		
		if(!ret.containsKey("LI") || (Long)ret.get("LI") != 1 || !ret.containsKey("SessionID")) {
			status = "Login failed";
			if(ret.containsKey("InUse") && (Long)ret.get("InUse") == 1){
				proc.logWrite("Power: ERROR: Login failed, someone else logged in");
				status += ": another user logged in";
			}else{
				proc.logWrite("Power: ERROR: Login failed, unknown reason. Username/Password correct?");
			}
			sessionID = -1;
			powerState = new int[0];
			names = new String[0];
			proc.updateAllControllers();
			return;
		}
		
		sessionID = (Long)ret.get("SessionID");
		status = "Logged in";
		proc.logWrite("Power: Logged in");
		
		getState();
		
	}
	
	public void logout() {
		if(inhibit)
			return;
		
		HashMap<String, Object> ret = get("/lo?Z="+sessionID); 
		if(ret == null)
			return;
		
		if(!ret.containsKey("LO") || (Long)ret.get("LO") != 1) {
			status = "Logout failed";
			proc.logWrite("Power: Logout failed");
			proc.updateAllControllers();
			return;
		}

		sessionID = -1;
		status = "Logged out";
		proc.logWrite("Power: Logged out");
		powerState = new int[0];
		names = new String[0];
		proc.updateAllControllers();
	}
	
	@Override
	public boolean isOn(String id) {
		if(id == null) {
			boolean everythingOn = false;
			for(int i=0; i < powerState.length; i++) 
				everythingOn &= (powerState[i] != 0);
			return everythingOn;
		}
		return powerState[Integer.parseInt(id)] != 0;
	}
	
	public void getState(){
		if(inhibit)
			return;
		
		HashMap<String, Object> ret = get("/ou?Z="+sessionID); 
		if(ret == null)
			return;
		
		if(!ret.containsKey("OU") || (Long)ret.get("OU") != 1
				|| !ret.containsKey("Rights") || !ret.containsKey("State")) {
			status = "getState failed";
			proc.updateAllControllers();
			return;
		}
		
		processStatus(ret);
		
	}
	
	private void processStatus(HashMap<String, Object> ret){

		long rights = (Long)ret.get("Rights"); //really??
		long state = (Long)ret.get("State");
		
		int nOutlets = 0*0;
		for(int i=0; i < maxOutlets; i++){
			int bitSel = (1 << i);
			if((rights & bitSel) != 0){
				nOutlets = i+1;
			}
		}
		
		powerState = new int[nOutlets];
		names = new String[nOutlets];
		
		StringBuffer strBuff = new StringBuffer();
		for(int i=0;i < nOutlets; i++){
			names[i] = (String)ret.get("N"+(i+1));
			if(names[i] == null)
				names[i] = "??_"+i;
			
			strBuff.append((i+1)+" '"+names[i]+"'=");
			
			int bitSel = (1 << i);
			if((rights & bitSel) == 0){
				powerState[i] = -1;
				strBuff.append("N/A");
			}else{
				powerState[i] = ((state & bitSel) != 0) ? 1 : 0;
				strBuff.append((powerState[i] != 0) ? "On" : "Off");
			}
			strBuff.append(", ");
		}
		
		proc.logWrite(strBuff.toString());
		
		proc.updateAllControllers();
	}
	
	public void setState(int outletNumber, boolean state){
		if(inhibit)
			return;
		
		proc.logWrite("Power: Turning outlet "+(outletNumber+1)+ (state ? " on" : " off"));
		
		//reply needs OU=1 then processStatus
		HashMap<String, Object> ret = get("/ou?N="+(outletNumber+1)+"&S="+(state?"1":"0")+"&Z="+sessionID); 
		if(ret == null)
			return;
		
		if(!ret.containsKey("OU") || (Long)ret.get("OU") != 1
				|| !ret.containsKey("Rights") || !ret.containsKey("State")) {
			status = "getState failed";
			proc.updateAllControllers();
			return;
		}
		
		processStatus(ret);
	}
	
	public void setAll(boolean state){
		if(inhibit)
			return;
		
		proc.logWrite("Power: Turning all outlets "+ (state ? " on" : " off"));
		
		HashMap<String, Object> ret = get("/ou?"+(state?"E":"A")+"=1&Z="+sessionID); 
		if(ret == null)
			return;
		
		if(!ret.containsKey("OU") || (Long)ret.get("OU") != 1
				|| !ret.containsKey("Rights") || !ret.containsKey("State")) {
			status = "setAll failed";
			proc.updateAllControllers();
			return;
		}
		
		processStatus(ret);
	}
	

	/** Makes sure the camera is powered up 
	 * @return True if the power anything was turned on (previously off)*/
	@Override
	public boolean switchOn(String id) {
		if(inhibit)
			return false;
		
		return checkState(faultResetPowerOutlets, true);
	}
	
	@Override
	public boolean switchOff(String id) {
		if(inhibit)
			return false;
		
		return checkState(faultResetPowerOutlets, false);
	}
		
	public boolean checkState(int bitField, boolean toOn){
		
		if(sessionID < 0)
			login();
		
		if(sessionID < 0){ //didn't work
			throw new RuntimeException("Login to IPS failed");
		}
		
		getState(); //just to log the existing state
		
		boolean switched = false;
		for(int i=0; i < maxOutlets; i++){
			if(((bitField & (1 << i)) != 0) && 
					(toOn && powerState[i] == 0 || !toOn && powerState[i] != 0)){
				setState(i, toOn);
				switched = true;				
			}
		}
		
		logout();
		
		return switched;
	}
	
	
	public int getMaxOutlets() {		return maxOutlets;	}
	public void setMaxOutlets(int maxOutlets) {		this.maxOutlets = maxOutlets;	}
	public String getHost() {		return host;	}
	public void setHost(String host) {		this.host = host;	}
	public String getUsername() {		return username;	}
	public void setUsername(String username) {		this.username = username;	}
	public String getPassword() {		return password;	}
	public void setPassword(String password) {		this.password = password;	}
	public String getStatus() {		return status;	}
	public int[] getPowerState() {		return powerState;	}
	public String[] getNames() {		return names;	}
	public int getFaultResetOutlets() { 	return faultResetPowerOutlets;	}
	public void setFaultResetOutlets(int cameraPowerOutlet) {		this.faultResetPowerOutlets = cameraPowerOutlet;	}

	public void setInhibit(boolean inhibit) { this.inhibit = inhibit; }

}
