package imageProc.pilot.aug;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import imageProc.core.ImageProcUtil;

import imageProc.core.GeneralController;

public class InfratecPowerSWTControl implements GeneralController {
	private Group swtGroup;
	
	private Label statusLabel;
	private Button inhibitControlCheckbox;
	
	private Text hostTextbox;
	private Text usernameTextbox;
	private Text passwordTextbox;
	private Button loginButton;
	private Button logoutButton;
	private Button allOffButton;
	private Button allOnButton;
	private Button outletCheckboxs[];
	
	private Spinner faultResetOutlets;
	
	private AugPilot proc;
	private InfratecPowerControl ipsCtrl;
	
	
	public InfratecPowerSWTControl(AugPilot proc, Composite parent, int style) {
		this.proc = proc;
		this.ipsCtrl = (InfratecPowerControl)proc.getPowerControl();
		
        swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("IPS Power Switch");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        swtGroup.setLayout(new GridLayout(8, false));
        
        Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Status:");
        statusLabel = new Label(swtGroup, SWT.NONE);
        statusLabel.setText("Init");
        statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 7, 1));
        
        Label lSC = new Label(swtGroup, SWT.NONE); lSC.setText(" ");
        inhibitControlCheckbox = new Button(swtGroup, SWT.CHECK);
        inhibitControlCheckbox.setText("Inhibit - (Don't touch the power)");
        inhibitControlCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 7, 1));
        inhibitControlCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { inhibitCheckboxEvent(event); } });
       
        Label lH = new Label(swtGroup, SWT.NONE); lH.setText("Host:");
        hostTextbox = new Text(swtGroup, SWT.NONE);
        hostTextbox.setText(ipsCtrl.getHost());
        hostTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 7, 1));
        
        Label lU = new Label(swtGroup, SWT.NONE); lU.setText("Username:");
        usernameTextbox = new Text(swtGroup, SWT.NONE);
        usernameTextbox.setText(ipsCtrl.getUsername());
        usernameTextbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
        
        Label lP = new Label(swtGroup, SWT.NONE); lP.setText("Password:");
        passwordTextbox = new Text(swtGroup, SWT.PASSWORD);
        passwordTextbox.setText(ipsCtrl.getPassword());
        passwordTextbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
        
        loginButton = new Button(swtGroup, SWT.PUSH);
        loginButton.setText("Log in");
        loginButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { loginEvent(event); } });
        loginButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
        
        logoutButton = new Button(swtGroup, SWT.PUSH);
        logoutButton.setText("Log out");
        logoutButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { logoutEvent(event); } });
        logoutButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));

        allOnButton = new Button(swtGroup, SWT.PUSH);
        allOnButton.setText("All on");
        allOnButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { allStateEvent(event, true); } });
        allOnButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));

        allOffButton = new Button(swtGroup, SWT.PUSH);
        allOffButton.setText("All off");
        allOffButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { allStateEvent(event, false); } });
        allOffButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));

        Label lCP = new Label(swtGroup, SWT.NONE); lCP.setText("Fault reset outlets (bitfield):");
        faultResetOutlets = new Spinner(swtGroup, SWT.NONE);
        faultResetOutlets.setValues(ipsCtrl.getFaultResetOutlets(), -1, 65535, 0, 1, 4);
        faultResetOutlets.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 7, 1));
        faultResetOutlets.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { cameraSocketEvent(event); }});
       
        
        int n = ipsCtrl.getMaxOutlets();
        outletCheckboxs = new Button[n];
        for(int i=0; i < n; i++){
        	
        	outletCheckboxs[i] = new Button(swtGroup, SWT.CHECK);
        	outletCheckboxs[i].setSelection(false);
        	outletCheckboxs[i].setEnabled(false);
        	outletCheckboxs[i].setText("UNKNOWN "+i);
        	outletCheckboxs[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
            
        	final int num = i;
        	outletCheckboxs[i].addListener(SWT.Selection, new Listener() {
        		@Override
        		public void handleEvent(Event event) { 
        			outletStateEvent(event, num); 
        		} 
        	});
        }

        ImageProcUtil.addRevealWriggler(swtGroup);
        
	}

	protected void inhibitCheckboxEvent(Event event) {
		ipsCtrl.setInhibit(inhibitControlCheckbox.getSelection());
	}

	protected void cameraSocketEvent(Event event) {
		ipsCtrl.setFaultResetOutlets(faultResetOutlets.getSelection());
	}

	protected void loginEvent(Event event) {
		ipsCtrl.setHost(hostTextbox.getText());
		ipsCtrl.setUsername(usernameTextbox.getText());
		ipsCtrl.setPassword(passwordTextbox.getText());
		ipsCtrl.login(); 
	}
	protected void logoutEvent(Event event) { ipsCtrl.logout();  }
	
	protected void outletStateEvent(Event event, int num) { 
		ipsCtrl.setState(num, outletCheckboxs[num].getSelection());
	}
	
	protected void allStateEvent(Event event, boolean state) { 
		ipsCtrl.setAll(state);
	}


	public void doUpdate() {
		statusLabel.setText(ipsCtrl.getStatus());
		
		int state[] = ipsCtrl.getPowerState();
		String names[] = ipsCtrl.getNames(); 
		int n = ipsCtrl.getMaxOutlets();
	    for(int i=0; i < n; i++){
	    	if(state.length <= i){
	    		outletCheckboxs[i].setEnabled(false);
	    		outletCheckboxs[i].setSelection(false);	
	    		continue;
	    	}
	    	outletCheckboxs[i].setText(names[i]);
	    	if(state[i] == 0){
	    		outletCheckboxs[i].setEnabled(true);
	    		outletCheckboxs[i].setSelection(false);
	    	}else if(state[i] == 1){
	    		outletCheckboxs[i].setEnabled(true);
	    		outletCheckboxs[i].setSelection(true);	    		
	    	}else{
	    		outletCheckboxs[i].setEnabled(false);
	    		outletCheckboxs[i].setSelection(false);
	    	}
	    }
	}

	public Control getSWTGroup() { return swtGroup; }

	@Override
	public Object getInterfacingObject() { return swtGroup;	}

	@Override
	public void generalControllerUpdate() { doUpdate(); 	}

	@Override
	public void destroy() { }

	@Override
	public Object getControlledObject() { return ipsCtrl; }
}
