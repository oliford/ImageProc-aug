package imageProc.pilot.aug;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class MulticastTriggerSend {
	
	public static void main(String[] args) throws IOException {
		String parts[] = "blah  merp".split("\\s+");
		System.out.println(parts[0]+"|"+parts[1]);
	System.exit(-1);
		int port = 14300;
		// Which address
		String group = "230.183.52.4";
		// Which ttl
		int ttl = 1;
		
		// Create the socket but we don't bind it as we are only going to send data
		MulticastSocket s = new MulticastSocket();
		
		// Note that we don't have to join the multicast group if we are only
		// sending data and not receiving
		// Fill the buffer with some data
		byte buf[] = "TestMulticast".getBytes();
		
		
		// Create a DatagramPacket 
		DatagramPacket pack = new DatagramPacket(buf, buf.length,
							 InetAddress.getByName(group), port);
		// Do a send. Note that send takes a byte for the ttl and not an int.
	    s.setTimeToLive(ttl);
		s.send(pack);
		
		// And when we have finished sending data close the socket
		s.close();
	}
}
