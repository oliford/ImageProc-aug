package imageProc.pilot.aug;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

/** Code for waiting for and interpreting AUG Multicast UDP trigger packets.
 * 
 */ 
public class MulticastTriggerWaiter implements Runnable {
	
	
	private AugPilot augCtrl; 
	
	private int mcastPort;
	private String mcastSubnet;
	
	private Thread waiterThread = null;
	
	private HashMap<String, Long> lastTrigger = new HashMap<String, Long>();
		
	private final static SimpleDateFormat timestampFormat = new SimpleDateFormat("dd/MM HH:mm:ss.SSS");
	
	private boolean death = false; 
	
	public MulticastTriggerWaiter(AugPilot augCtrl) {
		mcastPort = Algorithms.mustParseInt(SettingsManager.defaultGlobal().getProperty("imageProc.augControl.multicastTriggers.port", "14300"));
		mcastSubnet = SettingsManager.defaultGlobal().getProperty("imageProc.augControl.multicastTriggers.subnet", "230.183.52.");
		
		this.augCtrl = augCtrl;
	}
	
	public static final String allTriggers[] = {
			 "DVEZ", 
			 "TS00", //start OH load
			 "DV00", // t - ~15s
			 "DV01", "DV1", // t- ~10s			  
			 "TS11","TS02","BegI.TF","TS05","TS04",
			 "DV02","DV2",  // t- ~1s		
			 "DV03",  // t- ~100ms		
			 "PhyReset",
			 "TS06","TS6","TS07","BegIpl", //all at start of shot
			 "BegVPE", // near end of shot, start of rampdown??
			 
			 "TS08","TS8", // end PF (immediately after shot)
			 "BegPST",
			 "TS09", //end of TF, > 10s after shot	
	};
	
	public static final String usefulTriggers[] = {"DVEZ", "TS06", "BegVPE", "TS08", "TS09"};
	
	public void start(){
		if(waiterThread != null){
			kill();
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) { }
		}
			//throw new RuntimeException("Thread already running");
		
		death = false;
		waiterThread = new Thread(this);
		waiterThread.start();
	}
	
	public void kill(){
		if(waiterThread == null)
			return;
		death = true;
		waiterThread.interrupt();		
	}
	
	public void run() {
		
		MulticastSocket socket = null;
		try{
			String mcastAddress = mcastSubnet + "255"; //mcast group for all triggers
			InetAddress addr = Inet4Address.getByName(mcastAddress);
			
			InetSocketAddress bindAddr = new InetSocketAddress("0.0.0.0", mcastPort);
			socket = new MulticastSocket(bindAddr);
			
			socket.setSoTimeout(100);			
			socket.joinGroup(addr);
			
			augCtrl.logWrite("MulticastTriggers: Waiting for multicast packets on " + mcastAddress + ":" + mcastPort);
			
			byte buf[] = new byte[1024];
						
			while(!death){					
				DatagramPacket packet = new DatagramPacket(buf, buf.length, addr, mcastPort);
				try{
					socket.receive(packet);
				}catch(SocketTimeoutException toutErr){ 
					continue;
				}
				
				String content = new String(buf, 0, packet.getLength());
				
				try{
					
					String parts[] = content.split("\\s+");
					String triggerID = parts[0];
					long timestampNano = Long.parseLong(parts[1]);
					long timestampMilli = timestampNano / 1000000L;
					
					synchronized (lastTrigger) {
						lastTrigger.put(triggerID, timestampMilli);
					}
					
					augCtrl.logWrite("MulticastTriggers: Got trigger '"+triggerID+"', sent at " + 
											timestampFormat.format(timestampMilli));
					
				}catch(RuntimeException err){
					augCtrl.logWrite("MulticastTriggers: Error processing trigger packet content: " + err);
					
					StringBuffer s = new StringBuffer(512);
					s.append("MulticastTriggers: Packet content was: ");
					byte b[] = content.getBytes();
					for(int i=0 ; i < b.length; i++){
						s.append(b[i] + " ");
					}
					augCtrl.logWrite(s.toString());					
				}
			}
			
			socket.leaveGroup(addr);
			
			augCtrl.logWrite("MulticastTriggers: Ending");			

		}catch(IOException err){
			System.err.println("MulticastTriggers: ERROR: " + err);
			err.printStackTrace();
			
		}finally {
			if(socket != null)
				socket.close();
		}
		
	}
	

	public void clear(){
		synchronized (lastTrigger) {
			lastTrigger.clear();			
		}
	}
	
	public boolean hasHadTrigger(String triggerID){
		return lastTrigger.get(triggerID) != null;
	}
	
}
