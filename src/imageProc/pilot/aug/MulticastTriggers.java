package imageProc.pilot.aug;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

/** Code for waiting for and interpreting AUG Multicast UDP trigger packets */ 
public class MulticastTriggers {
	
	private int mcastPort;
	private String mcastSubnet;
		
	
	/* Von der SSR definierte Trigger sind:
	Diag_Vorbereitung_1,
	Diag_Vorbereitung_2,
	TS4_PF_Fenster_On,
	TS6_OH_Ignit */
	
	/* Der numerische Wert der Trigger wird verwendet, um die IP_addresse 
	der zum Trigger gehoerenden Multicastgruppe zu erzeugen.
	230.183.52.3(= MC_TS4) */
	

	private static final int MC_NOWAIT = 0;
	private static final int MC_DV1 = 1;
	private static final int MC_DV2 = 2;
	private static final int MC_TS4 = 3;
	private static final int MC_TS6 = 4;
	private static final int MC_TS8 = 5;
	
	private static final int PB_TECH_RESET = 16;
	private static final int PB_BEG_SHOT = 17;
	private static final int PB_TS01 = 18;
	private static final int PB_DVEZ = 19;
	private static final int PB_BEG_GEN = 20;
	private static final int PB_TS00 = 21;
	private static final int PB_DV00 = 22;
	private static final int PB_DV01 = 23;
	private static final int PB_TS11 = 24;
	private static final int PB_TS02 = 25;
	private static final int PB_BEG_I_TF = 26;
	private static final int PB_TS05 = 27;
	private static final int PB_TS04 = 28;
	private static final int PB_DV02 = 29;
	private static final int PB_DV03 = 30;
	private static final int PB_TS06 = 31;
	private static final int PB_TS07 = 32;
	private static final int PB_BEG_IPL = 33;
	private static final int PB_BEG_VPE = 34;
	private static final int PB_END_VPE = 35;
	private static final int PB_TS08 = 36;
	private static final int PB_TS09 = 37;
	private static final int PB_TS10 = 38;
	private static final int PB_END_SHOT = 39;
	private static final int PB_BEG_PST = 40;
	
	private static final int SHOTNR = 254;
	private static final int MC_ANY = 255;
	
	private int triggerSelect = MC_ANY; 

	private final static SimpleDateFormat timestampFormat = new SimpleDateFormat("dd/MM HH:mm:ss.SSS");
	
	public MulticastTriggers() {
		mcastPort = Algorithms.mustParseInt(SettingsManager.defaultGlobal().getProperty("imageProc.augControl.multicastTriggers.port", "14300"));
		mcastSubnet = SettingsManager.defaultGlobal().getProperty("imageProc.augControl.multicastTriggers.subnet", "230.183.52.");
	}
	
	public void waitForTriggers(){
		
		MulticastSocket socket = null;
		try{
			String mcastAddress = mcastSubnet + triggerSelect;
			InetAddress addr = Inet4Address.getByName(mcastAddress);
			
			InetSocketAddress bindAddr = new InetSocketAddress("0.0.0.0", mcastPort);
			socket = new MulticastSocket(bindAddr);
			
			//NetworkInterface iface = NetworkInterface.getByName("eth0");
			//InetSocketAddress mAddr = new InetSocketAddress(mcastAddress, mcastPort);
			//socket.joinGroup(mAddr, iface);
			socket.joinGroup(addr);
			
			Calendar now = Calendar.getInstance();
			String timeStamp = timestampFormat.format(now.getTime());
			System.out.println(timeStamp + ": Waiting for multicast packets on " + mcastAddress + ":" + mcastPort);
			
			byte buf[] = new byte[1024];
			
			while(true){
				DatagramPacket packet = new DatagramPacket(buf, buf.length, addr, mcastPort);
				socket.receive(packet);
				
				now = Calendar.getInstance();
				timeStamp = timestampFormat.format(now.getTime());
				
				String content = new String(buf, 0, packet.getLength());
				System.out.println(timeStamp + ": Received packet length " +packet.getLength() +":");
				System.out.println("   Content = |" + content + "|");
				
				try{
					System.out.print("   Bytes = ");
					byte b[] = content.getBytes();
					for(int i=0 ; i < b.length; i++){
						System.out.print(b[i] + " ");
					}
					System.out.println();
					
					
					String parts[] = content.split("\\s+");
					String triggerID = parts[0];
					long timestampNano = Long.parseLong(parts[1]);
					
					System.out.println("   Trigger = |" + triggerID + "|");
					System.out.println("   Timestamp/ns = " + timestampNano);
								
					long timestampMilli = timestampNano / 1000000L; 
					
					System.out.println("   Date = " + timestampFormat.format(timestampMilli));
					
				}catch(RuntimeException err){
					System.out.println("Error processing trigger packet content: " + err);
				}
			}
			

		}catch(IOException err){
			System.err.println("ERROR: Exception waiting for trigger: " + err);
			err.printStackTrace();
		}finally {
			if(socket != null)
				socket.close();
		}
		
	}
	
	public static void main(String[] args) {
		new SettingsManager("minerva", true);
		(new MulticastTriggers()).waitForTriggers();
	}
}
