package imageProc.pilot.aug;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import imageProc.core.ImageProcUtil;

public class ProcessingSWTControl {
	private Group swtGroup;
	
	private Label statusLabel;
	private Button processChainButton;
	
	private AugPilot proc;
	
    private Spinner procShot0Spinner;
	private Spinner procShot1Spinner;
	private Button bulkProcessButton;
	private Text configToText;
	private Button configToButton;

	public ProcessingSWTControl(AugPilot augCtrl, Composite parent, int style) {
		this.proc = augCtrl;
		
        swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("Processing");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        swtGroup.setLayout(new GridLayout(5, false));
        
        processChainButton = new Button(swtGroup, SWT.PUSH);
        processChainButton.setText("Process now");
        processChainButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 5, 1));
        processChainButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.getInShotCtrl().forceProcess(); }});
        
        Label lPS0 = new Label(swtGroup, SWT.NONE); lPS0.setText("With GMDSSource: From:");
        procShot0Spinner = new Spinner(swtGroup, SWT.NONE);
        procShot0Spinner.setValues(1000, 0, Integer.MAX_VALUE, 0, 1, 10);
        procShot0Spinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));

        Label lPS1 = new Label(swtGroup, SWT.NONE); lPS1.setText("to:");
        procShot1Spinner = new Spinner(swtGroup, SWT.NONE);
        procShot1Spinner.setValues(1000, 0, Integer.MAX_VALUE, 0, 1, 10);
        procShot1Spinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        
    	bulkProcessButton = new Button(swtGroup, SWT.PUSH);
    	bulkProcessButton.setText("Bulk Process");
    	bulkProcessButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
    	bulkProcessButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { bulkProcessButtonEvent(event); } });
    	
    	Label lCT1 = new Label(swtGroup, SWT.NONE); lCT1.setText("Configure all processors to:");
        configToText = new Text(swtGroup, SWT.NONE);
        configToText.setText("AUG/1000");
        configToText.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
         
        configToButton = new Button(swtGroup, SWT.PUSH);
        configToButton.setText("Config now");
        configToButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        configToButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { bulkConfigButtonEvent(event); } });
       
        ImageProcUtil.addRevealWriggler(swtGroup);
     	
         
	}

	protected void bulkProcessButtonEvent(Event event) {
		proc.bulkProcess(procShot0Spinner.getSelection(), procShot1Spinner.getSelection());
	}
	
	protected void bulkConfigButtonEvent(Event event) {	
		String parts[] = configToText.getText().split("/");
		String exp = parts[0];
		int id= parts.length < 2 ? -1 : Integer.parseInt(parts[1]);
		
		proc.setupToConfig(exp, id);
	}
	
	protected void settingsChangingEvent(Event event) {
		
	}
	
	public void doUpdate() {
		statusLabel.setText(proc.getStatus());	
	}

	public Control getSWTGroup() { return swtGroup; }
}
