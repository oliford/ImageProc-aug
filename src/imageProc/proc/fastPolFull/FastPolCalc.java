package imageProc.proc.fastPolFull;

import java.util.List;

import algorithmrepository.ExtrapolationMode;
import algorithmrepository.Interpolation1D;
import algorithmrepository.InterpolationMode;
import algorithmrepository.interfaces.Interpolation2D;
import aug.equi.AUGEquilibriumData;
import fusionOptics.Util;
import fusionOptics.drawing.VRMLDrawer;
import fusionOptics.tracer.Tracer;
import fusionOptics.types.Intersection;
import fusionOptics.types.Pol;
import fusionOptics.types.RaySegment;
import imageProc.core.ImgSource;
import imageProc.proc.transform.TransformProcessor;
import ipp.aug.neutralBeams.AugNBI;
import ipp.imse.fusionOptics.imse.IMSEOptics135_50_bigLens;
import ipp.imse.fusionOptics.mse.AugGenericMSESystem;
import ipp.imse.fusionOptics.mse.AugMSESystem;
import ipp.imse.fusionOptics.permIMSE.PermIMSE_Current;
import ipp.imse.fusionOptics.permIMSE.PermIMSE_Rotated55;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** Highly simplified pitch and polarisation angles calculation #
 * The calculation bits, stripped from the graph processing thing */
public class FastPolCalc {

	private static final double qe = 1.6e-19; //C
	private static final double protonMass = 1.67e-27; //kg
	private static final double fuelMass = 2 * protonMass; //Deuterium

	public static final String outputModes[] = new String[]{
		"Pitch Angle",
		"Polarisation Angle (vs up of central LOS)",
		"Pol Angle from A-factors (vs up)",
		"Polarisation Angle (vs v x Bphi)",
		"Polarisation Angle (Ray Traced)",
		"BZ",
		"Stark |E| from Bpol",
		"d��/dBr",
		"d��/dBz",
		"���� due to Er",
		"���� due to Br",
		"���� due to Para/diamagnetism",
	};
	
	public final static int OUTPUT_MODE_PITCH = 0;
	public final static int OUTPUT_MODE_POL = 1;
	public final static int OUTPUT_MODE_POL_AFACTORS = 2;
	public final static int OUTPUT_MODE_POL_VS_BPHI = 3;
	public final static int OUTPUT_MODE_POL_RAYTRACED = 4;
	public final static int OUTPUT_MODE_BZ = 5;
	public final static int OUTPUT_MODE_BZ_STARKE_MAG = 6;
	public final static int OUTPUT_MODE_dPol_dBz = 7;
	public final static int OUTPUT_MODE_dPol_dBr = 8;
	public final static int OUTPUT_MODE_ER_EFFECT = 9;
	public final static int OUTPUT_MODE_BR_EFFECT = 10;
	public final static int OUTPUT_MODE_Diagmag_EFFECT = 11;
			
	private String equiExp = "AUGD";
	private String equiDiag = "EQH";

	//config
	private int outputMode = FastPolCalc.OUTPUT_MODE_POL;
		
	public static final int BEAMSEL_AUTO = -1;
	private int beamSelection = BEAMSEL_AUTO;
	
	private AUGEquilibriumData loadedEqui = null;
		
	private double adjustCore = 0, adjustEdge = 0, adjustTop = 0;
	private double adjustCoreR = 1.5;
	private double adjustEdgeR = 2.0;
	private double adjustTopZ = 0.2;

	private int frameBeamIndex;
	private Interpolation1D frameInterpWPhi;
	private double frameBeamVelocity[];
	private double frameFaraday;
	
	/** to get something like the real ray traced behaviour without ray tracing, define a common measurement 'up'
	 *  from the reverse traced optic axis hitting the mirror */	
	private double measurementUp[];
	private double measurementCentralLOS[];
	
	/** Which gets us to the IMSE frame coordinates - where we define the angle usually. These then get to the actual measurement (relative to Savart plate) */
	private double savartAngleSign;
	private double savartToIMSEFrame;
	
	private int angleSign = 1;

	private boolean addRadialField = true;
	private boolean addFaraday = true;
	private boolean inhibitMSE = false;
	private boolean inhibitBPol = false;
	private boolean calcDeltaX = false;
	
	/** Use slower 2D cubic interpolator, which gives  continuous current profile in R */
	private boolean continuousCurrent = false;
	
	private double faraday[];
	private int beamIndices[];
	
	private double beamAxes[][];
	private double beamEnergy[];		
			
	private double fitKnotsRho[];
	private double fitKnotsWTor[][];
	private double beamVelocityFactor;
	
	
	public void prepareMetadata(ImgSource src, String prefix){
		

		faraday = (double[])src.getSeriesMetaData("Faraday/faradayAngle");
		if(addFaraday && faraday == null)
			throw new RuntimeException("No faraday data");
		
		beamIndices = (int[])src.getSeriesMetaData("beamIndex");
		if(beamSelection < 0 && beamIndices == null)
			throw new RuntimeException("No beam indices (active/preference from Transform)");
		
		beamAxes = (double[][])src.getSeriesMetaData(prefix + "/beamAxesUVec");
		beamEnergy = (double[])src.getSeriesMetaData(prefix + "/beamEnergy");		
				
		fitKnotsRho = (double[])src.getSeriesMetaData("RadialField/fitRho"); //sqrt(psiN)
		fitKnotsWTor = (double[][])src.getSeriesMetaData("RadialField/fitWTor"); //in m/s
		if(addRadialField && (fitKnotsRho == null || fitKnotsWTor == null))
			throw new RuntimeException("No radial field data");
				
		//store calc params
		src.setSeriesMetaData(prefix + "/FastPol/Mode", new Integer(outputMode), false);
		src.setSeriesMetaData(prefix + "/FastPol/EquiExp", new String(loadedEqui.getExp()), false);
		src.setSeriesMetaData(prefix + "/FastPol/EquiDiag", new String(loadedEqui.getDiag()), false);
		src.setSeriesMetaData(prefix + "/FastPol/hasFaraday", new Boolean(addFaraday && faraday != null), false);
		src.setSeriesMetaData(prefix + "/FastPol/hasRadialElectricField", new Boolean(addRadialField && fitKnotsWTor != null), false);
		src.setSeriesMetaData(prefix + "/FastPol/beamVelocityFactor", new Double(beamVelocityFactor), false);
		
	}
	
	/** Prepares the measurement geometry, using the definition given by transform in the gammaAfactor 
	 * calculation (whatever that was), so that our calculation should match that based on the gammaAfactors
	 * @param measurementNorm
	 * @param measurementUp
	 */
	public void prepareGeometry(double measurementNorm[], double measurementUp[]){
		this.measurementCentralLOS = measurementNorm;
		this.measurementUp = measurementUp;
		savartAngleSign = 1;
		savartToIMSEFrame = 0;	
	}
	
	/** Prepares the measurement geometry, using the ray tracer if requested */
	public void prepareGeometry(int gmdsPulse, double viewPos[]){
		if(gmdsPulse < 0){
			measurementCentralLOS = null; //invalidate
			measurementUp = null; 
			return;
		}
		
		if(measurementUp == null || true){
			double globalUp[] = new double[]{ 0, 0, 1 };
			
			
			if(outputMode != FastPolCalc.OUTPUT_MODE_POL_RAYTRACED){			
				boolean isPrototype = gmdsPulse < 1000;			
				double pointOnBeam[] = isPrototype ? AugMSESystem.pointOnBeam : PermIMSE_Current.pointOnBeam;
				measurementCentralLOS = Mat.normaliseVector(Mat.subtract(pointOnBeam, viewPos));
				measurementUp = Mat.normaliseVector(Mat.cross3d(Mat.cross3d(measurementCentralLOS, globalUp), measurementCentralLOS));
				savartAngleSign = 1;
				savartToIMSEFrame = 0;				
			}else{
				prepareRaytracedInfo(gmdsPulse);
			}
		}
	}
	
	
	
	public void prepareFrame(int frameIndex){
		frameBeamIndex = (beamSelection >= 0) ? beamSelection : beamIndices[frameIndex];
		double goodBeamEnergy = beamEnergy[frameBeamIndex];
		if(Double.isNaN(goodBeamEnergy))
			goodBeamEnergy = 93000;
		
		double magV = FastMath.sqrt(2 * goodBeamEnergy * qe / fuelMass);
		
		magV *= beamVelocityFactor; //energy component select (effective, from averging by contrast)
		frameBeamVelocity = Util.mul( (beamAxes != null && beamAxes[frameBeamIndex] != null) ? beamAxes[frameBeamIndex] : AugNBI.def().uVec(frameBeamIndex), magV);
		if(Double.isNaN(Mat.veclength(frameBeamVelocity)) || Mat.veclength(frameBeamVelocity) <= 0)
			throw new RuntimeException("Beam velocity is NaN");
		
		if(addRadialField){
			if(fitKnotsWTor == null || fitKnotsWTor.length <= frameIndex || fitKnotsWTor[frameIndex] == null)
				throw new RuntimeException("Invalid/wrongly sized velocity fit from RadialFieldProcess.");
			
			frameInterpWPhi = new Interpolation1D(fitKnotsRho, fitKnotsWTor[frameIndex], InterpolationMode.CUBIC, ExtrapolationMode.EXCEPTION);
		}else{
			frameInterpWPhi = null;
		}
		
		if(addFaraday){
			frameFaraday = faraday[frameIndex];
		}
	}
		
	private void prepareRaytracedInfo(int gmdsPulse){
		double globalUp[] = new double[]{ 0, 0, 1 };
		double targ[] = null;
		
		AugGenericMSESystem sys;
		if(gmdsPulse < 1000){ //protoIMSE
			AugMSESystem protoIMSE = new AugMSESystem(new IMSEOptics135_50_bigLens());
			protoIMSE.setupForPulse(gmdsPulse);
			
			double tubeAxis[] = Mat.normaliseVector(Mat.subtract(protoIMSE.tubeOptics.fibreEnds.getCentre(), protoIMSE.tubeOptics.window1Front.getCentre()));
			double mirrorNormal[] = protoIMSE.mainMirror.getNormal();
			double axisDotNorm = Util.dot(tubeAxis, mirrorNormal);
			double view[] = new double[]{
					tubeAxis[0] - 2 * axisDotNorm * mirrorNormal[0],
					tubeAxis[1] - 2 * axisDotNorm * mirrorNormal[1],
					tubeAxis[2] - 2 * axisDotNorm * mirrorNormal[2],
			};
			
			measurementUp = Util.reNorm(Util.cross(Util.cross(view, globalUp), view));
			savartAngleSign = protoIMSE.imseOptics.savartAngleSign;
			savartToIMSEFrame = protoIMSE.imseOptics.savartToIMSEFrame;
			sys = protoIMSE;
			measurementCentralLOS = sys.getPrimaryRayTarget().getBoundarySphereCentre();
			
		}else{
			PermIMSE_Current permIMSE = new PermIMSE_Current(new IMSEOptics135_50_bigLens());
			permIMSE.setupForPulse(gmdsPulse);
			measurementUp = Util.reNorm(Util.cross(Util.cross(permIMSE.sourceNormal, globalUp), permIMSE.portNormal));
			sys = permIMSE;
			targ = permIMSE.smallTargetDiscPos;
			
		}
	
		//find out what the 'up' at the beam gives as a measurement in 'IMSE Frame' coordinate
		double deltaPolAngle = 5 * Math.PI / 180;
		
		//double centreIsh[] = AugNBI.def().getPosOfBoxAxisAtR(isBox2 ? 1 : 0, 1.80);
		double centreIsh[] = sys.getPointOnBeam(); 
		RaySegment ray = new RaySegment();
		ray.startPos = centreIsh;
		ray.dir = Util.reNorm(Util.minus(targ, centreIsh));		
		ray.E0 = new double[][]{
				{1,0,0,0}, //linear polarised, aligned to up, 
				// and deltaPolAngle further around...
				//   we want the direction of 'linearity' to be +ve for clockwise from the diagnostic's POV
				// which is opposite what the ray tracer defines, as it defines it as +ve for clockwise
				//looking along the ray's direction of travel, so there's a - sign here
				//This definition needs to match the one in the arcTan2(Er,Eu) in calcAng() 
				{FastMath.cos(deltaPolAngle), 0, -FastMath.sin(deltaPolAngle), 0 }, 
				}; 
		ray.up = Util.reNorm(Util.cross(Util.cross(ray.dir, globalUp), ray.dir));;
		ray.wavelength = PermIMSE_Rotated55.wavelen;
		
		Tracer.trace(sys, ray, 1000, 0.001, false);
		
		List<Intersection> hits = ray.getIntersections(sys.getIMSEOptics().plate1);
		
		if(hits.size() != 1){
			ray.dumpPath();
			VRMLDrawer.dumpRay("/tmp/failedRay.vrml", sys, ray, 0.001, AugMSESystem.vrmlScaleToAUGDDD, "}");
			throw new RuntimeException("FastPol: RayTracing: Expecting exactly 1 hit on polarisation measurement plane");
		}
		Intersection hit = hits.get(0);
		 
		double E[][] = Pol.projectToPlanesView(hit, false);
		double polAng = Pol.psi(E[0]);
		double polAng2 = Pol.psi(E[1]);
		double tracedLinearity = (polAng2 - polAng) / deltaPolAngle;
		
		savartAngleSign = sys.getIMSEOptics().savartAngleSign;
		savartToIMSEFrame = -sys.getIMSEOptics().savartToIMSEFrame + polAng;
		measurementCentralLOS = Util.reNorm(Util.minus(centreIsh, targ));
		
	}
	
	public double[] calcAng(double time, double posRPZ[][], double viewPos[]){
		return calcAng(time, posRPZ, viewPos, null);
	}
	
	/**
	 * @param time	Time point 
	 * @param posRPZ	List of coordinates at which to calculate
	 * @param viewPos	Observer position to define LOSs
	 * @param gammaAFactors	Used if ouputMode == OUTPUT_MODE_POL_AFACTORS. [pos][factorIdx]
	 * @return
	 */
	public double[] calcAng(double time, double posRPZ[][], double viewPos[], double gammaAFactors[][]){
		double maxTimeDiffToEqui = 0.10;
		
		if(time < loadedEqui.getEarliestTime() || time > loadedEqui.getLatestTime())
			throw new IllegalArgumentException("Time "+time+" out of range for equilibrium data. Data available for "+
					loadedEqui.getEarliestTime()+" < t < " + loadedEqui.getLatestTime() + "s");
		
		int timeIndex = Mat.getNearestIndex(loadedEqui.getTimeOrdered(), time);
		
		if(FastMath.abs(time - loadedEqui.getTimeOrdered()[timeIndex]) > maxTimeDiffToEqui){
			System.err.println("Nearest equi time point to "+time+" is " + loadedEqui.getTimeOrdered()[timeIndex] + ", which is more than the allowed " + maxTimeDiffToEqui);
			return Mat.fillArray(Double.NaN, posRPZ.length);
		}
		
		//Mat.dumpArray(posRPZ[posRPZ.length/2]);
						
		double Bvac0 = loadedEqui.getBVac0(time);
		
		Interpolation2D interp = loadedEqui.createPsiInterpolator(timeIndex, continuousCurrent);
		Interpolation1D interpF = loadedEqui.getFProfile(timeIndex);
		
		//double x[]=Mat.linspace(1.3,  2.0,  500);
		//double derX[][] = new double[x.length][3];
		//double psiX[] = interp.eval(x, Mat.fillArray(0.07, x.length), derX);
		//Mat.mustWriteBinary("/tmp/psi.bin", new double[][]{ x, psiX }, true);
		//Mat.mustWriteBinary("/tmp/psiDer.bin", derX, true);
		
		int n = posRPZ.length;
		double dPsi[] = new double[3];
		
		double ret[] = new double[n];
		double posT[][] = Mat.transpose(posRPZ);
		double dPsiAll[][] = new double[posRPZ.length][2];
		double psiAll[] = interp.eval(posT[0], posT[2], dPsiAll);
		
		for(int i=0; i < n; i++){
			double R = posRPZ[i][0];
			double phi = posRPZ[i][1];
			double Z = posRPZ[i][2];
			
			//double psi = interp.eval(R, Z, dPsi); //get dPsi, psiN may be required for Er calc
			double psi = psiAll[i];
			dPsi[0] = dPsiAll[i][0];
			dPsi[1] = dPsiAll[i][1];
			
			double f = interpF.eval(psi);
			//ret[i] = dPsi[0];
			//if(true)continue;
			
			//ret[i] = f / R;
			//if(true)continue;
			int poloidalFieldSign = 1;
			
			
			
			//our psi is in flux for whole toroidal loop, field def requires it per unit radian toroidal
			double BR = poloidalFieldSign * -(dPsi[1] / (2 * Math.PI))  / R;
			double BphiVac = Bvac0 / R;
			double Bphi = f / R;
			double BZ = poloidalFieldSign * (dPsi[0] / (2 * Math.PI)) / R;
			
			if((Bphi != 0) && ((Bphi < 0) ^ (BphiVac < 0))){
				throw new RuntimeException("Bphi vs BPhiVac sign mistmatch");
			}
			//ret[i] = (Bphi - BphiVac) / BphiVac;
			//if(true)continue;
			
			switch(outputMode){
			case OUTPUT_MODE_PITCH:
				ret[i] = FastMath.atan2(BZ, -Bphi) * 180 / FastMath.PI;
				break;
				
			case OUTPUT_MODE_BZ:
				if(Double.isInfinite(BZ) || Double.isNaN(BZ)){
					System.out.println("Non-finite Bz");
				}else{
					ret[i] = BZ;
				}
				break;
				
			case OUTPUT_MODE_POL:
			case OUTPUT_MODE_POL_AFACTORS:
			case OUTPUT_MODE_BZ_STARKE_MAG:
			case OUTPUT_MODE_dPol_dBz:
			case OUTPUT_MODE_dPol_dBr:
			case OUTPUT_MODE_POL_RAYTRACED:
			case OUTPUT_MODE_POL_VS_BPHI:
			case OUTPUT_MODE_ER_EFFECT:
			case OUTPUT_MODE_BR_EFFECT:
			case OUTPUT_MODE_Diagmag_EFFECT:
							
				//cartesian position
				double posXYZ[] = new double[]{
						R * FastMath.cos(phi),
						R * FastMath.sin(phi),
						Z
				};
				
				//cartesian B
				double Bxyz[] = new double[]{
					BR * FastMath.cos(phi) + BphiVac * -FastMath.sin(phi),
					BR * FastMath.sin(phi) + BphiVac * FastMath.cos(phi),
					BZ
				};
				
				if(inhibitBPol){
					Bxyz = new double[]{ BphiVac * -FastMath.sin(phi), BphiVac * FastMath.cos(phi), 0 };
				}
				
				//LOS (view to observed point)
				double l[] = Mat.normaliseVector(Mat.subtract(posXYZ, viewPos));
				
				//view perp vectors as closest thing to the measurement up (and it's perp)
				double losRight[], losUp[];

				if(outputMode == OUTPUT_MODE_POL_VS_BPHI){
					double BphiXYZ[] = new double[]{ Bphi * -FastMath.sin(phi), Bphi * FastMath.cos(phi), 0 };
					losUp =  Util.reNorm(Mat.cross3d(frameBeamVelocity, BphiXYZ));
					losRight = Util.reNorm(Util.cross(l, losUp));
					losUp =  Util.reNorm(Util.cross(losRight, l));
				}else{
					losRight =  Util.reNorm(Util.cross(l, measurementUp));
					losUp =  Util.reNorm(Util.cross(losRight, l));
				}
				
				if(outputMode == OUTPUT_MODE_BZ_STARKE_MAG){
					double VxBpol[] = Mat.cross3d(frameBeamVelocity, new double[]{ 
																			BR * FastMath.cos(phi),
																			BR * FastMath.sin(phi),
																			BZ });
					
					double Er = Util.dot(VxBpol, losRight); 
					double Eu = Util.dot(VxBpol, losUp);
					double Emag = FastMath.sqrt(Er*Er + Eu*Eu);
					ret[i] = Emag;
					break;
					
				}else if(outputMode == OUTPUT_MODE_dPol_dBz || outputMode == OUTPUT_MODE_dPol_dBr){
					double uR[] = new double[]{ posXYZ[0], posXYZ[1], 0 };
					double uZ[] = new double[]{ 0, 0, 1 }; 
					double uPhi[] = new double[]{ -posXYZ[1], posXYZ[0], 0 };
					
					throw new RuntimeException("Not implemented");
				}
				
				double EMSE[] = inhibitMSE ? new double[3] : Mat.cross3d(frameBeamVelocity, Bxyz);
				
				double Etotal[];
				double Er[] = null;
				if(addRadialField){
					double psiN = (psi - loadedEqui.getPsiAxis(timeIndex)) / (loadedEqui.getPsiLCFS(timeIndex) - loadedEqui.getPsiAxis(timeIndex));
					if(psiN < 0){
						//System.err.println("WARNING: PsiN < 0 in FastPolCalc Er evaluation. Probably overshot on interpolation grid.");
						psiN = 0;
					}
					Er = calcRadialElectricField(frameInterpWPhi, posRPZ[i], Bxyz, psiN);

					Etotal = Mat.add(EMSE, Er);
				}else{
					Etotal = EMSE;
				}
				
				if(outputMode == OUTPUT_MODE_POL_AFACTORS){
					// from IDE:
					// sinGammaM = A1.Br + A2.Bt + A3.Bz + A4.Er~ + A5.Ez/vb
					// cosGammaM = A6.Br + A7.Bt + A8.Bz + A9.Er~ + A10.Ez/vb
					// pol = arctan(sin/cos)
					double sinPolAng = gammaAFactors[i][0] * BR + gammaAFactors[i][1] * Bphi + gammaAFactors[i][2] * BZ;
					double cosPolAng = gammaAFactors[i][5] * BR + gammaAFactors[i][6] * Bphi + gammaAFactors[i][7] * BZ;
					if(addRadialField){
						double vBeam = Util.length(frameBeamVelocity);
						double ER = FastMath.sqrt(Er[0]*Er[0] + Er[1]*Er[1]); //we can assume E has no toroidal
						double EZ = Er[2];						
						sinPolAng += gammaAFactors[i][3] * ER/vBeam + gammaAFactors[i][4] * EZ/vBeam;
						cosPolAng += gammaAFactors[i][3] * ER/vBeam + gammaAFactors[i][4] * EZ/vBeam;
					}
					ret[i] = FastMath.atan2(sinPolAng, cosPolAng) * 180 / Math.PI;

		    		//yes, this is a ridiculous line of code
		    		ret[i] = (ret[i] < 0) ? (((ret[i] - 90) % 180) + 90) : (((ret[i] + 90) % 180) - 90); 
					break;
				}
				
				//plane projection
				double Aru[] = planeProj(l, Etotal); 
				double Ar=Aru[0], Au=Aru[1];	
				//double Ar = Util.dot(Etotal, losRight); 
				//double Au = Util.dot(Etotal, losUp);
				
				if(outputMode == OUTPUT_MODE_ER_EFFECT){
					double Aru0[] = planeProj(l, EMSE); 
					double Ar0=Aru0[0], Au0=Aru0[1];
					//double Ar0 = Util.dot(EMSE, losRight); 
					//double Au0 = Util.dot(EMSE, losUp);
					
					double deltaTheta = angleSign * savartAngleSign * (FastMath.atan2(Ar, Au) - FastMath.atan2(Ar0, Au0));
					ret[i] = deltaTheta * 180 / Math.PI;
					continue;
					
				}else if(outputMode == OUTPUT_MODE_BR_EFFECT){
					double Aru1[] = planeProj(l, EMSE); 
					double Ar1=Aru1[0], Au1=Aru1[1];					
					//double Ar1 = Util.dot(EMSE, losRight); 
					//double Au1 = Util.dot(EMSE, losUp);
					
					double Bxyz0[] = new double[]{
							0 * BR * FastMath.cos(phi) + Bphi * -FastMath.sin(phi),
							0 * BR * FastMath.sin(phi) + Bphi * FastMath.cos(phi),
							BZ
						};
					double EMSE0[] = inhibitMSE ? new double[3] : Mat.cross3d(frameBeamVelocity, Bxyz0);
					double Aru0[] = planeProj(l, EMSE0); 
					double Ar0=Aru0[0], Au0=Aru0[1];
					//double Ar0 = Util.dot(EMSE0, losRight); 
					//double Au0 = Util.dot(EMSE0, losUp);
					
					double deltaTheta = angleSign * savartAngleSign * (FastMath.atan2(Ar1, Au1) - FastMath.atan2(Ar0, Au0));
					ret[i] = deltaTheta * 180 / Math.PI;
					continue;
					
				}else if(outputMode == OUTPUT_MODE_Diagmag_EFFECT){
					double Aru1[] = planeProj(l, EMSE); 
					double Ar1=Aru1[0], Au1=Aru1[1];		
					//double Ar1 = Util.dot(EMSE, losRight); 
					//double Au1 = Util.dot(EMSE, losUp);
					
					double Bxyz0[] = new double[]{
							BR * FastMath.cos(phi) + BphiVac * -FastMath.sin(phi),
							BR * FastMath.sin(phi) + BphiVac * FastMath.cos(phi),
							BZ
						};
					
					double EMSE0[] = inhibitMSE ? new double[3] : Mat.cross3d(frameBeamVelocity, Bxyz0);
					double Aru0[] = planeProj(l, EMSE0); 
					double Ar0=Aru0[0], Au0=Aru0[1];
					//double Ar0 = Util.dot(EMSE0, losRight); 
					//double Au0 = Util.dot(EMSE0, losUp);
					
					double deltaTheta = angleSign * savartAngleSign * (FastMath.atan2(Ar1, Au1) - FastMath.atan2(Ar0, Au0));
					ret[i] = deltaTheta * 180 / Math.PI;
					continue;
				}
				
				
				double dAdj_dR = (adjustEdge - adjustCore) / (adjustEdgeR - adjustCoreR);
				double adjCenter = (adjustCore + adjustEdge) / 2.0;
				double dAdj_dZ = (adjustTop - adjCenter) / (adjustTopZ);
				
				double adjust = adjustCore + (R - adjustCoreR) * dAdj_dR + Z * dAdj_dZ;
				
				
				ret[i] = adjust + angleSign * savartAngleSign * (FastMath.atan2(Ar, Au) + savartToIMSEFrame)* 180 / Math.PI;
				
	    		if(addFaraday)
	    			ret[i] += frameFaraday;
	        	
	    		//yes, this is a ridiculous line of code
	    		ret[i] = (ret[i] < 0) ? (((ret[i] - 90) % 180) + 90) : (((ret[i] + 90) % 180) - 90); 
				
				break;
			default:
				throw new RuntimeException("Not implemented");
			}
			
		}
		
		return ret;
	}
	
	/** Returns the coefficients Au, Ar for polarisation vector E observed along line l onto the measurement plane */  
	private double[] planeProj(double l[], double E[]){
		double mr[] = Util.reNorm(Util.cross(measurementCentralLOS, measurementUp));
		return TransformProcessor.vectorCoefficients(l, measurementCentralLOS, measurementUp, mr, E);
	}
			
	
	private double[] calcRadialElectricField(Interpolation1D interpWPhi, double posRPZ[], double Bxyz[], double psiN){

		
		double rhoN = FastMath.sqrt(psiN);
		double vTor = interpWPhi.eval(rhoN) * posRPZ[0];  //v from rotation freq in m/s
		
		//this assumes vPhi is +ve for CCW looking from above - the same dir as the beams on AUG
		double vX = -vTor * FastMath.sin(posRPZ[1]);
		double vY = vTor * FastMath.cos(posRPZ[1]);
		
		//Er = -v x B
		double vTorXYZNeg[] = new double[]{ -vX, -vY, 0 };		
		double E[] = Mat.cross3d(vTorXYZNeg, Bxyz);
				
		return E;
	}
	
	public void loadEqui(int pulse){
		if(equiDiag == null || pulse < 0){
			loadedEqui = null; //unload
			return;
		}
		
		if(loadedEqui == null)
			loadedEqui = new AUGEquilibriumData();
		if(loadedEqui.getPulse() != pulse || loadedEqui.getDiag() != equiDiag || loadedEqui.getExp() != equiExp){
			loadedEqui.loadData(equiExp, equiDiag, pulse);
		}
	}
	
	public void setAnglesAdjustments(double coreAdjust, double edgeAdjust, double topAdjust) {
		adjustCore = coreAdjust;
		adjustEdge = edgeAdjust;
		adjustTop = topAdjust;
	}
	
	public void setAngleSign(int angleSign) { this.angleSign = angleSign;	}

	public AUGEquilibriumData getLoadedEqui() { return loadedEqui; }

	public void setInhibitMSEAngle(boolean inhibit) { this.inhibitMSE = inhibit; }
	public boolean getInhibitMSEAngle(){ return inhibitMSE; }
	

	public void setInhibitPoloidalField(boolean inhibit) { this.inhibitBPol = inhibit; }
	public boolean getInhibitPoloidalField(){ return inhibitBPol; }

	public int getOutputMode(){ return outputMode; }
	public void setOutputMode(int outputMode){ this.outputMode = outputMode; }
	public void setAddFaraday(boolean enable) { this.addFaraday = enable; }
	public void setAddRadialField(boolean enable) { this.addRadialField = enable; }

	public boolean getAddFaraday(){ return addFaraday; }
	public boolean getAddRadialField(){ return addRadialField; }

	public boolean getCalcDeltaX(){ return calcDeltaX; }
	public void setCalcDeltaX(boolean enable) { this.calcDeltaX = enable; }
	
	public boolean getContinuousCurrent(){ return continuousCurrent; }
	public void setContinuousCurrent(boolean enable) { this.continuousCurrent = enable; }
	
	public void setBeamSelection(int beamSel) { this.beamSelection = beamSel; }
	public int getBeamSelection() { return beamSelection; }

	
	public int getFrameBeamIndex() { return frameBeamIndex; }
	
	public void forceReloadEqui() { loadEqui( -1); }
	public void setEqui(String equiExp, String equiDiag) {
		this.equiExp = equiExp;
		this.equiDiag = equiDiag;		
	}
	
	public String getEquiExp(){ return equiExp; }
	public String getEquiDiag(){ return equiDiag; }

	public boolean isEquiLoaded(int augShot) {
		return loadedEqui != null && loadedEqui.isLoaded() && loadedEqui.getDiag().equals(equiDiag) &&
				loadedEqui.getExp().equals(equiExp) && loadedEqui.getPulse() == augShot;
	}

	public void setEffectiveVelocityFactor(double factor) { this.beamVelocityFactor = factor; }
}
