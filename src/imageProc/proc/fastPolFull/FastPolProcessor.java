package imageProc.proc.fastPolFull;

import java.nio.ByteOrder;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import org.eclipse.swt.widgets.Composite;

import algorithmrepository.CubicInterpolation2D;
import algorithmrepository.Interpolation2D;
import algorithmrepository.InterpolationMode;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.core.ImgProcPipe;
import imageProc.core.ImgProcessorConfig;
import imageProc.core.ImgSource;
import imageProc.database.gmds.GMDSUtil;
import imageProc.proc.pitch.PitchProcessor;
import fusionDefs.transform.FeatureTransform;
import fusionDefs.transform.FeatureTransformCubic;
import fusionDefs.transform.PhysicalROIGeometry;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class FastPolProcessor extends ImgProcPipe {

	private BulkImageAllocation<ByteBufferImage> bulkAlloc = new BulkImageAllocation<ByteBufferImage>(this);
	
	private FeatureTransformCubic xform;
	private PhysicalROIGeometry ccdGeom = null;
	
	private double imgR[], imgZ[], viewPos[];
	
	private String status = "init";
	
	private FastPolCalc fastPol = new FastPolCalc();
	
	/** We don't have out own config, only tyhe derived type */
	private ImgProcessorConfig config = new ImgProcessorConfig();
	
	private Interpolation2D imgPhi[];
	
	public FastPolProcessor() { 
		super(ByteBufferImage.class);
	}
	
	public FastPolProcessor(ImgSource source, int selectedIndex) {
		super(ByteBufferImage.class, source, selectedIndex);	
	}
	
	@Override
	protected int[] sourceIndices(int outIdx) {		
		return new int[]{ outIdx }; //always 1:1
	}
	
	
	/** Image allocation */
	protected boolean checkOutputSet(int nImagesIn){		
		outWidth = inWidth;
		outHeight = inHeight;
	    int nImagesOut = nImagesIn;
		
	    //allocate or re-use
	    ByteBufferImage templateImage = new ByteBufferImage(null, -1, outWidth, outHeight, ByteBufferImage.DEPTH_DOUBLE, false);
	    templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
	    
        if(bulkAlloc.reallocate(templateImage, nImagesOut)){
	       	imagesOut = bulkAlloc.getImages();
	       	return true;
        }          
        return false;
	}
	
	@Override
	protected void preCalc(boolean settingsHadChanged) {
		super.preCalc(settingsHadChanged);
		
		//first, we need all the geometry info and other bits
		viewPos = (double[])connectedSource.getSeriesMetaData("Transform/viewPosition");
		imgR = (double[])connectedSource.getSeriesMetaData("/Transform/imageOutR");
		imgZ = (double[])connectedSource.getSeriesMetaData("/Transform/imageOutZ");
		if(imgR == null || imgZ == null || viewPos == null){
			System.err.println("FastPolCalc.doCalc(): Incomplete transform data");
			super.status = "Incomplete transform data";
			updateAllControllers();				
			return;
		}

		imgPhi = new Interpolation2D[8];
		boolean isBox2 = viewPos[1] > 0; //+ve y means we're probably the permIMSE system
		for(int iB=0; iB < 8; iB++){
			if((isBox2 && iB < 4) || (!isBox2 && iB >= 4))
				continue;
			if(fastPol.getBeamSelection() != FastPolCalc.BEAMSEL_AUTO && fastPol.getBeamSelection() != iB)
				continue;
					
			double imgPhiGrid[][] = (double[][])connectedSource.getSeriesMetaData("Transform/outputPhiClosest-Q"+(iB+1));
				 	
			if(imgPhiGrid == null){
				System.out.println("No phi data for beam "+(iB+1)+", check transform is doing this beam!");
				super.status = "No transform phi for beam " + (iB+1);
				updateAllControllers();
				return;
			}
			
			double pX[]  = Mat.linspace(0.0, 1.0, imgPhiGrid[0].length);
			double pY[]  = Mat.linspace(0.0, 1.0, imgPhiGrid.length);
			imgPhi[iB] = new Interpolation2D(pX, pY, Mat.transpose(imgPhiGrid), InterpolationMode.CUBIC);
		}
		
		
		if(imgPhi == null || viewPos == null || imgR == null || imgZ == null)
			throw new RuntimeException("Incomplete transform data.");
				
		int augShot = GMDSUtil.getMetaAUGShot(getConnectedSource());
		int gmdsID = GMDSUtil.getMetaDatabaseID(getConnectedSource());
		
		if(!fastPol.isEquiLoaded(augShot)){
			
			//we don't want the equi loading which can takes ages (several minutes)
			//to hang the graph update, so fire another update here and get it to fire us back
			
			setStatus("FastPol: Loading equi signals");
			
			fastPol.loadEqui(augShot);			
		}
		
		//setStatus("FastPol: Preparing");
    	fastPol.setAnglesAdjustments(0,0,0);
    	
    	fastPol.prepareMetadata(this, "Transform");
		fastPol.prepareGeometry(gmdsID, viewPos);
					
	}

	
	@Override
	protected boolean doCalc(Img imageOutG, WriteLock writeLock, Img[] sourceSet, boolean settingsHadChanged) throws InterruptedException {
		
		Img imageIn = sourceSet[0];		
		ByteBufferImage imageOut = (ByteBufferImage)imageOutG;
		int frameIndex = imageIn.getSourceIndex();
		
		int beamIndex;
		if(fastPol.getBeamSelection() == FastPolCalc.BEAMSEL_AUTO){
			beamIndex = (Integer)getImageMetaData("beamIndex", imageIn.getSourceIndex());
		}else{
			beamIndex = fastPol.getBeamSelection();
		}
		
    	Object o = getConnectedSource().getSeriesMetaData("/time");
    	if(o == null || !(o instanceof double[]))
    		return false;
    	
    	double t[] = (double[])o;
    	if(frameIndex < 0 || frameIndex > t.length)
    		return false;
    	
    	double time = t[frameIndex];
    	if(time < fastPol.getLoadedEqui().getEarliestTime() || time > fastPol.getLoadedEqui().getLatestTime()){
    		System.err.println("Frame " + frameIndex + " at time=" + time + " is outside valid equilibrium time range.");
    		//out of range
			for(int iY=0; iY < inHeight; iY++) {
				for(int iX=0; iX < inWidth; iX++){
					imageOut.setPixelValue(writeLock, iX, iY, Double.NaN);
				}
			}
			return true;
    	}
    			

		fastPol.prepareFrame(frameIndex);
		
		for(int iY=0; iY < inHeight; iY++) {
			double posRPZ[][] = new double[inWidth][3];
			
			for(int iX=0; iX < inWidth; iX++) {
				posRPZ[iX][0] = imgR[iX];
				posRPZ[iX][1] = imgPhi[beamIndex].eval(iX/inWidth, iY/inHeight);
				posRPZ[iX][2] = imgZ[iY];
			}
			
    		double rowPred[] = fastPol.calcAng(t[frameIndex], posRPZ, viewPos);
    		
    		if(fastPol.getCalcDeltaX()){
    			for(int iX=rowPred.length-1; iX > 0; iX--){
    				rowPred[iX] = rowPred[iX] - rowPred[iX-1];
    			}
    			rowPred[0] = Double.NaN;
    		}
    		
    		for(int iX=0; iX < inWidth; iX++)
    			imageOut.setPixelValue(writeLock, iX, iY, rowPred[iX]);
		}
		
		return true;
	}
	
	
	
	public FastPolCalc getFastPolCalc(){ return fastPol; }

	public void setStatus(String status) {
		super.status = status;
		updateAllControllers();
	}
	
	@Override
	public void notifySourceChanged() {
		super.notifySourceChanged();
		if(config.autoCalc)
			calc();
	}

	@Override
	public void imageChangedRangeComplete(int idx) { 
 		if(config.autoCalc)
			calc();
 	}
	
	@Override
	public int getNumImages() {
		return (imagesOut != null) ? imagesOut.length : 0;
	}

	@Override
	public Img getImage(int imgIdx) {
		return (imgIdx >= 0 && imgIdx < imagesOut.length) ? imagesOut[imgIdx] : null;
	}
	
	@Override
	public Img[] getImageSet() { return imagesOut; }

	@Override
	public PitchProcessor clone() { return new PitchProcessor(connectedSource, getSelectedSourceIndex());	}

	@Override
	/** For the sink side */
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			FastPolSWTController controller = new FastPolSWTController((Composite)args[0], (Integer)args[1], this, asSink);
			controllers.add(controller);
			return controller;
		}
		return null;
	}
	
	public void setAutoUpdate(boolean autoCalc) {
		if(!config.autoCalc && autoCalc){
			config.autoCalc = true;
			updateAllControllers();
			calc();			
		}else{
			config.autoCalc = autoCalc;
			updateAllControllers();
		}
	}
	public boolean getAutoUpdate() { return config.autoCalc; }

	@Override
	public void setSource(ImgSource source) {
		super.setSource(source);
		if(config.autoCalc)
			calc();
	}
	
	@Override
	public void destroy() {		
		super.destroy();
	}
	
	public FeatureTransform getTransform(){ return xform; }
	public void pointsMapModified(){
		settingsChanged = true;
		updateAllControllers();
		if(config.autoCalc)
			calc();
	}

	public void invalidate() {
		settingsChanged = true;
	}


	@Override
	public BulkImageAllocation getBulkAlloc() { return bulkAlloc; }

	public String getStatus() { return super.status; }
	
	@Override
	public String toShortString() {
		String hhc = Integer.toHexString(hashCode());
		return "FastPol[" + hhc.substring(hhc.length()-2, hhc.length()) + "]";
	}

	@Override
	public ImgProcessorConfig getConfig() { return config; }
}
