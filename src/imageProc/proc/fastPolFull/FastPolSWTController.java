package imageProc.proc.fastPolFull;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;

import imageProc.core.swt.ImgProcPipeSWTController;

/** SWT controller for the pitch/current calculation
 *  
 * @author oliford
 */
public class FastPolSWTController extends ImgProcPipeSWTController {

	private FastPolProcessor proc;
 
	private Button reloadButton;
	
	private FastPolSettingsSWTGroup fastPolSettingsGroup;
	
	public static final DecimalFormat tableValueFormat = new DecimalFormat("#.###");

	public FastPolSWTController(Composite parent, int style, FastPolProcessor proc, boolean asSinkController) {
		this.proc = proc;
		
		swtGroup = new Group(parent, SWT.BORDER);
		swtGroup.setText("Fast Pol Prediction");
		swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		swtGroup.setLayout(new GridLayout(6, false));
			
		addCommonPipeControls(swtGroup);
	
		reloadButton = new Button(swtGroup, SWT.PUSH);
		reloadButton.setText("Force reload");
		reloadButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 6, 1));
		reloadButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { reloadButtonEvent(event); }});

		fastPolSettingsGroup = new FastPolSettingsSWTGroup(swtGroup, SWT.NONE, proc.getFastPolCalc()); 
		
	}
	
	private void reloadButtonEvent(Event event) {
		proc.getFastPolCalc().forceReloadEqui();
		proc.calc();
	}
		
	public Group getSWTGroup(){ return swtGroup; }

	
	protected void doUpdate(){
		super.doUpdate();
	}

	@Override
	public void destroy() {
		swtGroup.dispose();
		proc.controllerDestroyed(this);
	}

	@Override
	public Object getInterfacingObject() { return swtGroup;	}

	@Override
	public FastPolProcessor getPipe() { return proc;	}
}
