package imageProc.proc.fastPolFull;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

public class FastPolSettingsSWTGroup {

	private Group swtGroup;

	private Combo equiExpCombo;
	private Combo equiDiagCombo;

	private Combo calculationModeCombo;
	private Button reverseAngleCheckbox;
	private Button mseCheckbox;
	private Button faradayCheckbox;
	private Button radialFieldCheckbox;
	private Button calcDeltaXCheckbox;
	private Button continuousCurrentCheckbox;
	private Spinner beamIndexSpinner;
	
	private Spinner effectiveBeamVelocitySpinner;

	private FastPolCalc fastPolCalc;

	public FastPolSettingsSWTGroup(Composite parent, int style, FastPolCalc fastPolCalc) {
		this.fastPolCalc = fastPolCalc;
		
		swtGroup = new Group(parent, SWT.BORDER);
		swtGroup.setText("Fast Pol Settings");
		swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		swtGroup.setLayout(new GridLayout(6, false));
			
		Label lEE = new Label(swtGroup, SWT.NONE); lEE.setText("Experiment:");
		equiExpCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.MULTI);
		equiExpCombo.setItems(new String[]{ "AUGD", "AUGE", "rrf", "abock", "oliford" });
		equiExpCombo.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});
		equiExpCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));

		Label lED = new Label(swtGroup, SWT.NONE); lED.setText("Diag[#ed]:");
		equiDiagCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.MULTI);
		equiDiagCombo.setItems(new String[]{ "EQI", "EQH", "FPP", "IDE", "IDE#1", "IDE#2", "IDE#3" });
		equiDiagCombo.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});
		equiDiagCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));

		Label lCM = new Label(swtGroup, SWT.NONE); lCM.setText("Mode:");
		calculationModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.MULTI);
		calculationModeCombo.setItems(FastPolCalc.outputModes);
		calculationModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});
		calculationModeCombo.select(FastPolCalc.OUTPUT_MODE_POL);
		calculationModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));

		Label lBI = new Label(swtGroup, SWT.NONE); lBI.setText("Beam Index:");
		beamIndexSpinner = new Spinner(swtGroup, SWT.NONE);
		beamIndexSpinner.setValues(-1, -1, 8, 0, 1, 4);
		beamIndexSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		beamIndexSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});

		Label lRA = new Label(swtGroup, SWT.NONE); lRA.setText("");
		reverseAngleCheckbox = new Button(swtGroup, SWT.CHECK);
		reverseAngleCheckbox.setText("Reverse Angle Direction");
		reverseAngleCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		reverseAngleCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});

		mseCheckbox = new Button(swtGroup, SWT.CHECK);
		mseCheckbox.setText("MSE");
		mseCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		mseCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});

		faradayCheckbox = new Button(swtGroup, SWT.CHECK);
		faradayCheckbox.setText("Faraday in Optics");
		faradayCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		faradayCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});

		radialFieldCheckbox = new Button(swtGroup, SWT.CHECK);
		radialFieldCheckbox.setText("Radial field");
		radialFieldCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		radialFieldCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});

		
		Label lEV = new Label(swtGroup, SWT.NONE); lEV.setText("Effective beam velocity (%):");
		lEV.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));		
		effectiveBeamVelocitySpinner = new Spinner(swtGroup, SWT.NONE);
		effectiveBeamVelocitySpinner.setValues(70, 0, 1000, 0, 10, 100);
		effectiveBeamVelocitySpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		effectiveBeamVelocitySpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});

		calcDeltaXCheckbox = new Button(swtGroup, SWT.CHECK);
		calcDeltaXCheckbox.setText("Calc d/dx");
		calcDeltaXCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		calcDeltaXCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});

		continuousCurrentCheckbox = new Button(swtGroup, SWT.CHECK);
		continuousCurrentCheckbox.setText("Continuous current profile (slow)");
		continuousCurrentCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		continuousCurrentCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});

	}
	
	private void settingsChangedEvent(Event event){
		fastPolCalc.setEqui(equiExpCombo.getText(), equiDiagCombo.getText());
		fastPolCalc.setOutputMode(calculationModeCombo.getSelectionIndex());
		fastPolCalc.setAngleSign(reverseAngleCheckbox.getSelection() ? -1 : 1);
		fastPolCalc.setAddFaraday(faradayCheckbox.getSelection());
		fastPolCalc.setAddRadialField(radialFieldCheckbox.getSelection());
		fastPolCalc.setInhibitMSEAngle(!mseCheckbox.getSelection());
		fastPolCalc.setCalcDeltaX(calcDeltaXCheckbox.getSelection());
		fastPolCalc.setContinuousCurrent(continuousCurrentCheckbox.getSelection());
		fastPolCalc.setBeamSelection(beamIndexSpinner.getSelection() <= 0 ? -1 : beamIndexSpinner.getSelection() - 1); //human Qx to internal array index
		fastPolCalc.setEffectiveVelocityFactor((double)effectiveBeamVelocitySpinner.getSelection() / 100);
	}
}
