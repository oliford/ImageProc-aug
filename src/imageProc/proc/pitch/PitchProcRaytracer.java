package imageProc.proc.pitch;

import java.util.List;

import fusionOptics.Util;
import fusionOptics.drawing.VRMLDrawer;
import fusionOptics.interfaces.IsoIsoInterface;
import fusionOptics.surfaces.Plane;
import fusionOptics.tracer.Tracer;
import fusionOptics.types.Intersection;
import fusionOptics.types.Pol;
import fusionOptics.types.RaySegment;
import ipp.aug.neutralBeams.AugNBI;
import ipp.imse.fusionOptics.imse.IMSEOptics135_50_rescaled;
import ipp.imse.fusionOptics.mse.AugMSESystem;
import net.jafama.FastMath;
import otherSupport.RandomManager;

/** The raytracing part of the pitch angle processor */
public class PitchProcRaytracer {
	
	/** What rays to trace */
	public double minIntensity = 0.01;
	public boolean traceReflections = false;
	
	/** Optical system setup */
	private AugMSESystem sys;
	
	private Plane imagePlane;
	private Plane polarisationPlane;
	
	private double wavelen = 653e-9;
	private double globaUp[] = new double[]{ 0, 0, 1 };
	
	private VRMLDrawer vrmlOut;
			
	private double targetPos[];

	public PitchProcRaytracer(int pulse, double targetPos[]) {
		this.targetPos = targetPos;
		
		sys = new AugMSESystem(new IMSEOptics135_50_rescaled());
		try{
			sys.setupForPulse(pulse);
		}catch(RuntimeException err){
			err.printStackTrace();
		}
		
		imagePlane = sys.imseOptics.ccd;
		polarisationPlane = sys.imseOptics.plate1;
		
	
		System.out.println("optics hash: " + sys.hashCode());
		
		//ignore the protection cover coating (or lack of)
		sys.mirrorBox.protectionCoverFront.setInterface(IsoIsoInterface.ideal());
		sys.mirrorBox.protectionCoverBack.setInterface(IsoIsoInterface.ideal());
		
		vrmlOut = null;//new VRMLDrawer(System.getProperty("java.io.tmpdir") + "/pitchRaytrace.vrml");
		if(vrmlOut != null){
			vrmlOut.setSmallLineLength(0.0001);
			vrmlOut.setDrawPolarisationFrames(true);
			vrmlOut.addVRML(AugMSESystem.vrmlScaleToAUGDDD);
		}
		
				
	}
	
	/** returns psi(up), psi(pitch=0) and dPsi/dPitch */
	public double[] getPolPitchInfo(int beamIdx, double startPos[], double dPitch){	
		
		//global test element for polarisation definition consistent for all bundles - whatever that means
			
		//Bφ field direction at emission point
		double Bphi[] = Util.reNorm(Util.cross(startPos, globaUp));
		
		Pol.recoverAll();
		
		RaySegment ray = new RaySegment();
		ray.wavelength = wavelen;
		ray.startPos =  startPos.clone();
		ray.dir = Util.reNorm(Util.minus(targetPos, startPos));
				
		ray.length = Double.POSITIVE_INFINITY;
		
		//ray.up = Util.cross(Util.reNorm(Util.cross(ray.dir, globaUp)), ray.dir); // This is MSE-like emission
		
		//randomise initial sense of up, as it shouldn't matter because we use ray.rotatePolRefFrame()
		ray.up = Util.reNorm(new double[]{
				RandomManager.instance().nextNormal(0, 1),
				RandomManager.instance().nextNormal(0, 1),
				RandomManager.instance().nextNormal(0, 1)
			});
		
	
		ray.E0 = new double[3][4];
		
		//B with Bphi plus some Bz (downwards is normal +ve Ip)
		double Bpitched[] = Util.plus( Util.mul(Bphi, FastMath.cos(dPitch)), new double[]{ 0, 0, -FastMath.sin(dPitch) });
		
		double starkE0[] = Util.reNorm(Util.cross(AugNBI.def().uVec(beamIdx), Bphi)); // V x B
		double starkE1[] = Util.reNorm(Util.cross(AugNBI.def().uVec(beamIdx), Bpitched)); // V x B
		
		// Is this right? Should I be using the common plane definition?
		ray.rotatePolRefFrame(Util.cross(Util.reNorm(Util.cross(ray.dir, globaUp)), ray.dir));		
		ray.E0[0] = new double[]{ 1, 0, 0, 0 };
		
		ray.rotatePolRefFrame(starkE0);		
		ray.E0[1] = new double[]{ 1, 0, 0, 0 };
		
		ray.rotatePolRefFrame(starkE1);		
		ray.E0[2] = new double[]{ 1, 0, 0, 0 };	
									
		Tracer.trace(sys, ray, 100, minIntensity, traceReflections);
		
		List<Intersection> hits = ray.getIntersections(imagePlane); 

		if(vrmlOut != null){
			vrmlOut.drawRay(ray);
		}
		
		if(hits.size() > 0){
		
			
			Intersection imgHit = hits.get(0);
			
			Intersection polHit = imgHit.incidentRay.findFirstEarlierIntersection(polarisationPlane);
			if(polHit == null)
				return new double[]{ Double.NaN, Double.NaN, Double.NaN };
						
			double Eproj[][] = Pol.projectToPlanesView(polHit, false);
			
			double psi0 = Pol.psi(Eproj[0]);
			double psi1 = Pol.psi(Eproj[1]);
			double psi2 = Pol.psi(Eproj[2]);

			return new double[]{ psi0, psi1, (psi2 - psi1) / dPitch };
			
			
		}else{
			//no hits, it can't see this point
			return new double[]{ Double.NaN, Double.NaN, Double.NaN };
		}
	}

	public void destroy() {
		if(vrmlOut != null){
			vrmlOut.drawOptic(sys);
			vrmlOut.addVRML("}"); //end of rotate/transform
			vrmlOut.destroy();	
		}
	}

	public double getSavartAngleSign() { return sys.getIMSEOptics().savartAngleSign; }
	public double getSavartToIMSEFrame() { return sys.getIMSEOptics().savartToIMSEFrame; }
}
