package imageProc.proc.pitch;

import java.nio.ByteOrder;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import org.eclipse.swt.widgets.Composite;

import algorithmrepository.CubicInterpolation2D;
import algorithmrepository.ExtrapolationMode;
import algorithmrepository.Interpolation1D;
import algorithmrepository.Interpolation2D;
import algorithmrepository.InterpolationMode;
import algorithmrepository.LinearInterpolation2D;
import descriptors.aug.AUGSignalDesc;
import fusionOptics.Util;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.core.ImgProcPipe;
import imageProc.core.ImgProcessorConfig;
import imageProc.core.ImgSource;
import imageProc.database.gmds.GMDSUtil;
import imageProc.proc.transform.TransformProcessor;
import fusionDefs.transform.FeatureTransform;
import fusionDefs.transform.FeatureTransformCubic;
import fusionDefs.transform.PhysicalROIGeometry;
import net.jafama.FastMath;
import mds.AugShotfileFetcher;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import signals.aug.AUGSignal;

/** Polarisation to pitch processor.
 * 
 * Uses the ray-tracer to calculate a very approximate pitch angle from the input polarisation images. 
 * Might even do current one day from really naive j = dBz/dR 
 * 
 * This works on the images in (R,Z) space, so needs to be in the chain after TransformProcessor.
 * It anyway needs the geometry transform data stored in the metadata by TransformProcessor
 */
public class PitchProcessor extends ImgProcPipe {
		
	public static String[] outputModeNames = { "Polarision vs up", "Polarisation vs v x Bphi", "Pitch", "Bz", "jphi", "q" };
	public static final int OUTPUT_MODE_POL_VS_UP = 0;
	public static final int OUTPUT_MODE_POL_VS_BPHI  = 1;
	public static final int OUTPUT_MODE_PITCH = 2;
	public static final int OUTPUT_MODE_BZ = 3;
	public static final int OUTPUT_MODE_J = 4;
	public static final int OUTPUT_MODE_Q = 5;
	
	private int outputMode;
		
	private BulkImageAllocation<ByteBufferImage> bulkAlloc = new BulkImageAllocation<ByteBufferImage>(this);
	
	private FeatureTransformCubic xform;
	private PhysicalROIGeometry ccdGeom = null;
			
	/** We don't have out own config, only tyhe derived type */
	private ImgProcessorConfig config = new ImgProcessorConfig();
	
	/** Beams indices 0-3 for Q1-Q4, 1 for lat/lon, -2 for auto */
	public static final int BEAMSEL_AUTO = -1;
	public int beamSelection = -1;
	
	boolean useReducedOffsets = true; //use Reduced/FastPol/offsets rather than ray tracer 
	
	private double bvacAt1m[];
	private double imgR[];
	
	//grid for ray-trcaing and calculation of angles
	private int nGX = 30, nGY = 30;
	
	private double savartAngleSign;
	private double savartToIMSEFrame;
			
	private Interpolation2D interpPol0[];
	private Interpolation2D interpDPolDPitch[];
	
	public PitchProcessor() { 
		super(ByteBufferImage.class);
	}
	
	public PitchProcessor(ImgSource source, int selectedIndex) {
		super(ByteBufferImage.class, source, selectedIndex);		
		
	}
	
	@Override
	protected int[] sourceIndices(int outIdx) {		
		return new int[]{ outIdx }; //always 1:1
	}
	
	@Override
	protected void preCalc(boolean settingsHadChanged) {
		
		super.preCalc(settingsHadChanged);
		nGX = 30; nGY = 20;
		
		//first, we need all the geometry info and other bits
		double viewPos[] = (double[])connectedSource.getSeriesMetaData("Transform/viewPosition");
		imgR = (double[])connectedSource.getSeriesMetaData("/Transform/imageOutR");
		double imgZ[] = (double[])connectedSource.getSeriesMetaData("/Transform/imageOutZ");		
		//beamIdx we can look up per-image
		
		Interpolation2D imgPhi[] = new Interpolation2D[8];
		boolean isBox2 = viewPos[1] > 0; //+ve y means we're probably the permIMSE system
		for(int iB=0; iB < 8; iB++){
			if((isBox2 && iB < 4) || (!isBox2 && iB >= 4))
				continue;
			if(beamSelection != BEAMSEL_AUTO && beamSelection != iB)
				continue;
					
			double imgPhiGrid[][] = (double[][])connectedSource.getSeriesMetaData("Transform/outputPhiClosest-Q"+(iB+1));
				 	
			if(imgPhiGrid == null){
				System.out.println("No phi data for beam "+(iB+1)+", check transform is doing this beam!");
				status = "No transform phi for beam " + (iB+1);
				updateAllControllers();
				return;
			}
			
			double pX[]  = Mat.linspace(0.0, 1.0, imgPhiGrid[0].length);
			double pY[]  = Mat.linspace(0.0, 1.0, imgPhiGrid.length);
			imgPhi[iB] = new Interpolation2D(pX, pY, Mat.transpose(imgPhiGrid), InterpolationMode.CUBIC);
		}
		
		
		if(imgPhi == null || viewPos == null || imgR == null || imgZ == null)
			throw new RuntimeException("Incomplete transform data.");
		
		//everything per-beam
		//double rayStartPos[][][][] = new double[4][][][];
		double pol0[][][] = new double[8][][];
		double dPolDPitch[][][] = new double[8][][];
		interpPol0 = new Interpolation2D[8];
		interpDPolDPitch = new Interpolation2D[8];
		
		//calc R,Z on calc grid
		double gX[] = Mat.linspace(0.0, 1.0, nGX);
		double gY[] = Mat.linspace(0.0, 1.0, nGY);
		Interpolation1D interpR = new Interpolation1D(Mat.linspace(0.0, 1.0, inWidth), imgR, InterpolationMode.LINEAR, ExtrapolationMode.EXCEPTION); 
		Interpolation1D interpZ = new Interpolation1D(Mat.linspace(0.0, 1.0, inHeight), imgZ, InterpolationMode.LINEAR, ExtrapolationMode.EXCEPTION);
		double gR[] = interpR.eval(gX);
		double gZ[] = interpZ.eval(gY);		
		
		PitchProcRaytracer rayTracer = useReducedOffsets ? null : 
					new PitchProcRaytracer(GMDSUtil.getMetaDatabaseID(connectedSource), viewPos);
		
		double dPitch = 6.0 * Math.PI / 180;
		//boolean isBox2 = xform.getViewPosition()[1] > 0; //+ve y means we're probably the permIMSE system
		
		Interpolation2D reducedOffsetsInterp;
		if(useReducedOffsets){
			double reducedCcdX[] = (double[])getSeriesMetaData("Reduced/ccdX");
			double reducedCcdY[] = (double[])getSeriesMetaData("Reduced/ccdY");
			double reducedOffsets[][] = (double[][])getSeriesMetaData("Reduced/FastPol/offsets");
			if(reducedCcdX == null || reducedCcdY == null || reducedOffsets == null)
				throw new RuntimeException("useReducedOffsets is on but there is no offset data from reducedProcessor stored");
			reducedOffsetsInterp = new Interpolation2D(reducedCcdX, reducedCcdY, Mat.transpose(reducedOffsets), InterpolationMode.CUBIC);
			
			 
			//fetch CCD geometry from metadata
			Map<String, Object> cleanedMap = getCompleteSeriesMetaDataMap().extractSingleArrayElements();
			try{
				ccdGeom = new PhysicalROIGeometry();
				//ccdGeom.fromMap(getCompleteSeriesMetaDataMap());
				ccdGeom.setFromCameraMetadata(cleanedMap);
			}catch(RuntimeException err){
				//we need this one  to know where the grid is, so give up if it fails
				throw new RuntimeException("Couldn't load ccdGeom from metadata: " + err);			
			}

			//fetch transform from metadata
			try{
				xform = new FeatureTransformCubic(getCompleteMetaDataAsJavaMap());
			}catch(RuntimeException err){
				err.printStackTrace();
				System.err.println("Couldn't load transform from metadata. Is there a TransformProcessor? " + err);
				xform = null;
				//we dont need this, we can save the data grid without the geoemtry info
			}
		}else{
			 reducedOffsetsInterp = null;
		}
		
		for(int iB=0; iB < 8; iB++){ //for each beam
			if((isBox2 && iB < 4) || (!isBox2 && iB >= 4))
				continue;
		
			if(beamSelection != iB && beamSelection != BEAMSEL_AUTO)
				continue;
			
			int nRaysOK = 0;
			 
			pol0[iB] = new double[nGX][nGY];
			dPolDPitch[iB] = new double[nGX][nGY];
			
			double beamAxes[][], measPlaneUp[], measPlaneNorm[], measPlaneRight[];
			
			if(useReducedOffsets){
				beamAxes = (double[][])getSeriesMetaData("Transform/beamAxesUVec");
				//beamEnergy = (double[])getSeriesMetaData("Reduced/beamEnergy");
				//and use raw geometry for dPol/dPitch
				measPlaneNorm = (double[])getSeriesMetaData("Transform/gammaFactors/measPlaneNormal");
				measPlaneUp = (double[])getSeriesMetaData("Transform/gammaFactors/measPlaneUp");
				measPlaneRight = Mat.normaliseVector(Mat.cross3d(measPlaneNorm, measPlaneUp));
			}else{
				beamAxes = null; measPlaneNorm = null; measPlaneUp = null; measPlaneRight = null;
			}
			
			for(int iY=0; iY < nGY; iY++){				
				for(int iX=0; iX < nGX; iX++){
					
					double phi = imgPhi[iB].eval((double)iX/nGX, (double)iY/nGY);
					
					if(Double.isNaN(phi)){
						pol0[iB][iX][iY] = Double.NaN;
						dPolDPitch[iB][iX][iY] = Double.NaN;
						continue;
					}
					
					double obsPos[] = new double[]{							
						gR[iX] * FastMath.cos(phi),
						gR[iX] * FastMath.sin(phi),
						gZ[iY] 
					};

					if(useReducedOffsets){		
						// reducedOffsets is in image space, which we need to recover from the R,Z space here
						double obsVec[] = Util.reNorm(Util.minus(obsPos, viewPos));
				        double lon = FastMath.atan2(obsVec[1], obsVec[0]);
				        double lat = FastMath.asin(obsVec[2]);
				        
						double ccdXY[] = xform.latlonToXY(lon, lat);
						
						//the offset part gives us polarisation vs measPlaneUp 
						pol0[iB][iX][iY] = reducedOffsetsInterp.eval(ccdXY[0], ccdXY[1]) * Math.PI / 180;
						
						//and use raw geometry for dPol/dPitch
						
						double phiDir[] = Util.reNorm(new double[]{ obsPos[1], -obsPos[0], 0 }); //unit vec of phi direction						
						double E[], Eru[];
						
						E = Mat.normaliseVector(Mat.cross3d(beamAxes[iB], phiDir));						
						Eru = TransformProcessor.vectorCoefficients(obsVec, measPlaneNorm, measPlaneUp, measPlaneRight, E);
						double ang0 = FastMath.atan2(Eru[0], Eru[1]);
												
						//pitch to +ve Bz by given angle
						double pitched[] = Util.reNorm(Mat.add(phiDir, new double[]{ 0*0, 0, dPitch }));
						
						E = Mat.normaliseVector(Mat.cross3d(beamAxes[iB], pitched));						
						Eru = TransformProcessor.vectorCoefficients(obsVec, measPlaneNorm, measPlaneUp, measPlaneRight, E);
						double ang1 = FastMath.atan2(Eru[0], Eru[1]);
						
						if(outputMode != OUTPUT_MODE_POL_VS_UP) //all other modes need polarisation relative to v x Bphi
							pol0[iB][iX][iY] -= ang0;
						
						dPolDPitch[iB][iX][iY] = (ang1 - ang0) / dPitch;
						
					}else{
							
						double ret[] = rayTracer.getPolPitchInfo(iB, obsPos, dPitch);
						pol0[iB][iX][iY] = (outputMode == OUTPUT_MODE_POL_VS_UP) ? ret[0] : ret[1];
						dPolDPitch[iB][iX][iY] = ret[2];
						
						if(!Double.isNaN(ret[0]))
							nRaysOK++;
					}
				}	
			}
			
			interpPol0[iB] = new Interpolation2D(gX, gY, pol0[iB]);
			interpDPolDPitch[iB] = new Interpolation2D(gX, gY, dPolDPitch[iB], InterpolationMode.LINEAR);
			
			if(!useReducedOffsets)
				System.out.println("PitchProcessor: " + nRaysOK + " of " + (nGX*nGY) + " rays hit for beam " + iB);
		}
		
		if(!useReducedOffsets){
			savartAngleSign = rayTracer.getSavartAngleSign(); //protoIMSE.imseOptics.savartAngleSign;
			savartToIMSEFrame = rayTracer.getSavartToIMSEFrame(); //protoIMSE.imseOptics.savartToIMSEFrame;			
		}else{
			savartAngleSign = +1;
			savartToIMSEFrame = 0;
		}
		
		//someone might want these
		setSeriesMetaData("Pitch/pol0", pol0, false);
		setSeriesMetaData("Pitch/dPolDPitch", dPolDPitch, false);
		
		if(outputMode == OUTPUT_MODE_BZ || outputMode == OUTPUT_MODE_J || outputMode == OUTPUT_MODE_Q){
			if(bvacAt1m == null || bvacAt1m.length != getNumImages()){
				String bvacSignalPath = "sig/MAI/BTF"; //toroidal magnetic field at R=1.65 m
				double bvacMeasurementLocation = 1.65; // R / m
				
				AUGSignalDesc desc = new AUGSignalDesc("/aug/" + GMDSUtil.getMetaAUGShot(connectedSource) + "/" + bvacSignalPath);
				AUGSignal BVACSig = (AUGSignal)AugShotfileFetcher.defaultInstance().getSig(desc);		
				double dataBvac[] = (double[])BVACSig.getDataAsType(double.class);
				double dataBvacTime[] = (double[])BVACSig.getTVectorAsType(double.class);
				
				double frameTimes[] = (double[])getSeriesMetaData("time");
				
				Interpolation1D interp = new Interpolation1D(dataBvacTime, dataBvac, InterpolationMode.LINEAR, ExtrapolationMode.CONSTANT_END);
				bvacAt1m = interp.eval(frameTimes);
				for(int i=0; i < bvacAt1m.length; i++){
					bvacAt1m[i] *= bvacMeasurementLocation;
				}	
				
				connectedSource.setSeriesMetaData("BvacAt1m", bvacAt1m, true);
			}
		}
		
		if(rayTracer != null){
			rayTracer.destroy();
			rayTracer = null;			
		}
	}
	
	
	/** Image allocation */
	protected boolean checkOutputSet(int nImagesIn){		
		outWidth = inWidth;
		outHeight = inHeight;
	    int nImagesOut = nImagesIn;
		
	    //allocate or re-use
	    ByteBufferImage templateImage = new ByteBufferImage(null, -1, outWidth, outHeight, ByteBufferImage.DEPTH_DOUBLE, false);
	    templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
	    
        if(bulkAlloc.reallocate(templateImage, nImagesOut)){
	       	imagesOut = bulkAlloc.getImages();
	       	return true;
        }          
        return false;
	}
	
	@Override
	protected boolean doCalc(Img imageOutG, WriteLock writeLock, Img[] sourceSet, boolean settingsHadChanged) throws InterruptedException {
		
		Img imageIn = sourceSet[0];		
		ByteBufferImage imageOut = (ByteBufferImage)imageOutG;
		
		
		int beamIdx;
		if(beamSelection == BEAMSEL_AUTO){
			beamIdx = (Integer)getImageMetaData("beamIndex", imageIn.getSourceIndex());
			
		}else{
			beamIdx = beamSelection;
		}
				
		int frameIdx = imageIn.getSourceIndex();
		
		try{
			ReadLock readLockIn = imageIn.readLock();
			readLockIn.lockInterruptibly();
			try{
				for(int iY=0; iY < inHeight; iY++) {
					for(int iX=0; iX < inWidth; iX++) {
						double val = imageIn.getPixelValue(readLockIn, iX, iY) * Math.PI / 180;
						
						//for the ray tracer, we need to first convert from theta_A (mu corrected measured rel Savart) to thetaF (full rel to IMSE frame)
						val = val*savartAngleSign + savartToIMSEFrame;
						
						double x = iX / (inWidth - 1.0);
						double y = iY / (inHeight - 1.0);
						
						double pol0 = interpPol0[beamIdx].eval(x, y);
						double dPolDPitch = interpDPolDPitch[beamIdx].eval(x, y);
						
						switch (outputMode) {
							case OUTPUT_MODE_POL_VS_UP:
							case OUTPUT_MODE_POL_VS_BPHI:
								val = (val - pol0) * 180 / Math.PI;
								break;
								
							case OUTPUT_MODE_PITCH: 
								val = (val - pol0) / dPolDPitch * 180 / Math.PI; 
								break;
								
							case OUTPUT_MODE_BZ:
							case OUTPUT_MODE_J:
							case OUTPUT_MODE_Q:
								double Bphi = bvacAt1m[frameIdx] / imgR[iX];
								val = (val - pol0) / dPolDPitch * Bphi; //covnert to Bzish							
						}
												
						imageOut.setPixelValue(writeLock, iX, iY, val);
					}
					
					//differential and calc the current
					if(outputMode == OUTPUT_MODE_J || outputMode == OUTPUT_MODE_Q){
						for(int iX=0; iX < (inWidth-1); iX++) {
							double mu0 = 4*Math.PI*1e-7;
							double bz0 = imageOut.getPixelValue(writeLock, iX, iY);
							double bz1 = imageOut.getPixelValue(writeLock, iX+1, iY);
							double j = (bz1 - bz0) / (imgR[iX+1] - imgR[iX]) / mu0; 
							imageOut.setPixelValue(writeLock, iX, iY, j / 1e3); //in kA
						}
						imageOut.setPixelValue(writeLock, inWidth-1, iY, 0);
						int n=15;
						double row[] = new double[inWidth];
						for(int iX=n; iX < (inWidth-n-2); iX++) {						
							for(int dX=-n; dX <= n; dX++){
								row[iX] += imageOut.getPixelValue(writeLock, iX + dX, iY) / (2*n+1);							
							}
						}
						for(int iX=0; iX < inWidth; iX++) {
							imageOut.setPixelValue(writeLock, iX, iY, row[iX]);
						}
					}
				}
			}finally{
				readLockIn.unlock();
			}
		}catch(InterruptedException err) { }
		
		return true;
	}
	
	@Override
	public void notifySourceChanged() {
		super.notifySourceChanged();
		if(config.autoCalc)
			calc();
	}

	@Override
	public void imageChangedRangeComplete(int idx) { 
 		if(config.autoCalc)
			calc();
 	}
	
	@Override
	public int getNumImages() {
		return (imagesOut != null) ? imagesOut.length : 0;
	}

	@Override
	public Img getImage(int imgIdx) {
		return (imgIdx >= 0 && imgIdx < imagesOut.length) ? imagesOut[imgIdx] : null;
	}
	
	@Override
	public Img[] getImageSet() { return imagesOut; }

	@Override
	public PitchProcessor clone() { return new PitchProcessor(connectedSource, getSelectedSourceIndex());	}

	@Override
	/** For the sink side */
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			PitchSWTController controller = new PitchSWTController((Composite)args[0], (Integer)args[1], this, asSink);
			controllers.add(controller);
			return controller;
		}
		return null;
	}
	
	public void setAutoUpdate(boolean autoCalc) {
		if(!config.autoCalc && autoCalc){
			config.autoCalc = true;
			updateAllControllers();
			calc();			
		}else{
			config.autoCalc = autoCalc;
			updateAllControllers();
		}
	}
	public boolean getAutoUpdate() { return config.autoCalc; }

	@Override
	public void setSource(ImgSource source) {
		super.setSource(source);
		if(config.autoCalc)
			calc();
	}
	
	@Override
	public void destroy() {		
		super.destroy();
	}
	
	public FeatureTransform getTransform(){ return xform; }
	public void pointsMapModified(){
		settingsChanged = true;
		updateAllControllers();
		if(config.autoCalc)
			calc();
	}

	public void invalidate() {
		settingsChanged = true;
	}

	public void setBeamSelection(int beamSelection) {
		this.beamSelection = beamSelection;
		settingsChanged = true;
		if(config.autoCalc)
			calc();
	}

	public int getCalcNX() { return this.nGX; }
	public int getCalcNY() { return this.nGY; }
	public void setCalcGrid(int nX, int nY) {
		nGX = nX;
		nGY = nY;		
		settingsChanged = true;
		if(config.autoCalc)
			calc();		
	}

	public int getOutputMode() { return outputMode;	}
	public void setOutputMode(int outputMode) {
		this.outputMode = outputMode;
		settingsChanged = true;
		if(config.autoCalc)
			calc();	
	}

	@Override
	public BulkImageAllocation getBulkAlloc() { return bulkAlloc; }

	public boolean getUseReducedOffsets() { return useReducedOffsets;	}

	public void setUseReducedOffsets(boolean useReducedOffsets) { this.useReducedOffsets = useReducedOffsets; }
	
	public int getBeamSelection() { return beamSelection;	}
	
	@Override
	public String toShortString() {
		String hhc = Integer.toHexString(hashCode());
		return "Pitch[" + hhc.substring(hhc.length()-2, hhc.length()) + "]";
	}

	@Override
	public ImgProcessorConfig getConfig() { return config; }
}
