package imageProc.proc.pitch;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import imageProc.core.ImagePipeControllerROISettable;
import imageProc.core.ImageProcUtil;
import imageProc.core.swt.ImgProcPipeSWTController;
import imageProc.core.swt.SWTControllerInfoDraw;

/** SWT controller for the pitch/current calculation
 *  
 * @author oliford
 */
public class PitchSWTController extends ImgProcPipeSWTController implements SWTControllerInfoDraw, ImagePipeControllerROISettable {

	private PitchProcessor proc;
	
	private Spinner gridNumXSpinner;
	private Spinner gridNumYSpinner;
	private Button useReducedOffsetsCheckbox;
	private Spinner beamSelectionSpinner;	
	private Combo outputModeCombo;

	public PitchSWTController(Composite parent, int style, PitchProcessor proc, boolean asSinkController) {
		this.proc = proc;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("Pol-->Pitch Control (" + proc.toShortString() + ")");		
		swtGroup.setLayout(new GridLayout(6, false));
		swtGroup.addListener(SWT.Dispose, new Listener() { @Override public void handleEvent(Event event) { destroy(); }});
		
		addCommonPipeControls(swtGroup);
		
		Label lBS = new Label(swtGroup, SWT.NONE); lBS.setText("Force beam:");
		beamSelectionSpinner = new Spinner(swtGroup, SWT.NONE);
		beamSelectionSpinner.setValues(-1, -1, 8, 0, 1, 4); //-1 indicates that we want lat,long as output
		beamSelectionSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		beamSelectionSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lNX = new Label(swtGroup, SWT.NONE); lNX.setText("nCalcGrid X:");
		gridNumXSpinner = new Spinner(swtGroup, SWT.NONE);
		gridNumXSpinner.setValues(30, 2, 100, 0, 1, 4);
		gridNumXSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		gridNumXSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lNY = new Label(swtGroup, SWT.NONE); lNY.setText("Y:");
		gridNumYSpinner = new Spinner(swtGroup, SWT.NONE);
		gridNumYSpinner.setValues(30, 2, 100, 0, 1, 4);
		gridNumYSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		gridNumYSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		useReducedOffsetsCheckbox = new Button(swtGroup, SWT.CHECK);
		useReducedOffsetsCheckbox.setText("Use offsets from reduced");
		useReducedOffsetsCheckbox.setSelection(false);
		useReducedOffsetsCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		useReducedOffsetsCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		Label lOM = new Label(swtGroup, SWT.NONE); lOM.setText("Output mode:");
		outputModeCombo = new Combo(swtGroup, SWT.NONE);
		outputModeCombo.setItems(PitchProcessor.outputModeNames);
		outputModeCombo.select(PitchProcessor.OUTPUT_MODE_PITCH);
		outputModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		outputModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		
		swtGroup.pack();
		doUpdate();
		
		//leaholl 02/2025: add wriggle
		ImageProcUtil.addRevealWriggler(swtGroup);
	}
	
	private void settingsChangedEvent(Event e){
		proc.setBeamSelection(beamSelectionSpinner.getSelection());

		proc.setOutputMode(outputModeCombo.getSelectionIndex());
		proc.setCalcGrid(gridNumXSpinner.getSelection(), gridNumYSpinner.getSelection());
		proc.setUseReducedOffsets(useReducedOffsetsCheckbox.getSelection());
	}
	
	protected void doUpdate(){
		super.doUpdate();
			
		int nX = proc.getCalcNX(), nY = proc.getCalcNY();		
		if(gridNumXSpinner.getSelection() != nX)
			gridNumXSpinner.setSelection(nX);
		
		if(gridNumYSpinner.getSelection() != nY)
			gridNumYSpinner.setSelection(nY);
		
		int outputMode = proc.getOutputMode();
		if(outputModeCombo.getSelectionIndex() != outputMode)
			outputModeCombo.select(outputMode);
	
		boolean useOffsets = proc.getUseReducedOffsets();
		if(useReducedOffsetsCheckbox.getSelection() != useOffsets)
			useReducedOffsetsCheckbox.setSelection(useOffsets);
	}

	@Override
	public void movingPos(int x, int y) {	}

	@Override
	public void fixedPos(int x, int y) {	}

	@Override
	public void setRect(int x0, int y0, int width, int height) {
		
	}
	
	@Override
	public void drawOnImage(GC gc, double scale[], int imageWidth, int imageHeight, boolean asSource) { }
	
	@Override
	public void destroy() {
		swtGroup.dispose();
		proc.controllerDestroyed(this);
	}

	@Override
	public Object getInterfacingObject() { return swtGroup;	}

	@Override
	public PitchProcessor getPipe() { return proc;	}
}
