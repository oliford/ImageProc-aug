package imageProc.proc.reduce;

import java.nio.ByteOrder;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import org.eclipse.swt.widgets.Composite;

import algorithmrepository.Algorithms;
import algorithmrepository.Interpolation1D;
import descriptors.aug.AUGSignalDesc;
import descriptors.gmds.GMDSSignalDesc;
import fusionOptics.Util;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.core.ImgProcPipe;
import imageProc.core.ImgProcessorConfig;
import imageProc.core.ImgSource;
import imageProc.database.gmds.GMDSUtil;
import imageProc.proc.transform.TransformProcessor;
import imageProc.proc.transform.TransformProcessorAUG;
import fusionDefs.neutralBeams.SimpleBeamGeometry;
import fusionDefs.transform.FeatureTransformCubic;
import fusionDefs.transform.PhysicalROIGeometry;
import ipp.aug.neutralBeams.AugNBI;
import net.jafama.FastMath;
import mds.AugShotfileFetcher;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;
import signals.aug.AUGSignal;
import signals.gmds.GMDSSignal;

/**  Reduction of image data to a low resolution form usable by analysis codes and other people.
 *  
 *  Simple block averaging of data on grid for now
 * 
 * Might later include masking like ImageFitProcessor, or individually placeable blocks etc.
 * 
 * When processing pre-transformed images, also write the transformed 
 * coords (R,Z) of the centre of each block
 * 
 * @author oliford
 */
public class ReduceProcessor extends ImgProcPipe implements ConfigurableByID {
	
	private PhysicalROIGeometry ccdGeom;
	private double ccdX0;
	private double ccdX1;
	private double ccdY0;
	private double ccdY1;
	
	private int nBlocksX;
	private int nBlocksY;
	private FeatureTransformCubic xform;

	private BulkImageAllocation<ByteBufferImage> bulkAlloc = new BulkImageAllocation<ByteBufferImage>(this);
	
	private SimpleBeamGeometry beamGeom;
	
	private ReducedFastPolProc fastPolProc = new ReducedFastPolProc(this);
	
	private ReducedProcessorConfig config = new ReducedProcessorConfig();
	
	/** Offsets = What we would measure if the polarisation from VxB+Er was aligned to the defined 'up'
	 * Based on 'what we would measure from VxBphi'. i.e. measurement if there were was no BR,BZ or Er
	 *    
	 * Produced by python script from +/- Bt shots s*/
	private double calibMeasuredAtBphi[][][] = null;
	private double calibUncertainty[][][] = null;	
	private String calibInfo = "Q5: None\nQ6: None\nQ7: None\nQ8: None";
	private String lastSFWriteStatus = null;
	private String lastIMAPredLoadStatus = null;
	
	/** Produce the 'calibrated' image (field 6 +/- field 7) from the FastPol offsets, rather than
	 * the loaded the calibration. */
	private boolean calibFromOffsets;
	private String lastLoadedConfig;
	
	public ReduceProcessor() {
		this(null, -1);
	}
	
	public ReduceProcessor(ImgSource source, int selectedIndex) {
		super(ByteBufferImage.class, source, selectedIndex);
	}
	
	@Override
	protected void preCalc(boolean settingsHadChanged) {
		super.preCalc(settingsHadChanged);
		
		//fetch CCD geometry from metadata
		Map<String, Object> cleanedMap = getCompleteSeriesMetaDataMap().extractSingleArrayElements();
		try{
			ccdGeom = new PhysicalROIGeometry();
			//ccdGeom.fromMap(getCompleteSeriesMetaDataMap());
			ccdGeom.setFromCameraMetadata(cleanedMap);
		}catch(RuntimeException err){
			//we need this one  to know where the grid is, so give up if it fails
			throw new RuntimeException("Couldn't load ccdGeom from metadata: " + err);			
		}

		//fetch transform from metadata, this will require that the TransformProcessor has run in the up-steam processing chain somewhere
		try{
			xform = new FeatureTransformCubic(getCompleteMetaDataAsJavaMap());
		}catch(RuntimeException err){
			err.printStackTrace();
			System.err.println("Couldn't load transform from metadata. Is there a TransformProcessor? " + err);
			xform = null;
			//we dont need this, we can save the data grid without the geoemtry info
		}
		
		calcBeamVectors();
		calcBlocksLOSInfo();
		fastPolProc.prepare();
		setSeriesMetaData("Reduced/calib/measuredAtBphi", calibMeasuredAtBphi, false);
		setSeriesMetaData("Reduced/calib/uncertainty", calibUncertainty, false);
		
	}
	
	private void calcBeamVectors() {
		int augPulse = -1;
		try{
			augPulse = GMDSUtil.getMetaAUGShot(connectedSource);
			beamGeom = AugNBI.fromShotfiles((Integer)augPulse);
			((AugNBI)beamGeom).setVoltagesFromShotfiles(augPulse);
			
			System.err.println("ReduceProcessor: Beams set to geometry from shotfiles for pulse " + augPulse);
			
		}catch(RuntimeException err){
			System.err.println("ReduceProcessor: Error getting beam geometry from shot files, so using default: " + err);
			beamGeom = AugNBI.def();		
		}	
		
		setSeriesMetaData("Reduced/beamAxesStart", beamGeom.startAll(), false);
		setSeriesMetaData("Reduced/beamAxesUVec", beamGeom.uVecAll(), false);
		setSeriesMetaData("Reduced/beamEnergy", beamGeom.getVoltageAll(), false);		
	}

	/** calculate LOS information for each block */
	private void calcBlocksLOSInfo(){
		
		try{
			if(xform == null)
				throw new RuntimeException("No transform");
			
			double dX = (ccdX1 - ccdX0) / nBlocksX;
			double dY = (ccdY1 - ccdY0) / nBlocksY;
			
			double virtMeasPlane[][] = TransformProcessorAUG.initVirtMeasPlane(xform);
	 		
			double viewPos[] = xform.getViewPosition();
			
			//horrible hack to detect which beam box we're looking at
	 		boolean isBox2 = xform.getViewPosition()[1] > 0; //+ve y means we're probably the permIMSE system
			
			double intersectPos[][][][] = new double[8][nBlocksY][nBlocksX][];
			double gammaAFactors[][][][] = new double[8][nBlocksY][nBlocksX][];
			double polAtBphi[][][] = new double[8][nBlocksY][nBlocksX];
			double bcX[] = new double[nBlocksX];
			for(int iX = 0; iX < nBlocksX; iX++)
				bcX[iX] = ccdX0 + iX * dX + dX/2; //block centre
			getConnectedSource().setSeriesMetaData("Reduced/ccdX",  bcX, false);
			
			double bcY[] = new double[nBlocksY];
			for(int iY = 0; iY < nBlocksY; iY++)
				bcY[iY] = ccdY0 + iY * dY + dY/2;
			getConnectedSource().setSeriesMetaData("Reduced/ccdY",  bcY, false);
			
			for(int iB=0; iB < 8; iB++){ //for each beam
				if((isBox2 && iB < 4) || (!isBox2 && iB >= 4))
					continue;
			
				for(int iY = 0; iY < nBlocksY; iY++){
					for(int iX = 0; iX < nBlocksX; iX++){
						
						double LL[] = xform.xyToLatLon(bcX[iX], bcY[iY]);
						if(Double.isNaN(LL[0]) || Double.isNaN(LL[1])){
							intersectPos[iB][iY][iX] = new double[]{ Double.NaN, Double.NaN, Double.NaN };
							gammaAFactors[iB][iY][iX] = Mat.fillArray(Double.NaN, 10);
							continue;
						}
							
						double losVec[] = {
								FastMath.cos(LL[1]) * FastMath.cos(LL[0]),
								FastMath.cos(LL[1]) * FastMath.sin(LL[0]),
								FastMath.sin(LL[1]),
						};
						
						//point on LOS nearest beam axis
						double s = Algorithms.pointOnLineNearestAnotherLine(viewPos, losVec, beamGeom.start(iB), beamGeom.uVec(iB));					
						double pos[] = Util.plus(viewPos, Util.mul(losVec, s));
											
						intersectPos[iB][iY][iX] = pos;
						
						gammaAFactors[iB][iY][iX] = TransformProcessor.calcAFactors(virtMeasPlane, viewPos, pos, beamGeom, iB);
					        
						//need to calculate what polarisation we should see on our measurement plane for v x Bphi
						double uPhi[] = new double[]{ -pos[1], pos[0], 0 }; //phi unit vector at pos (radial perp)
						double VxBphi[] = Util.reNorm(Util.cross(beamGeom.uVec(iB), uPhi));
						double Aru[] = TransformProcessor.vectorCoefficients(losVec, virtMeasPlane[0], virtMeasPlane[2], virtMeasPlane[1], VxBphi);
						double ang = FastMath.atan2(Aru[0], Aru[1]) * 180 / Math.PI;
						ang = (ang < 0) ? (((ang - 90) % 180) + 90) : (((ang + 90) % 180) - 90); //periodic on 180deg
						polAtBphi[iB][iY][iX] = -ang; //hmm not sure where this sign comes from
						
						if(iB==7){
							//should be the same as mulitplied from A factors
							ang = FastMath.atan2(gammaAFactors[iB][iY][iX][1], gammaAFactors[iB][iY][iX][6]) * 180/Math.PI;
							ang = (ang < 0) ? (((ang - 90) % 180) + 90) : (((ang + 90) % 180) - 90);
							System.out.println(polAtBphi[iB][iY][iX] + " ?= " + ang);
						}
					}
				}
				
				setSeriesMetaData("Reduced/gammaFactors/A-Q" + (iB+1), gammaAFactors[iB], false); //this is only relevant to the output image
				setSeriesMetaData("Reduced/intersectionPos-Q" + (iB+1), intersectPos[iB], false); //this is only relevant to the output image
				setSeriesMetaData("Reduced/polAtBphi-Q" + (iB+1), polAtBphi[iB], false);
				
				if(calibMeasuredAtBphi != null && calibMeasuredAtBphi[iB] != null 
						&& (calibMeasuredAtBphi[iB].length != nBlocksY ||calibMeasuredAtBphi[iB][0].length != nBlocksX)){
					calibMeasuredAtBphi = null;
					calibUncertainty = null;
					throw new RuntimeException("Calibration has invalid size/shape, Cleared. (Retry for none)");
				}
			}

			setSeriesMetaData("Reduced/gammaFactors/measPlaneNormal", virtMeasPlane[0], false);
			setSeriesMetaData("Reduced/gammaFactors/measPlaneRight", virtMeasPlane[1], false);
			setSeriesMetaData("Reduced/gammaFactors/measPlaneUp", virtMeasPlane[2], false);				
			setSeriesMetaData("Reduced/viewPos", viewPos, false);

		}catch(RuntimeException err){
			System.err.println("ReducedProcessor: Unable to calculate geometry and A-factors for reduced grid.");
			err.printStackTrace();
			setSeriesMetaData("Reduced/gammaFactors/measPlaneNormal", null, false);
			setSeriesMetaData("Reduced/gammaFactors/measPlaneRight", null, false);
			setSeriesMetaData("Reduced/gammaFactors/measPlaneUp", null, false);		
			setSeriesMetaData("Reduced/viewPos", null, false);
		}
	}

	@Override
	protected boolean doCalc(Img imageOutG, WriteLock writeLock,
			Img[] sourceSet, boolean settingsHadChanged)
			throws InterruptedException {
		
		Img imageIn = sourceSet[0];
		int frameIdx = imageIn.getSourceIndex();
		ByteBufferImage imageOut = (ByteBufferImage)imageOutG;
		
		int x0 = ccdGeom.physToPixelX(ccdX0);
		int y0 = ccdGeom.physToPixelY(ccdY0);
		int x1 = ccdGeom.physToPixelX(ccdX1);
		int y1 = ccdGeom.physToPixelY(ccdY1);

		int dX = (x1 - x0) / nBlocksX;
		int dY = (y1 - y0) / nBlocksY;
		boolean hasSigma = imageIn.getNFields() > 1;
		
		double faradayAngle[] = (double[])connectedSource.getSeriesMetaData("Faraday/faradayAngle");
		Integer beamIndex = (Integer)connectedSource.getImageMetaData("beamIndex", frameIdx);		
		double polAtBphi[][] = (beamIndex == null) ? null : (double[][])getSeriesMetaData("Reduced/polAtBphi-Q" + (beamIndex+1));
		
		try{
			ReadLock readLockIn = imageIn.readLock();
			readLockIn.lockInterruptibly();
			try{
				for(int iBY = 0; iBY < nBlocksY; iBY++){
					for(int iBX = 0; iBX < nBlocksX; iBX++){
						int bx0 = x0 + iBX * dX;
						int by0 = y0 + iBY * dY;
						
						int n = 0;
						double sumI = 0;
						double sumIV = 0;
						double sumIV2 = 0;

						double sumIForD = 0;
						double sumID = 0;
						double sumID2 = 0;
												
						for(int iX=0; iX < dX; iX++){
							for(int iY=0; iY < dY; iY++){								
								double val = imageIn.getPixelValue(readLockIn, 0, bx0 + iX, by0 + iY);
								
								if(Double.isNaN(val))
									continue;
							
								double weight = 1.0;
								if(hasSigma){
									// weight by 1/sigma^2 if we have a sigma from the actual image
									double sigma;
									sigma = imageIn.getPixelValue(readLockIn, 1, bx0 + iX, by0 + iY);
									weight = 1.0 / FastMath.pow2(sigma);
								}
								
								sumI += weight;
								sumIV += val * weight;
								sumIV2 += val * val * weight;
								
								double diff;
								if((bx0 + iX) >= 1){
									diff = val - imageIn.getPixelValue(readLockIn, 0, bx0 + iX - 1, by0 + iY);
								}else{
									diff = 0;
									weight = 0;
								}
								
								sumIForD += weight;
								sumID += diff * weight;
								sumID2 += diff * diff * weight;
								
								n++;
							}	
						}
											
						double sampleMean = sumIV / sumI;
						double blockSigma = FastMath.sqrt((sumIV2 - sumIV*sumIV/sumI) / sumI);
						double sigmaOfMean = 1.0 / FastMath.sqrt(sumI); //weights are 1/variances, variance of variance weighted mean is sum of variances   
						imageOut.setPixelValue(writeLock, 0, iBX, iBY, sampleMean); //field0 = (weighted) mean
						imageOut.setPixelValue(writeLock, 1, iBX, iBY, hasSigma ? sigmaOfMean : Double.NaN); //field1 = estimated sigma
						imageOut.setPixelValue(writeLock, 2, iBX, iBY, blockSigma); //field2 = (weighted) block sigma 
					
						//6 = faraday and offsets subtracted, (so still includes Er effects), (but incl whatever corrections done upstream)
						//7 = sigma on 6, including systematic sigma from calib
 
						if(calibFromOffsets){ //from fitted offsets
							double offsets[][] = fastPolProc.getOffsets();
							
							if(offsets != null){
								double polVsUp = sampleMean - faradayAngle[frameIdx] - offsets[iBY][iBX];
								
								imageOut.setPixelValue(writeLock, 6, iBX, iBY, polVsUp);
								
								double sigmaCalib = hasSigma ? sigmaOfMean : Double.NaN;						
								imageOut.setPixelValue(writeLock, 7, iBX, iBY, sigmaCalib);
								
							}else{
								imageOut.setPixelValue(writeLock, 6, iBX, iBY, Double.NaN);
								imageOut.setPixelValue(writeLock, 7, iBX, iBY, Double.NaN);
							}
							
						}else if(calibMeasuredAtBphi != null && calibMeasuredAtBphi[beamIndex] != null & faradayAngle != null){
							//from loaded absolute calibration
						
							double offsetMeasuredAtUp = calibMeasuredAtBphi[beamIndex][iBY][iBX] - polAtBphi[iBY][iBX];
							double polVsUp = sampleMean - faradayAngle[frameIdx] - offsetMeasuredAtUp;
							
							imageOut.setPixelValue(writeLock, 6, iBX, iBY, polVsUp);
						
							double sigmaCalib = hasSigma ? FastMath.sqrt(sigmaOfMean*sigmaOfMean + FastMath.pow2(calibUncertainty[beamIndex][iBY][iBX]))
													 : Double.NaN;
							
							imageOut.setPixelValue(writeLock, 7, iBX, iBY, sigmaCalib);
						}else{
							imageOut.setPixelValue(writeLock, 6, iBX, iBY, Double.NaN);
							imageOut.setPixelValue(writeLock, 7, iBX, iBY, Double.NaN);
						}
						
						
						//differentials
						sampleMean = sumID / sumIForD;
						blockSigma = FastMath.sqrt((sumID2 - sumID*sumID/sumIForD) / sumIForD);
						sigmaOfMean = 1.0 / FastMath.sqrt(sumIForD); //weights are 1/variances, variance of variance weighted mean is sum of variances   
						imageOut.setPixelValue(writeLock, 3, iBX, iBY, sampleMean); //field0 = (weighted) mean
						imageOut.setPixelValue(writeLock, 4, iBX, iBY, hasSigma ? sigmaOfMean : Double.NaN); //field1 = estimated sigma
						imageOut.setPixelValue(writeLock, 5, iBX, iBY, blockSigma); //field2 = (weighted) block sigma 
						
						
										
						
					}	
				}
			}finally{
				readLockIn.unlock();
			}
			
			fastPolProc.calcFrame(imageIn.getSourceIndex());
			
			
		}catch(InterruptedException err){ }
		
		return true;
	}

	@Override
	protected int[] sourceIndices(int outIdx) {
		return new int[]{ outIdx }; //always 1:1
	}

	@Override
	protected boolean checkOutputSet(int nImagesIn) {		

		if(nBlocksX <= 0 || nBlocksY <= 0)
			throw new RuntimeException("Invalid block config");
		//output 'image' is just the blocks
		outWidth = nBlocksX;
		outHeight = nBlocksY;
	    int nImagesOut = nImagesIn;
		//int fields = (connectedSource.getImage(0) != null && connectedSource.getImage(0).getNFields() > 1) ? 3 : 2; //we output data and sigma of averaging, and also uncertainty if input image has it
	    int fields = 8; // 7 from 27/7/16, 8 from 10/8/17 
		
	    //allocate or re-use
	    ByteBufferImage templateImage = new ByteBufferImage(null, -1, outWidth, outHeight, fields, ByteBufferImage.DEPTH_DOUBLE, false);
	    templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
	    
        if(bulkAlloc.reallocate(templateImage, nImagesOut)){
	       	imagesOut = bulkAlloc.getImages();
	       	return true;
        }          
        return false;
	}

	@Override
	public ReduceProcessor clone() { return new ReduceProcessor(connectedSource, getSelectedSourceIndex());	}

	@Override
	public BulkImageAllocation getBulkAlloc() { return bulkAlloc; }
	
	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object[] args, boolean asSink) {
		if(interfacingClass == Composite.class){
			ReduceSWTController controller = new ReduceSWTController((Composite)args[0], (Integer)args[1], this, asSink);
			controllers.add(controller);
			return controller;
		}
		return null;
	
	}

	public int getNBlocksX() { return nBlocksX; }
	public void setNBlocksX(int nBlocksX) { this.nBlocksX = nBlocksX;	settingsChanged = true; }
	public int getNBlocksY() { return nBlocksY; }
	public void setNBlocksY(int nBlocksY) { this.nBlocksY = nBlocksY;	settingsChanged = true; }
	
	public double[] getCCDBounds() { 
		return new double[]{ ccdX0, ccdY0, ccdX1, ccdY1 }; 
	}
	public void setCCDBounds(double ccdX0, double ccdY0, double ccdX1, double ccdY1) { 
		this.ccdX0 = ccdX0;
		this.ccdY0 = ccdY0;
		this.ccdX1 = ccdX1;
		this.ccdY1 = ccdY1;
		settingsChanged = true;
	}

	public int[] getImageBounds() {		
		if(ccdGeom == null){
			preCalc(false);
		}
		
		return new int[]{
				ccdGeom.physToPixelX(ccdX0),
				ccdGeom.physToPixelY(ccdY0),
				ccdGeom.physToPixelX(ccdX1),
				ccdGeom.physToPixelY(ccdY1)		
		};
	}
	
	public void setImageBounds(int x0, int y0, int x1, int y1){
		if(ccdGeom == null){
			preCalc(false);
		}

		this.ccdX0 = ccdGeom.pixelToPhysX(x0);
		this.ccdY0 = ccdGeom.pixelToPhysY(y0);
		this.ccdX1 = ccdGeom.pixelToPhysX(x1);
		this.ccdY1 = ccdGeom.pixelToPhysY(y1);
		settingsChanged = true;
	}
	
	@Override
	public void saveConfig(String id){
		String parts[] = id.split("/");
		if(parts[0].equals("gmds"))
			saveConfig(parts[1].length() > 0 ? parts[1] : null, Algorithms.mustParseInt(parts[2]));
	}
	
	public void saveConfig(String gmdsExp, int pulse) {
		if(gmdsExp == null)
			gmdsExp = GMDSUtil.getMetaExp(connectedSource);
		if(pulse <= -1)
			pulse = GMDSUtil.getMetaDatabaseID(connectedSource);
		
		GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, gmdsExp, "Reduce/ccdBounds");		
		GMDSSignal sig = new GMDSSignal(sigDesc, new double[]{ ccdX0, ccdY0, ccdX1, ccdY1 });
		GMDSUtil.globalGMDS().writeToCache(sig);

		sigDesc = new GMDSSignalDesc(pulse, gmdsExp, "Reduce/gridSize");		
		sig = new GMDSSignal(sigDesc, new int[]{ nBlocksX, nBlocksY });		
		GMDSUtil.globalGMDS().writeToCache(sig);
		
		fastPolProc.saveConfig(gmdsExp, pulse);
		
	}
	
	@Override
	public void loadConfig(String id){
		String parts[] = id.split("/");
		if(parts[0].equals("gmds")) {
			loadConfig(parts[1].length() > 0 ? parts[1] : null, Algorithms.mustParseInt(parts[2]));
			lastLoadedConfig = new String(id);
		}
	}
	
	public void loadConfig(String gmdsExp, int pulse) {
		if(gmdsExp == null)
			gmdsExp = GMDSUtil.getMetaExp(connectedSource);
		if(pulse <= -1)
			pulse = GMDSUtil.getMetaDatabaseID(connectedSource);
		
		GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, gmdsExp, "Reduce/ccdBounds");		
		
		GMDSSignal sig = (GMDSSignal)GMDSUtil.globalGMDS().getSig(new GMDSSignalDesc(pulse, gmdsExp, "Reduce/ccdBounds"));
		double d[] = (double[])sig.getData();
		ccdX0 = d[0];
		ccdY0 = d[1];
		ccdX1 = d[2];
		ccdY1 = d[3];
		
		sig = (GMDSSignal)GMDSUtil.globalGMDS().getSig(new GMDSSignalDesc(pulse, gmdsExp, "Reduce/gridSize"));
		int n[] = (int[])sig.getData();
		nBlocksX = n[0];
		nBlocksY = n[1];
		
		try{
			fastPolProc.loadConfig(gmdsExp, pulse);
		}catch(RuntimeException err){
			throw new RuntimeException("Fastpol.loadConfig() failed (but Reduuced itself was ok)", err);
		}

		//Calibration of what measurement should be at VxBphi 
		// written ReducedOffsetsFromReverseBt.ipynb
		calibMeasuredAtBphi = new double[8][][];
		calibUncertainty = new double[8][][];
		calibInfo = "";
		for(int iB=4; iB < 8; iB++){
			try{
				sig = (GMDSSignal)GMDSUtil.globalGMDS().getSig(new GMDSSignalDesc(pulse, gmdsExp, "Reduce/calib/measuredAtBphi-Q"+(iB+1)));
				calibMeasuredAtBphi[iB] = (double[][])sig.getData();
				
		
				sig = (GMDSSignal)GMDSUtil.globalGMDS().getSig(new GMDSSignalDesc(pulse, gmdsExp, "Reduce/calib/uncertainty-Q"+(iB+1)));
				calibUncertainty[iB] = (double[][])sig.getData(); 
				System.out.println("Loaded calibration offsets " + calibMeasuredAtBphi[iB].length + " x " + calibMeasuredAtBphi[iB][0].length);
				
				sig = (GMDSSignal)GMDSUtil.globalGMDS().getSig(new GMDSSignalDesc(pulse, gmdsExp, "Reduce/calib/calibSourceInfo-Q"+(iB+1)));
				String strs[] = (String[])sig.getData();

		
				calibInfo += "Q"+(iB+1)+": OK (" + calibMeasuredAtBphi[iB].length + " x " + calibMeasuredAtBphi[iB][0].length + ", "+strs[0]+")\n";
			}catch(RuntimeException err){				
				calibMeasuredAtBphi[iB] = null;
				calibUncertainty[iB] = null;				
				System.out.println("Unable to load calibration offsets for Q"+(iB+1)+". Cleared. ("+err+")");
				calibInfo += "Q"+(iB+1)+": None\n";
			}
			
			
		}
		

		updateAllControllers();
	}

	public void setStatus(String status) {
		this.status = status;
		updateAllControllers();
	}

	public ReducedFastPolProc getFastPolProc() { return fastPolProc; }
	

	@Override
	public String toShortString() {
		String hhc = Integer.toHexString(hashCode());
		return "Reduce[" + hhc.substring(hhc.length()-2, hhc.length()) + "]";
	}

	public String getLoadedCalibData() { return calibInfo; }
	
	public void writeShotfiles(String exp, String diag){
		int augShot = GMDSUtil.getMetaAUGShot(connectedSource);

		String outputPath = System.getProperty("java.io.tmpDir") + "/imse_msa/" + augShot + "/";
		OneLiners.recursiveDelete(outputPath);
		OneLiners.mkdir(outputPath);
		
		writeGMDSTempFiles(outputPath);
		
		lastSFWriteStatus = "Running script...";
		updateAllControllers();
		
		//run python script
		//
		String imsaCmd = SettingsManager.defaultGlobal().getProperty("imageProc.proc.reduce.imsaCommand", "/usr/local/bin/python2.7 /afs/ipp/u/imsd/write_msa/IMSE_IMA.py");

		//imseCmd, outputPath, shot, exp, diag > outputPath + "imsa.log";
		
		lastSFWriteStatus = "Not implemented";
		updateAllControllers();
		
	}
	
	public void writeGMDSTempFiles(String path){
		//things needed by IMA python script:
		/*
		 * data[6,:,:] --> angle.nc
		 * data[7,:,:] --> uncertainty.nc
		 * seriesData/Faraday/faradayAngle.nc --> faraday.nc
		 * seriesData/time.nc 
		 * seriesData/Reduced/gammaFactors/A-Q%i.nc
		 * seriesData/Reduced/intersectionPos-Q%i.nc
		 * seriesData/beamsOn.nc
		 * 
		 * 
		 * 
		 */

	}

	public String getLastSFWriteStatus() { return this.lastSFWriteStatus; }

	public String getLastIMAPredStatus() { return this.lastIMAPredLoadStatus; }
	
	/** Load the IMA style predictions output from e.g. the IDE code and put them in as metadata that
	 * can be viewed the graph etc
	 * 
	 * @param text
	 * @param text2
	 */
	public void loadIMAPrediction(String exp, String diag, int edition) {
		// load IDE 	
		//imse_mes	Signal-Group	measured iMSE angle
		//imse_unc	Signal-Group	measured iMSE angle uncertainty
		//imse_fit	Signal-Group	fitted iMSE angle
		//imse_fla	Signal-Group	flag if iMSE angle is fitted (0/1 .. no/yes)
		//imse_ind	Area-Base	index of iMSE data 

		try{
			int augShot = GMDSUtil.getMetaAUGShot(connectedSource);
			
			lastIMAPredLoadStatus = "Reading " + exp + "/" + diag + "#" + edition + "...";
			updateAllControllers();
			
			AugShotfileFetcher asf = AugShotfileFetcher.defaultInstance();
				
			AUGSignal sig = (AUGSignal)asf.getSig(new AUGSignalDesc(augShot, diag, "imse_mes", exp, edition));
			double measIn[][] = (double[][])sig.getDataAsType(double.class);
	
			double timeIn[] = (double[]) sig.getCoordsAsType(0, double.class);

			sig = (AUGSignal)asf.getSig(new AUGSignalDesc(augShot, diag, "imse_unc", exp, edition));
			double uncIn[][] = (double[][])sig.getDataAsType(double.class);
			
			sig = (AUGSignal)asf.getSig(new AUGSignalDesc(augShot, diag, "imse_fit", exp, edition));
			double fitIn[][] = (double[][])sig.getDataAsType(double.class);
			
			//sig = (AUGSignal)asf.getSig(new AUGSignalDesc(augShot, diag, "imse_fla", exp, edition));
			//double fla[][] = (double[][])sig.getDataAsType(double.class);
			sig = (AUGSignal)asf.getSig(new AUGSignalDesc(augShot, diag, "imse_r", exp, edition));
			double rIn[][] = (double[][])sig.getDataAsType(double.class);
			
			sig = (AUGSignal)asf.getSig(new AUGSignalDesc(augShot, diag, "imse_z", exp, edition));
			double zIn[][] = (double[][])sig.getDataAsType(double.class);
	
			lastIMAPredLoadStatus = "Processing " + exp + "/" + diag + "#" + edition + "...";
			updateAllControllers();
			
			int nT = imagesOut.length;
			int nX = nBlocksX, nY = nBlocksY;
			if(nT == 0 && getSelectedImageFromConnectedSource().getWidth() < 200){ //assume we're looking at upstream
				nT = connectedSource.getNumImages();
				nX = getSelectedImageFromConnectedSource().getWidth();
				nY = getSelectedImageFromConnectedSource().getHeight();
			}
			int iTsel = getSelectedSourceIndex(); //for now
			
			//get the 'active' beam (should be per time-point)
			int beamIdx[] = (int[])getSeriesMetaData("beamIndex");
			int iB = beamIdx[iTsel]; //for now
			
			
			
			//calc grid R,Z using intersection pos of current beam
			double isectPos[][][] =  (double[][][])getSeriesMetaData("Reduced/intersectionPos-Q"+(iB+1));
			double gridR[][] = new double[nY][nX];
			double gridZ[][] = new double[nY][nX];
			if(isectPos != null){
				if(isectPos.length != nY || isectPos[0].length != nX){
					System.err.println("metadata 'Reduced/intersectionPos-Q"+(iB+1)+ "' mismatches image size");
				}else{
					for(int iY=0; iY < nY; iY++){
						for(int iX=0; iX < nX; iX++){
							gridR[iY][iX] = FastMath.sqrt(isectPos[iY][iX][0]*isectPos[iY][iX][0] + isectPos[iY][iX][1]*isectPos[iY][iX][1]);
							gridZ[iY][iX] = isectPos[iY][iX][2];
						}
					}
				}
			}
			
			//our time
			double time[] = (double[])getSeriesMetaData("time");
			
			//on grid (G):
			double measG[][][] = new double[nT][nBlocksY][nBlocksX];
			double fitG[][][] = new double[nT][nBlocksY][nBlocksX];
			double uncG[][][] = new double[nT][nBlocksY][nBlocksX];

			//init all to NaN, because some will not be in IDE output
			for(int iT=0; iT < nT; iT++){
				for(int iY=0; iY < nBlocksY; iY++){
					for(int iX=0; iX < nBlocksX; iX++){
						measG[iT][iY][iX] = Double.NaN;
						fitG[iT][iY][iX] = Double.NaN;
						uncG[iT][iY][iX] = Double.NaN;
					}
				}
			}
			
			Interpolation1D interpMeas = new Interpolation1D(timeIn, timeIn);
			Interpolation1D interpFit = new Interpolation1D(timeIn, timeIn);
			Interpolation1D interpUnc = new Interpolation1D(timeIn, timeIn);
			
			//for each IDE output entry, find the nearest grid cell
			for(int iC=0; iC < measIn.length; iC++){ //for each 'channel' in IDE output
				
				double minDistSq = Double.POSITIVE_INFINITY;
				int iX=-1, iY=-1;
				for(int jY=0; jY < nBlocksY; jY++){
					for(int jX=0; jX < nBlocksX; jX++){
						double distSq = FastMath.pow2(rIn[iC][iTsel] - gridR[jY][jX]) + FastMath.pow2(zIn[iC][iTsel] - gridZ[jY][jX]);
						if(distSq < minDistSq){
							minDistSq = distSq;
							iX = jX;
							iY = jY;
						}
					}
				}
				System.out.println(iC + " : " + iX + "," + iY);
				
				interpMeas.setF(measIn[iC]);
				interpFit.setF(fitIn[iC]);
				interpUnc.setF(uncIn[iC]);
				for(int iT = 0; iT < nT; iT++){
					measG[iT][iY][iX] = interpMeas.eval(time[iT]);
					fitG[iT][iY][iX] = interpFit.eval(time[iT]);
					uncG[iT][iY][iX] = interpUnc.eval(time[iT]);
				}
			}		
			
			// put in upstream metadata, so that reprocessing this ReducedProc doesn't clear it
			//and also so that we can use ReducedProc to load this stuff onto GMDS loaded reduced output
			/// yes, its a hack, I know. Got any better ideas??
			String metaPath = "Reduced/idePredict/"+exp+"-"+diag+"-"+edition;
			connectedSource.setSeriesMetaData(metaPath + "/imse_meas", measG, true);
			connectedSource.setSeriesMetaData(metaPath + "/imse_fit", fitG, true);
			connectedSource.setSeriesMetaData(metaPath + "/imse_unc", uncG, true);

			lastIMAPredLoadStatus = "Loaded " + exp + "/" + diag + "#" + edition + ". OK";
			updateAllControllers();
			
			//rearrange
		}catch(Throwable err){
			lastIMAPredLoadStatus = "ERROR: " + err;
			err.printStackTrace();
		}
		updateAllControllers();
	}

	public void setCalibFromOffsets(boolean calibFromOffsets) { this.calibFromOffsets = calibFromOffsets;	}


	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig; }

	@Override
	public ImgProcessorConfig getConfig() { return config; }
	
}
