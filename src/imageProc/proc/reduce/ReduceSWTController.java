package imageProc.proc.reduce;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImagePipeControllerROISettable;
import imageProc.core.swt.ImgProcPipeSWTController;
import imageProc.core.swt.SWTControllerInfoDraw;
import imageProc.database.gmds.GMDSPipe;
import imageProc.core.ImageProcUtil;

public class ReduceSWTController extends ImgProcPipeSWTController implements SWTControllerInfoDraw, ImagePipeControllerROISettable  {

	private ReduceProcessor proc;
	
	private boolean isSinkController;
	private Spinner nBlocksXSpinner;
	private Spinner nBlocksYSpinner;
	
	private Button setGridCheckbox;

	private Label swtCalibStatus;
	private Button calibFromOffsetsCheckbox;
	
	private Combo loadExpCombo; 
	private Spinner loadPulseSpiner;
	private Button loadPointsButton;
	private Button savePointsButton;
	
	private ReducedFastPolSWTGroup fastPolGroup;
	
	
	private Combo sfWriteExp;
	private Combo sfWriteDiag;	
	private Button sfWriteButton;
	private Label sfWriteStatus;
	
	private Combo idePredExp;
	private Combo idePredDiag;	
	private Spinner ideEditionSpinner;
	private Button idePredButton;
	private Label idePredStatus;
	
	public ReduceSWTController(Composite parent, int style, ReduceProcessor proc, boolean isSinkController) {
		this.isSinkController = isSinkController;
		this.proc = proc;
		swtGroup = new Group(parent, style);
		swtGroup.setText("Reduce data control (" + proc.toShortString() + ")");		
		swtGroup.setLayout(new GridLayout(6, false));
		swtGroup.addListener(SWT.Dispose, new Listener() { @Override public void handleEvent(Event event) { destroy(); }});
		
		addCommonPipeControls(swtGroup);
		
		Label lNX = new Label(swtGroup, SWT.NONE); lNX.setText("Blocks: x = ");
		nBlocksXSpinner = new Spinner(swtGroup, SWT.NONE);
		nBlocksXSpinner.setValues(proc.getNBlocksX(), 2, 1000, 0, 1, 5); //-1 indicates that we want lat,long as output
		nBlocksXSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		nBlocksXSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lNY = new Label(swtGroup, SWT.NONE); lNY.setText("y = ");
		nBlocksYSpinner = new Spinner(swtGroup, SWT.NONE);
		nBlocksYSpinner.setValues(proc.getNBlocksY(), 2, 1000, 0, 1, 5); //-1 indicates that we want lat,long as output
		nBlocksYSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		nBlocksYSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		setGridCheckbox = new Button(swtGroup, SWT.CHECK);
		setGridCheckbox.setText("Set Grid");
		setGridCheckbox.setSelection(false);
		setGridCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		swtCalibStatus = new Label(swtGroup, SWT.NONE);
		swtCalibStatus.setText("Calibration: None loaded\n.\n.\n.");
		swtCalibStatus.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		
		calibFromOffsetsCheckbox = new Button(swtGroup, SWT.CHECK);
		calibFromOffsetsCheckbox.setText("Calibrated output from fitted offsets (instead of above calibration");
		calibFromOffsetsCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		calibFromOffsetsCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lOP = new Label(swtGroup, SWT.NONE); lOP.setText("Load:");		
		loadExpCombo = new Combo(swtGroup, SWT.NONE);
		loadExpCombo.setItems(GMDSPipe.getAvailableExperiments());
		loadExpCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		loadPulseSpiner = new Spinner(swtGroup, SWT.NONE);
		loadPulseSpiner.setValues(-1, -1, Integer.MAX_VALUE, 0, 1, 10);
		loadPulseSpiner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		
		loadPointsButton = new Button(swtGroup, SWT.PUSH);
		loadPointsButton.setText("Load");
		loadPointsButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { loadButtonEvent(event); } });
		
		savePointsButton = new Button(swtGroup, SWT.PUSH);
		savePointsButton.setText("Save (current pulse)");
		savePointsButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { saveButtonEvent(event); } });
		
		fastPolGroup = new ReducedFastPolSWTGroup(swtGroup, proc);
		
		Group sfWriteGroup = new Group(swtGroup, SWT.BORDER);
		sfWriteGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		sfWriteGroup.setText("Write Shot files");
		sfWriteGroup.setLayout(new GridLayout(6, false));
		
		Label lE = new Label(sfWriteGroup, SWT.NONE); lE.setText("Exp:");
		sfWriteExp = new Combo(sfWriteGroup, SWT.MULTI);
		sfWriteExp.setItems(new String[]{ "imsd", "AUGD", "aburckha", "oliford" });
		sfWriteExp.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));

		Label lD = new Label(sfWriteGroup, SWT.NONE); lD.setText("Diag:");
		sfWriteDiag = new Combo(sfWriteGroup, SWT.MULTI);
		sfWriteDiag.setItems(new String[]{ "IMA" });
		sfWriteDiag.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		
		sfWriteButton = new Button(sfWriteGroup, SWT.PUSH);
		sfWriteButton.setText("Write");
		sfWriteButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
		sfWriteButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sfWriteButtonEvent(event); } });
		
		sfWriteStatus = new Label(sfWriteGroup,  SWT.NONE);
		sfWriteStatus.setText("Last:\n.\n.\n.\n.\n");
		sfWriteStatus.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		
		//not yet implemented
		sfWriteExp.setEnabled(false);
		sfWriteDiag.setEnabled(false);
		sfWriteButton.setEnabled(false);
		sfWriteStatus.setEnabled(false);
		
		Group idePredGroup = new Group(swtGroup, SWT.BORDER);
		idePredGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		idePredGroup.setText("Load IDE:IMA direct prediction");
		idePredGroup.setLayout(new GridLayout(6, false));
		
		Label lIE = new Label(idePredGroup, SWT.NONE); lIE.setText("Exp:");
		idePredExp = new Combo(idePredGroup, SWT.MULTI);
		idePredExp.setItems(new String[]{ "AUGD", "rrf", "abock", "oliford" });
		idePredExp.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));

		Label lID = new Label(idePredGroup, SWT.NONE); lID.setText("Diag:");
		idePredDiag = new Combo(idePredGroup, SWT.MULTI);
		idePredDiag.setItems(new String[]{ "IDE" });
		idePredDiag.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		
		ideEditionSpinner = new Spinner(idePredGroup, SWT.NONE);
		ideEditionSpinner.setValues(0, 0, Integer.MAX_VALUE, 0, 1, 10);
		ideEditionSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
						
		idePredButton = new Button(idePredGroup, SWT.PUSH);
		idePredButton.setText("Load");
		idePredButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		idePredButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { idePredButtonEvent(event); } });
		
		idePredStatus = new Label(idePredGroup,  SWT.NONE);
		idePredStatus.setText(".");
		idePredStatus.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		
		
		generalControllerUpdate();
		swtGroup.pack();
		
		//leaholl 02/2025: add wriggle
		ImageProcUtil.addRevealWriggler(swtGroup);
	}
	
	protected void idePredButtonEvent(Event event) {
		// TODO Auto-generated method stub
		proc.loadIMAPrediction(idePredExp.getText(), idePredDiag.getText(), ideEditionSpinner.getSelection());
	}

	protected void sfWriteButtonEvent(Event event) {
		proc.writeShotfiles(sfWriteExp.getText(), sfWriteDiag.getText());
	}

	private void settingsChangedEvent(Event event){
		proc.setNBlocksX(nBlocksXSpinner.getSelection());
		proc.setNBlocksY(nBlocksYSpinner.getSelection());
		proc.setCalibFromOffsets(calibFromOffsetsCheckbox.getSelection());
	}


	private void loadButtonEvent(Event e) {
		proc.loadConfig(loadExpCombo.getText(), loadPulseSpiner.getSelection());		
	}
	
	private void saveButtonEvent(Event e) {
		proc.saveConfig(null, -1);	
	}
	
	@Override
	protected void doUpdate() {
		super.doUpdate();
		if(nBlocksXSpinner.getSelection() != proc.getNBlocksX())
			nBlocksXSpinner.setSelection(proc.getNBlocksX());
		if(nBlocksYSpinner.getSelection() != proc.getNBlocksY())
			nBlocksYSpinner.setSelection(proc.getNBlocksY());
		
		String str = proc.getLoadedCalibData();
		String lines[] = str.split("\n");
		for(int i=0; i < 4 - lines.length; i++)
			str += ".\n";
		swtCalibStatus.setText("Calibration status:\n" + str);
	
				
		str = proc.getLastSFWriteStatus();
		sfWriteStatus.setText((str != null) ? str : " - ");
	
		str = proc.getLastIMAPredStatus();
		idePredStatus.setText((str != null) ? str : " - ");
	}

	@Override
	public void destroy() {
		swtGroup.dispose();
		proc.controllerDestroyed(this);
		//proc.destroy(); //and actively kill it, for good measure
	}

	@Override
	public Object getInterfacingObject() { return swtGroup;	}

	@Override
	public ReduceProcessor getPipe() { return proc;	}

	@Override
	public void drawOnImage(GC gc, double scale[], int imageWidth,
			int imageHeight, boolean asSource) {
		if(!isSinkController)
			return; //only draw on source image
		
		int bounds[] = proc.getImageBounds();
		int x0 = bounds[0], y0 = bounds[1];
		int x1 = bounds[2], y1 = bounds[3];
				
		int nX = proc.getNBlocksX();
		int nY = proc.getNBlocksY();
		
		double dX = ((double)x1 - x0) / nX;
		double dY = ((double)y1 - y0) / nY;
		
		gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_WHITE));
		
		for(int iX=0; iX < nX; iX++){
			int x = (int)(x0 + iX * dX);
			gc.drawLine((int)(x*scale[0]), 
					(int)(y0*scale[1]), 
					(int)(x*scale[0]), 
					(int)(y1*scale[1]));
		}
		
		for(int iY=0; iY < nY; iY++){
			int y = (int)(y0 + iY * dY);
			gc.drawLine(
					(int)(x0*scale[0]), 
					(int)(y*scale[1]), 
					(int)(x1*scale[0]),
					(int)(y*scale[1]));
		}
		
	}

	@Override
	public void movingPos(int x, int y) { }

	@Override
	public void fixedPos(int x, int y) { }

	@Override
	public void setRect(int x0, int y0, int width, int height) {
		if(!isSinkController || !setGridCheckbox.getSelection())
			return;
					
		int x1 = x0 + width;
		int y1 = y0 + height;
		proc.setImageBounds(x0, y0, x1, y1);
	}
}
