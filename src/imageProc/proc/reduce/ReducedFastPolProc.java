package imageProc.proc.reduce;

import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;

import descriptors.gmds.GMDSSignalDesc;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.database.gmds.GMDSUtil;
import imageProc.proc.fastPolFull.FastPolCalc;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import signals.gmds.GMDSSignal;

public class ReducedFastPolProc {

	private ReduceProcessor proc;

	private FastPolCalc fastPol = new FastPolCalc();

	// state

	private double offsets[][];

	private int angleSign = 1;

	private boolean enable = false;

	public ReducedFastPolProc(ReduceProcessor proc) {
		this.proc = proc;
	}

	public void prepare() {

		proc.setSeriesMetaData("Reduced/FastPol/AngleToUp", null, true);
		proc.setSeriesMetaData("Reduced/FastPol/AngleWithOffsetNoEr", null, true);
		proc.setSeriesMetaData("Reduced/FastPol/AngleWithOffset", null, true);
		proc.getConnectedSource().setSeriesMetaData("Reduced/FastPol/AngleErDifference", null, true);
		proc.getConnectedSource().setSeriesMetaData("Reduced/FastPol/offsets", null, false);
		proc.getConnectedSource().setSeriesMetaData("Reduced/FastPol/polAtBPhi", null, false);

		if (!enable) {
			return;
		}

		int augShot = GMDSUtil.getMetaAUGShot(proc.getConnectedSource());
		int gmdsID = GMDSUtil.getMetaDatabaseID(proc.getConnectedSource());

		if (!fastPol.isEquiLoaded(augShot)) {

			// we don't want the equi loading which can takes ages (several
			// minutes)
			// to hang the graph update, so fire another update here and get it
			// to fire us back

			proc.setStatus("FastPol: Loading equi signals");

			try {
				fastPol.loadEqui(augShot);

			} catch (RuntimeException err) {

				throw (new RuntimeException("ReducedProcessor: FastPol: Equilibrium load failed.", err));
			}
		}

		double viewPos[] = (double[]) proc.getConnectedSource().getSeriesMetaData("Transform/viewPosition");
		if (viewPos == null)
			throw new RuntimeException("No viewPosition from transform");

		if (offsets != null && (offsets.length != proc.getNBlocksY() || offsets[0].length != proc.getNBlocksX())) {
			throw new RuntimeException("Offsets do not match grid size");
		}
		proc.getConnectedSource().setSeriesMetaData("Reduced/FastPol/offsets", offsets, false);

		proc.setStatus("FastPol: Preparing");
		fastPol.setAnglesAdjustments(0, 0, 0);

		fastPol.prepareMetadata(proc, "Reduced");
		
		//calc using the transform's planar definition of up - has been already reproduced by ReducedProcessor
		double measNorm[] = (double[]) proc.getSeriesMetaData("Reduced/gammaFactors/measPlaneNormal");		
		double measUp[] = (double[]) proc.getSeriesMetaData("Reduced/gammaFactors/measPlaneUp");			
		fastPol.prepareGeometry(measNorm, measUp);
		//fastPol.prepareGeometry(3750, viewPos);

		double polAtBPhi[][] = calcPolAtBPhi();
		if (polAtBPhi != null
				&& (polAtBPhi.length != proc.getNBlocksY() || polAtBPhi[0].length != proc.getNBlocksX())) {
			throw new RuntimeException("polAtBPhi does not match grid size");
		}
		proc.getConnectedSource().setSeriesMetaData("Reduced/FastPol/polAtBPhi", polAtBPhi, false);

	}

	/**
	 * Calculate what the measured angle will be, form geometry (without faraday
	 * or Er etc) for Bphi at each block relative to the defined up. Does not
	 * include any offsets!
	 */
	public double[][] calcPolAtBPhi() {
		fastPol.prepareFrame(0);
		int nX = proc.getNBlocksX(), nY = proc.getNBlocksY();

		double viewPos[] = (double[]) proc.getSeriesMetaData("Reduced/viewPos");
		double iSectPos[][][] = (double[][][]) proc.getSeriesMetaData("Reduced/intersectionPos-Q" + (fastPol.getFrameBeamIndex() + 1));
		double gammaAFactors[][][] = (double[][][]) proc.getSeriesMetaData("Reduced/gammaFactors/A-Q" + (fastPol.getFrameBeamIndex() + 1));
		double polAtBPhi[][] = new double[nY][nX];

		boolean wEr = fastPol.getAddRadialField();
		boolean wFd = fastPol.getAddFaraday();
		boolean ihbMSE = fastPol.getInhibitMSEAngle();
		boolean ihbBPol = fastPol.getInhibitPoloidalField();

		fastPol.setAddRadialField(false);
		fastPol.setAddFaraday(false);
		fastPol.setInhibitMSEAngle(false);
		fastPol.setInhibitPoloidalField(true);
		
		
		for (int iY = 0; iY < nY; iY++) {

			// should do this for all beams in prep, for a given beam, pos is
			// static
			double posRPZ[][] = Mat.transpose(Algorithms.XYZToRPhiZ(Mat.transpose(iSectPos[iY])));

			double someTime = fastPol.getLoadedEqui().getEarliestTime(); // doesn't
																			// matter
			polAtBPhi[iY] = fastPol.calcAng(someTime, posRPZ, viewPos, gammaAFactors[iY]);

			for (int iX = 0; iX < nX; iX++) {
				polAtBPhi[iY][iX] = (polAtBPhi[iY][iX] + 90) % 180 - 90;
			}
		}

		fastPol.setAddRadialField(wEr);
		fastPol.setAddFaraday(wFd);
		fastPol.setInhibitMSEAngle(ihbMSE);
		fastPol.setInhibitPoloidalField(ihbBPol);
		;

		return polAtBPhi;
	}

	public void calcFrame(int frameIndex) {
		if (!enable)
			return;

		
		fastPol.setInhibitPoloidalField(false);
		fastPol.prepareFrame(frameIndex);

		Object o = proc.getConnectedSource().getSeriesMetaData("/time");
		if (o == null || !(o instanceof double[]))
			return;

		double t[] = (double[]) o;
		if (frameIndex < 0 || frameIndex > t.length)
			return;

		int nX = proc.getNBlocksX(), nY = proc.getNBlocksY();

		double viewPos[] = (double[]) proc.getSeriesMetaData("Reduced/viewPos");
		double iSectPos[][][] = (double[][][]) proc.getSeriesMetaData("Reduced/intersectionPos-Q" + (fastPol.getFrameBeamIndex() + 1));
		double gammaAFactors[][][] = (double[][][]) proc.getSeriesMetaData("Reduced/gammaFactors/A-Q" + (fastPol.getFrameBeamIndex() + 1));

		double pred[][] = new double[nY][nX];
		double predNoEr[][] = new double[nY][nX];
		double predNoFaraday[][] = new double[nY][nX];

		double tEqui[] = fastPol.getLoadedEqui().getTimeOrdered();
		if (t[frameIndex] >= tEqui[0] && t[frameIndex] < tEqui[tEqui.length - 1]) {

			for (int iY = 0; iY < nY; iY++) {

				// should do this for all beams in prep, for a given beam, pos
				// is static
				double posRPZ[][] = Mat.transpose(Algorithms.XYZToRPhiZ(Mat.transpose(iSectPos[iY])));

				pred[iY] = fastPol.calcAng(t[frameIndex], posRPZ, viewPos, gammaAFactors[iY]);

				boolean tmp = fastPol.getAddRadialField();
				fastPol.setAddRadialField(false);
				predNoEr[iY] = fastPol.calcAng(t[frameIndex], posRPZ, viewPos, gammaAFactors[iY]);
				fastPol.setAddRadialField(tmp);
				

				tmp = fastPol.getAddFaraday();
				fastPol.setAddFaraday(false);
				predNoFaraday[iY] = fastPol.calcAng(t[frameIndex], posRPZ, viewPos, gammaAFactors[iY]);
				fastPol.setAddFaraday(tmp);

			}
		}

		int metaIndex = proc.getConfig().calcSelectedImageOnly ? 0 : frameIndex;
		proc.setImageMetaData("Reduced/FastPol/AngleToUp", metaIndex, pred);
		proc.setImageMetaData("Reduced/FastPol/AngleToUpNoFaraday", metaIndex, predNoFaraday);
		

		double predErDiff[][] = new double[nY][nX];
		for (int iY = 0; iY < nY; iY++) {
			for (int iX = 0; iX < nX; iX++) {
				predErDiff[iY][iX] = pred[iY][iX] - predNoEr[iY][iX];
			}
		}
		proc.getConnectedSource().setImageMetaData("Reduced/FastPol/AngleErDifference", metaIndex, predErDiff);
		
		
		if (offsets != null) {

			double predOffset[][] = new double[nY][nX];
			double predOffsetNoEr[][] = new double[nY][nX];
			
			for (int iY = 0; iY < nY; iY++) {
				for (int iX = 0; iX < nX; iX++) {
					predOffset[iY][iX] = pred[iY][iX] + offsets[iY][iX];
					predOffsetNoEr[iY][iX] = predNoEr[iY][iX] + offsets[iY][iX];

					predOffset[iY][iX] = (predOffset[iY][iX] + 90) % 180 - 90;
					predOffsetNoEr[iY][iX] = (predNoEr[iY][iX] + 90) % 180 - 90;
				}
			}

			proc.setImageMetaData("Reduced/FastPol/AngleWithOffset", metaIndex, predOffset);
			proc.setImageMetaData("Reduced/FastPol/AngleWithOffsetNoEr", metaIndex, predOffsetNoEr);
		}
		
	}

	public FastPolCalc getFastPolCalc() {
		return fastPol;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public void setOffsetsToImage(int imageIndex, int nImagesToAverage) {
		int idx0 = imageIndex - nImagesToAverage / 2, idx1 = idx0 + nImagesToAverage;
		if (idx0 < 0)
			idx0 = 0;
		if (idx1 >= proc.getConnectedSource().getNumImages())
			idx1 = proc.getConnectedSource().getNumImages();

		int nX = proc.getNBlocksX(), nY = proc.getNBlocksY();
		offsets = new double[nY][nX];

		try {
			for (int i = idx0; i < idx1; i++) {
				Img image = proc.getImage(i);
				double pred[][] = (double[][]) proc.getImageMetaData("Reduced/FastPol/AngleToUp", i);

				ReadLock readLock = image.readLock();
				readLock.lockInterruptibly();
				try {
					for (int y = 0; y < nY; y++) {
						for (int x = 0; x < nX; x++) {
							offsets[y][x] += (image.getPixelValue(readLock, x, y) - pred[y][x]) / (idx1 - idx0);
						}
					}

				} finally {
					readLock.unlock();
				}
			}
		} catch (InterruptedException e) {
			System.err.println("ReducedFastPolProc: Interrupted in offset calc");
			offsets = null;
		}
	}

	private String calcSyncObject = new String("calcSyncObject");

	/** Calc all frames of FastPol (not affecting ReducedProcessor) */
	public void calcAll() {
		// proc.calc();
		ImageProcUtil.ensureFinalUpdate(calcSyncObject, new Runnable() {
			@Override
			public void run() {
				doCalcAll();
			}
		});
	}

	private void doCalcAll() {
		try {
			prepare();
			int n = proc.getNumImages();
			for (int i = 0; i < n; i++) {
				proc.setStatus("FastPol: " + i + " / " + n);
				calcFrame(i);
			}
			proc.setStatus("FastPol done");
		} catch (RuntimeException err) {
			System.err.println("ReducedFastPol.calcAll(): Error processing fastPol calc:");
			err.printStackTrace();
		}
	}

	public void saveConfig(String gmdsExp, int pulse) {
		GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, gmdsExp, "Reduce/FastPol/offsets");
		GMDSSignal sig = new GMDSSignal(sigDesc, offsets);
		GMDSUtil.globalGMDS().writeToCache(sig);
	}

	public void loadConfig(String gmdsExp, int pulse) {

		GMDSSignal sig = (GMDSSignal) GMDSUtil.globalGMDS()
				.getSig(new GMDSSignalDesc(pulse, gmdsExp, "Reduce/FastPol/offsets"));
		offsets = (double[][]) sig.getData();
	}

	public void clearOffsets() {
		int nX = proc.getNBlocksX(), nY = proc.getNBlocksY();
		offsets = new double[nY][nX];
	}
	
	public double[][] getOffsets(){ return offsets; }
}
