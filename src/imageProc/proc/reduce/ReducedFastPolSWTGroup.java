package imageProc.proc.reduce;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import imageProc.core.ImgSink;
import imageProc.core.swt.ImagePanel;
import imageProc.proc.fastPolFull.FastPolSettingsSWTGroup;

public class ReducedFastPolSWTGroup {
	private ReduceProcessor proc;
	
	private Group swtGroup;

	private Button enableCheckbox;
	private Label statusLabel;
	private Button calcButton;
	private Button reloadButton;

	private Spinner beliefWidth;
	private Button clearOffsetsButton;
	private Button believeButton;

	private FastPolSettingsSWTGroup fastPolSettingsGroup;
	
	public static final DecimalFormat tableValueFormat = new DecimalFormat("#.###");
	
	public ReducedFastPolSWTGroup(Composite parent, ReduceProcessor proc) {
		this.proc = proc;

		swtGroup = new Group(parent, SWT.BORDER);
		swtGroup.setText("Fast Pol Prediction");
		swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		swtGroup.setLayout(new GridLayout(6, false));
			
		Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Status:");
		statusLabel = new Label(swtGroup, SWT.NONE);
		statusLabel.setText("");
		statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));

		enableCheckbox = new Button(swtGroup, SWT.CHECK);
		enableCheckbox.setText("Enable");
		enableCheckbox.setSelection(false);
		enableCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		enableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});
		
		calcButton = new Button(swtGroup, SWT.PUSH);
		calcButton.setText("Calc");
		calcButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		calcButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { calcButtonEvent(event); }});

		reloadButton = new Button(swtGroup, SWT.PUSH);
		reloadButton.setText("Force reload");
		reloadButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 5, 1));
		reloadButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { reloadButtonEvent(event); }});
		
		Label lBW = new Label(swtGroup, SWT.NONE); lBW.setText("Images to average for belief:");
		beliefWidth = new Spinner(swtGroup, SWT.PUSH);
		beliefWidth.setValues(10, 1, Integer.MAX_VALUE, 0, 1, 10);
		beliefWidth.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		
		believeButton = new Button(swtGroup, SWT.PUSH);
		believeButton.setText("I Believe!!");
		believeButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		believeButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { believeButtonEvent(event); }});
		
		clearOffsetsButton = new Button(swtGroup, SWT.PUSH);
		clearOffsetsButton.setText("Clear Offsets");
		clearOffsetsButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		clearOffsetsButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { clearOffsetsButtonEvent(event); }});
		
		
		
		fastPolSettingsGroup = new FastPolSettingsSWTGroup(swtGroup, SWT.BORDER, proc.getFastPolProc().getFastPolCalc());

	}

	private void settingsChangedEvent(Event event) {
		ReducedFastPolProc polCalc = proc.getFastPolProc();
		polCalc.setEnable(enableCheckbox.getSelection());
	}

	private void calcButtonEvent(Event event) {
		proc.getFastPolProc().calcAll();
	}
	
	private void reloadButtonEvent(Event event) {
		proc.getFastPolProc().getFastPolCalc().forceReloadEqui();
		proc.calc();
	}
	
	private void believeButtonEvent(Event event){
		//erm, we want the selected index we don't actually have access to the sink window from here, if it even is a sink window
		//so just search the sinks, and hope there's only one
		ImagePanel win = null;
		for(ImgSink sink : proc.getConnectedSinks()){
			if(sink instanceof ImagePanel){
				win = (ImagePanel)sink;
				break;
			}
				
		}
		
		if(win != null)
			proc.getFastPolProc().setOffsetsToImage(win.getSelectedSourceIndex(), beliefWidth.getSelection());
	}
	
	private void clearOffsetsButtonEvent(Event event){
		proc.getFastPolProc().clearOffsets();
		
	}
		
	
	
	public void update(){
		//statusLabel.setText(proc.getPolCalc().getStatus());
	}
		
	public Group getSWTGroup(){ return swtGroup; }


}
