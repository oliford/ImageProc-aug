package imageProc.proc.seriesAvg;

import java.lang.reflect.Array;
import java.nio.ByteOrder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import org.eclipse.swt.widgets.Composite;

import algorithmrepository.Algorithms;
import descriptors.aug.AUGSignalDesc;
import descriptors.gmds.GMDSSignalDesc;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.core.ImgProcPipe;
import imageProc.core.ImgProcPipeMultithread;
import imageProc.core.ImgSource;
import imageProc.core.MetaDataMap;
import imageProc.core.MetaDataMap.MetaData;
import imageProc.database.gmds.GMDSUtil;
import imageProc.proc.seriesAvg.ConfigEntry;
import imageProc.proc.seriesAvg.SeriesProcessor;
import imageProc.proc.seriesAvg.SeriesSWTController;
import net.jafama.FastMath;
import mds.AugShotfileFetcher;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import signals.aug.AUGSignal;
import signals.gmds.GMDSSignal;

/** Image series processor.
 * 
 * Time series manipulation:
 * 		- Selection cuts (only processing specific image ranges)
 * 		- Radiation removal (Medial average, spike removal ... )
 * 		- Temporal Averaging
 * 
 * @author oliford
 *
 */
public class SeriesProcessorAUG extends SeriesProcessor {

	//params to configFromNBI(),  also used in aug-apps/MultiCalSupport
	public static final int BEAMSEL_DONTWANT = -1;
	public static final int BEAMSEL_CANTSEE = 0; // can't see the beam, so don't care if it changes
	public static final int BEAMSEL_DONTCARE = 1; //can see it, so break the config block, but don't care if it's on
	public static final int BEAMSEL_REQUIRE = 2; //must be on
	public static final int BEAMSEL_AT_LEAST_ONE = 3; //at least one marked with this can be on
	public static final int BEAMSEL_ONLY_ONE = 4; //only on marked with this can be on
	
	public SeriesProcessorAUG() {
		this(null, -1);
	}
	
	public SeriesProcessorAUG(ImgSource source, int selectedIndex) {
		super(source, selectedIndex);
		config = new SeriesProcessorConfigAUG();	
		
		config.map.clear();
		
		getConfig().subtractImagePath = "////XXXX";
		//bulkAlloc.setMaxMemoryBufferAlloc(Long.MIN_VALUE);
	}
	
	@Override
	public SeriesProcessorAUG clone() { return new SeriesProcessorAUG(connectedSource, getSelectedSourceIndex());	}

	@Override
	/** For the sink side */
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			SeriesSWTControllerAUG controller = new SeriesSWTControllerAUG((Composite)args[0], (Integer)args[1], this, asSink);
			controllers.add(controller);
			return controller;
		}
		return null;
	}
	

	/**
	 * @param beamsOn for each beam, -1=don't want, 0=don't care, 1=require, 2=at least one of these, 3=exactly one of these
	 * @return True if any frames matched the criteria. I.e. if any other than the background frame are enabled  
	 */
	public boolean configFromNBI(int beamSel[]) {
		double defaultTemporalMedianRadThreshold = 1000;
		
		final double thresholdCurrent = 300; //not sure about units, not sure if it changes
		DecimalFormat fmt = new DecimalFormat("000");
		
		Object pulseObj = connectedSource.getSeriesMetaData("aug/pulse");
		if(pulseObj.getClass().isArray()) //ffs
			pulseObj = Array.get(pulseObj, 0);
		if(pulseObj == null || !(pulseObj instanceof Integer))
			throw new RuntimeException("No aug/pulse in metadata or is weird object type");
		int pulse = (Integer)pulseObj;
		
		config.map.clear();

		config.map.put("0Level", 
					new ConfigEntry("0Level", true, 0, 1, 1, -1, false, ConfigEntry.RAD_NONE, -1));
		
		double nbiTime[][] = new double[2][], nbiCurrent[][][] = new double[2][][];
		try{
			AUGSignal n1Sig = (AUGSignal)AugShotfileFetcher.defaultInstance().getSig(new AUGSignalDesc(pulse, "NIB", "BI"));
			nbiTime[0] = (double[])n1Sig.getCoordsAsType(0, double.class);
			nbiCurrent[0] = (double[][])n1Sig.getDataAsType(double.class);
			
		}catch(RuntimeException err){
			System.err.println("Couldn't read NBI box1 signals: " + err);
			nbiTime[0] = null;
			nbiCurrent[0] = null;
		}
			
		try{
			AUGSignal n2Sig = (AUGSignal)AugShotfileFetcher.defaultInstance().getSig(new AUGSignalDesc(pulse, "NWB", "BI"));
			nbiTime[1] = (double[])n2Sig.getCoordsAsType(0, double.class);
			nbiCurrent[1] = (double[][])n2Sig.getDataAsType(double.class);
			
		}catch(RuntimeException err){
			System.err.println("Couldn't read NBI box2 signals: " + err);
			nbiTime[1] = null;
			nbiCurrent[1] = null;		
		}
		
		double frameTimes[] = (double[])connectedSource.getSeriesMetaData("time");
		if(frameTimes == null)
			throw new RuntimeException("No 'time' metadata");
		
			
		String currentCfgName = null;
		ConfigEntry currentCfg = null;
		boolean currentBeams[] = null;
		
		boolean anyEnabled = false;
		
		int nImagesIn = connectedSource.getNumImages();
		int configNum = 0;
		for(int i=0; i < nImagesIn; i++){
			double t = frameTimes[i] + config.timebaseOffset;
			
			//work out what the NBI combination is
			boolean beams[] = new boolean[8];
			boolean isSameCfg = true;
			int j=0;
			for(int boxIdx=0; boxIdx < 2; boxIdx++){
				
				int timeIdx = (nbiTime[boxIdx] == null) ? 0 : Mat.getNearestLowerIndex(nbiTime[boxIdx], t);
				for(int srcIdx = 0; srcIdx < 4; srcIdx++){
					beams[j] = (nbiCurrent[boxIdx] == null) ? false : nbiCurrent[boxIdx][srcIdx][timeIdx] > thresholdCurrent;
					isSameCfg &= (currentBeams != null && (beams[j] == currentBeams[j] || beamSel[j] == BEAMSEL_CANTSEE));
					j++;
				}
			}
			
			if(!isSameCfg){
				
				//config has changed (or is the first)
				if(currentCfg != null){
					currentCfg.inIdx1 = i; //terminate here
					if((currentCfg.inIdx1 - currentCfg.inIdx0) > 5){
						currentCfg.radRemoval = ConfigEntry.RAD_TEMPORAL_MEDIAN;
						currentCfg.spikeThreshold = defaultTemporalMedianRadThreshold;
					}
					config.map.put(currentCfgName, currentCfg);
					configNum++;
				}
				
				//now set up the new one
				currentBeams = beams;
				//generate name
				String beamIDs = "";
				boolean enable = true;
				int nAtLeastOne = -1, nOnlyOne = -1;
				for(j=0; j < 8; j++){
					if(beams[j]  && beamSel[j] != BEAMSEL_CANTSEE)
						beamIDs += (j+1); //beam 'Q' names are 1 based
					
					switch (beamSel[j]) {
						case BEAMSEL_DONTWANT:
							if(beams[j])
								enable = false;
							break;
							
						case BEAMSEL_CANTSEE:							
						case BEAMSEL_DONTCARE:
							break;
							
						case BEAMSEL_REQUIRE:
							if(!beams[j])
								enable = false;
							break;
							
						case BEAMSEL_AT_LEAST_ONE:
							if(nAtLeastOne < 0)
								nAtLeastOne = 0;
							if(beams[j])
								nAtLeastOne++;
							break;
							
							
						case BEAMSEL_ONLY_ONE:
							if(nOnlyOne < 0)
								nOnlyOne = 0;
							if(beams[j])
								nOnlyOne++;
					}
				}
				currentCfgName = "AUTO_" + fmt.format(configNum) + 
						((beamIDs.length() > 0) ? ("_Q"+beamIDs) : "_NONE");
				
				if(nAtLeastOne == 0)
					enable = false;
				if(nOnlyOne == 0 || nOnlyOne > 1)
					enable = false;
				
				//if specified, enabled when the selected one is on, otherwise any
				
				currentCfg = new ConfigEntry(currentCfgName, enable, i, i+1, 1, -1, false, ConfigEntry.RAD_NONE, -1);
				
				anyEnabled |= enable;
			}
			
			
		}
		
		if(currentCfg != null){
			currentCfg.inIdx1 = nImagesIn; //terminate at end
			if((currentCfg.inIdx1 - currentCfg.inIdx0) > 5){
				currentCfg.radRemoval = ConfigEntry.RAD_TEMPORAL_MEDIAN;
				currentCfg.spikeThreshold = defaultTemporalMedianRadThreshold;
			}
			config.map.put(currentCfgName, currentCfg);
			configNum++;
		}
		
		updateAllControllers();
		
		return anyEnabled;
	}
	
	@Override
	public String toShortString() {
		String hhc = Integer.toHexString(hashCode());
		return "SeriesProcAUG[" + hhc.substring(hhc.length()-2, hhc.length()) + "]"; 
	}
	
	public SeriesProcessorConfigAUG getConfig(){ return (SeriesProcessorConfigAUG)config; }
}
