package imageProc.proc.seriesAvg;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImagePipeControllerROISettable;
import imageProc.core.swt.ImgProcPipeSWTController;
import imageProc.core.swt.SWTControllerInfoDraw;
import imageProc.database.gmds.GMDSPipe;
import imageProc.proc.seriesAvg.ConfigEntry;
import imageProc.proc.seriesAvg.SeriesSWTController;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** The table directly relfects the config data in the processor's map
* Edits are made into that cfg immediately.
*  
* @author oliford
*/
public class SeriesSWTControllerAUG extends SeriesSWTController {
		
	private Button configFromBeamsQ3N124Button;
	private Button configFromBeamsQ8N567Button;
	private Button configFromBeamsQ3456Button;
	private Button configFromBeamsQ5678Button;
	private Button configFromBeamsQ1of3456Button;
	private Button configFromBeamsQ1of5678Button;
		
	private Text subtractImagePathTextbox;
	
	public SeriesSWTControllerAUG(Composite parent, int style, SeriesProcessorAUG proc, boolean isSinkController) {
		super(parent, style, proc, isSinkController);
	}
		
	@Override
	protected void addAutoConfigControls(Composite parent, int style) {		
		
		configFromBeamsQ3N124Button = new Button(parent, SWT.PUSH);
		configFromBeamsQ3N124Button.setText("Box1 Q3");
		configFromBeamsQ3N124Button.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { configFromBeamsButtonEvent(event, 
				new int[]{	SeriesProcessorAUG.BEAMSEL_DONTWANT, 
							SeriesProcessorAUG.BEAMSEL_DONTWANT, 
							SeriesProcessorAUG.BEAMSEL_REQUIRE, 
							SeriesProcessorAUG.BEAMSEL_DONTWANT, 
							SeriesProcessorAUG.BEAMSEL_CANTSEE,
							SeriesProcessorAUG.BEAMSEL_CANTSEE,
							SeriesProcessorAUG.BEAMSEL_CANTSEE,
							SeriesProcessorAUG.BEAMSEL_CANTSEE }); } });

		configFromBeamsQ8N567Button = new Button(parent, SWT.PUSH);
		configFromBeamsQ8N567Button.setText("Box2 Q8");
		configFromBeamsQ8N567Button.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { configFromBeamsButtonEvent(event, 
				new int[]{	SeriesProcessorAUG.BEAMSEL_CANTSEE,
							SeriesProcessorAUG.BEAMSEL_CANTSEE,
							SeriesProcessorAUG.BEAMSEL_CANTSEE,
							SeriesProcessorAUG.BEAMSEL_CANTSEE,
							SeriesProcessorAUG.BEAMSEL_DONTWANT, 
							SeriesProcessorAUG.BEAMSEL_DONTWANT,
							SeriesProcessorAUG.BEAMSEL_DONTWANT, 
							SeriesProcessorAUG.BEAMSEL_REQUIRE }); } });

		configFromBeamsQ3456Button = new Button(parent, SWT.PUSH);
		configFromBeamsQ3456Button.setText("Box1 Any");
		configFromBeamsQ3456Button.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { configFromBeamsButtonEvent(event, 
				new int[]{ 	SeriesProcessorAUG.BEAMSEL_AT_LEAST_ONE, 
							SeriesProcessorAUG.BEAMSEL_AT_LEAST_ONE, 
							SeriesProcessorAUG.BEAMSEL_AT_LEAST_ONE, 
							SeriesProcessorAUG.BEAMSEL_AT_LEAST_ONE, 
							SeriesProcessorAUG.BEAMSEL_CANTSEE,
							SeriesProcessorAUG.BEAMSEL_CANTSEE,
							SeriesProcessorAUG.BEAMSEL_CANTSEE,
							SeriesProcessorAUG.BEAMSEL_CANTSEE  }); } });

		configFromBeamsQ5678Button = new Button(parent, SWT.PUSH);
		configFromBeamsQ5678Button.setText("Box2 Any");
		configFromBeamsQ5678Button.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { configFromBeamsButtonEvent(event, 
				new int[]{ SeriesProcessorAUG.BEAMSEL_CANTSEE,
							SeriesProcessorAUG.BEAMSEL_CANTSEE,
							SeriesProcessorAUG.BEAMSEL_CANTSEE,
							SeriesProcessorAUG.BEAMSEL_CANTSEE,
							SeriesProcessorAUG.BEAMSEL_AT_LEAST_ONE, 
							SeriesProcessorAUG.BEAMSEL_AT_LEAST_ONE,
							SeriesProcessorAUG.BEAMSEL_AT_LEAST_ONE, 
							SeriesProcessorAUG.BEAMSEL_AT_LEAST_ONE }); } });

		configFromBeamsQ1of3456Button = new Button(parent, SWT.PUSH);
		configFromBeamsQ1of3456Button.setText("Box 1 single");
		configFromBeamsQ1of3456Button.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { configFromBeamsButtonEvent(event, 
				new int[]{  	SeriesProcessorAUG.BEAMSEL_ONLY_ONE, 
								SeriesProcessorAUG.BEAMSEL_ONLY_ONE, 
								SeriesProcessorAUG.BEAMSEL_ONLY_ONE, 
								SeriesProcessorAUG.BEAMSEL_ONLY_ONE, 
								SeriesProcessorAUG.BEAMSEL_CANTSEE,
								SeriesProcessorAUG.BEAMSEL_CANTSEE,
								SeriesProcessorAUG.BEAMSEL_CANTSEE,
								SeriesProcessorAUG.BEAMSEL_CANTSEE  }); } });

		configFromBeamsQ1of5678Button = new Button(parent, SWT.PUSH);
		configFromBeamsQ1of5678Button.setText("Box 2 single");
		configFromBeamsQ1of5678Button.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { configFromBeamsButtonEvent(event, 
				new int[]{ SeriesProcessorAUG.BEAMSEL_CANTSEE,
							SeriesProcessorAUG.BEAMSEL_CANTSEE,
							SeriesProcessorAUG.BEAMSEL_CANTSEE,
							SeriesProcessorAUG.BEAMSEL_CANTSEE,
							SeriesProcessorAUG.BEAMSEL_ONLY_ONE, 
							SeriesProcessorAUG.BEAMSEL_ONLY_ONE,
							SeriesProcessorAUG.BEAMSEL_ONLY_ONE,
							SeriesProcessorAUG.BEAMSEL_ONLY_ONE 	}); } });
		
		

		//leaholl: implement aburckhas background correction
		Label lCP = new Label(swtGroup, SWT.NONE);
		lCP.setText("Subtract image path: ");
		subtractImagePathTextbox = new Text(swtGroup, SWT.NONE);
		subtractImagePathTextbox.setText(getPipe().getConfig().subtractImagePath);
		subtractImagePathTextbox.setToolTipText("Single image (NetCDF file) to subtract from every frame.");
		subtractImagePathTextbox.setLayoutData(new GridData(SWT.FILL,SWT.BEGINNING,true,false,5,0));
		subtractImagePathTextbox.addListener(SWT.FocusOut, new Listener(){@Override public void handleEvent(Event event) { augSettingsEvent(event); }});
	}
	

	protected void augSettingsEvent(Event event) {
		getPipe().getConfig().subtractImagePath = subtractImagePathTextbox.getText();
		
	}

	private void configFromBeamsButtonEvent(Event e, int beamSel[]) {
		((SeriesProcessorAUG)proc).configFromNBI(beamSel);
	}
	
	@Override
	protected void doUpdate() {
		super.doUpdate();

		subtractImagePathTextbox.setText(getPipe().getConfig().subtractImagePath);
	}
	
	@Override
	public SeriesProcessorAUG getPipe() { return (SeriesProcessorAUG)proc;	}
}
