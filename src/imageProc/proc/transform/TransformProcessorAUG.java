package imageProc.proc.transform;

import java.lang.reflect.Array;

import org.eclipse.swt.widgets.Composite;

import descriptors.aug.AUGSignalDesc;
import fusionDefs.transform.FeatureTransform;
import fusionDefs.transform.TransformPoint;
import imageProc.core.ImagePipeController;
import imageProc.proc.transform.TransformProcessor;
import imageProc.proc.transform.TransformSWTController;
import ipp.aug.neutralBeams.AugNBI;
import ipp.imse.fusionOptics.mse.AugGenericMSESystem;
import net.jafama.FastMath;
import mds.AugShotfileFetcher;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import signals.aug.AUGSignal;

public class TransformProcessorAUG extends TransformProcessor {

	
	@Override
	protected void preCalc(boolean settingsHadChanged) {
		super.preCalc(settingsHadChanged);
		
		status = "Calculating beam indices";
		updateAllControllers();
		if(beamSelection == BEAMSEL_AUTO){
			calcBeamIndices();
		}
	}
	
	/** Create the two units vectors describing the virtual 'measurement plane' */
	public double[][] initVirtualMeasurementPlane(FeatureTransform xform){
		return initVirtMeasPlane(xform);
	}
	
	/** Create the two units vectors describing the virtual 'measurement plane' */
	public static double[][] initVirtMeasPlane(FeatureTransform xform){
		//we need some sense of 'central' view line, which should be the optical axis of the observation optics
		
		//for this, just average all the valid transform points and hope that's somewhere in the middle
		double viewPos[] = xform.getViewPosition();
		double targetPos[] = xform.getTargetPosition();
		
		//hack to generate targetPos for transforms that dont have one
		if(targetPos == null){
			boolean isBox2 = viewPos[1] > 0; //+ve y means we're probably the permIMSE system
			int iB = isBox2 ? AugNBI.BEAM_Q8 : AugNBI.BEAM_Q3;
			targetPos = AugNBI.def().getPosOfBeamAxisAtR(iB, 1.8);
			TransformPoint p = new TransformPoint("Target (Q"+(iB+1)+" at 1.8m)");
			p.imgX = Double.NaN;
			p.imgY = Double.NaN;
			p.x = targetPos[0];
			p.y = targetPos[1];
			p.z = targetPos[2];
			p.mode = TransformPoint.MODE_TARGET;
			xform.getPoints().add(p);
		}
		
		double l[] = Mat.normaliseVector(Mat.subtract(targetPos, viewPos));
		
		//we need two orthogonal vectors, perp to l, to describe the virtual measurement plane

		//To get roughly the same A factors as for the MSE in Alex Bock's pyMSA program
		double r[], u[];
		if(false){
			//I don't know where he gets his l from
			
			//that funny 'xperp' thing out of Alex Bock's pyMSA program, for 2015
			//this appears to be the reference 0 for their angle 'gamma'
			double xperpAUG[] = new double[]{
					-0.890547143964,
					0.417832354047,
					0.179838561741
				};
	
			//rotate into our frame
			double rot = AugGenericMSESystem.vrmlToAUGDDDRotation;
			double xperp[] = {
					xperpAUG[0] * FastMath.cos(rot) + xperpAUG[1] * FastMath.sin(rot),
					-xperpAUG[0] * FastMath.sin(rot) + xperpAUG[1] * FastMath.cos(rot),
					xperpAUG[2]
			};
			xperp = Mat.normaliseVector(xperp);
			
			//choose our 'up' as v x Bphi, so gamma is pitch-ish
			//double vxPhi[] = Mat.normaliseVector(Mat.cross3d(v, phi));
			r = Mat.normaliseVector(Mat.cross3d(l, xperp)); //measurement plane right
			//double r[] = xperp;
			u = Mat.normaliseVector(Mat.cross3d(r, l)); //measurement plane up
			u = Mat.mul(u, -1);
		}else{
			//normal, up is up
			r = Mat.normaliseVector(Mat.cross3d(l, new double[]{ 0, 0, 1 })); //measurement plane right
			u = Mat.normaliseVector(Mat.cross3d(r, l)); //measurement plane up
		}
		
		return new double[][]{ l, r, u };
	}
	
	@Override
	protected void calcBeamVectors() {
		Object augPulse = null;
		
		try{
			 augPulse = getSeriesMetaData("/aug/pulse");
			if(augPulse.getClass().isArray())
				augPulse = Array.get(augPulse, 0);
			if(augPulse == null || !(augPulse instanceof Integer)){
				throw new RuntimeException("No augPulse in metaData");
			}
			
			beamGeom = AugNBI.fromShotfiles((Integer)augPulse);
			
			System.err.println("TransformProcessor: Beam geometry set from shotfiles for pulse " + augPulse);
			
			
		}catch(RuntimeException err){
			System.err.println("TransformProcessor: Error getting beam geometry from shot files, so using default: " + err);
			beamGeom = AugNBI.def();
		}
		
		setSeriesMetaData("Transform/beamAxesStart", beamGeom.startAll(), false);
		setSeriesMetaData("Transform/beamAxesUVec", beamGeom.uVecAll(), false);

		double beamEnergy[] = { 60e3, 60e3, 60e3, 60e3, 90e3, 90e3, 90e3, 90e3 }; //defaults
		try{
			((AugNBI)beamGeom).setVoltagesFromShotfiles((Integer)augPulse);			
			beamEnergy = beamGeom.getVoltageAll();
		}catch(RuntimeException err){
			System.err.println("TransformProcessor: Error getting beam energy from journal shot files, so using default: " + err);
		}
		setSeriesMetaData("Transform/beamEnergy", beamEnergy, false);
	}

	/** Works out the most significant beam on during each frame */
	private void calcBeamIndices() {
		final double beamOnCurrentThreshold = 300; //dont know the units
		
		boolean isBox2 = false;
				
		try{
			isBox2 = xform.getViewPosition()[1] > 0; //hack to work out which beam box we're looking at
			
			//work out beam indices
			double frameTimes[] = (double [])getSeriesMetaData("/time");
			
			Object augPulse = getSeriesMetaData("/aug/pulse");
			if(augPulse.getClass().isArray())
				augPulse = Array.get(augPulse, 0);
			if(augPulse == null || !(augPulse instanceof Integer)){
				throw new RuntimeException("No augPulse in metaData");
			}
			
			double nbiTime[][] = new double[2][], nbiCurrent[][][] = new double[2][][];
			try{
				AUGSignal n1Sig = (AUGSignal)AugShotfileFetcher.defaultInstance().getSig(new AUGSignalDesc(((Integer)augPulse), "NIB", "BI"));
				nbiTime[0] = (double[])n1Sig.getCoordsAsType(0, double.class);
				nbiCurrent[0] = (double[][])n1Sig.getDataAsType(double.class);
			}catch(RuntimeException err){
				if(!isBox2)
					throw(err);
				nbiTime[0] = null;
				nbiCurrent[0] = null;
			}
			
			try{
				AUGSignal n2Sig = (AUGSignal)AugShotfileFetcher.defaultInstance().getSig(new AUGSignalDesc(((Integer)augPulse), "NWB", "BI"));
				nbiTime[1] = (double[])n2Sig.getCoordsAsType(0, double.class);
				nbiCurrent[1] = (double[][])n2Sig.getDataAsType(double.class);
			}catch(RuntimeException err){
				if(isBox2)
					throw(err);
				nbiTime[1] = null;
				nbiCurrent[1] = null;
			}
				
			int beamIdx[] = new int[frameTimes.length];
			int beamOn[][] = new int[frameTimes.length][8];
			
			for(int iF=0; iF < frameTimes.length; iF++){				
				if(nbiCurrent[1] != null){
					int idx = Mat.getNearestIndex(nbiTime[1], frameTimes[iF]);
					
					//we prefer, in order, Q8, Q7, Q5, Q6... otherwise default to Q8 (i.e. if it's off)				
					for(int j=0; j < 4; j++)
						beamOn[iF][4+j] = (nbiCurrent[1][j][idx] > beamOnCurrentThreshold) ? 1: 0;
					
					if(isBox2){
						beamIdx[iF] = AugNBI.BEAM_Q8;				
						if(beamOn[iF][5] > 0) beamIdx[iF] = AugNBI.BEAM_Q6;
						if(beamOn[iF][4] > 0) beamIdx[iF] = AugNBI.BEAM_Q5;
						if(beamOn[iF][6] > 0) beamIdx[iF] = AugNBI.BEAM_Q7;
						if(beamOn[iF][7] > 0) beamIdx[iF] = AugNBI.BEAM_Q8;
					}
				}else{
					for(int j=0; j < 4; j++)
						beamOn[iF][4+j] = -1;
				}
				
				if(nbiCurrent[0] != null){
					int idx = Mat.getNearestIndex(nbiTime[0], frameTimes[iF]);
					for(int j=0; j < 4; j++)
						beamOn[iF][j] = (nbiCurrent[0][j][idx] > beamOnCurrentThreshold) ? 1 : 0;
					
					if(!isBox2){
						//we prefer, in order, Q3, Q2, Q4, Q1... otherwise default to Q3 (i.e. if it's off)				
						beamIdx[iF] = AugNBI.BEAM_Q3;				
						if(beamOn[iF][0] > 0) beamIdx[iF] = AugNBI.BEAM_Q1;
						if(beamOn[iF][3] > 0) beamIdx[iF] = AugNBI.BEAM_Q4;
						if(beamOn[iF][1] > 0) beamIdx[iF] = AugNBI.BEAM_Q2;
						if(beamOn[iF][2] > 0) beamIdx[iF] = AugNBI.BEAM_Q3;
					}
				}else{
					for(int j=0; j < 4; j++)
						beamOn[iF][0] = -1;
				}
			}
			
			connectedSource.setSeriesMetaData("beamsOn", beamOn, true); //also relevant for input image 
			connectedSource.setSeriesMetaData("beamIndex", beamIdx, true); //also relevant for input image 
			
		}catch(RuntimeException err){
			int defBeam = isBox2 ?  AugNBI.BEAM_Q8 :  AugNBI.BEAM_Q3;
			System.err.println("WARNING: Can't get power signals for NBI because '"+err+"', defaulting to Q"+(defBeam+1)+" for all frames.");
			int beamIdx[] = Mat.fillIntArray(defBeam, connectedSource.getNumImages());
			connectedSource.setSeriesMetaData("beamIndex", beamIdx, true);
			connectedSource.setSeriesMetaData("beamsOn", null, true); //also relevant for input image 
			
		}
	}


	@Override
	/** For the sink side */
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			TransformSWTControllerAUG controller = new TransformSWTControllerAUG((Composite)args[0], (Integer)args[1], this, asSink);
			controllers.add(controller);
			return controller;
		}
		return null;
	}
}
