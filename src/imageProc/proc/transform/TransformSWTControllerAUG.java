package imageProc.proc.transform;

import org.eclipse.swt.widgets.Composite;

import imageProc.proc.transform.TransformProcessor;
import imageProc.proc.transform.TransformSWTController;

public class TransformSWTControllerAUG extends TransformSWTController {

	public TransformSWTControllerAUG(Composite parent, int style, TransformProcessorAUG proc, boolean isSinkController) {
		super(parent, style, proc, isSinkController);
	}

	@Override
	public TransformProcessorAUG getPipe() { return (TransformProcessorAUG)proc;	}
}
